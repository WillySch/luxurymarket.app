﻿
namespace LuxuryApp.NotificationService
{
    public abstract class ChannelDistribution
    {
        /// <summary>
        /// If this is false the system is in debug mode.
        /// </summary>
        public bool IsActive { get; set; }

    }
}
