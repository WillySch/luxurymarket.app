﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.NotificationService.Emails.MailTemplates
{
    [Serializable]
    public class MailTemplateReaderException : Exception
    {
        public MailTemplateReaderException()
        {
        }

        public MailTemplateReaderException(string message)
            : base(message)
        {
        }

        public MailTemplateReaderException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public MailTemplateReaderException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
