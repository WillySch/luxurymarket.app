﻿using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.NotificationService.Configurations;
using LuxuryApp.NotificationService.Emails.MailTemplates;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.NotificationService.Emails
{
    public class EmailService<T> : INotificationHandler<T> where T : ChannelDistribution
    {
        public readonly ILogger _logger;
        public readonly ElasticSettings _clientSettings;

        public EmailService(ILogger logger)
        {
            _logger = logger;
            _clientSettings = new ElasticSettings
            {
                ApiKey = ConfigurationKey.ElasticPassword,
                From = ConfigurationKey.EmailSendingFrom,
                FromDisplayName = ConfigurationKey.EmailSendingFromName,
                Username = ConfigurationKey.ElasticUser,
                ServerUrl = ConfigurationKey.ElasticServer,
            };
        }

        public EmailService(ElasticSettings clientSettings)
        {
            _clientSettings = clientSettings;
        }

        public async Task SendAsync(T context)
        {
            try
            {
                var model = context as EmailModel;
                if (model == null) return;

                var emailData = ProcessEmailContext(model);

                if (model.Attachments != null && model.Attachments.Any())
                {
                    var filesStream = new List<Stream>();
                    var filenames = new List<string>();

                    foreach (var item in model.Attachments)
                    {
                        filesStream.Add(File.OpenRead(item.FilePath));
                        filenames.Add(item.FileName);
                    }

                    await SendEmailWithAttachments(emailData, filesStream.ToArray(), filenames.ToArray());
                }
                else
                    // await SendSimpleEmail(emailData);
                    await SendEmailWithAttachments(emailData, null, null);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
        }

        #region Private Methods

        private NameValueCollection ProcessEmailContext(EmailModel model)
        {
            var body = string.Empty;

            if (!string.IsNullOrEmpty(model.EmailTemplateContent))//this is for dynamic templates 
            {
                body = (model.DataBag == null) ? model.EmailTemplateContent : MailTemplate.GetParsedContentFromTemplateContent(model.EmailTemplateContent, model.DataBag, model.MasterTemplateName);
            }
            if (!string.IsNullOrEmpty(model.EmailTemplateName))
            {
                if (model.UseRazor)
                    body = MailTemplate.GetRazorParsedContent(model.EmailTemplateName, model.DataBag, model.HasOwnMasterTemplate, model.MasterTemplateName);
                else
                    body = MailTemplate.GetParsedContent(model.EmailTemplateName, model.DataBag, model.HasOwnMasterTemplate, model.MasterTemplateName);
            }

            bool isNotificationEnabled;
            bool.TryParse(ConfigurationManager.AppSettings["IsNotificationEnabled"], out isNotificationEnabled);

            Trace.TraceInformation($@"[EmailChannelHandler] Sending email. Message:template name: {model.EmailTemplateName},templateContent:{model.EmailTemplateContent}, IsNotificationEnabled {isNotificationEnabled}, 
            to: {model.To}, cc:{model.Cc}");

            if (string.IsNullOrEmpty(model.To))
            {
                throw new ArgumentException("Invalid context. Expected an EmailModel To value");
            }

            var values = new NameValueCollection()
                {
                    {"username",_clientSettings.Username },
                    {"api_key",_clientSettings.ApiKey },
                    {"from", (string.IsNullOrWhiteSpace( model.From))?_clientSettings.From: model.From },
                    {"from_name", (string.IsNullOrWhiteSpace( model.FromName))?_clientSettings.FromDisplayName: model.FromName },
                    {"subject", model.Subject}

                };

            var recipient = GetEmailReceipient(model.To, model.IsActive && isNotificationEnabled);
            values.Add("to", recipient);
            if (model.IsHtmlBody)
                values.Add("body_html", body);
            else
                values.Add("body_text", body);

            return values;

        }

        private Task SendSimpleEmail(NameValueCollection values)
        {
            using (var client = new WebClient())
            {
                try
                {
                    client.UploadValuesAsync(new Uri(_clientSettings.ServerUrl), values);
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message + ": " + ex.StackTrace);
                }
            }
            return Task.FromResult(0);
        }

        private async Task SendEmailWithAttachments(NameValueCollection values, Stream[] paramFileStream = null, string[] filenames = null)
        {
            using (var client = new HttpClient())
            using (var formData = new MultipartFormDataContent())
            {
                foreach (string key in values)
                {
                    HttpContent stringContent = new StringContent(values[key]);
                    formData.Add(stringContent, key);
                }

                if (paramFileStream != null && filenames != null)
                {
                    for (int i = 0; i < paramFileStream.Length; i++)
                    {
                        HttpContent fileStreamContent = new StreamContent(paramFileStream[i]);
                        formData.Add(fileStreamContent, "file" + i, filenames[i]);
                    }
                }

                var response = await client.PostAsync(_clientSettings.ServerUrl, formData);
                if (!response.IsSuccessStatusCode)
                {
                    Trace.TraceError(response.Content.ReadAsStringAsync().Result);
                }
            }
        }

        private static string GetEmailReceipient(string to, bool sendToRecipient)
        {
            if (sendToRecipient)
                return to;

            var rerouteAddress = ConfigurationManager.AppSettings["RerouteAddress"];
            return rerouteAddress;
        }

        #endregion
    }
}
