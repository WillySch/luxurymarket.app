﻿using Autofac;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.DataAccess;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.Processors.Agents.Sellers;
using LuxuryMarket.Services.Vendor.AlducaSync.Implementations;

namespace LuxuryMarket.Services.Vendor.AlducaSync
{
    public class DependencyBuilder
    {
        public IContainer GetDependencyContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<LuxuryLogger>().As<ILogger>();
            builder.RegisterType<LuxuryMarketConnectionStringProvider>().As<IConnectionStringProvider>();
            builder.RegisterType<SellerProductSyncRepository>().As<ISellerProductSyncRepository>();
            builder.RegisterType<SellerAPISettingRepository>().As<ISellerAPISettingRepository>();

            builder.RegisterType<SellerApiSettingService>().As<ISellerApiSettingService>();
            builder.RegisterType<AlducaProductSyncAgent>().As<ISellerProductSyncAgent>();

            builder.RegisterType<WorkerConfigAppSettings>().As<IWorkerConfig>();
            builder.RegisterType<SyncWorker>().As<IWorker>();

            builder.RegisterType<AlDucaService>().AsSelf();
            var container = builder.Build();
            return container;
        }
    }
}
