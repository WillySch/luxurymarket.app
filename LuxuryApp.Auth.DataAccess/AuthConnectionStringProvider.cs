﻿using LuxuryApp.Auth.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Auth.DataAccess.Repositories
{
    public class AuthConnectionStringProvider : IConnectionStringProvider
    {
        public string ReadWriteConnectionString { get; }

        public AuthConnectionStringProvider()
        {
            string conString;
            try
            {
                var connectionStringName = ConfigurationManager.AppSettings["ConnectionStringName"];
                conString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            }
            catch (Exception)
            {
                throw new Exception("Make sure you have set \"ConnectionStringName\" in you app config");
            }
            ReadWriteConnectionString = conString;
        }
    }
}
