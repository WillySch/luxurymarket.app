﻿using Autofac;
using Luxury.FtpUploadImages;
using LuxuryApp.Contracts.BackgroudServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Debug.Luxury.FtpUploadImages
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new DependencyBuilder().GetDependencyContainer();
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();


            var worker = container.Resolve<IWorker>();
            worker.Start(cancellationTokenSource.Token);

            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();
        }
    }
}
