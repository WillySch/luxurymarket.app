﻿using Autofac;
using Luxury.VendorImagesUploader.Implementations;
using LuxuryApp.Cdn.CdnServers.Model;
using LuxuryApp.Cdn.CdnServers.Repository;
using LuxuryApp.Cdn.CdnServers.Services;
using LuxuryApp.Cdn.Contracts;
using LuxuryApp.Cdn.Contracts.Model;
using LuxuryApp.Cdn.Image.Contracts;
using LuxuryApp.Cdn.SqlRepository;
using LuxuryApp.CDN.ImageCdnServers.Services;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.DataAccess;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.Processors.Agents;
using System.Configuration;

namespace Luxury.VendorImagesUploader
{
    public class DependencyBuilder
    {
        public IContainer GetDependencyContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<LuxuryLogger>().As<ILogger>();
            builder.RegisterType<LuxuryMarketConnectionStringProvider>().As<IConnectionStringProvider>();
            builder.RegisterType<VendorImageAgent>().As<IVendorImageAgent>();
            builder.RegisterType<VendorImageRepository>().As<IVendorImageRepository>();

            builder.RegisterType<WorkerConfigAppSettings>().As<IWorkerConfig>();
            builder.RegisterType<VendorImagesWorker>().As<IWorker>();

            builder.RegisterType<ProductRepository>().As<IProductRepository>();

            //amazon
            var connectionStringName = ConfigurationManager.AppSettings["CDNConnectionString"];
            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;

            var amazonS3CdnRepository = new AmazonS3CdnSettingsRepository(connectionString);
            builder.RegisterInstance(amazonS3CdnRepository).As<IMultiCdnSettingsRepository<AmazonS3CdnSettings>>();

            var amazonSettings = amazonS3CdnRepository.GetAllCdnSettings();
            const string cdnComponetsKey = "cdnComp";

            foreach (var setting in amazonSettings)
            {
                builder.RegisterInstance(setting).As<ICdnSettings>();
                builder.Register(c => new AmazonS3CdnService(c.Resolve<ICdnAuthor>(), setting))
                    .Named<IFileCdnService<ICdnAuthor, ICdnSettings>>(cdnComponetsKey).As<IFileCdnService<ICdnAuthor, ICdnSettings>>();
            }
            var author = new CdnAuthor { UserId = -1, UserIdSource = "LuxuryVendorImagesUploader" };
            builder.RegisterInstance(author).As<ICdnAuthor>();

            //builder.Register(c => new MultiCdnService(
            //  author,
            //  new NullCdnSettings(),
            //  c.Resolve<IMultiCdnRepository>(),
            //  c.ResolveNamed<IEnumerable<IFileCdnService<ICdnAuthor, ICdnSettings>>>(cdnComponetsKey)))
            //  .As<IFileCdnService<ICdnAuthor, ICdnSettings>>();


            builder.RegisterType<ImageCdnService>().As<IImageCdnService>();
            builder.RegisterType<ServiceVendorImages>().AsSelf();
            var container = builder.Build();
            return container;
        }
    }
}
