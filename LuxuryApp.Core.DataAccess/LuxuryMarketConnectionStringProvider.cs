﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Interfaces;

namespace LuxuryApp.Core.DataAccess
{
    public class LuxuryMarketConnectionStringProvider: IConnectionStringProvider
    {
        public string ReadWriteConnectionString { get; }
        public string ReadOnlyConnectionString { get; }

        public LuxuryMarketConnectionStringProvider()
        {
            string conString;
            try
            {
                var connectionStringName = ConfigurationManager.AppSettings["ConnectionStringName"];
                conString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            }
            catch (Exception)
            {
                throw new Exception("Make sure you have set \"ConnectionStringName\" in you app config" );
            }
            ReadOnlyConnectionString = conString;
            ReadWriteConnectionString = conString;
        }
    }
}
