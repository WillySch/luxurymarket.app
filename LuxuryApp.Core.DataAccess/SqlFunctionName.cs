﻿
namespace LuxuryApp.DataAccess
{
    public static class SqlFunctionName
    {
        #region Company

        public static string fn_CompanyGetById = "dbo.fn_CompanyGetById";
        public static string fn_CompaniesGetByCustomer = "dbo.fn_CompaniesGetByCustomer";
        public static string fn_CompanySearchByName = "dbo.fn_CompanySearchByName";
        public static string fn_IsUserAssociatedToCompany = "dbo.fn_IsUserAssociatedToCompany";

        #endregion

        #region Customer

        public static string fn_CustomerGetByUserId = "dbo.fn_CustomerGetByUserId";
        public static string fn_CustomerWithCompaniesGetByUserID = "dbo.fn_CustomerWithCompaniesGetByUserID";

        #endregion

        #region Order

        public static string fn_PaymentMethods_GetAll = "dbo.fn_PaymentMethods_GetAll";
        public static string fn_OrderGetPoNumbersAndEmails = "dbo.fn_OrderGetPoNumbersAndEmails";

        public static string fn_OrdersFilterListForSeller = "dbo.fn_OrdersFilterListForSeller";
        public static string fn_OrdersFilterListForBuyer = "dbo.fn_OrdersFilterListForBuyer";

        public static string fn_OrderSellerGetItems = "dbo.fn_OrderSellerGetItems";

        public static string fn_OrderSellerGetAllStatusTypes = "dbo.fn_OrderSellerGetAllStatusTypes";
        public static string fn_OrderProductGetTotalsForSeller = "dbo.fn_OrderProductGetTotalsForSeller";
        public static string fn_OrderGetTotalsForSeller = "dbo.fn_OrderGetTotalsForSeller";
        public static string fn_OrderProductCanConfirmItems = "dbo.fn_OrderProductCanConfirmItems";
        public static string fn_OrderGetBuyerDataForEmail = "dbo.fn_OrderGetBuyerDataForEmail";

        public static string fn_OrderFilesGetByOrderId = "dbo.fn_OrderFilesGetByOrderId";

        #endregion

        #region Addresses

        public static string fn_CompanyAddresses_GetAllMain = "dbo.fn_CompanyAddresses_GetAllMain";

        #endregion

        public static string fn_CartGetTotalSummary = "dbo.fn_CartGetTotalSummary";
        public static string fn_IsUserAssociatedToCart = "dbo.fn_IsUserAssociatedToCart";

        #region OfferImport

        public static string fn_OfferImport_GetSummary = "dbo.fn_OfferImport_GetSummary";
        public static string fn_CanUserAddOffers = "dbo.fn_CanUserAddOffers";
        public static string fn_OfferImportBatchItems_GetItems = "dbo.fn_OfferImportBatchItems_GetItems";
        public static string fn_OfferImportBatchValidate = "dbo.fn_OfferImportBatchValidate";
        public static string fn_OfferImportBatchItemGetSizes = "dbo.fn_OfferImportBatchItemGetSizes";

        #endregion

        #region OrderSeller

        public static string fn_OrderSellerPackagesGet = "dbo.fn_OrderSellerPackagesGet";
        public static string fn_BoxSizeTypesGet = "dbo.fn_BoxSizeTypesGet";
        public static string fn_OrderSeller_GetPackingInformation = "dbo.fn_OrderSeller_GetPackingInformation";
        #endregion

        #region Products 
        public static string fn_IsvalidProductSKU = "dbo.fn_IsvalidProductSKU";
        #endregion

        public static string fn_ProductSyncBatchItemImagesSelect = "dbo.fn_ProductSyncBatchItemImagesSelect";

        public static string fn_IsUserAssociatedToMainOrder = "dbo.fn_IsUserAssociatedToMainOrder";
        public static string fn_IsUserAssociatedToOrderSeller = "dbo.fn_IsUserAssociatedToOrderSeller";

        #region Offers

        public static string fn_OfferIsActive = "dbo.fn_OfferIsActive";
        public static string fn_OfferHasReservedItems = "dbo.fn_OfferHasReservedItems";
        public static string fn_OfferProductSizeHasAvailableQuantity = "dbo.fn_OfferProductSizeHasAvailableQuantity";
        public static string fn_CartItems_GetInvalidItems = "dbo.fn_CartItems_GetInvalidItems";

        #endregion

        #region SizeMappings

        public static string fn_SizeRegionsSearch = "dbo.fn_SizeRegionsSearch";
        public static string fn_SizeRegionsGet = "dbo.fn_SizeRegionsGet";

        #endregion


    }
}
