﻿using System;
using System.Data;
using System.Linq;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models.Ftp;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class FtpRepository : RepositoryBase,
        IFtpRepository
    {
        public FtpRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public FtpConfig[] GetAllFtpConfigs(FtpConfigType? type)
        {
            var ftpConfigId = (int?)(type == 0 ? null : type);
            const string sql =
                "select " +
                "Id," +
                "Host," +
                "Username," +
                "Password," +
                "UploadDirectory " +
                "  from dbo.fn_FtpGetAllConfigs(@FtpConfigID)";

            FtpConfig[] result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<FtpConfig>(sql, new { ftpConfigId }).ToArray();
                conn.Close();
            }
            return result;
        }

        public int EnqueueItem(FtpQueueItem item)
        {
            const string spName = "dbo.FtpEnqueueFile";
            var p = new DynamicParameters(
                new
                {
                    ftpConfigId = item.FtpConfigId,
                    fileName = item.FileName,
                    fileContent = item.FileContentBytes,
                    enqueueDate = new DateTimeOffset(DateTime.UtcNow),
                    orderSellerId = item.OrderSellerId,
                    LocalDirectory = item.LocalDirectory,
                    FileSizeBytes = item.FileSizeBytes
                });
            p.Add("@ftpQueueItemId", DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                conn.Execute(spName,
                    p,
                    commandType: CommandType.StoredProcedure);
                conn.Close();
            }
            item.Id = p.Get<int>("@ftpQueueItemId");
            return item.Id;
        }

        public FtpQueueItem[] GetItemsToProcess(FtpDirectionType directionType)
        {
            var directionTypeId = (int)directionType;
            const string sql =
                "select " +
                "Id," +
                "FtpConfigId," +
                "OrderSellerId," +
                "PONumber," +
                "FileName," +
                "FileContent as FileContentBytes, " +
                "LocalDirectory," +
                "StatusDate"+
                "  from dbo.fn_FtpGetItemsToProcess(@DirectionTypeID,@batchSize, @currentDate)";
            const int batchSize = 20; // todo: config
            var currentDate = new DateTimeOffset(DateTime.UtcNow);
            FtpQueueItem[] result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<FtpQueueItem>(sql, new { directionTypeId, batchSize, currentDate }).ToArray();
                conn.Close();
            }
            return result;
        }

        public void SetItemUploadStatus(int ftpQueueItemId, FtpUploadStatus status, string error)
        {
            const string spName = "dbo.FtpSetUploadStatus";
            var p = new DynamicParameters(new
            {
                ftpQueueItemId,
                ftpStatusId = (int)status,
                statusDate = new DateTimeOffset(DateTime.UtcNow),
                error,
            });
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                conn.Execute(spName, p, commandType: CommandType.StoredProcedure);
                conn.Close();
            }
        }

        public string GetApplicationSettingsByName(string name)
        {
            const string sql = "select settingValue from dbo.fn_ApplicationSettingsGetByName(@SettingName)";
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var result = conn.Query<string>(sql, new { name }).FirstOrDefault();
                conn.Close();

                return result;
            }
        }
    }
}
