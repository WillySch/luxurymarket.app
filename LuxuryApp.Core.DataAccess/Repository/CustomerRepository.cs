﻿using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Models;
using Dapper;
using LuxuryApp.DataAccess;
using System;
using System.Data.SqlClient;
using System.Data;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class CustomerRepository : RepositoryBase, ICustomerRepository
    {
        private readonly Repository _connection;
        private const string fieldsSql = @"select CustomerID
			                                      ,UserID
			                                      ,Email
			                                      ,Phone
			                                      ,FirstName
			                                      ,LastName
			                                      ,Notes";

        public CustomerRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        private async Task<Customer> GetCustomerData(int userId)
        {
            var sql = $"{fieldsSql} ,UserName FROM {SqlFunctionName.fn_CustomerGetByUserId}(@UserID)";
            IEnumerable<Customer> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<Customer>(sql, new { userId });
                conn.Close();
            }
            return result.FirstOrDefault();
        }

        public async Task<Customer> GetCustomerByUserId(int userId)
        {
            var customer = await GetCustomerData(userId);
            return customer;
        }

        public async Task<bool> UpdateAccountInfo(AccountInfo model, int userId, DateTimeOffset currentDate)
        {
            return await UpdateAccount(model, userId, currentDate);
        }

        public async Task<Customer> GetCustomerWithCompaniesAsync(int userId)
        {
            var query = @"SELECT DISTINCT 
                                    CustomerID
                                   ,FirstName
			                       ,LastName
                                   ,CompanyID
                                   ,CompanyName
		                           ,CompanyTypeID as CompanyType
                          from " + $" {SqlFunctionName.fn_CustomerWithCompaniesGetByUserID}(@UserId)";

            IEnumerable<CustomerDenormalized> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<CustomerDenormalized>(query, new { userId });
                conn.Close();
            }
            var customer = result.GroupBy(x => new { x.CustomerId, x.FirstName, x.LastName }).Select(x => new Customer
            {
                CustomerId = x.Key.CustomerId,
                FirstName = x.Key.FirstName,
                LastName = x.Key.LastName,
                Companies = x.Select(y => new Company { CompanyId = y.CompanyId, CompanyName = y.CompanyName, CompanyType = y.CompanyType }).ToList()
            }).FirstOrDefault();

            return customer;
        }

        private async Task<bool> UpdateAccount(AccountInfo model, int userId, DateTimeOffset currentDate)
        {
            var success = false;

            var parameters = new DynamicParameters(
            new
            {
                CustomerID = model.CustomerId,
                FirstName = model.FirstName != null ? model.FirstName : null,
                LastName = model.LastName != null ? model.LastName : null,
                ContactPhoneNumber = model.ContactPhoneNumber != null ? model.ContactPhoneNumber : null,
                CurrentUser = userId,
                CurrentDate = currentDate
            });

            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var reader = await conn.ExecuteReaderAsync(StoredProcedureNames.UpdateACcountInfo, param: parameters, commandType: CommandType.StoredProcedure);
                success = true;
                conn.Close();
            }
            return success;
        }
    }
}
