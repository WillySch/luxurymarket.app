﻿using System;
using System.Data;
using System.Data.SqlClient;
using LuxuryApp.Contracts.Interfaces;
using System.Configuration;
using System.Data.Common;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class Repository
    {
        private readonly string _connectionString;

        private readonly DbConnection _connection;

        public Repository()
        {
            var connectionStringName = ConfigurationManager.AppSettings["ConnectionStringName"];
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
        }

        //public Repository(DbConnection connection)
        //{
        //    _connection = connection;
        //}

        public int ExecuteProcedure(string storedProcedureName, SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, storedProcedureName, commandParameters);
        }

        public int ExecuteProcedure(string storedProcedureName)
        {
            return SqlHelper.ExecuteNonQuery(_connectionString, CommandType.StoredProcedure, storedProcedureName);
        }

        public DataReader LoadData(string storedProcedureName, SqlParameter commandParameter)
        {
            var dataReader = SqlHelper.ExecuteDataReader(_connectionString, CommandType.StoredProcedure, storedProcedureName, commandParameter);
            return dataReader;
        }

        public DataReader LoadData(string storedProcedureName, SqlParameter[] commandParameters)
        {
            var dataReader = SqlHelper.ExecuteDataReader(_connectionString, CommandType.StoredProcedure, storedProcedureName, commandParameters);
            return dataReader;
        }

        public DataReader ExcuteSqlReader(string sql, SqlParameter[] commandParameters)
        {
            var dataReader = SqlHelper.ExecuteDataReader(_connectionString, CommandType.Text, sql, commandParameters);
            return dataReader;
        }
    }
}
