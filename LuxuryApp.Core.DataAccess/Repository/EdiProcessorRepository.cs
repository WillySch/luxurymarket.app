﻿using System;
using System.Data;
using System.Linq;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models.Ftp;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class EdiProcessorRepository : RepositoryBase, IEdiProcessorRepository
    {
        public EdiProcessorRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public FtpConfig[] GetAllFtpConfigs(FtpConfigType? type)
        {
            var ftpConfigId = (int?)(type == 0 ? null : type);
            const string sql =
                "select " +
                "Id," +
                "Host," +
                "Username," +
                "Password," +
                "UploadDirectory " +
                "  from dbo.fn_FtpGetAllConfigs(@FtpConfigID)";

            FtpConfig[] result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<FtpConfig>(sql, new { ftpConfigId }).ToArray();
                conn.Close();
            }
            return result;
        }

        public int InsertEdi997Items(int ftpQueueItemId, int ediFileTypeId, EdiExtractedACK997 item, int? createdBy)
        {
            const string spName = "dbo.usp_Edi997ReceivedItemsInsert";
            var p = new DynamicParameters(
                new
                {
                    FTPQueueItemID = ftpQueueItemId,
                    EdiFileTypeID = ediFileTypeId,
                    PONumber = item.PONumber,
                    IsAccepted = item.IsAccepted,
                    PickupDate = item.PickupDate,
                    AckFor = item.FunctionalIdentifierCode,
                    InterchangeDateTime = item.InterchangeDateTime,
                    CreatedBy = createdBy
                });
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                var result = conn.Execute(spName,
                      p,
                      commandType: CommandType.StoredProcedure);
                conn.Close();

                return result;
            }
        }

        public int InsertEdi856Items(DataTable products, int ftpQueueItemId, int ediFileTypeId
                                                                 , DateTime? InterchangeDateTime
                                                                 , DateTime? ActualDepartureDate
                                                                 , DateTime? EstimatedArrivalDate
                                                                 , DateTime? ActualArrivalDate
                                                                 , DateTime? CarrierPickupDate
                                                                 , DateTime? DeliveryToCustomer
                                                                 , int? createdBy)
        {
            const string spName = "dbo.usp_Edi856ReceivedItemsInsert";
            var p = new DynamicParameters(
                new
                {
                    FTPQueueItemID = ftpQueueItemId,
                    EdiFileTypeID = ediFileTypeId,
                    Items = products,
                    InterchangeDateTime = InterchangeDateTime,
                    ActualDepartureDate = ActualDepartureDate,
                    EstimatedArrivalDate = EstimatedArrivalDate,
                    ActualArrivalDate = ActualArrivalDate,
                    CarrierPickupDate = CarrierPickupDate,
                    DeliveryToCustomer= DeliveryToCustomer,
                    CreatedBy = createdBy
                });
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                var result = conn.Execute(spName,
                      p,
                      commandType: CommandType.StoredProcedure);
                conn.Close();

                return result;
            }
        }
    }
}
