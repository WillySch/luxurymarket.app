﻿using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class CompanyRepository : RepositoryBase,
        ICompanyRepository
    {
        private const string companyFieldsSql = @"select CompanyID as CompanyId
                                                ,Name as CompanyName
                                                ,StatusID
                                                ,Email as EmailAddress
                                                ,Phone as PhoneNumber
                                                , CompanyTypeID
                                                , CompanyType
                                                , RetailID as RetailId
                                                , Description
                                                , Website ";

        public CompanyRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public Company[] SearchCompaniesByName(string name, out int totalRecords)
        {
            var sql = $"{companyFieldsSql} FROM {SqlFunctionName.fn_CompanySearchByName}(@name)";
            Company[] result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn
                    .Query<Company>(sql, new { name })
                    .ToArray();
                conn.Close();
            }
            totalRecords = result.Length; // todo: proper pagination
            return result;
        }

        public Company[] SearchCompaniesByName(string name)
        {
            int totalRecords;
            return SearchCompaniesByName(name, out totalRecords);
        }

        public bool IsUserAssociatedToCompany(int userId, int companyId)
        {
            var sql = $"SELECT {SqlFunctionName.fn_IsUserAssociatedToCompany}(@userId, @companyId)";
            bool result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.ExecuteScalar<bool>(sql, new { userId, companyId });
                conn.Close();
            }
            return result;
        }

        public int CreateRequest(RegistrationRequest model, out string errorMessage)
        {
            errorMessage = string.Empty;
            var p = new DynamicParameters();
            p.Add("@ContactName", model.ContactName);
            p.Add("@CompanyName", model.CompanyName);
            p.Add("@CompanyTypeID", (Int16)model.CompanyType);
            p.Add("@Email", model.Email);
            p.Add("@Phone", model.PhoneNumber);
            p.Add("@StreetAddress", model.StreetAddress);
            p.Add("@City", model.City);
            p.Add("@StateID", model.StateId);
            p.Add("@ZipCode", model.ZipCode);
            p.Add("@CountryID", model.CountryId);
            p.Add("@CreatedDateTime", DateTimeOffset.Now);
            p.Add("ID", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            using (var conn = GetReadWriteConnection())
            {
                try
                {
                    conn.Open();
                    conn.Execute(StoredProcedureNames.RegisterRequest_Insert, p, commandType: CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
            return p.Get<int>("ID");
        }

        public async Task<IEnumerable<Company>> GetCompaniesByCustomerId(int customerId, int companyTypeId)
        {
            var sql = $"{companyFieldsSql} from {SqlFunctionName.fn_CompaniesGetByCustomer}(@CustomerID,@CompanyTypeID)";

            IEnumerable<Company> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<Company>(sql, new { customerId, companyTypeId });
                conn.Close();
            }
            return result.ToList();
        }

        public async Task<Company> GetCompanyById(int companyId)
        {
            var sql = $"{companyFieldsSql} FROM {SqlFunctionName.fn_CompanyGetById}(@CompanyId)";

            IEnumerable<Company> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<Company>(sql, new { companyId });
                conn.Close();
            }
            return result.FirstOrDefault();
        }

        public async Task<bool> AcceptTermsAndConditions(int companyId, int userId, DateTimeOffset currentDate)
        {
            var p = new DynamicParameters();
            p.Add("@CompanyID", companyId);
            p.Add("@CurrentUser", userId);
            p.Add("@CurrentDate", currentDate);

            using (var conn = GetReadWriteConnection())
            {
                try
                {
                    conn.Open();
                    conn.Execute(StoredProcedureNames.AcceptTermsAndConditonsForCompany, p, commandType: CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    conn.Close();
                }
            }
            return true;
        }
    }
}
