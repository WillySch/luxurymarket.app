﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.DataReaderExtensions
{
    public static partial class DataReaderExtensions
    {
        public static List<Customer> ToCustomerList(this DataReader reader)
        {
            var items = new List<Customer>();

            while (reader.Read())
            {
                items.Add(new Customer
                {
                    CustomerId = reader.GetInt("CustomerID"),
                    UserId = reader.GetInt("UserID"),
                    Email = reader.GetString("Email"),
                    Phone = reader.GetString("Phone"),
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName"),
                    Notes = reader.GetString("Notes")
                });
            }

            return items;
        }

        public static List<Company> ToCompanyList(this DataReader reader)
        {
            var items = new List<Company>();

            while (reader.Read())
            {
                items.Add(new Company
                {
                    CompanyType = (CompanyType)reader.GetInt("CompanyTypeID"),
                    CompanyId = reader.GetInt("CompanyID"),
                    CompanyName = reader.GetString("CompanyName"),
                    EmailAddress = reader.GetString("EmailAddress"),
                    PhoneNumber = reader.GetString("PhoneNumber"),
                    RetailId = reader.GetString("RetailID"),
                    Description = reader.GetString("Description"),
                    Website = reader.GetString("Website")
                });
            }

            return items;
        }
    }
}
