﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.DataReaderExtensions
{
    public static partial class DataReaderExtensions
    {
        public static Product ToProductDetails(this DataReader reader)
        {
            if (reader == null) return null;

            var product = new Product();

            while (reader.Read())
            {

                product.ID = reader.GetInt("ID");
                product.Name = reader.GetString("Name");
                product.SKU = reader.GetString("SKU");

                product.RetailPrice = reader.GetDoubleNullable("RetailPrice");

                product.BrandName = reader.GetString("BrandName");
                product.ColorName = reader.GetString("ColorName");
                product.MaterialName = reader.GetString("MaterialName");
                product.OriginName = reader.GetString("OriginName");
                product.SeasonName = reader.GetString("SeasonName");
                product.Description = reader.GetString("Description");
                product.MainImageName = reader.GetString("MainImageName");
            }

            if (reader.NextResult())
            {
                product.Images =reader.ToEntityImages();
            }

            if (reader.NextResult())
            {
                var productOffers = new List<ProductOfferModel>();
                while (reader.Read())
                {
                    productOffers.Add(new ProductOfferModel()
                    {
                        OfferID = reader.GetInt("OfferID"),
                        OfferName = reader.GetString("OfferName"),
                        TakeAll = reader.GetBool("TakeAll"),
                        RetailPrice = reader.GetDouble("RetailPrice"),
                        LMPrice = reader.GetDouble("LMPrice"),
                        LineBuy = reader.GetBool("LineBuy"),
                        TotalQuantity = reader.GetInt("TotalQuantity"),
                        Discount = reader.GetDouble("Discount"),
                        OfferProductSizeID = reader.GetInt("OfferProductSizeID"),
                        Quantity = reader.GetInt("Quantity"),
                        SizeName = reader.GetString("SizeName"),
                        ShipsFrom = reader.GetString("ShipsFrom"),
                        CompanyName = reader.GetString("CompanyName")
                    });
                }

                product.Offers = (from o in productOffers
                                  group o by o.OfferID into grpOffers
                                  let grpOfferKey = productOffers.First(x => x.OfferID == grpOffers.Key)

                                  let offer = new ProductOffer
                                  {
                                      OfferID = grpOfferKey.OfferID,
                                      TakeAll = grpOfferKey.TakeAll,
                                      RetailPrice = grpOfferKey.RetailPrice,
                                      LMPrice = grpOfferKey.LMPrice,
                                      LineBuy = grpOfferKey.LineBuy,
                                      TotalQuantity = grpOfferKey.TotalQuantity,
                                      Discount = grpOfferKey.Discount,
                                      ShipsFrom = grpOfferKey.ShipsFrom,
                                      CompanyName = grpOfferKey.CompanyName,
                                      Sizes = (from os in grpOffers
                                               select new ProductOfferSize
                                               {
                                                   OfferProductSizeID = os.OfferProductSizeID,
                                                   Quantity = os.Quantity,
                                                   SizeName = os.SizeName
                                               }).ToList()
                                  }
                                  select offer).OrderBy(x => x.RetailPrice).ToList();
            }


            return product;
        }

        public static IEnumerable<ProductSearchResult> ToProductSearchResult(this DataReader reader)
        {
            var products = new List<ProductSearchResult>();

            while (reader.Read())
            {
                products.Add(
                    new ProductSearchResult
                    {
                        Id = reader.GetInt("ID"),
                        Name = reader.GetString("ProductName"),
                        SKU = reader.GetString("Sku"),
                        MainImageName = reader.GetString("MainImageName"),
                        BrandId = reader.GetInt("BrandID"),
                        BrandName = reader.GetString("BrandName"),
                        LMPrice = reader.GetDouble("LMPrice")
                    });
            }
            return products;
        }

        public static List<Size> ToSizes(this DataReader reader)
        {
            var list = new List<Size>();

            while (reader.Read())
            {
                list.Add(new Size
                {
                    Id = reader.GetInt("ID"),
                    Name = reader.GetString("Name"),
                    SizeType = new SizeType
                    {
                        Id = reader.GetInt("SizeTypesID"),
                        Name = reader.GetString("SizeTypesName"),
                        Description = reader.GetString("SizeTypesDescription"),
                    }
                });
            }
            return list;
        }
    }
}
