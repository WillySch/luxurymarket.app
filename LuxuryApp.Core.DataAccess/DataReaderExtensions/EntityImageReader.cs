﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.DataReaderExtensions
{
    public static partial class DataReaderExtensions
    {
        public static List<EntityImage> ToEntityImages(this DataReader reader)
        {
            var images = new List<EntityImage>();

            while (reader.Read())
            {
                images.Add(new EntityImage
                {
                    Id = reader.GetInt("ID"),
                    EntityId = reader.GetInt("EntityID"),
                    EntityType = (EntityType)reader.GetInt("EntityTypeID"),
                    Name = reader.GetString("Name"),
                    //Link = reader.GetString("Link"),
                    DisplayOrder = reader.GetInt("DisplayOrder")
                });
            }

            return images;
        }

    }
}
