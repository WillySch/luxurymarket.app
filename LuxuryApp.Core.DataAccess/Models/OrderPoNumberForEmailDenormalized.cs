﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.Models
{
    public class OrderPoNumberForEmailDenormalized
    {
        public int OrderId { get; set; }

        public int? OrderSellerId { get; set; }

        public string PoNumber { get; set; }

        public bool IsBuyer { get { return OrderSellerId == null; } }

        public string Email { get; set; }
    }
}
