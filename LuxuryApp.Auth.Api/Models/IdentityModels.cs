﻿using LuxuryApp.Auth.Core.Attributes;
using LuxuryApp.Auth.Core.Entities;
using LuxuryApp.Auth.Core.Interfaces;
using LuxuryApp.Auth.DataAccess;
using LuxuryApp.Auth.DataAccess.Repositories;
using LuxuryApp.NotificationService;
using LuxuryApp.NotificationService.Emails;
using Microsoft.AspNet.Identity;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LuxuryApp.Auth.Api.Models
{
    [Table("app.Users")]
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : User<int>
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

    }

    [Table("app.Roles")]
    public class ApplicationRole : Role
    {
    }

    [Table("app.UserRoles")]
    public class ApplicationUserRole : UserRole<int, string>
    {
    }

    [Table("app.RefreshTokens")]
    public class ApplicationRefreshToken : RefreshToken
    {
    }

    public class ApplicationDbContext : DapperIdentityDbContext<ApplicationUser, ApplicationRole>
    {
        private static readonly string _connectionString = ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["ConnectionStringName"]].ConnectionString;

        public ApplicationDbContext(IConnectionStringProvider connection)
            : base(connection)
        {
        }

        public static ApplicationDbContext Create()
        {
            var conn = new AuthConnectionStringProvider();
            return new ApplicationDbContext(conn);
        }
    }
}