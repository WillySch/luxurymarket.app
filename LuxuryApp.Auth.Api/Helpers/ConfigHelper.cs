﻿using System.Configuration;

namespace LuxuryApp.Auth.Api.Helpers
{
    public static class ConfigHelper
    {
        public static int RefreshTokenMinutesTime
        {
            get
            {
                var minutes = 60;
                int.TryParse(ConfigurationManager.AppSettings["RefreshTokenMinutesTime"], out minutes);
                return minutes;
            }
        }

        public static int AccessTokenExpireTimeSpan
        {
            get
            {
                var minutes = 60;
                int.TryParse(ConfigurationManager.AppSettings["AccessTokenExpireTimeSpan"], out minutes);
                return minutes;
            }
        }

        public static int TokenLifeSpanDays
        {
            get
            {
                var days = 14;
                int.TryParse(ConfigurationManager.AppSettings["TokenLifeSpanDays"], out days);
                return days;
            }
        }
    }
}