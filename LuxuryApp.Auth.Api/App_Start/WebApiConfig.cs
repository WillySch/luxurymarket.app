﻿using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using Newtonsoft.Json.Serialization;
using System.Configuration;
using System.Web.Http.Cors;
using LuxuryApp.Core.Infrastructure.Localization;

namespace LuxuryApp.Auth.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
           // config.MessageHandlers.Add(new LanguageMessageHandler());
            // CORS
            var trustedDomains = ConfigurationManager.AppSettings["AllowedRequestOrigins"];

            var cors = new EnableCorsAttribute(trustedDomains, "*", "*");
            cors.SupportsCredentials = true;
            cors.PreflightMaxAge = 600;
            config.EnableCors(cors);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Formatters.Insert(0, new JsonMediaTypeFormatter());
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
