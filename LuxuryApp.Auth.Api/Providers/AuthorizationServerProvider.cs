﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Microsoft.AspNet.Identity.Owin;
using System;
using LuxuryApp.Auth.Core.Interfaces;
using LuxuryApp.Auth.Core.Entities;
using Newtonsoft.Json;
using System.Diagnostics;
using LuxuryApp.Auth.Api.Helpers;

namespace LuxuryApp.Auth.Api.Providers
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public Func<IUserRepository> _userRepositoryFactory;

        public AuthorizationServerProvider(Func<IUserRepository> userRepositoryFactory)
        {
            _userRepositoryFactory = userRepositoryFactory;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {

            context.OwinContext.Set<string>("as:clientAllowedOrigin", "*");
            context.OwinContext.Set<string>("as:clientRefreshTokenLifeTime",ConfigHelper.RefreshTokenMinutesTime.ToString());

            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

            if (allowedOrigin == null) allowedOrigin = "*";

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });


            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            if (string.IsNullOrEmpty(context.UserName) || string.IsNullOrEmpty(context.Password))
            {
                context.SetError("invalid_request", "No username or password are provided.");
                return;
            }

            var user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            if (!user.EmailConfirmed)
            {
                context.SetError("invalid_grant", "User did not confirm email.");
                return;
            }

            IEnumerable<Customer> customers = null;
            try
            {
                customers = await _userRepositoryFactory().FindCustomerInfoAsync(user.Id);

            }
            catch (Exception ex)
            {
                Trace.TraceError($"Error at getting the cutomer info {ex.InnerException}");
                context.SetError("server_error", "Error at getting the cutomer info");
            }

            if (customers == null || !customers.Any())
            {
                context.SetError("invalid_customer", "Invalid customer or company");
                return;
            }

            try
            {
                var serializedCustomer = JsonConvert.SerializeObject(customers.FirstOrDefault(), new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
                identity.AddClaim(new Claim("sub", context.UserName));
                identity.AddClaim(new Claim("ValueID", user.UserId.ToString()));
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
                var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                   { "customer",serializedCustomer }
                });

                var ticket = new AuthenticationTicket(identity, props);
                context.Validated(ticket);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"The ClaimsIdentity could not be created by the UserManager {ex.InnerException}");
                context.SetError("server_error", "The ClaimsIdentity could not be created by the UserManager.");
            }
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);

            var newClaim = newIdentity.Claims.FirstOrDefault(c => c.Type == "newClaim");
            if (newClaim != null)
            {
                newIdentity.RemoveClaim(newClaim);
            }
            newIdentity.AddClaim(new Claim("newClaim", "refreshToken"));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

    }
}