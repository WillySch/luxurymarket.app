﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryMarket.Services.Vendor.InventoryUpdate.Helpers
{
    public class Configurations
    {
        /// <summary>
        /// Interval in seconds for each service run.
        /// </summary>
        public static int RunInterval
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["RunIntervalSeconds"]);
            }
        }

        public static string ServiceName
        {
            get
            {
                return ConfigurationManager.AppSettings["ServiceName"];
            }
        }
    }
}
