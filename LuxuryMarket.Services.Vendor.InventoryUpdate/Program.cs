﻿using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryMarket.Services.Vendor.InventoryUpdate
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {

            if (System.Environment.UserInteractive)
            {
                string parameter = string.Concat(args);
                switch (parameter)
                {
                    case "--install":
                        ManagedInstallerClass.InstallHelper(new string[]
                            {Assembly.GetExecutingAssembly().Location});
                        break;
                    case "--uninstall":
                        ManagedInstallerClass.InstallHelper(new string[]
                            {"/u", Assembly.GetExecutingAssembly().Location});
                        break;
                }
            }
            else
            {
                ServiceBase[] servicesToRun = new[] { new InventoryUpdateService() };
                ServiceBase.Run(servicesToRun);
            }
            //#if (!DEBUG)
            // above code
            //#else
            //            // Debug code: this allows the process to run as a non-service.
            //            // It will kick off the service start point, but never kill it.
            //            // Shut down the debugger to exit
            //            InventoryUpdateService.DoWork();
            //            // Put a breakpoint on the following line to always catch
            //            // your service when it has finished its work
            //            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
            //#endif 
        }
    }
}
