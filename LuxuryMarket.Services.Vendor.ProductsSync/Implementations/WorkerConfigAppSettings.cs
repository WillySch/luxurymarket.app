﻿using System;
using System.Configuration;
using LuxuryApp.Contracts.Enums;

namespace LuxuryMarket.Services.Vendor.ProductsSync.Implementations
{
    public static class ConfigAppSettings
    {
        public static int DefaultProductSyncDelayMinutes
        {
            get
            {
                var value = 10;
                int.TryParse(ConfigurationManager.AppSettings["DefaultProductSyncDelayMinutes"] ?? "", out value);
                return value;
            }
        }

        public static int DefaultIntervalSyncDelayMinutes
        {
            get
            {
                var value = 10;
                int.TryParse(ConfigurationManager.AppSettings["DefaultIntervalSyncDelayMinutes"] ?? "", out value);
                return value;
            }
        }

        public static string ServiceName
        {
            get { return ConfigurationManager.AppSettings["ServiceName"]; }
        }

        public static string ServiceDisplayName
        {
            get { return ConfigurationManager.AppSettings["ServiceDisplayName"]; }
        }
    }
}
