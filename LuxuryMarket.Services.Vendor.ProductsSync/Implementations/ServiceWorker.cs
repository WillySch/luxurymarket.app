﻿using Autofac;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Processors.Agents.Sellers;
using LuxuryApp.Processors.Agents.Sellers.Icon;
using LuxuryApp.Utils.Helpers;
using NLog;
using System;
using System.Threading;

namespace LuxuryMarket.Services.Vendor.ProductsSync.Implementations
{
    public class ServiceWorker : IWorker
    {
        //private readonly Func<ISellerProductSyncAgent> _sellerProductSyncAgent;
        private readonly Func<ISellerApiSettingService> _sellerApiSettingService;
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly int taskWaitMiliseconds = 216000;
        private readonly Func<IconProductSyncAgent> _iconProductSyncAgent;
        private readonly Func<AlducaProductSyncAgent> _alducaProductSyncAgent;
        private readonly Func<FashionDealerProductSyncAgent> _fashionProductSyncAgent;



        public ServiceWorker(Func<ISellerProductSyncAgent> sellerProductSyncAgent,
                             Func<IconProductSyncAgent> iconProductSyncAgent,
                             Func<AlducaProductSyncAgent> alducaProductSyncAgent,
                             Func<FashionDealerProductSyncAgent> fashionProductSyncAgent,
                             Func<ISellerApiSettingService> sellerApiSettingService)
        {
            // _sellerProductSyncAgent = sellerProductSyncAgent;
            _sellerApiSettingService = sellerApiSettingService;
            _iconProductSyncAgent = iconProductSyncAgent;
            _alducaProductSyncAgent = alducaProductSyncAgent;
            _fashionProductSyncAgent = fashionProductSyncAgent;
        }

        public void Start(CancellationToken cancellationToken)
        {
            try
            {
                _logger.Info("Thread started");
                //todo: logging
                new Thread(() => DoProductSyncWork(SellerAPISettingType.AlDuca)).Start();
                // new Thread(() => DoInventorySyncWork(SellerAPISettingType.AlDuca)).Start();

                new Thread(() => DoProductSyncWork(SellerAPISettingType.Icon)).Start();
                new Thread(() => DoInventorySyncWork(SellerAPISettingType.Icon)).Start();

                new Thread(() => DoProductSyncWork(SellerAPISettingType.FashionDealer)).Start();
                new Thread(() => DoInventorySyncWork(SellerAPISettingType.FashionDealer)).Start();

            }
            catch (Exception ex)
            {
                _logger.Error($"Thread error: {ex.ToString()}");
            }
        }

        #region Private Methods

        private void DoProductSyncWork(SellerAPISettingType sellerType)
        {
            DelayExcecution(3);
            while (true)
            {
                var settings = GetSettings(sellerType);
                if (settings != null)
                    ProductSync(settings);
                var sleepInterval = (settings?.ProductDelayMinutes ?? ConfigAppSettings.DefaultProductSyncDelayMinutes);
                DelayExcecution(sleepInterval);
            }
        }

        private void DoInventorySyncWork(SellerAPISettingType sellerType)
        {
            DelayExcecution(3);
            while (true)
            {
                var settings = GetSettings(sellerType);
                if (settings != null)
                    ProductStockSync(settings);

                var sleepInterval = (settings?.InventoryDelayMinutes ?? ConfigAppSettings.DefaultIntervalSyncDelayMinutes);
                DelayExcecution(sleepInterval);
            }
        }

        private APISettings GetSettings(SellerAPISettingType type)
        {
            var settings = _sellerApiSettingService().GetSellerApiSettings(type).Result;
            settings.SellerType = (SellerAPISettingType)settings.Id;
            return settings;
        }

        private void ProductStockSync(APISettings settings)
        {
            try
            {
                switch (settings.SellerType)
                {
                    case SellerAPISettingType.AlDuca:
                        _alducaProductSyncAgent().SyncProductStock(settings).Wait(taskWaitMiliseconds);
                        return;
                    case SellerAPISettingType.Icon:
                        _iconProductSyncAgent().SyncProductStock(settings).Wait(taskWaitMiliseconds);
                        return;
                    case SellerAPISettingType.FashionDealer:
                        _fashionProductSyncAgent().SyncProductStock(settings).Wait(taskWaitMiliseconds);
                        return;
                }
            }
            catch (AggregateException ex)
            {
                _logger.Error($"Product sync for sellerid {settings.SellerID.ToString()} Sync AggregateException {ex.ToString()}");
            }
            catch (Exception ex)
            {
                _logger.Error($"Product sync for sellerid {settings.SellerID.ToString()} Sync {ex.ToString()}");
            }
        }

        private void ProductSync(APISettings settings)
        {
            try
            {
                switch (settings.SellerType)
                {
                    case SellerAPISettingType.AlDuca:
                        _alducaProductSyncAgent().SyncProducts(settings).Wait(taskWaitMiliseconds);
                        return;
                    case SellerAPISettingType.Icon:
                        _iconProductSyncAgent().SyncProducts(settings).Wait(taskWaitMiliseconds);
                        return;
                    case SellerAPISettingType.FashionDealer:
                        _fashionProductSyncAgent().SyncProducts(settings).Wait(taskWaitMiliseconds);
                        return;
                }
            }
            catch (AggregateException ex)
            {
                _logger.Error($"Product Sync AggregateException {ex.ToString()}");
            }
            catch (Exception ex)
            {
                _logger.Error($"Product Sync {ex.ToString()}");
            }
        }

        private void DelayExcecution(int numberMinutes)
        {
            Thread.Sleep(numberMinutes.ConvertToMilliseconds());
        }

        #endregion
    }
}
