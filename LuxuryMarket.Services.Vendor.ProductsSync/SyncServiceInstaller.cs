﻿using LuxuryMarket.Services.Vendor.ProductsSync.Implementations;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.ServiceProcess;

namespace LuxuryMarket.Services.Vendor.ProductsSync
{
    [RunInstaller(true)]
    public class SyncServiceInstaller : Installer
    {
        public SyncServiceInstaller()
        {
            var processInstaller = new ServiceProcessInstaller();
            var serviceInstaller = new ServiceInstaller();

            //set the privileges
            processInstaller.Account = ServiceAccount.LocalSystem;

            serviceInstaller.ServiceName = "LuxuryMarket.VendorProductsSync";
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            //must be the same as what was set in Program's constructor
            serviceInstaller.DisplayName = "LuxuryMarket.VendorProductsSync";
            this.Installers.Add(processInstaller);
            this.Installers.Add(serviceInstaller);

        }
    }
}
