﻿using LuxuryApp.Contracts.BackgroudServices;
using LuxuryMarket.Services.Vendor.ProductsSync.Implementations;
using System.ServiceProcess;
using System.Threading;

namespace LuxuryMarket.Services.Vendor.ProductsSync
{
    public partial class SyncService : ServiceBase
    {
        private readonly IWorker _worker;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        public SyncService(IWorker worker)
        {
            InitializeComponent();

            // settings
            this.ServiceName = "LuxuryMarket.Vendor.ProductsSync";
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;

            _worker = worker;

        }

        protected override void OnStart(string[] args)
        {
            _worker.Start(_cancellationTokenSource.Token);
        }

        protected override void OnStop()
        {
            _cancellationTokenSource.Cancel();
        }

    }
}
