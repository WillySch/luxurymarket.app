﻿using Autofac;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.DataAccess;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.Processors.Agents.Sellers;
using LuxuryApp.Processors.Agents.Sellers.Icon;
using LuxuryMarket.Services.Vendor.ProductsSync.Implementations;
using System;

namespace LuxuryMarket.Services.Vendor.ProductsSync
{
    public class DependencyBuilder
    {
        public IContainer GetDependencyContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<LuxuryLogger>().As<ILogger>();
            builder.RegisterType<LuxuryMarketConnectionStringProvider>().As<IConnectionStringProvider>();
            builder.RegisterType<SellerProductSyncRepository>().As<ISellerProductSyncRepository>();
            builder.RegisterType<SellerAPISettingRepository>().As<ISellerAPISettingRepository>();

            builder.RegisterType<SellerApiSettingService>().As<ISellerApiSettingService>();
           
            builder.RegisterType<IconProductSyncAgent>().As<ISellerProductSyncAgent>();
            builder.RegisterType<FashionDealerProductSyncAgent>().As<ISellerProductSyncAgent>();
            builder.RegisterType<AlducaProductSyncAgent>().Named<ISellerProductSyncAgent>("AlducaService");

            builder.Register(c => new FashionDealerCategoriesAgent(c.Resolve<Func<ILogger>>()));

            builder.Register(c => new IconProductSyncAgent(c.Resolve<Func<ISellerAPISettingRepository>>(),
                                                           c.Resolve<Func<ISellerProductSyncRepository>>(),
                                                           c.Resolve<Func<ILogger>>()));
            builder.Register(c => new AlducaProductSyncAgent(c.Resolve<Func<ISellerAPISettingRepository>>(),
                                                      c.Resolve<Func<ISellerProductSyncRepository>>(),
                                                      c.Resolve<Func<ILogger>>()));

            builder.Register(c => new FashionDealerProductSyncAgent(c.Resolve<Func<ILogger>>(),
                                                                    c.Resolve<Func<ISellerProductSyncRepository>>(),
                                                                    c.Resolve<FashionDealerCategoriesAgent>()));


            builder.RegisterType<ServiceWorker>().As<IWorker>();

            builder.RegisterType<SyncService>().AsSelf();
            var container = builder.Build();
            return container;
        }
    }
}
