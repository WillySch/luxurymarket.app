﻿using System.Reflection;
using System.ServiceProcess;
using Autofac;
using System.Configuration.Install;
using System;
using LuxuryApp.Contracts.BackgroudServices;
using System.Threading;

namespace LuxuryMarket.Services.Vendor.ProductsSync
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                string parameter = string.Concat(args);
                switch (parameter)
                {
                    case "--install":
                        ManagedInstallerClass.InstallHelper(new string[]
                            {Assembly.GetExecutingAssembly().Location});
                        break;
                    case "--uninstall":
                        ManagedInstallerClass.InstallHelper(new string[]
                            {"/u", Assembly.GetExecutingAssembly().Location});
                        break;
                }
            }
            else
            {
                var container = new DependencyBuilder().GetDependencyContainer();
                ServiceBase[] servicesToRun = new[] { container.Resolve<SyncService>() };
                ServiceBase.Run(servicesToRun);
            }

            //var container = new DependencyBuilder().GetDependencyContainer();
            //CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            //var worker = container.Resolve<IWorker>();
            //worker.Start(cancellationTokenSource.Token);

            //Console.WriteLine("Press Enter to exit");
            //Console.ReadLine();
        }
    }
}
