﻿using LuxuryApp.Auth.Core.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LuxuryApp.Auth.Core.Interfaces
{
    public interface IRepository<T> : IRepository<T, object> where T : class
    {

    }

    public interface IRepository<T, TKey> where T : class
    {
        long Insert(T item);
        Task<int> InsertAsync(T item);
        bool Remove(T item);
        Task<bool> RemoveAsync(T item);
        bool RemoveAll();
        Task<bool> RemoveAllAsync();

        void Remove(Expression<Func<T, bool>> predicate);

        Task RemoveAsync(Expression<Func<T, bool>> predicate);
        bool Update(T item);
        Task<bool> UpdateAsync(T item);
        T Get(TKey id);
        Task<T> GetAsync(TKey id);
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);

        Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> predicate);
        IEnumerable<TS> Query<TS>(string sql, object param);
        Task<IEnumerable<TS>> QueryAsync<TS>(string sql, object param);
        string GetTableName<T>();
    }
}
