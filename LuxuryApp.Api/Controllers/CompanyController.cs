﻿using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Authorization;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("api/companies")]
    [Authorize]
    public class CompanyController : ApiController
    {
        private Func<ICompanyService> _companyServiceFactory;
        private readonly IContextProvider _contextProvider;

        public CompanyController(Func<ICompanyService> companyServiceFactory, IContextProvider contextProvider)
        {
            _companyServiceFactory = companyServiceFactory;
            _contextProvider = contextProvider;
        }

        /// <summary>
        /// Save Registration Request
        /// </summary>
        /// <param name="model"><see cref="RegistrationRequest"/> </param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("{companyId:int}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(int))]
        public async Task<IHttpActionResult> GetCompaniesById(int companyId)
        {
            var result =await _companyServiceFactory().GetCompanyById(companyId);
            return Ok(result);
        }

        /// <summary>
        /// Accept Terms and conditions
        /// </summary>
        /// <param name="companyId"><see cref="int"/></param>
        /// <returns></returns>
        [HttpPost]
        [Route("accept_terms_and_conditions/{companyId:int}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(bool))]
        public async Task<IHttpActionResult> AcceptTermsAndConditions(int companyId)
        {
            var result =await _companyServiceFactory().AcceptTermsAndConditions(companyId, _contextProvider.GetLoggedInUserId(), DateTimeOffset.Now);
            return Ok(result);
        }
    }
}