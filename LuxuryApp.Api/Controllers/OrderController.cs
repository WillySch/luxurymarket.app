﻿using LuxuryApp.Api.ActionFilterAttributes;
using LuxuryApp.Api.Helpers;
using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Contracts.Models.Orders;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Extensions;
using LuxuryApp.Core.Infrastructure.Providers;
using LuxuryApp.Processors.Resources.Export;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LuxuryApp.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/orders")]
    public class OrderController : ApiController
    {
        private readonly Func<IOrderService> _orderServiceFactory;
        private readonly Func<ICartService> _ICartService;

        public OrderController(Func<IOrderService> orderServiceFactory)
        {
            _orderServiceFactory = orderServiceFactory;
        }

        [HttpGet]
        [Route("paymentMethods")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<PaymentMethod>))]
        public async Task<IHttpActionResult> GetPaymentMethodAsync()
        {
            var paymentMethods = await _orderServiceFactory().GetPaymentMethodAsync();
            return Ok(paymentMethods);
        }

        [HttpPost]
        [Route("submit")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<string>))]
        public async Task<IHttpActionResult> SubmitOrderAsync(OrderSubmitDataView model)
        {
            var result = await _orderServiceFactory().SubmitOrderAsync(model, DateTimeOffset.Now);
            return Ok(result);
        }

        [HttpGet]
        [Route("getBuyerDetails/{orderId:int}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<Order>))]
        public async Task<IHttpActionResult> GetOrderDetailsForBuyerAsync(int orderId)
        {
            var order = await _orderServiceFactory().GetOrderDetailsForBuyerAsync(orderId);
            return Ok(order);
        }

        [HttpGet]
        [Route("getSellerDetails/{ordersellerId:int}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OrderSellerView>))]
        public async Task<IHttpActionResult> GetOrderDetailsForSellerAsync(int ordersellerId)
        {
            var orderSellerItem = await _orderServiceFactory().GetOrderItemForSellerByIdAsync(ordersellerId);
            return Ok(orderSellerItem);
        }

        [HttpPost]
        [Route("getAllForSeller")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<OrderSellerManageList>))]
        public async Task<IHttpActionResult> FilterSellerOrdersAsync(RequestFilterOrders model)
        {
            var result = await _orderServiceFactory().FilterSellerOrdersAsync(model);
            return Ok(result);
        }

        [HttpPost]
        [Route("getAllForBuyer")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<OrderSellerManageList>))]
        public async Task<IHttpActionResult> FilterBuyerOrdersAsync(RequestFilterOrders model)
        {
            var result = await _orderServiceFactory().FilterBuyerOrdersAsync(model);
            return Ok(result);
        }

        [HttpPost]
        [Route("confirmUnits")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OrderProductTotalForSeller>))]
        public async Task<IHttpActionResult> ConfirmUnitsAsync(OrderConfirmUnitView model)
        {
            var result = await _orderServiceFactory().ConfirmUnitsAsync(model, DateTimeOffset.Now);
            return Ok(result);
        }

        [HttpGet]
        [Route("generate_order_pdf")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(byte[]))]
        public IHttpActionResult GenerateOrderSellerPDF(int orderSellerId)
        {
            var result = _orderServiceFactory().GenerateOrderSellerPDF(orderSellerId);
            return result.ToHttpActionResult(this);
        }

        [HttpGet]
        [Route("export_order")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(System.Web.Mvc.FileResult))]
        public async Task<HttpResponseMessage> ExportOrderExcel(int orderId, bool isBuyer)
        {
            var excelFile = await _orderServiceFactory().ExportOrderExcel(orderId, isBuyer);
            string fileName = ExcelNames.ORDER_FILE_NAME + DateTimeOffset.Now.ToFileTime() + ExcelNames.EXPORT_EXTENSION;

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(excelFile.Data);
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.ms-excel");

            return response;
        }

        #region Deprecated - one single save for package information
        //[HttpGet]
        //[Route("boxSizeTypes")]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<BoxSizeTypeCollection>))]
        //public async Task<IHttpActionResult> GetBoxSizeTypes()
        //{
        //    var result = await _orderServiceFactory().GetBoxSizeTypes();
        //    return Ok(result);
        //}

        //[HttpGet]
        //[Route("suborderPackages")]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OrderSellerPackageCollection>))]
        //public async Task<IHttpActionResult> GetBoxSizeTypes(int orderSellerId)
        //{
        //    var result = await _orderServiceFactory().GetOrderSellerPackages(orderSellerId);
        //    return Ok(result);
        //}

        //[HttpPost]
        //[Route("suborderPackages")]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        //public async Task<IHttpActionResult> InsertOrderSellerPackage(OrderSellerPackageInsert model)
        //{
        //    //todo: validation efluent
        //    var result = await _orderServiceFactory().InsertOrderSellerPackage(model);
        //    return Ok(result);
        //}

        //[HttpPost]
        //[Route("pickupDates")]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        //public async Task<IHttpActionResult> UpdateOrderSellerPickupDates(OrderSellerPickupDate model)
        //{
        //    var result = await _orderServiceFactory().UpdateOrderPickupDates(model);
        //    return Ok(result);
        //}

        //[HttpGet]
        //[Route("pickupDates")]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(OrderSellerPickupDate))]
        //public async Task<IHttpActionResult> GetOrderSellerPickupDates(int ordersellerId)
        //{
        //    var result = await _orderServiceFactory().GetOrderSellerPickupDates(ordersellerId);
        //    return Ok(result);
        //}
        #endregion

        [HttpPost]
        [Route("confirmPackages")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(int))]
        public async Task<IHttpActionResult> InsertOrderSellerPackingInformationAsync(OrderSellerPackingInformationInsert model)
        {
            var result = await _orderServiceFactory().InsertOrderSellerPackingInformationAsync(model);
            return Ok(result);
        }

        [HttpGet]
        [Route("isSubmittedPackageInformation/{ordersellerId:int}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(bool))]
        public async Task<IHttpActionResult> IsSubmittedPackageInformation(int ordersellerId)
        {
            var result = await _orderServiceFactory().IsSubmittedPackageInformation(ordersellerId);
            return Ok(result);
        }

        [HttpPost]
        [Route("qualityControlSendFailEmail")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<bool>))]
        [AllowAnonymous]
        public async Task<IHttpActionResult> QualityControlSendFailEmail(QualityControlFailProductsEmailModel model)
        {
            var result = await _orderServiceFactory().QualityControlSendFailEmail(model.PoNumber, model.EmailAddresses, model.QualityControlExcelFileContent);
            return Ok(result);
        }


        #region Get StatusTypes

        [HttpGet]
        [Route("suborderStatusTypes")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<OrderStatusType>))]
        public async Task<IHttpActionResult> GetSuborderStatusTypesAsync()
        {
            var result = await _orderServiceFactory().GetSuborderStatusTypesAsync();
            return Ok(result);
        }

        [HttpPost]
        [Route("updateStatus")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<bool>))]
        public async Task<IHttpActionResult> UpdateStatusTypesAsync(OrderStatusUpdateView model)
        {
            var result = await _orderServiceFactory().UpdateStatusAsync(model, DateTimeOffset.Now);
            return Ok(result);
        }

        #endregion

        #region Files

        /// <summary>
        /// Get offer import files
        /// </summary>
        /// <param name="orderId">Id of offerImport</param>
        /// <returns>Collection of <see cref="OfferImportFile"/> </returns>
        [HttpGet]
        [Route("{orderSellerId:int}/files")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OrderFileList>))]
        public async Task<IHttpActionResult> GetFilesAsync(int orderSellerId)
        {
            int? companyId = null;
            if (Request.Headers.Contains(RequestHeaders.CompanyID))
            {
                companyId = Convert.ToInt32(Request.Headers.GetValues(RequestHeaders.CompanyID).First());
            }

            var result = await _orderServiceFactory().GetFilesAsync(null, orderSellerId, companyId);
            return Ok(result);
        }

        [HttpGet]
        [Route("mainOrder/{mainOrderId:int}/files")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OrderFileList>))]
        public async Task<IHttpActionResult> GetMainOrderFilesAsync(int mainOrderId)
        {
            int? companyId = null;
            if (Request.Headers.Contains(RequestHeaders.CompanyID))
            {
                companyId = Convert.ToInt32(Request.Headers.GetValues(RequestHeaders.CompanyID).First());
            }
            var result = await _orderServiceFactory().GetFilesAsync(mainOrderId, null, companyId);
            return Ok(result);
        }

        /// <summary>
        /// Upload offer files
        /// Buyer can upload files only on the main order
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{orderId:int}/files/upload")]
        [Route("mainOrder/{mainOrderId:int}/files/upload")]
        [ValidateMimeMultipartContentFilter]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<int>))]
        public async Task<IHttpActionResult> UploadFileAsync(int? orderId = null, int? mainOrderId = null)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Request!");
                throw new HttpResponseException(response);
            }

            int? companyId = null;
            if (Request.Headers.Contains(RequestHeaders.CompanyID))
            {
                companyId = Convert.ToInt32(Request.Headers.GetValues(RequestHeaders.CompanyID).First());
            }

            var streamProvider = new ImportProvider(WebConfigs.OrderDocumentsFolder);
            await Request.Content.ReadAsMultipartAsync(streamProvider);

            var data = streamProvider.FileData.FirstOrDefault();
            var name = Path.GetFileNameWithoutExtension(data.LocalFileName);
            var file = new OrderFile
            {
                FileName = Path.GetFileName(data.LocalFileName),
                OriginalName = string.Concat(
                            name.Remove(name.LastIndexOf("_")),
                            Path.GetExtension(data.LocalFileName)),
                MainOrderId = mainOrderId,
                OrderSellerId = orderId
            };

            var result = await _orderServiceFactory().InsertFileAsync(file, companyId);

            var token = Request.Headers.GetValues("Authorization").FirstOrDefault();
            await ApiClient.UploadFile(data.LocalFileName, token);

            return Ok(result);
        }

        /// <summary>
        /// Update offer import file (delete or change original name)
        /// </summary>
        /// <param name="orderId">Id of offerImportBatch</param>
        /// <param name="file">object of <see cref="OfferImportFile"/> </param>
        /// <returns></returns>
        [HttpPost]
        [Route("files")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public async Task<IHttpActionResult> UpdateFileAsync(OrderFile model)
        {
            if ((model?.Id ?? 0) <= 0 || (!model.MainOrderId.HasValue && !model.OrderSellerId.HasValue))
            {
                var error = new ApiResultBadRequest<int>(0, "Invalid input").ToApiResult();
                return Ok(error);
            }

            int? companyId = null;
            if (Request.Headers.Contains(RequestHeaders.CompanyID))
            {
                companyId = Convert.ToInt32(Request.Headers.GetValues(RequestHeaders.CompanyID).First());
            }

            var result = await _orderServiceFactory().UpdateFileAsync(model, companyId);
            return Ok(result);
        }

        #endregion

    }

}