﻿using System;
using System.Net;
using System.Web.Http;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Processors.Services;
using System.Web.Http.Description;
using LuxuryApp.Api.Helpers;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Extensions;
using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Core.Infrastructure.Authorization;

namespace LuxuryApp.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/products")]
    public class ProductController : ApiController
    {
        private IProductService _productServiceFactory;
        private readonly IContextProvider _contextProvider;
        private readonly ICustomerService _customerService;

        public ProductController(IContextProvider contextProvider, ICustomerService customerService)
        {
            _productServiceFactory = new ProductService(contextProvider);
            _contextProvider = contextProvider;
            _customerService = customerService;
        }
        public ProductController(IProductService productServiceFactory)
        {
            _productServiceFactory = productServiceFactory;
        }

        [HttpGet]
        [Route("details")]
        //[ResponseType(typeof(Product))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Product))]
        public IHttpActionResult GetProductById(int id, int companyId)
        {
            var product = _productServiceFactory.GetProductById(id, companyId);
            return Ok(product);
        }

        [HttpGet]
        [Route("{sku}")]
        //[ResponseType(typeof(Product))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Product))]
        public IHttpActionResult GetProductBySku(string sku)
        {
            var product = _productServiceFactory.GetProductBySku(sku);
            return Ok(product);
        }


        [HttpPost]
        [Route("Search")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ProductSearchResponse))]
        public IHttpActionResult SearchProduct([FromBody]ProductSearchItems searchItems)
        {
            if (searchItems == null)
                throw new Exception("Search Items required");

            searchItems.PageSize = WebConfigs.ProductsPageSize;

            var response = _productServiceFactory.SearchProduct(searchItems);
            response.TotalPages = (int)Math.Ceiling((double)response.TotalProducts / (int)searchItems.PageSize);
            return Ok(response);
        }

        [HttpGet]
        [Route("get_best_sellers")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<ProductSearchResult>))]
        public IHttpActionResult GetBestSellers(int companyID)
        {
            var response = _productServiceFactory.GetBestSellers(WebConfigs.BestSellersNumberOfItems, companyID);
            return Ok(response);
        }

        [HttpGet]
        [Route("feature_event/{featureEventId}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<ProductSearchResult>))]
        public IHttpActionResult SearchProduct(int featureEventId)
        {
            var product = _productServiceFactory.SearchProduct(featureEventId);
            return Ok(product);
        }

        [HttpPost]
        [Route("Filter")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<ProductSearchResult[]>))]
        public IHttpActionResult SearchProduct(ProductFilterModel model)
        {
            if (model.PageSize == 0)
            {
                model.PageSize = WebConfigs.ProductsPageSize;
            }
            return _productServiceFactory.SearchProduct(model).ToHttpActionResult(this);
        }
    }
}