﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Extensions;
using Swashbuckle.Swagger.Annotations;
using System.Threading.Tasks;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("orderBooking")]
    public class OrderBookingController : ApiController
    {
        private readonly IOrderBookingService _orderBookingService;

        public OrderBookingController(IOrderBookingService orderBookingService)
        {
            _orderBookingService = orderBookingService;
        }

        [Route("get_856Booking")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(byte[]))]
        [SwaggerResponse((HttpStatusCode)422, Type = typeof(ApiResult))]
        [SwaggerResponse(HttpStatusCode.Forbidden, Type = typeof(ApiResult))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ApiResult))]
        public IHttpActionResult Get856BookingFile(int orderSellerId)
        {
            var poNumber = string.Empty;
            var r = _orderBookingService.GetSellerOrderBookingDocument(orderSellerId, out poNumber);
            return r.ToHttpActionResult(this);
        }


        [Route("push_856Booking")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(byte[]))]
        [SwaggerResponse((HttpStatusCode)422, Type = typeof(ApiResult))]
        [SwaggerResponse(HttpStatusCode.Forbidden, Type = typeof(ApiResult))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ApiResult))]
        public IHttpActionResult Push856BookingFile(int orderSellerId)
        {
            return _orderBookingService.PushSellerOrderBookingDocument(orderSellerId).ToHttpActionResult(this);
        }

        [Route("sendEmail")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(byte[]))]
        [SwaggerResponse((HttpStatusCode)422, Type = typeof(ApiResult))]
        [SwaggerResponse(HttpStatusCode.Forbidden, Type = typeof(ApiResult))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ApiResult))]
        public async Task<IHttpActionResult> SendEmailAsync(int orderSellerId)
        {
            var result = await _orderBookingService.SendEmailAsync(orderSellerId);

            return result.ToHttpActionResult(this);
        }


    }
}
