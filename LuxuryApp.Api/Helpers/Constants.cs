﻿namespace LuxuryApp.Api.Helpers
{
    public static class Constants
    {
        #region EmailTemplates

        public static string RegistrationRequestEmailTemplate = "RegistrationRequest";
        public static string OfferImportReportErrorEmailTemplate = "OfferImportReportError";
        public static string OrderSubmitEmailTemplate = "OrderSubmitEmailTemplate";

        #endregion
    }
}