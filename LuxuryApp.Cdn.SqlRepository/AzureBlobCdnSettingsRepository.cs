﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using LuxuryApp.Cdn.CdnServers;
using LuxuryApp.Cdn.CdnServers.Model;

namespace LuxuryApp.Cdn.SqlRepository
{
    public class AzureBlobCdnSettingsRepository:
        CdnSettingsRepositoryBase<AzureBlobCdnSettings>
    {
        public AzureBlobCdnSettingsRepository(string connectionString): base(connectionString, CdnConfigurationType.AzureBlob) 
        {
        }

        public override int SaveCdnSettings(AzureBlobCdnSettings settings)
        {
            int result;

            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                result = conn.ExecuteScalar<int>("cdn.SaveConfigurationAzureBlob",
                    new
                    {
                        storage_connection_string = settings.StorageConnectionString,
                        container = settings.Container
                    },
                    commandType: CommandType.StoredProcedure);

                conn.Close();
            }

            return result;
        }

        public override AzureBlobCdnSettings GetCdnSettings(int configurationId)
        {
            AzureBlobCdnSettings result;
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                result = conn.Query<AzureBlobCdnSettings>(
                    "SELECT ConfigurationId = CdnConfigurationId, " +
                    "StorageConnectionString," +
                    "Container" +
                    "   FROM cdn.GetConfigurationAzureBlob(@cdn_configuration_id)",
                    new {cdn_configuration_id = configurationId}).SingleOrDefault();
                conn.Close();
            }
            return result;
        }
    }
}
