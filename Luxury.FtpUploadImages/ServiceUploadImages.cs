﻿using System.ServiceProcess;
using System.Threading;
using LuxuryApp.Contracts.BackgroudServices;

namespace Luxury.FtpUploadImages
{
    public partial class ServiceUploadImages : ServiceBase
    {
        private readonly IWorker _worker;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        public ServiceUploadImages(IWorker worker)
        {
            InitializeComponent();

            _worker = worker;
        }

        protected override void OnStart(string[] args)
        {
            _worker.Start(_cancellationTokenSource.Token);
        }

        protected override void OnStop()
        {
            _cancellationTokenSource.Cancel();
        }
    }
}
