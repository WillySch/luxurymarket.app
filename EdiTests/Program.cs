﻿using Edidev.FrameworkEDIx64;
using System;
using System.IO;
using System.Reflection;

namespace EdiTests
{
    class Program
    {
        static void Main(string[] args)  //E:\Mejix\APP\EdiTests\Resources\Icon997Test.txt
        {
            ediDocument oEdiDoc = null;
            ediDataSegment oSegment = null;
            string sSegmentID;
            string sLoopSection;
            int nArea;
            string sValue;
            string sQlfr;
            string sLoopQlfr = "";
            ediSchemas oSchemas = null;
            ediAcknowledgment oAck = null;



            string sPath = AppDomain.CurrentDomain.BaseDirectory;
          //  var filePath = "C:\\LuxuryMarket\\FTPOutbound\\856OH\\OH_ND_234_829044_NODelivery.txt";
            var filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Resources\\OH_NODelivery.txt");
           
            //instantiat edi document object
            ediDocument.Set(ref oEdiDoc, new ediDocument());    // oEdiDoc = new ediDocument();

            // Disabling the internal standard reference library to makes sure that 
            // FREDI uses only the SEF file provided
            //  ediSchemas.Set(ref oSchemas, (ediSchemas)oEdiDoc.GetSchemas());    //oSchemas = (ediSchemas) oEdiDoc.GetSchemas();
            // oSchemas.EnableStandardReference = false;

            // This makes certain that the EDI file must use the same version SEF file, otherwise
            // the process will stop.
            //  oSchemas.set_Option(SchemasOptionIDConstants.OptSchemas_VersionRestrict, 1);

            // By setting the cursor type to ForwardOnly, FREDI does not load the entire file into memory, which
            // improves performance when processing larger EDI files.
            oEdiDoc.CursorType = DocumentCursorTypeConstants.Cursor_ForwardOnly;

            oEdiDoc.LoadEdi(filePath);

            //GETS THE FIRST DATA SEGMENT
            ediDataSegment.Set(ref oSegment, oEdiDoc.FirstDataSegment);  //oSegment = (ediDataSegment) oEdiDoc.FirstDataSegment


            //LOOP THAT WILL TRAVERSE THRU EDI FILE FROM TOP TO BOTTOM
            while (oSegment != null)
            {
                //DATA SEGMENTS WILL BE IDENTIFIED BY THEIR ID, THE LOOP SECTION AND AREA
                //(OR TABLE) NUMBER THAT THEY ARE IN.
                sSegmentID = oSegment.ID;
                sLoopSection = oSegment.LoopSection;
                nArea = oSegment.Area;

                if (nArea == 0)
                {
                    if (sLoopSection == "")
                    {
                        if (sSegmentID == "ISA")
                        {
                            sValue = oSegment.get_DataElementValue(1, 0);     //Authorization Information Qualifier
                            sValue = oSegment.get_DataElementValue(2, 0);     //Authorization Information
                            sValue = oSegment.get_DataElementValue(3, 0);     //Security Information Qualifier
                            sValue = oSegment.get_DataElementValue(4, 0);     //Security Information
                            Console.WriteLine(oSegment.get_DataElement(5).Description + " = " + oSegment.get_DataElementValue(5, 0));     //Interchange ID Qualifier
                            Console.WriteLine(oSegment.get_DataElement(6).Description + " = " + oSegment.get_DataElementValue(6, 0));     //Interchange Sender ID
                            sValue = oSegment.get_DataElementValue(7, 0);     //Interchange ID Qualifier
                            sValue = oSegment.get_DataElementValue(8, 0);     //Interchange Receiver ID
                            sValue = oSegment.get_DataElementValue(9, 0);     //Interchange Date
                            sValue = oSegment.get_DataElementValue(10, 0);     //Interchange Time
                            sValue = oSegment.get_DataElementValue(11, 0);     //Interchange Control Standards Identifier
                            sValue = oSegment.get_DataElementValue(12, 0);     //Interchange Control Version Number
                            Console.WriteLine(oSegment.get_DataElement(6).Description + " = " + oSegment.get_DataElementValue(6, 0));     //Interchange Control Number
                            sValue = oSegment.get_DataElementValue(14, 0);     //Acknowledgment Requested
                            sValue = oSegment.get_DataElementValue(15, 0);     //Usage Indicator
                            sValue = oSegment.get_DataElementValue(16, 0);     //Component Element Separator
                        }
                        else if (sSegmentID == "GS")
                        {
                            sValue = oSegment.get_DataElementValue(1, 0);     //Functional Identifier Code
                            sValue = oSegment.get_DataElementValue(2, 0);     //Application Sender's Code
                            sValue = oSegment.get_DataElementValue(3, 0);     //Application Receiver's Code
                            sValue = oSegment.get_DataElementValue(4, 0);     //Date
                            sValue = oSegment.get_DataElementValue(5, 0);     //Time
                            Console.WriteLine(oSegment.get_DataElement(6).Description + " = " + oSegment.get_DataElementValue(6, 0));     //Group Control Number
                            sValue = oSegment.get_DataElementValue(7, 0);     //Responsible Agency Code
                            sValue = oSegment.get_DataElementValue(8, 0);     //Version / Release / Industry Identifier Code
                        }   //sSegmentID
                    }    //sLoopSection
                }
                else if (nArea == 1)
                {
                    if (sLoopSection == "")
                    {
                        if (sSegmentID == "ST")
                        {
                            sValue = oSegment.get_DataElementValue(1, 0);     //Transaction Set Identifier Code
                            Console.WriteLine(oSegment.get_DataElement(2).Description + " = " + oSegment.get_DataElementValue(2, 0));     //Transaction Set Control Number
                        }
                        else if (sSegmentID == "B3")
                        {
                            sValue = oSegment.get_DataElementValue(1, 0);     //Shipment Qualifier
                            Console.WriteLine(oSegment.get_DataElement(2).Description + " = " + oSegment.get_DataElementValue(2, 0));     //Invoice Number
                            sValue = oSegment.get_DataElementValue(3, 0);     //Shipment Identification Number
                            sValue = oSegment.get_DataElementValue(4, 0);     //Shipment Method of Payment
                            sValue = oSegment.get_DataElementValue(5, 0);     //Weight Unit Code
                            sValue = oSegment.get_DataElementValue(6, 0);     //Date
                            Console.WriteLine(oSegment.get_DataElement(7).Description + " = " + oSegment.get_DataElementValue(7, 0));     //Net Amount Due
                            sValue = oSegment.get_DataElementValue(8, 0);     //Correction Indicator
                            sValue = oSegment.get_DataElementValue(9, 0);     //Delivery Date
                            sValue = oSegment.get_DataElementValue(10, 0);     //Date/Time Qualifier
                            sValue = oSegment.get_DataElementValue(11, 0);     //Standard Carrier Alpha Code
                            sValue = oSegment.get_DataElementValue(12, 0);     //Date
                        }
                        else if (sSegmentID == "B2A")
                        {
                            sValue = oSegment.get_DataElementValue(1, 0);     //Transaction Set Purpose Code
                        }
                        else if (sSegmentID == "N9")
                        {
                            sQlfr = oSegment.get_DataElementValue(1, 0);     //Reference Identification Qualifier
                            if (sQlfr == "CN")    //Carrier's Reference Number
                            {
                                Console.WriteLine("Carrier's Reference Number = " + oSegment.get_DataElementValue(2, 0));     //Reference Identification
                            }
                        }
                        else if (sSegmentID == "G62")
                        {
                            sQlfr = oSegment.get_DataElementValue(1, 0);     //Date Qualifier
                            if (sQlfr == "03")    //Invoice Date
                            {
                                Console.WriteLine("Invoice Date = " + oSegment.get_DataElementValue(2, 0));     //Date
                            }
                            sValue = oSegment.get_DataElementValue(3, 0);     //Time Qualifier
                            sValue = oSegment.get_DataElementValue(4, 0);     //Time
                            sValue = oSegment.get_DataElementValue(5, 0);     //Time Code
                        }   //Segment ID
                    }
                    else if (sLoopSection == "N1")
                    {
                        //if loop has more that one instance, then you should check for the qualifier that differentiates the loop instances
                        if (sSegmentID == "N1")
                        {
                            sLoopQlfr = oSegment.get_DataElementValue(1, 0);   //In most cases the loop qualifier is the first element of the first segment in the loop, but not necessarily
                        }
                        if (sLoopQlfr == "SH") //SHIPPER INFORMATION
                        {
                            if (sSegmentID == "N1")
                            {
                                sValue = oSegment.get_DataElementValue(1, 0);     //Entity Identifier Code
                                Console.WriteLine("Shipper Name = " + oSegment.get_DataElementValue(2, 0));     //Name
                                sValue = oSegment.get_DataElementValue(3, 0);     //Identification Code Qualifier
                                Console.WriteLine("Shipper ID = " + oSegment.get_DataElementValue(4, 0));     //Identification Code
                            }
                            else if (sSegmentID == "N2")
                            {
                                sValue = oSegment.get_DataElementValue(1, 0);     //Name
                            }
                            else if (sSegmentID == "N3")
                            {
                                Console.WriteLine("Shipper Address = " + oSegment.get_DataElementValue(1, 0));     //Address Information
                            }
                            else if (sSegmentID == "N4")
                            {
                                Console.WriteLine("Shipper City = " + oSegment.get_DataElementValue(1, 0));     //City Name
                                Console.WriteLine("Shipper State = " + oSegment.get_DataElementValue(2, 0));     //State or Province Code
                                Console.WriteLine("Shipper Zip = " + oSegment.get_DataElementValue(3, 0));     //Postal Code
                            }   //Segment ID
                        }
                        else if (sLoopQlfr == "BT") //BILL-TO INFORMATION
                        {
                            if (sSegmentID == "N1")
                            {
                                sValue = oSegment.get_DataElementValue(1, 0);     //Entity Identifier Code
                                Console.WriteLine("Bill-To Name = " + oSegment.get_DataElementValue(2, 0));     //Name
                                sValue = oSegment.get_DataElementValue(3, 0);     //Identification Code Qualifier
                                Console.WriteLine("Bill-To ID = " + oSegment.get_DataElementValue(4, 0));     //Identification Code
                            }
                            else if (sSegmentID == "N2")
                            {
                                sValue = oSegment.get_DataElementValue(1, 0);     //Name
                            }
                            else if (sSegmentID == "N3")
                            {
                                Console.WriteLine("Bill-To Address = " + oSegment.get_DataElementValue(1, 0));     //Address Information
                            }
                            else if (sSegmentID == "N4")
                            {
                                Console.WriteLine("Bill-To City = " + oSegment.get_DataElementValue(1, 0));     //City Name
                                Console.WriteLine("Bill-To State = " + oSegment.get_DataElementValue(2, 0));     //State or Province Code
                                Console.WriteLine("Bill-To Zip = " + oSegment.get_DataElementValue(3, 0));     //Postal Code
                            }   //Segment ID
                        }

                    }
                    else if (sLoopSection == "N7")
                    {
                        if (sSegmentID == "N7")
                        {
                            sValue = oSegment.get_DataElementValue(1, 0);     //Equipment Initial
                            Console.WriteLine(oSegment.get_DataElement(2).Description + " = " + oSegment.get_DataElementValue(2, 0));     //Equipment Number
                            sValue = oSegment.get_DataElementValue(3, 0);     //Weight
                            sValue = oSegment.get_DataElementValue(4, 0);     //Weight Qualifier
                            sValue = oSegment.get_DataElementValue(5, 0);     //Tare Weight
                            sValue = oSegment.get_DataElementValue(6, 0);     //Weight Allowance
                            sValue = oSegment.get_DataElementValue(7, 0);     //Dunnage
                            sValue = oSegment.get_DataElementValue(8, 0);     //Volume
                            sValue = oSegment.get_DataElementValue(9, 0);     //Volume Unit Qualifier
                            sValue = oSegment.get_DataElementValue(10, 0);     //Ownership Code
                            sValue = oSegment.get_DataElementValue(11, 0);     //Equipment Description Code
                            sValue = oSegment.get_DataElementValue(12, 0);     //Standard Carrier Alpha Code
                            sValue = oSegment.get_DataElementValue(13, 0);     //Temperature Control
                            sValue = oSegment.get_DataElementValue(14, 0);     //Position
                            sValue = oSegment.get_DataElementValue(15, 0);     //Equipment Length
                            sValue = oSegment.get_DataElementValue(16, 0);     //Tare Qualifier Code
                            sValue = oSegment.get_DataElementValue(17, 0);     //Weight Unit Code
                            Console.WriteLine(oSegment.get_DataElement(18).Description + " = " + oSegment.get_DataElementValue(18, 0));     //Equipment Number Check Digit
                        }   //sSegmentID
                    }    //sLoopSection
                }
                else if (nArea == 2)
                {
                    if (sLoopSection == "LX")
                    {
                        if (sSegmentID == "LX")
                        {
                            sValue = oSegment.get_DataElementValue(1, 0);     //Assigned Number
                        }
                        else if (sSegmentID == "N9")
                        {
                            sQlfr = oSegment.get_DataElementValue(1, 0);     //Reference Identification Qualifier
                            if (sQlfr == "F9")    //Freight Payor Reference Number
                            {
                                Console.WriteLine("Freight Payor Reference Number = " + oSegment.get_DataElementValue(2, 0));     //Reference Identification
                            }
                        }
                        else if (sSegmentID == "L1")
                        {
                            sValue = oSegment.get_DataElementValue(1, 0);     //Lading Line Item Number
                            Console.WriteLine(oSegment.get_DataElement(2).Description + " = " + oSegment.get_DataElementValue(2, 0));     //Freight Rate
                            sValue = oSegment.get_DataElementValue(3, 0);     //Rate/Value Qualifier
                            sValue = oSegment.get_DataElementValue(4, 0);     //Charge
                            sValue = oSegment.get_DataElementValue(5, 0);     //Advances
                            sValue = oSegment.get_DataElementValue(6, 0);     //Prepaid Amount
                            sValue = oSegment.get_DataElementValue(7, 0);     //Rate Combination Point Code
                            sValue = oSegment.get_DataElementValue(8, 0);     //Special Charge or Allowance Code
                            sValue = oSegment.get_DataElementValue(9, 0);     //Rate Class Code
                            sValue = oSegment.get_DataElementValue(10, 0);     //Entitlement Code
                            sValue = oSegment.get_DataElementValue(11, 0);     //Charge Method of Payment
                            Console.WriteLine(oSegment.get_DataElement(12).Description + " = " + oSegment.get_DataElementValue(12, 0));     //Special Charge Description
                        }   //sSegmentID
                    }    //sLoopSection
                }
                else if (nArea == 3)
                {
                    if (sLoopSection == "")
                    {
                    }   //sLoopSection
                }   //nArea

                //GETS THE NEXT DATA SEGMENT
                ediDataSegment.Set(ref oSegment, (ediDataSegment)oSegment.Next());  //oSegment = (ediDataSegment) oSegment.Next();

            }   //while


            ////Iterate through all segments in the EDI fle so that component has a chance to validate them.
            while (oSegment != null)
            {
                //get next data segment
                ediDataSegment.Set(ref oSegment, (ediDataSegment)oSegment.Next());    //oSegment = (ediDataSegment) oSegment.Next();
            }


            // Gets the first segment of the 997 acknowledgment file
            ediDataSegment.Set(ref oSegment, (ediDataSegment)oAck.GetFirst997DataSegment());    //oSegment = (ediDataSegment) oAck.GetFirst997DataSegment();

            while (oSegment != null)
            {
                nArea = oSegment.Area;
                sLoopSection = oSegment.LoopSection;
                sSegmentID = oSegment.ID;

                if (nArea == 1)
                {
                    if (sLoopSection == "")
                    {
                        if (sSegmentID == "AK9")
                        {
                            if (oSegment.get_DataElementValue(1, 0) == "R")
                            {
                                // reject EDI file
                            }
                        }
                    }    // sLoopSection == ""
                }    //nArea == 1

                ediDataSegment.Set(ref oSegment, (ediDataSegment)oSegment.Next());    //oSegment = (ediDataSegment) oSegment.Next();
            }    //oSegment != null

        }
    }
}
