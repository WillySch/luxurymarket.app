﻿using System;
using System.Collections.Generic;
using Gs1.Vics.Contracts.Factories;
using Gs1.Vics.Contracts.Services;
using Gs1.Vics.Implementations.Services;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.Infrastructure.Logging;

namespace Gs1.Vics.Implementations.Factories
{
    public class X2EdiDeserializeServiceFactory : IEdiDeserializeServiceFactory
    {
        private readonly Dictionary<Type, object> _instances;
        private readonly IEdiParseService _parserService;
        private readonly ILogger _logger;

        public X2EdiDeserializeServiceFactory(IEdiParseService parserService, ILogger logger)
        {
            _logger = logger;
            _parserService = parserService;
            _instances = new Dictionary<Type, object>()
            {
                { typeof(EdiExtracted865Data), new X2Edi856DeserializeService(_parserService)},
                { typeof(EdiExtractedACK997), new X2Edi997DeserializeService(_parserService,logger)},
            };
        }

        public IEdiDeserializeService<T> GetDeserializeService<T>() where T : EdiExtractedData
        {
            return _instances[typeof(T)] as IEdiDeserializeService<T>;
        }

        public IEnumerable<T> Deserialize<T>(string filePath) where T : EdiExtractedData
        {
            return GetDeserializeService<T>().Deserialize(filePath);
        }
    }
}
