﻿using Gs1.Vics.Contracts.Models;
using System.Collections.Generic;
using System.Linq;

namespace Gs1.Vics.Implementations.EdiExtractors
{
    public static class EdiExtractorHelper
    {
        public static List<EdiSegmentHierarchy> GetHierachyItemsByCode(List<EdiSegmentHierarchy> items, string loopCode, int? parentLoopInstance = null)
        {
            if (items?.Any() == false) return null;

            var resultItems = items.Where(x => x.LoopCode?.ToLower() == loopCode?.ToLower()
                                              && (parentLoopInstance == null || x.ParentLoopInstance== parentLoopInstance))                
                                    .Select(x => x).ToList();

            return resultItems;
        }

        public static List<EdiSegmentHierarchy> GetOrderHierachySegments(List<EdiSegmentHierarchy> items)
        {
            var resultHierarchySegments = GetHierachyItemsByCode(items,"O");

            return resultHierarchySegments;
        }

        public static List<EdiSegmentHierarchy> GetOrderProductItems(this List<EdiSegmentHierarchy> items,int parentOrderInstance)
        {
           var resultHierarchySegments = GetHierachyItemsByCode(items, "I", parentOrderInstance);

            return resultHierarchySegments;
        }

        public static List<EdiSegmentHierarchy> GetShippingHierachySegments(List<EdiSegmentHierarchy> items)
        {
            var resultHierarchySegments = GetHierachyItemsByCode(items, "S");

            return resultHierarchySegments;
        }

        public static string GetValueSegmentIdAndSubItemId( this List<EdiSegmentItem> items,string segmentId,string id)
        {
            if (items?.Any() == false) return null;

            var result = items?.Where(x => x.SegmentID == segmentId).SelectMany(x => x.Items.Where(y => y.ID?.ToLower() == id?.ToLower()))
                                       .Select(x => x.Value).FirstOrDefault()??string.Empty;

            return result;
        }

        public static string GetValueSegmentIdAndPosition(this List<EdiSegmentItem> items, string segmentId, int pos)
        {
            if (items?.Any() == false) return null;

            var result = items?.Where(x => x.SegmentID == segmentId).SelectMany(x => x.Items.Where(y => y.Position== pos))
                            .Select(x => x.Value).FirstOrDefault() ?? string.Empty;

            return result;
        }
    }
}
