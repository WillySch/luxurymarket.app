﻿using Gs1.Vics.Contracts.Models;
using LuxuryApp.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gs1.Vics.Implementations.EdiExtractors
{
    public class Edi997Extractor
    {
        public EdiExtractedACK997 ExtractData(EdiFileSegments documentSegments)
        {
            if (documentSegments?.HierarchicalItems?.Any() == false && documentSegments?.SimpleItems?.Any() == false) return null;

            var hierarchicalSegments = documentSegments.HierarchicalItems ?? new List<EdiSegmentHierarchy>();
            var simpleItems = documentSegments.SimpleItems ?? new List<EdiSegmentItem>();

            var returnData = new EdiExtractedACK997();
            var itemsAK1 = simpleItems.Where(x => x.SegmentID == "AK1").FirstOrDefault()?.Items;
            var itemsAK5 = simpleItems.Where(x => x.SegmentID == "AK5").Select(x => x.Items.FirstOrDefault()).Select(x => x.Value).FirstOrDefault();
            var itemsAk9 = simpleItems.Where(x => x.SegmentID == "AK9").Select(x => x.Items.FirstOrDefault()).Select(x => x.Value).FirstOrDefault();
            var itemsISA = simpleItems.Where(x => x.SegmentID == "ISA").Select(x => x.Items).ToList();
            if (itemsISA?.Any() == true && itemsISA.Count() > 10)
            {
                returnData.InterchangeDateTime = Convert.ToDateTime(itemsISA[9]);//.{ itemsISA[11]}
                var time = Convert.ToDateTime(itemsISA[11]);
            }

            returnData.FunctionalIdentifierCode = itemsAK1.Count >= 2 ? itemsAK1[2].Value : null;
            returnData.PONumber = itemsAK1.Count >= 1 ? itemsAK1[1].Value : null;
            returnData.TransactionSetAcknowledgmentCode = itemsAK5;
            returnData.FunctionalGroupAcknowledgeCode = itemsAk9;

            return returnData;
        }

    }
}
