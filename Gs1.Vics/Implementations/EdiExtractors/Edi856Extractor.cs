﻿using Gs1.Vics.Contracts.Models;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gs1.Vics.Implementations.EdiExtractors
{
    public class Edi856Extractor
    {
        public List<EdiExtracted865Data> ExtractData(EdiFileSegments documentSegments)
        {
            if (documentSegments?.HierarchicalItems?.Any() == false && documentSegments?.SimpleItems?.Any() == false) return null;

            var hierarchicalSegments = documentSegments.HierarchicalItems ?? new List<EdiSegmentHierarchy>();
            var simpleItems = documentSegments.SimpleItems ?? new List<EdiSegmentItem>();

            DateTime? interchangedateTime = null;

            var isaItems = simpleItems.Where(x => x.SegmentID == "ISA").SelectMany(X => X.Items).ToList();
            var interchangeDate = isaItems.Where(x => x.Position == 9).FirstOrDefault().Value;
            var interchangeTime = isaItems.Where(x => x.Position == 10).FirstOrDefault().Value;

            interchangedateTime = interchangeDate?.ConvertToDateTime(interchangeTime);

            var returnData = new List<EdiExtracted865Data>();

            DateTime? actualDepartureDate = null; //370   --asn no ata
            DateTime? estimatedArrivalDate = null; //371  --asn  no ata
            DateTime? actualArrivalDate = null; //178  --asn with ata
            DateTime? carrierPickupDate = null; //050  --oh
            DateTime? deliveryToCustomer = null; //9  --oh with delivery

            var shippingSegment = EdiExtractorHelper.GetShippingHierachySegments(hierarchicalSegments).FirstOrDefault();
            var dtmSegments = shippingSegment.Items.Where(x => x.SegmentID == "DTM").Select(x => x).ToList();
            foreach (var dtmSegment in dtmSegments)
            {
                string value;
                var items = dtmSegment.Items;
                if (items.Where(x => x.Position == 1 && x.Value == "9").Any())
                {
                    value = items.Where(x => x.Position == 2).Select(x => x).FirstOrDefault()?.Value;
                    deliveryToCustomer = value?.ConvertToDateTime();
                }
                else if (items.Where(x => x.Position == 1 && x.Value == "370").Any())
                {
                    value = items.Where(x => x.Position == 2).Select(x => x).FirstOrDefault()?.Value;
                    actualDepartureDate = value?.ConvertToDateTime();
                }
                else if (items.Where(x => x.Position == 1 && x.Value == "178").Any())
                {
                    value = items.Where(x => x.Position == 2).Select(x => x).FirstOrDefault()?.Value;
                    actualArrivalDate = value?.ConvertToDateTime();
                }
                else if (items.Where(x => x.Position == 1 && x.Value == "371").Any())
                {
                    value = items.Where(x => x.Position == 2).Select(x => x).FirstOrDefault()?.Value;
                   estimatedArrivalDate = value?.ConvertToDateTime();
                }
                else if (items.Where(x => x.Position == 1 && x.Value == "050").Any())
                {
                    value = items.Where(x => x.Position == 2).Select(x => x).FirstOrDefault()?.Value;
                    carrierPickupDate = value?.ConvertToDateTime();
                }
            }

            var orderSegments = EdiExtractorHelper.GetOrderHierachySegments(hierarchicalSegments);
            if (orderSegments?.Any() == false)
            {
                throw new Exception("No orders found!");
            }
            foreach (var orderSegment in orderSegments)
            {
                var ponumber = orderSegment.Items.GetValueSegmentIdAndSubItemId("PRF", "324");
                var item = new EdiExtracted865Data()
                {
                    ActualDepartureDate = actualDepartureDate,
                    ActualArrivalDate = actualArrivalDate,
                    CarrierPickupDate = carrierPickupDate,
                    DeliveryToCustomer = deliveryToCustomer,
                    PONumber = ponumber,
                    EstimatedArrivalDate = estimatedArrivalDate,
                    InterchangeDateTime = interchangedateTime
                };

                item.Products = GetOrderProductItems(hierarchicalSegments, orderSegment.LoopInstance, item.PONumber);
                returnData.Add(item);
            }

            return returnData;
        }

        public List<Edi865Product> GetOrderProductItems(List<EdiSegmentHierarchy> items, int parentOrderInstance, string PONumber)
        {
            var result = new List<Edi865Product>();

            var productHierarchies = items.GetOrderProductItems(parentOrderInstance);

            if (productHierarchies?.Any() == false) return result;
            var productIndex = 0;
            foreach (var productHierarchy in productHierarchies)
            {
                productIndex++;

                var quantity = productHierarchy.Items?.GetValueSegmentIdAndSubItemId("SN1", "382");
                quantity = quantity.Remove(quantity.IndexOf(","));
                var productItem = new Edi865Product()
                {
                    PONumber = PONumber,
                    SKU = productHierarchy.Items?.GetValueSegmentIdAndSubItemId("LIN", "234"),  //3
                    Size = productHierarchy.Items?.GetValueSegmentIdAndPosition("LIN", 11), //LIN10
                    Quantity = quantity.TryParse<int>() ?? null,
                    ProductIndex = productIndex
                };

                //move to another method
                var measureItems = productHierarchy.Items.Where(x => x.SegmentID == "MEA").Select(x => x).ToList();
                foreach (var measureItem in measureItems)
                {
                    var measurementQualifer = measureItem.Items.Where(x => x.Position == 2).Select(x => x).FirstOrDefault()?.Value ?? string.Empty;
                    var measurementCode = measureItem.Items.Where(x => x.Position == 4).Select(x => x).FirstOrDefault()?.Value;
                    var measurementValue = measureItem.Items.Where(x => x.Position == 3).Select(x => x).FirstOrDefault()?.Value;

                    if (string.IsNullOrWhiteSpace(measurementCode))
                        measurementCode = "ca";

                    var value = measurementValue?.TryParse<decimal>();
                    switch (measurementCode.ToLower())
                    {
                        case "ca":
                            //cases
                            productItem.NumberCases = value;
                            break;
                        case "kg":
                            //Weights
                            break;
                        case "cr"://Cubic Meter
                            productItem.Volume = value;
                            break;
                    }
                }
                result.Add(productItem);
            }
            return result;
        }

    }
}
