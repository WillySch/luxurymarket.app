﻿using Edidev.FrameworkEDIx64;
using Gs1.Vics.Contracts.Models;
using Gs1.Vics.Contracts.Services;
using LuxuryApp.Utils.Helpers;
using System.Collections.Generic;

namespace Gs1.Vics.Implementations.Services
{
    public class X12EdiParseService : IEdiParseService
    {
        public EdiFileSegments Parse(string filePath)
        {
            ediDocument oEdiDoc = null;
            ediDataSegment oSegment = null;

            //instantiate edi document object
            ediDocument.Set(ref oEdiDoc, new ediDocument());    // oEdiDoc = new ediDocument();

            oEdiDoc.CursorType = DocumentCursorTypeConstants.Cursor_ForwardOnly;

            oEdiDoc.LoadEdi(filePath);

            //GETS THE FIRST DATA SEGMENT
            ediDataSegment.Set(ref oSegment, oEdiDoc.FirstDataSegment);  //oSegment = (ediDataSegment) oEdiDoc.FirstDataSegment

            var hierarchicalSegments = new List<EdiSegmentHierarchy>();
            var segments = new List<EdiSegmentItem>();
            var area = 0;
            EdiSegmentHierarchy segmentHl = null;
            var parentOrderId = 0;

            while (oSegment != null)
            {
                area = oSegment.Area;
                if (oSegment.ID == "HL")
                {
                    if (segmentHl != null)
                        hierarchicalSegments.Add(segmentHl);
                    segmentHl = new EdiSegmentHierarchy()
                    {
                        Area = oSegment.Area,
                        Loop = oSegment.LoopSection,
                        LoopLevel = oSegment.LoopLevel,
                        PositionInSet = oSegment.PositionInSet,
                        LoopInstance = oSegment.LoopInstance,
                        LoopCode = oSegment.get_DataElementValue(3),
                        Items = new List<EdiSegmentItem>()
                    };

                    if (segmentHl.LoopCode == "O")
                        parentOrderId = segmentHl.LoopInstance;

                    if (segmentHl.LoopCode == "I") segmentHl.ParentLoopInstance = parentOrderId;
                    else
                    {
                        segmentHl.ParentLoopInstance = oSegment.get_DataElementValue(2)?.Trim().TryParse<int>() ?? 0;
                    }
                }
                else
                {
                    var segment = ProcessEdiSegment(oSegment);
                    if (segment.Loop.Contains("HL") && segmentHl != null)
                    {
                        segmentHl.Items.Add(segment);
                    }
                    else
                    {
                        segments.Add(segment);
                        if (segmentHl != null)
                        {
                            hierarchicalSegments.Add(segmentHl);
                            segmentHl = null;
                        }
                    }
                }
                ediDataSegment.Set(ref oSegment, oSegment.Next());
            }   //while

            return new EdiFileSegments() { HierarchicalItems = hierarchicalSegments, SimpleItems = segments };
        }

        private EdiSegmentItem ProcessEdiSegment(ediDataSegment oSegment)
        {
            var segment = new EdiSegmentItem()
            {
                Area = oSegment.Area,
                Loop = oSegment.LoopSection,
                LoopLevel = oSegment.LoopLevel,
                Items = new List<EdiSegmentItemValue>(),
                SegmentID = oSegment.ID,
                PositionInSet = oSegment.PositionInSet,
                LoopInstance = oSegment.LoopInstance,
                CurrentLoop = oSegment.CurrentLoop
            };

            var count = oSegment.Count;
            for (var pos = 1; pos <= count; pos++)
            {
                var element = oSegment.get_DataElement(pos);
                if (element == null)
                    continue;
                segment.Items.Add(new EdiSegmentItemValue
                {
                    Value = element.Value,
                    Position = pos,
                    Description = element.Description,
                    ID = element.ID
                });
            }
            return segment;
        }

    }

}
