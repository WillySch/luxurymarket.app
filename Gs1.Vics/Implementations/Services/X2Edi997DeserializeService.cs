﻿using LuxuryApp.Contracts.Models;
using System.Collections.Generic;
using Gs1.Vics.Contracts.Services;
using System;
using LuxuryApp.Utils.Helpers;
using LuxuryApp.Core.Infrastructure.Logging;

namespace Gs1.Vics.Implementations.Services
{
    public class X2Edi997DeserializeService : IEdiDeserializeService<EdiExtractedACK997>
    {
        private readonly IEdiParseService _parserService;
        private readonly ILogger _logger;

        public X2Edi997DeserializeService(IEdiParseService parserService, ILogger logger)
        {
            _parserService = parserService;
            _logger = logger;
        }

        public IEnumerable<EdiExtractedACK997> Deserialize(string filePath)
        {
            var result = GetFileContent(filePath);
            var list = new List<EdiExtractedACK997> { result };
            return list;
        }

        private EdiExtractedACK997 GetFileContent(string filePath)
        {
            var ack997Model = new EdiExtractedACK997();
            string line;

            var file = new System.IO.StreamReader(filePath);
            while ((line = file.ReadLine()) != null)
            {
                var lineSegments = line.Replace("~","").Replace("/", "").Split('*');
                try
                {
                    if (line.StartsWith("ISA"))
                    {
                        var interchangeDate = lineSegments[6];
                        var interchangeTime = lineSegments[7];

                        ack997Model.InterchangeDateTime = interchangeDate.ConvertToDateTime(interchangeTime);

                    }
                    else if (line.StartsWith("AK1"))
                    {
                        ack997Model.FunctionalIdentifierCode = lineSegments[1];
                        ack997Model.PONumber = lineSegments[2];
                    }
                    else if (line.StartsWith("AK2"))
                    {
                        ack997Model.TransactionSetIdentifierCode = lineSegments[1];
                    }
                    else if (line.StartsWith("AK5"))
                    {
                        ack997Model.FunctionalGroupAcknowledgeCode = lineSegments[1];
                    }
                    else if (line.StartsWith("AK9"))
                    {
                        ack997Model.TransactionSetAcknowledgmentCode = lineSegments[1];
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error($"X2Edi997DeserializeService-GetFileContent :{ex.ToString()}");
                }
            }

            return ack997Model;
        }
    }
}
