﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Enums;
using Gs1.Vics.Implementations.EdiExtractors;
using System.Collections.Generic;
using System;
using Gs1.Vics.Contracts.Services;

namespace Gs1.Vics.Implementations.Services
{
    public class X2Edi856DeserializeService : IEdiDeserializeService<EdiExtracted865Data>
    {
        private readonly IEdiParseService _parserService;

        public X2Edi856DeserializeService(IEdiParseService parserService)
        {
            _parserService = parserService;
        }

        public IEnumerable<EdiExtracted865Data> Deserialize(string filePath)
        {
            var documentAreas = _parserService.Parse(filePath);
            var result = new Edi856Extractor().ExtractData(documentAreas); ;
            return result;
        }
    }
}
