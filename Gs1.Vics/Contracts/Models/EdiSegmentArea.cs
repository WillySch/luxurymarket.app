﻿using System;
using System.Collections.Generic;

namespace Gs1.Vics.Contracts.Models
{
    public class EdiSegmentArea
    {
        public int Area { get; set; }

        public List<EdiSegmentItem> Items { get; set; }

        public List<EdiSegmentHierarchy> HierarchicalItems { get; set; }

    }

    public class EdiFileSegments
    {
        public List<EdiSegmentItem> SimpleItems { get; set; }

        public List<EdiSegmentHierarchy> HierarchicalItems { get; set; }

    }

    public class EdiSegmentHierarchy
    {
        public int Area { get; set; }

        public string Loop { get; set; }

        public int LoopLevel { get; set; }

        public string LoopCode { get; set; }

        public int LoopInstance { get; set; }

        public int ParentLoopInstance { get; set; }

        public List<EdiSegmentItem> Items { get; set; }

        public int PositionInSet { get; set; }


    }

    public class EdiSegmentItem
    {
        public int Area { get; set; }

        public string Loop { get; set; }

        public int LoopLevel { get; set; }

        public int LoopInstance { get; set; }

        public string LoopCode { get; set; }

        public List<EdiSegmentItemValue> Items { get; set; }

        public int PositionInSet { get; set; }

        public string SegmentID { get; set; }

        public string CurrentLoop { get; set; }
    }

    public class EdiSegmentItemValue
    {
        public string ID { get; set; }

        public int Position { get; set; }

        public string Description { get; set; }

        public string Value { get; set; }

       //public int RepeatCount { get; set; }

        //public int RepetNumber { get; set; }
    }
}
