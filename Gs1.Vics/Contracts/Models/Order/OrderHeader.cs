﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gs1.Vics.Contracts.Models.Common;

namespace Gs1.Vics.Contracts.Models.Order
{
    public class OrderHeader: 
        IInterchangeHeader,
        IGsFunctionalGroupHeader,
        IStTransactionSetHeader
    {
        public string InterchangeSenderId { get; set; }
        public string InterchangeReceiverId { get; set; }
        public DateTimeOffset InterchangeDateTime { get; set; }
        public string InterchangeControlVersionNumber { get; set; }
        public int InterchangeControlNumber { get; set; }
        public TestIndicator TestIndicator { get; set; }
        public string DocumentId => "PO";
        public string ApplicationSenderCode { get; set; }
        public string ApplicationReceiverCode { get; set; }
        public DateTimeOffset GroupDateTime { get; set; }
        public int GroupControlNumber => InterchangeControlNumber;
        public string Version => "004050VICS";
        public string TransactionType => "850";
        public string TransactionSetControlNumber { get; set; }
        public string ResponsibleAgencyCode => "X";   //Accredited Standard Committee X12

        public string PoNumber { get; set; }
        public DateTimeOffset PurchaseOrderDate { get; set; }
        public string VendorOrSupplierSiteNumber { get; set; }
        public string DepartmentNumber { get; set; }
        public string ManufacturerNumber { get; set; }

        public TermsOfSale TermsOfSale { get; set; }

        public int TermsNetDays { get; set; }
        public DateTimeOffset RequestedShipDate { get; set; }
        public DateTimeOffset CancelAfterDate { get; set; }
        //public string ShipTo { get; set; }
        //public string IdentificationCodeQualifier { get; set; }

        public char ShipmentMethod { get; set; }

        public string SupplierName { get; set; }
        public string SupplierIdentificationCode { get; set; }
        public string SupplierAddressInformation { get; set; }
        public string SupplierCityName { get; set; }
        public string SupplierStateOrProvinceCode { get; set; }
        public string SupplierPostalCode { get;set; }
        public string SupplierCountry { get; set; }

        public string ShipToDestinationName { get; set; }
        public string ShipToIdentificationCode { get; set; }
        public string ShipToAddressInformation { get; set; }
        public string ShipToCityName { get; set; }
        public string ShipToStateOrProvinceCode { get; set; }
        public string ShipToPostalCode { get; set; }
        public string ShipToCountryCode { get; set; }

        public OrderHeader()
        {
        }
    }
}