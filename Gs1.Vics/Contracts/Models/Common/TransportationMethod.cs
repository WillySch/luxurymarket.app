﻿namespace Gs1.Vics.Contracts.Models.Common
{
    public enum TransportationMethod
    {
        Air,
        Motor,
        Ocean
    }
}