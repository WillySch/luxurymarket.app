﻿using Gs1.Vics.Contracts.Models.Common;
using System;

namespace Gs1.Vics.Contracts.Models.Booking
{
    public class BookingOrderLevel
    {
        public int HierarchicalIdNumber { get; set; }
        public int HierarchicalParentIdNumber { get; set; }
        public string PONumber { get; set; }
        public string VendorBookingNumber { get; set; }

        public BookingItemLevel[] ItemDetails { get; set; }
    }
}