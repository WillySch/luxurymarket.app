﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using System.Collections.Generic;

namespace Gs1.Vics.Contracts.Services
{
    public interface IEdiDeserializeService<out T> where T: EdiExtractedData
    {
        IEnumerable<T> Deserialize(string filePath);
    }
}
