﻿using Gs1.Vics.Contracts.Models;

namespace Gs1.Vics.Contracts.Services
{
    public interface IEdiParseService
    {
        EdiFileSegments Parse(string filePath);
    }
}
