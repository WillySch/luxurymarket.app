﻿using Gs1.Vics.Contracts.Services;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using System.Collections.Generic;

namespace Gs1.Vics.Contracts.Factories
{
    public interface IEdiDeserializeServiceFactory
    {
        IEnumerable<T> Deserialize<T>(string filePath) where T : EdiExtractedData;

        IEdiDeserializeService<T> GetDeserializeService<T>() where T : EdiExtractedData;
     }
}
