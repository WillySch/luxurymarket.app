﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using SeleniumAppTests.PageObjects.authenticated;
using SeleniumAppTests.PageObjects.authenticated.home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authentication
{
    public class NewAccSetPasswordPage : BasePage
    {
        #region Web Element Identifiers

        private String _passwordID = "password";
        private String _confirmpassXpath = "/html/body/ui-view/ui-view/div/div[2]/div[2]/form/div[3]/input";
        private String _submitID = "submitSetupAccount";
        private String _setupPassFormID = "setupPasswordForm";
        private String _browseFileClass = "drag-and-drop-zone";
        private String _userMenuID = "usermenu";
        private String _termspopupClass = "modal-content";
        private String _acceptedTermsXpath = ".//div[contains(@class, 'col-md-12')]//..//button[contains(@class, 'btn-lm')]";
        private String _logOutBtnClass = "logout";

        #endregion

        #region

        IWebElement pass;
        IWebElement confirmpass;
        IWebElement submit;

        #endregion

        public NewAccSetPasswordPage(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_setupPassFormID)));
            initWebElements();
        }

        public void setNewPassword()
        {
            WebDriverHelper.Driver.FindElementById(_passwordID).SendKeys("luxury!");
        }

        public void setConfirmPass()
        {
            WebDriverHelper.Driver.FindElementByXPath(_confirmpassXpath).SendKeys("luxury!");
        }

        public BasePage submitNewAccount(LoginAccountTypes accType)
        {
            WebDriverHelper.Driver.FindElementById(_submitID).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_acceptedTermsXpath)));

            WebDriverHelper.Driver.FindElementByXPath(_acceptedTermsXpath).Click();


            if (accType == LoginAccountTypes.Buyer)
            {
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath(_acceptedTermsXpath)));

                WebDriverHelper.Driver.FindElementById(_userMenuID).Click();
                WebDriverHelper.Driver.FindElementByLinkText("Sign out").Click();

                return new LoginPage(WebDriverHelper.Driver);
            }
            else
            {
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath(_acceptedTermsXpath)));

                WebDriverHelper.Driver.FindElementByClassName(_logOutBtnClass).Click();

                return new LoginPage(WebDriverHelper.Driver);
            }
        }

        public void initWebElements()
        {
            pass = WebDriverHelper.Driver.FindElementById(_passwordID);
            confirmpass = WebDriverHelper.Driver.FindElementByXPath(_confirmpassXpath);
            submit = WebDriverHelper.Driver.FindElementById(_submitID);
        }
    }
}
