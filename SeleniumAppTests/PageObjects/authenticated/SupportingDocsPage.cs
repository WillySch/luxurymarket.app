﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SeleniumAppTests.PageObjects.authenticated.home
{
    public class SupportingDocsPage
    {
        #region Web Element Identifiers

        private String _browseFileClass = "drag-and-drop-zone";
        private String _suportDocTermsClass = "terms-conditions";
        private String _continueBtnXpath = "//*[contains(text(), 'Continue')]";
        private String _publishBtnXpath = "/html/body/ui-view/div[2]/div[2]/ui-view/ui-view/div/div[2]/div[2]/form/div/button";

        #endregion

        #region Web Elements

        private IWebElement continuebtn;

        #endregion

        public SupportingDocsPage()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_browseFileClass)));
            InitWebElements();
        }

        public void supportUpload()
        {
            WebDriverHelper.Driver.FindElementByClassName(_browseFileClass).Click();
            Thread.Sleep(1000);
            SendKeys.SendWait(AppConfig.uploadSupportDoc);
            Thread.Sleep(1000);
            SendKeys.SendWait(@"{Enter}");

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(_continueBtnXpath)));

            //WebDriverHelper.Driver.FindElementByClassName(_suportDocTermsClass).Click();
            WebDriverHelper.Driver.FindElementByXPath(_continueBtnXpath).Click();
        }

        private void InitWebElements()
        {
            continuebtn = WebDriverHelper.Driver.FindElementByXPath(_continueBtnXpath);
        }
    }
}
