﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class CartShipmentPage
    {
        #region Web Element Identifiers

        private String _continueBtnClass = "checkout-btn";
        private String _paymentDropdownClass = "btn-lm-payment";

        #endregion


        #region Web Elements

        IWebElement continuecheckout;

        #endregion

        public CartShipmentPage()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_continueBtnClass)));
            InitWebElements();
        }

        public CartCheckoutPage continueToCheckout()
        {
            WebDriverHelper.Driver.FindElementByClassName(_continueBtnClass).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_paymentDropdownClass)));

            return new CartCheckoutPage();
        }

        public void InitWebElements()
        {
            continuecheckout = WebDriverHelper.Driver.FindElementByClassName(_continueBtnClass);
        }
    }
}
