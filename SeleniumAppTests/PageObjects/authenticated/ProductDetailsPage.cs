﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class ProductDetailsPage
    {
        #region Web Element Identifiers

        private String _buyNowBtnId = "dLabel";
        private String _continueShoppingXpath = "/html/body/ui-view/ui-view/div[2]/div[2]/div/div[2]/div[3]/div/div[2]/div[2]/button";
        private String _addToLineXpath = "//*[contains(text(), 'Add to Linesheet')]";
        private String _productImageXpath = "/html/body/ui-view/ui-view/div[2]/div[2]/div/div[1]/div[1]/div/img";
        private String _sizesListXpath = ".//button[contains(@aria-expanded, 'true')]//..//ul[contains(@class, 'qty-sizes-menu')]";
        private String _addSizeXpath = ".//button[contains(@aria-expanded, 'true')]//..//ul[contains(@class, 'dropdown-menu')]//.//i[contains(@class, 'fa-plus-square-o')]";
        private String _addToLineSheetXpath = ".//button[contains(@aria-expanded, 'true')]//..//ul[contains(@class, 'dropdown-menu')]//..//.[contains(@class, 'btn-lm-add')]";
        private String _cartItemsClass = "items-cart";
        private String _cartItemsTableClass = "cart-items-table";
        private String _productNameClass = "product-subtitle";
        private String _brandNameXpath = ".//button[contains(@aria-expanded, 'true')]//..//ul//.[contains(@class, 'dropdown-title')]";
        private String _productPriceXpath = ".//button[contains(@aria-expanded, 'true')]//..//..//.[contains(text(), '$')]";
        private String _allProductsClass = "/html/body/ui-view/ui-view/div[2]/div[2]/div";
        private String _waitSizeList = ".//button[contains(@aria-expanded, 'true')]//..//ul[contains(@class,'qty-sizes-menu')]";

        #endregion

        #region Web Elements

        private IWebElement buy;
        private IWebElement continueshopping;
        private IWebElement cartitems;
        
        #endregion

        public ProductDetailsPage()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_productImageXpath)));
            InitWebElements();
        }

        public void clickBuyBtn(out string getprodbrand)
        {
            IWebElement productsList = WebDriverHelper.Driver.FindElementByXPath(_allProductsClass);
            List<IWebElement> buyBtn = productsList.FindElements(By.Id(_buyNowBtnId)).ToList();

            if (buyBtn.Count()>0)
            {
                Random pickItem = new Random();
                int randomitem = pickItem.Next(buyBtn.Count - 1);

                buyBtn[randomitem].Click();

                var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_waitSizeList)));
            }

            getprodbrand = WebDriverHelper.Driver.FindElementByXPath(_brandNameXpath).Text.ToString();
        }

        public void addSizeToCart(out double getprodprice)
        {
            //IWebElement sizeList = WebDriverHelper.Driver.FindElementByXPath(_sizesListXpath);
            List<IWebElement> addBtn = WebDriverHelper.Driver.FindElements(By.XPath(_addSizeXpath)).ToList();

            if (addBtn.Count()>0)
            {
                Random pickSize = new Random();
                int randomsize = pickSize.Next(addBtn.Count -1);

                addBtn[randomsize].Click();
            }

            //else
            //{
            //    WebDriverHelper.Driver.FindElementByXPath(_addSizeXpath).Click();
            //}

            getprodprice = double.Parse(WebDriverHelper.Driver.FindElementByXPath(_productPriceXpath).Text.Replace("$", ""));
        }

        public void addToLineSheet()
        {
            WebDriverHelper.Driver.FindElementByXPath(_addToLineSheetXpath).Click(); 
        }

        public CartSelectionPage goToCart()
        {
            WebDriverHelper.Driver.FindElementByClassName(_cartItemsClass).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_cartItemsTableClass)));       

            return new CartSelectionPage();
        }

        public void getProductName(out string getprodname)
        {
           getprodname =  WebDriverHelper.Driver.FindElementByClassName(_productNameClass).Text.ToString();
        }

        public void getProductBrand(out string getprodbrand)
        {
            getprodbrand = WebDriverHelper.Driver.FindElementByXPath(_brandNameXpath).Text.ToString();
        }

        public void getProductPrice(out double getprodprice)
        {
            getprodprice = double.Parse(WebDriverHelper.Driver.FindElementByXPath(_productPriceXpath).Text.Replace("$", ""));
        }

        public void InitWebElements()
        {
            buy = WebDriverHelper.Driver.FindElementById(_buyNowBtnId);
            continueshopping = WebDriverHelper.Driver.FindElementByXPath(_continueShoppingXpath);
            cartitems = WebDriverHelper.Driver.FindElementByClassName(_cartItemsClass);
        }
    }
}
