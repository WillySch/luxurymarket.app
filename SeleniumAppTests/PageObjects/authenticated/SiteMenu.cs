﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{

    public enum SiteMenuPages
    {
        [Description("FEATURED")]
        Featured,

        [Description("WOMEN'S APPAREL")]
        Womens,

        [Description("HANDBAGS / ACCESORIES")]
        Handbags,

        [Description("MEN'S SHOP")]
        Mens,

        [Description("BRANDS")]
        Brands
    }

    public class SiteMenu
    {
        #region Web Element Identifiers

        private String _userSiteMenuID = "sitemenu";
        private String _chooseBrandXpath = "//*[contains(text(), 'qa brand')]";

        #endregion

        #region

        private IWebElement siteMenu;

        #endregion

        public SiteMenu(RemoteWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_userSiteMenuID)));
            InitWebElements();
        }


        public void Navigate(SiteMenuPages page)
        {
            string description = Utils.GetEnumDescription(page);
            siteMenu.FindElement(By.LinkText(description)).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));

            if (page == SiteMenuPages.Brands)
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(_chooseBrandXpath)));
            }


        }

        private void InitWebElements()
        {
            siteMenu = WebDriverHelper.Driver.FindElementById(_userSiteMenuID);
        }
    }
}
