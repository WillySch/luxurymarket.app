﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using SeleniumAppTests.PageObjects.CMS.authenticated;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.CMS.authentication
{
    public class CmsLoginPage : BasePage
    {
        #region Web Element Identifiers

        private String _cmsLoginFormClass = "login-wrapper";
        private String _headerMenuXpath = "/html/body/nav/div/div/div/ul";
        private String _usernameID = "login";
        private String _passwordID = "password";
        private String _loginbtnClass = "waves-button-input";

        #endregion

        #region Web Elements

        private IWebElement cmsLoginForm;
        private IWebElement username;
        private IWebElement password;
        private IWebElement loginbtn;

        #endregion

        public CmsLoginPage(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_cmsLoginFormClass)));
            initWebElements();
        }

        //public void setCmsUsername(string value)
        //{
        //    username.SendKeys(value);
        //}

        //public void setCmsPassword(string value)
        //{
        //    password.SendKeys(value);
        //}

        public void loginToCmsClick()
        {
            loginbtn.Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_headerMenuXpath)));
        } 
       
        public void setUsername(string value)
        {
            username.SendKeys(value);
        }

        public void setPassword(string value)
        {
            password.SendKeys(value);
        }

        public CmsHeader loginClick()
        {
            loginToCmsClick();

            return new CmsHeader(WebDriverHelper.Driver);            
        }
        
        private void initWebElements()
        {
            cmsLoginForm = WebDriver.FindElementByClassName(_cmsLoginFormClass);
            username = WebDriver.FindElementById(_usernameID);
            password = WebDriver.FindElementById(_passwordID);
            loginbtn = WebDriver.FindElementByClassName(_loginbtnClass);
        }

    }
}
