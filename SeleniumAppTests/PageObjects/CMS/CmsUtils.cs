﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.CMS
{
    public class CmsUtils
    {

        //#region Web Element Identifiers

        //private String _cmsLoginFormClass = "login-wrapper";
        //private String _headerMenuClass = "right hide-on-med-and-down";
        //private String _usernameID = "login";
        //private String _passwordID = "password";
        //private String _loginbtnClass = "waves-button-input";
        //private String _requestsXpath = "/html/body/nav/div/div/div/ul/li[7]/a";
        //private String _partnersXpath = "/html/body/nav/div/div/div/ul/li[3]/a";
        //private String _searchID = "search";
        //private String _contactClass = "contact";
        //private String _approveID = "btnAprove";
        //private String _decline = "btnDecline";
        //private String _confirmRequestPopupID = "confirmAction";
        //private String _yesApprovalID = "yesAnswer";
        //private String _requestStatusClass = "select-dropdown";

        //#endregion


        //#region Web Elements

        //private IWebElement cmsLoginForm;
        //private IWebElement username;
        //private IWebElement password;
        //private IWebElement loginbtn;
        //private IWebElement requests;
        //private IWebElement partners;
        //private IWebElement searchInput;
        //private IWebElement contactVerification;
        //private IWebElement approve;
        //private IWebElement decline;
        //private IWebElement requestPopup;
        //private IWebElement yesApproval;

        //#endregion

        //public CmsUtils(RemoteWebDriver driver) : base(driver)
        //{
        //    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
        //    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_cmsLoginFormClass)));
        //    initWebElements();
        //}

        //public void setCmsUsername(string value)
        //{
        //    username.SendKeys(value);
        //}

        //public void setCmsPassword(string value)
        //{
        //    password.SendKeys(value);
        //}

        //public void loginToCmsClick()
        //{
        //    loginbtn.Click();
        //    Thread.Sleep(2000);
        //    //var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
        //    //wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/nav/div/div/div/ul/li[7]/a")));
        //}

        //public void requestsClick()
        //{
        //    WebDriverHelper.Driver.FindElementByXPath(_requestsXpath).Click();
        //}

        //public void partnersMenuClick()
        //{
        //    WebDriverHelper.Driver.FindElementByXPath(_partnersXpath).Click();
        //}

        //public void existingPartnersClick()
        //{
        //    WebDriverHelper.Driver.FindElementByLinkText("Existing Partners").Click();
        //}

        //public void searchField(string value)
        //{
        //    WebDriverHelper.Driver.FindElementById(_searchID).SendKeys(value);
        //    WebDriverHelper.Driver.FindElementById(_searchID).SendKeys(Keys.Enter);

        //    var page = ExpectedConditions.TextToBePresentInElementLocated(By.ClassName("page-header"), "Partner Requests");
        //    if (page == null)
        //    {
        //        var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
        //        wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.ClassName(_contactClass), value));
        //    }
        //    else
        //    {
        //        var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
        //        wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.ClassName("company"), value));
        //    }
        //}

        //public void approveClick()
        //{
        //    WebDriverHelper.Driver.FindElementById(_approveID).Click();

        //    var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
        //    wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_confirmRequestPopupID)));
        //}

        //public void declineClick()
        //{
        //    WebDriverHelper.Driver.FindElementById((_decline)).Click();

        //    var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
        //    wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_confirmRequestPopupID)));
        //    Thread.Sleep(2000);
        //}

        //public void yesPopupClick()
        //{
        //    WebDriverHelper.Driver.FindElementById(_yesApprovalID).Click();
        //    Thread.Sleep(2000);
        //}

        //public void statusRequestsDropdown()
        //{
        //    Thread.Sleep(1000);
        //    WebDriverHelper.Driver.FindElementByClassName(_requestStatusClass).Click();
        //    Thread.Sleep(1000);
        //}

        //public void declinedRequestsPage()
        //{
        //    statusRequestsDropdown();
        //    WebDriverHelper.Driver.FindElementByXPath("/html/body/main/div/div[2]/div[1]/div/div/div/ul/li[3]/span").Click();

        //    var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
        //    wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/main/div/div[2]/div[1]/table/tbody/tr[1]/td[1]/div")));
        //}

        //public void allRequests()
        //{
        //    statusRequestsDropdown();
        //    WebDriverHelper.Driver.FindElementByLinkText("All").Click();
        //}

        public void newTab()
        {
            IWebElement newTab = WebDriverHelper.Driver.FindElementByXPath("/html/body");
            newTab.SendKeys(Keys.Control + 't');
            WebDriverHelper.Driver.Navigate().GoToUrl("http://staging.cms.luxurymarket.com");
        }

        public void tabSwitch()
        {
            IWebElement tabSwitch = WebDriverHelper.Driver.FindElementByXPath("/html/body");
            tabSwitch.SendKeys(Keys.Control + '1');
        }

        //public void aproveRequests(string value)
        //{
        //    username.SendKeys("marius.b@mejix.com");
        //    password.SendKeys("luxury");
        //    loginbtn.Click();
        //    requestsClick();
        //    searchField(value);
        //    approveClick();
        //    yesPopupClick();
        //    partnersMenuClick();
        //    existingPartnersClick();
        //    searchField(value);
        //}

        //public void declineRequests(string value)
        //{
        //    username.SendKeys("marius.b@mejix.com");
        //    password.SendKeys("luxury");
        //    loginbtn.Click();
        //    requestsClick();
        //    searchField(value);
        //    declineClick();
        //    yesPopupClick();
        //    declinedRequestsPage();
        //    searchField(value);
        //}

        //private void initWebElements()
        //{
        //    cmsLoginForm = WebDriver.FindElementByClassName(_cmsLoginFormClass);
        //    username = WebDriver.FindElementById(_usernameID);
        //    password = WebDriver.FindElementById(_passwordID);
        //    loginbtn = WebDriver.FindElementByClassName(_loginbtnClass);
        //}
    }
}
