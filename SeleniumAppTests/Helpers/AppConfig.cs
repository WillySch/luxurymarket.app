﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.Helpers
{
    public class AppConfig
    {
        public static String AppUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["appUrl"];
            }
        }

        public static String CmsUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["cmsUrl"];
            }
        }

        public static String ElasticUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["elasticUrl"];
            }
        }

        public static String appBuyerUsername
        {
            get
            {
                return ConfigurationManager.AppSettings["buyerUsername"];
            }
        }

        public static String appBuyerPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["buyerPassword"];
            }
        }

        public static String appSellerUsername
        {
            get
            {
                return ConfigurationManager.AppSettings["sellerUsername"];
            }
        }

        public static String appSellerPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["sellerPassword"];
            }
        }


        public static String cmsUsername
        {
            get
            {
                return ConfigurationManager.AppSettings["cmsUsername"];
            }
        }

        public static String cmsPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["cmsPassword"];
            }
        }

        public static String uploadOffer
        {
            get
            {
                return ConfigurationManager.AppSettings[@"uploadOffer"];
            }
        }

        public static String uploadSupportDoc
        {
            get
            {
                return ConfigurationManager.AppSettings[@"uploadSupportingDoc"];
            }
        } 

        public static String buyerCompanyName
        {
            get
            {
                return ConfigurationManager.AppSettings["autBuyerCompanyName"];
            }
        }

        public static String buyerContactName
        {
            get
            {
                return ConfigurationManager.AppSettings["autBuyerContactName"];
            }
        }

        public static String buyerEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["autBuyerEmail"];
            }
        }

        public static String sellerCompanyName
        {
            get
            {
                return ConfigurationManager.AppSettings["autSellerCompanyName"];
            }
        }

        public static String sellerContactName
        {
            get
            {
                return ConfigurationManager.AppSettings["autSellerContactName"];
            }
        }
        public static String sellerEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["autSellerEmail"];
            }
        }

        public static int WaitForPageLoad
        {
            get
            {
                int pageLoadTime = -1;
                int.TryParse(ConfigurationManager.AppSettings["waitForPageLoad"], out pageLoadTime);

                return pageLoadTime == -1 ? 10 : pageLoadTime;
            }
        }
    }
}
