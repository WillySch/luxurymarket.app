﻿using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.Helpers
{
    public class WebDriverHelper
    {
        private static FirefoxDriver _driver = null;

        public static RemoteWebDriver Driver
        {
            get
            {
                return _driver;
            }
        }

        public static void Init()
        {
            _driver = new FirefoxDriver();
            _driver.Manage().Window.Maximize();
        }
    }
}
