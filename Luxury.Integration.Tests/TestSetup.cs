﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using FluentAssertions.Equivalency;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace Luxury.Integration.Tests
{
    [SetUpFixture]
    public class TestSetup
    {
        [OneTimeSetUp]
        public void AssemblyInitialize()
        {
            ConfigureFluentFilters();
        }

        private void ConfigureFluentFilters()
        {
            AssertionOptions.AssertEquivalencyUsing(
                config => 
                    config
                    .Excluding(ctx => ctx.SelectedMemberPath.EndsWith("Id")
                #region temporary
                    || ctx.SelectedMemberPath.EndsWith("MainImageLink")
                    || ctx.SelectedMemberPath.EndsWith("MainImageName")
                    || ctx.SelectedMemberPath.EndsWith("DiscountPercentage")
                #endregion
                    )
                    .Using<DateTime>(ctx => ctx.Subject.Should().BeCloseTo(ctx.Expectation)).WhenTypeIs<DateTime>());
        }
    }
}
