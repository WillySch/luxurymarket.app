﻿//using System;
//using System.Collections.Generic;
//using System.Data.SqlClient;
//using System.Linq;
//using System.Runtime.CompilerServices;
//using System.Text;
//using System.Threading.Tasks;
//using Autofac;
//using LuxuryApp.Auth.Api.Models;
//using LuxuryApp.Auth.DataAccess;
//using LuxuryApp.Contracts.Access;
//using LuxuryApp.Contracts.Enums;
//using LuxuryApp.Contracts.Interfaces;
//using LuxuryApp.Contracts.Models.Offer;
//using LuxuryApp.Contracts.Repository;
//using LuxuryApp.Contracts.Services;
//using LuxuryApp.Core.DataAccess;
//using LuxuryApp.Core.DataAccess.Repository;
//using LuxuryApp.Processors.Access;
//using LuxuryApp.Processors.Services;
//using Microsoft.AspNet.Identity;
//using NUnit.Framework;
//using FluentAssertions;
//using LuxuryApp.Contracts.Models;
//using LuxuryApp.Core.Infrastructure.Api.Models;
//using LuxuryApp.Contracts.Models.Cart;
//using LuxuryApp.Core.Infrastructure.Authorization;

//namespace Luxury.Integration.Tests
//{
    
//    public abstract class TestCartService
//    {
//        protected static IContainer Container;
//        private const string BuyerUserName = "RhondaMorfunt@mejix.com";
//        private const string BuyerCompanyName = "Eyelash Bouquet";

//        private const string SellerUserName = "UlyssesPlulpigger@mejix.com";
//        private const string SellerCompanyName = "Polka Inferno";
//        private const string SellerAlias = "ONR13";
//        private const string SellerRegion = "Europe";

//        private Offer _offerTakeAll;
//        private Offer _offerNoTakeAll;

//        [OneTimeSetUp]
//        public static void OneTimeSetup()
//        {
//            var builder = new ContainerBuilder();
//            builder.RegisterType<LuxuryMarketConnectionStringProvider>()
//                .As<IConnectionStringProvider>();

//            #region fake IContextProvider

//            TestHelpers.RegisterUserStore(builder);
//            builder.Register(c => new FakeContextProvider(c.Resolve<IUserStore<ApplicationUser, int>>(), BuyerUserName))
//                .As<IContextProvider>();
//            #endregion

//            builder.RegisterType<CompanyRepository>()
//                .As<ICompanyRepository>();
//            builder.RegisterType<OfferRepository>()
//                .As<IOfferRepository>();
//            builder.RegisterType<CurrencyService>()
//                .As<ICurrencyService>();
//            builder.RegisterType<CartRepository>()
//                .As<ICartRepository>();
//            builder.RegisterType<TrivialCompanyAccess>()
//                .As<ICompanyAccess>();
//            builder.RegisterType<CartService>()
//                .As<ICartService>();
//            builder.RegisterType<ProductService>()
//                .As<IProductService>();
//            Container = builder.Build();
//        }

//        [SetUp]
//        public void Setup()
//        {
//            SetupOffers();
//        }

//        private void SetupOffers()
//        {
//            const string productSku = "1BB010ASKF0MW4";
//            var product = Container.Resolve<IProductService>().GetProductBySku(productSku);
//            if (product?.ID == 0)
//            {
//                throw new ApplicationException($"Failed to retreive product with sku {productSku}");
//            }
//            var offerRepository = Container.Resolve<IOfferRepository>();
//            var sellerCompanyId = GetSellerCompanyId();
//            var currency = TestHelpers.GetDefaultCurrency();
//            var sellerUserId = GetSellerUserId();
//            _offerTakeAll = new Offer
//            {
//                OfferNumber = 93039021,
//                OfferName = "Test offer by Polka Inferno",
//                EndDate = null,
//                StartDate = DateTime.Now,
//                OfferStatus = OfferStatus.Canceled,
//                TakeAll = true,
//                Currency = currency,
//                SellerCompanyId = sellerCompanyId,
//                OfferProducts = new OfferProduct[]
//                {
//                    new OfferProduct
//                    {
//                        ProductId = product.ID,
//                        LineBuy = false,
//                        OfferCost = 2000,
//                        Currency = currency,
//                        OfferProductSizes = new OfferProductSize[]
//                        {
//                            new OfferProductSize
//                            {
//                                Quantity = 10,
//                                Size = new Size {Name = "38", SizeType = new SizeType {Name = "T2"}},
//                            },
//                            new OfferProductSize
//                            {
//                                Quantity = 20,
//                                Size = new Size {Name = "40", SizeType = new SizeType {Name = "T2"}},
//                            },
//                            new OfferProductSize
//                            {
//                                Quantity = 25,
//                                Size = new Size {Name = "42", SizeType = new SizeType {Name = "T2"}},
//                            }
//                        }
//                    },
//                    new OfferProduct
//                    {
//                        ProductId = product.ID,
//                        LineBuy = true,
//                        OfferCost = 2000,
//                        Currency = currency,
//                        OfferProductSizes = new OfferProductSize[]
//                        {
//                            new OfferProductSize
//                            {
//                                Quantity = 10,
//                                Size = new Size {Name = "38", SizeType = new SizeType {Name = "T2"}},
//                            },
//                            new OfferProductSize
//                            {
//                                Quantity = 20,
//                                Size = new Size {Name = "40", SizeType = new SizeType {Name = "T2"}},
//                            },
//                            new OfferProductSize
//                            {
//                                Quantity = 25,
//                                Size = new Size {Name = "42", SizeType = new SizeType {Name = "T2"}},
//                            }
//                        }
//                    }
//                }
//            };
//            offerRepository.SaveOffer(_offerTakeAll, sellerUserId);

//            _offerNoTakeAll = new Offer
//            {
//                OfferNumber = 93039021,
//                OfferName = "Test offer by Polka Inferno",
//                EndDate = null,
//                StartDate = DateTime.Now,
//                OfferStatus = OfferStatus.Canceled,
//                TakeAll = false,
//                Currency = currency,
//                SellerCompanyId = sellerCompanyId,
//                OfferProducts = new OfferProduct[]
//                {
//                    new OfferProduct
//                    {
//                        ProductId = product.ID,
//                        LineBuy = false,
//                        OfferCost = 2000,
//                        Currency = currency,
//                        OfferProductSizes = new OfferProductSize[]
//                        {
//                            new OfferProductSize
//                            {
//                                Quantity = 10,
//                                Size = new Size {Name = "38", SizeType = new SizeType {Name = "T2"}},
//                            },
//                            new OfferProductSize
//                            {
//                                Quantity = 20,
//                                Size = new Size {Name = "40", SizeType = new SizeType {Name = "T2"}},
//                            },
//                            new OfferProductSize
//                            {
//                                Quantity = 25,
//                                Size = new Size {Name = "42", SizeType = new SizeType {Name = "T2"}},
//                            }
//                        }
//                    },
//                    new OfferProduct
//                    {
//                        ProductId = product.ID,
//                        LineBuy = true,
//                        OfferCost = 2000,
//                        Currency = currency,
//                        OfferProductSizes = new OfferProductSize[]
//                        {
//                            new OfferProductSize
//                            {
//                                Quantity = 10,
//                                Size = new Size {Name = "38", SizeType = new SizeType {Name = "T2"}},
//                            },
//                            new OfferProductSize
//                            {
//                                Quantity = 20,
//                                Size = new Size {Name = "40", SizeType = new SizeType {Name = "T2"}},
//                            },
//                            new OfferProductSize
//                            {
//                                Quantity = 25,
//                                Size = new Size {Name = "42", SizeType = new SizeType {Name = "T2"}},
//                            }
//                        }
//                    }
//                }
//            };
//            offerRepository.SaveOffer(_offerNoTakeAll, sellerUserId);
//        }

//        [TearDown]
//        public void TearDown()
//        {
//            CleanOffers();
//        }

//        private void CleanOffers()
//        {
//            var offerRepository = Container.Resolve<IOfferRepository>();
//            offerRepository.DeleteOffer(_offerTakeAll.Id);
//            offerRepository.DeleteOffer(_offerNoTakeAll.Id);
//        }

//        protected ICartService GetCartService()
//        {
//            return Container.Resolve<ICartService>();
//        }

//        protected CartSummary GetExpectedCartSummaryWhenProductOfferHasNotSetLineBuy(int buyerCompanyId, bool isItemActive)
//        {
//            var expectedTotals = isItemActive
//                ? GetExpectedCartTotalsWhenProductOfferHasNotSetLineBuy()
//                : GetExpectedCartTotalsZero();
//            return new CartSummary
//            {
//                BuyerCompanyId = buyerCompanyId,
//                UnitsCount = expectedTotals.UnitsCount,
//                ItemsCount = expectedTotals.ItemsCount,
//                TotalCustomerCost = expectedTotals.TotalOfferCost,
//                Currency = expectedTotals.Currency,
//            };
//        }

//        protected Cart GetExpectedCartWhenProductOfferHasNotSetLineBuy(int buyerCompanyId, bool isItemActive)
//        {
//            //var expectedTotals = isItemActive
//            //    ? GetExpectedCartTotalsWhenProductOfferHasNotSetLineBuy()
//            //    : GetExpectedCartTotalsZero();
//            return TestHelpers.ComputedTotalsCart(new Cart
//            {
//                BuyerCompanyId = buyerCompanyId,
//                //ItemsCount = expectedTotals.ItemsCount,
//                //UnitsCount = expectedTotals.UnitsCount,
//                //TotalCustomerCost = expectedTotals.TotalOfferCost,
//                //Currency = expectedTotals.Currency,
//                Items = new CartItem[]
//                {
//                    new CartItem
//                    {
//                        //ProductId = ,
//                        ProductName = "Soft Calf Medium Inside Bag",
//                        ProductMainImageName = "c448f569-67fb-4617-8d1d-1e67af910754",

//                        //BrandId = ,
//                        BrandName = "PRADA",

//                        CategoryName = "TOP HANDLES & SATCHELS",

//                        ProductSku = "1BB010ASKF0MW4",

//                        //MaterialId = ,
//                        MaterialCode = "ASK",

//                        //ColorId = ,
//                        ColorName = "F0MW4",

//                        //SeasonId = ,
//                        SeasonName = "FW16",

//                        //AverageUnitCost = isItemActive ? Math.Floor((decimal)(2000*Constants.CompanyFee)) : 0,
//                        //AverageUnitDiscountPercentage = isItemActive ? 23 : 0,
//                        //TotalCustomerCost = isItemActive ? 2*(decimal)(2000*Constants.CompanyFee) : 0,

//                        SellerUnits = new[]
//                        {
//                            new CartItemSellerUnit
//                            {
//                                CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(), // in tests are ignored properties ending with Id
//                                SellerAlias = GetSellerAlias(SellerCompanyName),
//                                ShipsFromRegion = SellerRegion,
//                                UnitsCount = 2,
//                                CustomerUnitPrice = (decimal) (2000*Constants.CompanyFee),
//                                DiscountPercentage = 23,
//                                UnitShippingTaxPercentage = Constants.ShippingPercentageNonApparel,
//                                //TotalCustomerCost = (decimal) (2000*Constants.CompanyFee)*2,
//                                IsTakeOffer = false,
//                                IsLineBuy = false,
//                                Active = isItemActive,
//                                SellerUnitSizes = new CartItemSellerUnitSize[]
//                                {
//                                    new CartItemSellerUnitSize
//                                    {
//                                        Quantity = 2,
//                                        AvailableQuantity = 10,
//                                        //public int SizeId = ,
//                                        SizeName = "38",
//                                    },
//                                    new CartItemSellerUnitSize
//                                    {
//                                        Quantity = 0,
//                                        AvailableQuantity = 20,
//                                        //public int SizeId = ,
//                                        SizeName = "40",
//                                    },
//                                    new CartItemSellerUnitSize
//                                    {
//                                        Quantity = 0,
//                                        AvailableQuantity = 25,
//                                        //public int SizeId = ,
//                                        SizeName = "42",
//                                    }
//                                }
//                            },
//                        }
//                    }
//                }
//            });
//        }

//        protected ExpectedCartTotals GetExpectedCartTotalsWhenProductOfferHasNotSetLineBuy()
//        {
//            return new ExpectedCartTotals
//            {
//                ItemsCount = 1,
//                UnitsCount = 2,
//                TotalOfferCost = 2*2000*(decimal) Constants.CompanyFee,
//                Currency = new Currency
//                {
//                    CurrencyId = 0,
//                    Name = "USD",
//                    Rate = 1.0000M
//                }
//            };
//        }

//        protected Cart GetExpectedCartWhenProductOfferHasSetLineBuy(int buyerCompanyId, bool isItemActive)
//        {
//            //var expectedTotals = isItemActive
//            //    ? GetExpectedCartTotalsWhenProductOfferHasSetLineBuy()
//            //    : GetExpectedCartTotalsZero();
//            return TestHelpers.ComputedTotalsCart(new Cart
//            {
//                BuyerCompanyId = buyerCompanyId,
//                //ItemsCount = expectedTotals.ItemsCount,
//                //UnitsCount = expectedTotals.UnitsCount,
//                //TotalCustomerCost = expectedTotals.TotalOfferCost,
//                //Currency = expectedTotals.Currency,
//                Items = new CartItem[]
//                {
//                    new CartItem
//                    {
//                        //ProductId = ,
//                       ProductName = "Soft Calf Medium Inside Bag",
//                        ProductMainImageName = "c448f569-67fb-4617-8d1d-1e67af910754",

//                        //BrandId = ,
//                        BrandName = "PRADA",

//                        CategoryName = "TOP HANDLES & SATCHELS",

//                        ProductSku = "1BB010ASKF0MW4",

//                        //MaterialId = ,
//                        MaterialCode = "ASK",

//                        //ColorId = ,
//                        ColorName = "F0MW4",

//                        //SeasonId = ,
//                        SeasonName = "FW16",

//                        //AverageUnitCost = isItemActive ? Math.Floor((decimal)(2000*Constants.CompanyFee)) : 0,
//                        //AverageUnitDiscountPercentage = isItemActive ? 23 : 0,
//                        //TotalCustomerCost = isItemActive ? (10 + 20 + 25) * (decimal)(2000*Constants.CompanyFee) : 0,

//                        SellerUnits = new[]
//                        {
//                            //new CartItemSellerUnit
//                            //{
//                            //    SellerAlias = GetSellerAlias(SellerCompanyName),
//                            //    ShipsFromRegion = SellerRegion,
//                            //    UnitOfferCost = (decimal) (2000*Constants.CompanyFee),
//                            //    UnitOfferDiscount = 50,
//                            //    UnitsCount = 0,
//                            //    TotalOfferCost = 0,
//                            //    IsTakeOffer = false,
//                            //    IsLineBuy = false,
//                            //    Active = false,
//                            //    SellerUnitSizes = new CartItemSellerUnitSize[]
//                            //    {
//                            //        new CartItemSellerUnitSize
//                            //        {
//                            //            OfferProductSizeId = 422, // TODO: db hardcoded value

//                            //            Quantity = 0,
//                            //            AvailableQuantity = 10,
//                            //            //public int SizeId = ,
//                            //            SizeName = "38",
//                            //        },
//                            //        new CartItemSellerUnitSize
//                            //        {
//                            //            OfferProductSizeId = 423, // TODO: db hardcoded value

//                            //            Quantity = 0,
//                            //            AvailableQuantity = 20,
//                            //            //public int SizeId = ,
//                            //            SizeName = "40",
//                            //        },
//                            //        new CartItemSellerUnitSize
//                            //        {
//                            //            OfferProductSizeId = 424, // TODO: db hardcoded value

//                            //            Quantity = 0,
//                            //            AvailableQuantity = 25,
//                            //            //public int SizeId = ,
//                            //            SizeName = "42",
//                            //        }
//                            //    }
//                            //},
//                            new CartItemSellerUnit
//                            {
//                                CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(), // in tests are ignored properties ending with Id
//                                SellerAlias = GetSellerAlias(SellerCompanyName),
//                                ShipsFromRegion = SellerRegion,
//                                CustomerUnitPrice = (decimal) (2000*Constants.CompanyFee),
//                                DiscountPercentage = 23,
//                                UnitsCount = 10 + 20 + 25,
//                                //TotalCustomerCost = (decimal) (2000*Constants.CompanyFee)*(10 + 20 + 25),
//                                IsTakeOffer = false,
//                                IsLineBuy = true,
//                                Active = isItemActive,
//                                UnitShippingTaxPercentage = Constants.ShippingPercentageNonApparel,
//                                SellerUnitSizes = new CartItemSellerUnitSize[]
//                                {
//                                    new CartItemSellerUnitSize
//                                    {
//                                        Quantity = 10,
//                                        AvailableQuantity = 10,
//                                        //public int SizeId = ,
//                                        SizeName = "38",
//                                    },
//                                    new CartItemSellerUnitSize
//                                    {
//                                        Quantity = 20,
//                                        AvailableQuantity = 20,
//                                        //public int SizeId = ,
//                                        SizeName = "40",
//                                    },
//                                    new CartItemSellerUnitSize
//                                    {
//                                        Quantity = 25,
//                                        AvailableQuantity = 25,
//                                        //public int SizeId = ,
//                                        SizeName = "42",
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            });
//        }

//        protected ExpectedCartTotals GetExpectedCartTotalsWhenProductOfferHasSetLineBuy()
//        {
//            return new ExpectedCartTotals
//            {
//                ItemsCount = 1,// one product 
//                UnitsCount = 55,
//                TotalOfferCost = (decimal)(110000 * Constants.CompanyFee),
//                Currency = new Currency
//                {
//                    CurrencyId = 0,
//                    Name = "USD",
//                    Rate = 1.0000M
//                }
//            };
//        }

//        protected Cart GetExpectedCartWhenOfferHasTakeAll(int buyerCompanyId, bool isItemActive)
//        {
//            //var expectedTotals = isItemActive
//            //    ? GetExpectedCartTotalsWhenOfferHasTakeAll()
//            //    : GetExpectedCartTotalsZero();
//            return TestHelpers.ComputedTotalsCart(new Cart
//            {
//                BuyerCompanyId = buyerCompanyId,
//                //ItemsCount = expectedTotals.ItemsCount,
//                //UnitsCount = expectedTotals.UnitsCount,
//                //TotalCustomerCost = expectedTotals.TotalOfferCost,
//                //Currency = expectedTotals.Currency,
//                Items = new CartItem[]
//                {
//                    new CartItem
//                    {
//                        //ProductId = ,
//                         ProductName = "Soft Calf Medium Inside Bag",
//                        ProductMainImageName = "c448f569-67fb-4617-8d1d-1e67af910754",

//                        //BrandId = ,
//                        BrandName = "PRADA",

//                        CategoryName = "TOP HANDLES & SATCHELS",

//                        ProductSku = "1BB010ASKF0MW4",

//                        //MaterialId = ,
//                        MaterialCode = "ASK",

//                        //ColorId = ,
//                        ColorName = "F0MW4",

//                        //SeasonId = ,
//                        SeasonName = "FW16",

//                        //AverageUnitCost = isItemActive ? Math.Floor(1*(decimal) (2000*Constants.CompanyFee)) : 0,
//                        //AverageUnitDiscountPercentage = isItemActive ? 23 : 0,
//                        //TotalCustomerCost = isItemActive ? 1*110*(decimal) (2000*Constants.CompanyFee) : 0,

//                        SellerUnits = new[]
//                        {
//                            new CartItemSellerUnit
//                            {
//                                CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(), // in tests are ignored properties ending with Id
//                                SellerAlias = GetSellerAlias("Polka Inferno"),
//                                ShipsFromRegion = "Europe",
//                                CustomerUnitPrice = (decimal) (2000*Constants.CompanyFee),
//                                DiscountPercentage = 23,
//                                UnitsCount = 55,
//                                //TotalCustomerCost = (decimal) (2000*Constants.CompanyFee)*55,
//                                IsTakeOffer = true,
//                                IsLineBuy = false,
//                                Active = isItemActive,
//                                UnitShippingTaxPercentage = Constants.ShippingPercentageNonApparel,
//                                SellerUnitSizes = new CartItemSellerUnitSize[]
//                                {
//                                    new CartItemSellerUnitSize
//                                    {
//                                        OfferProductSizeId = 428, // TODO: db hardcoded value

//                                        Quantity = 10,
//                                        AvailableQuantity = 10,
//                                        //public int SizeId = ,
//                                        SizeName = "38",
//                                    },
//                                    new CartItemSellerUnitSize
//                                    {
//                                        OfferProductSizeId = 429, // TODO: db hardcoded value

//                                        Quantity = 20,
//                                        AvailableQuantity = 20,
//                                        //public int SizeId = ,
//                                        SizeName = "40",
//                                    },
//                                    new CartItemSellerUnitSize
//                                    {
//                                        OfferProductSizeId = 430, // TODO: db hardcoded value

//                                        Quantity = 25,
//                                        AvailableQuantity = 25,
//                                        //public int SizeId = ,
//                                        SizeName = "42",
//                                    }
//                                }
//                            },
//                            new CartItemSellerUnit
//                            {
//                                CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(), // in tests are ignored properties ending with Id
//                                SellerAlias = GetSellerAlias("Polka Inferno"),
//                                ShipsFromRegion = "Europe",
//                                CustomerUnitPrice = (decimal) (2000*Constants.CompanyFee),
//                                DiscountPercentage = 23,
//                                UnitsCount = 55,
//                                //TotalCustomerCost = (decimal) (2000*Constants.CompanyFee)*55,
//                                UnitShippingTaxPercentage = Constants.ShippingPercentageNonApparel,
//                                IsTakeOffer = true,
//                                IsLineBuy = true,
//                                Active = isItemActive,
//                                SellerUnitSizes = new CartItemSellerUnitSize[]
//                                {
//                                    new CartItemSellerUnitSize
//                                    {
//                                        OfferProductSizeId = 431, // TODO: db hardcoded value

//                                        Quantity = 10,
//                                        AvailableQuantity = 10,
//                                        //public int SizeId = ,
//                                        SizeName = "38",
//                                    },
//                                    new CartItemSellerUnitSize
//                                    {
//                                        OfferProductSizeId = 432, // TODO: db hardcoded value

//                                        Quantity = 20,
//                                        AvailableQuantity = 20,
//                                        //public int SizeId = ,
//                                        SizeName = "40",
//                                    },
//                                    new CartItemSellerUnitSize
//                                    {
//                                        OfferProductSizeId = 433, // TODO: db hardcoded value

//                                        Quantity = 25,
//                                        AvailableQuantity = 25,
//                                        //public int SizeId = ,
//                                        SizeName = "42",
//                                    }
//                                }
//                            },
//                        }
//                    }
//                }
//            });
//        }

//        private string GetSellerAlias(string sellerCompanyName)
//        {
//            if (sellerCompanyName == SellerCompanyName)
//            {
//                return SellerAlias;
//            }
//            throw new ArgumentOutOfRangeException(nameof(sellerCompanyName), sellerCompanyName);
//            //return sellerCompanyName;
//        }

//        protected ExpectedCartTotals GetExpectedCartTotalsWhenOfferHasTakeAll()
//        {
//            return new ExpectedCartTotals
//            {
//                ItemsCount = 1,
//                UnitsCount = 110,
//                TotalOfferCost = (decimal) (220000*Constants.CompanyFee),
//                Currency = new Currency
//                {
//                    CurrencyId = 0,
//                    Name = "USD",
//                    Rate = 1.0000M
//                }
//            };
//        }

//        protected ExpectedCartTotals GetExpectedCartTotalsZero()
//        {
//            return new ExpectedCartTotals
//            {
//                ItemsCount = 0,
//                UnitsCount = 0,
//                TotalOfferCost = 0,
//                Currency = new Currency
//                {
//                    CurrencyId = 0,
//                    Name = "USD",
//                    Rate = 1.0000M
//                }
//            };
//        }

//        protected int GetBuyerCompanyId()
//        {
//            int totalRecords;
//            return Container.Resolve<ICompanyRepository>()
//                .SearchCompaniesByName(BuyerCompanyName, out totalRecords)
//                .Single().CompanyId;
//        }

//        protected int GetSellerCompanyId()
//        {
//            return GetSellerCompanyId(SellerCompanyName);
//        }

//        protected int GetSellerCompanyId(string companyName)
//        {
//            return Container.Resolve<ICompanyRepository>()
//                .SearchCompaniesByName(companyName)
//                .Single().CompanyId;
//        }

//        protected void CleanCart(int cartId)
//        {
//            Container.Resolve<ICartRepository>()
//                .PhysicalDeleteCart(cartId);
//        }

//        protected void CleanOffer(int offerId)
//        {
//            Container.Resolve<IOfferRepository>().DeleteOffer(offerId);
//        }


//        #region database dependent hardcoded values

//        protected int GetSellerUserId()
//        {
//            return Container.Resolve<IUserStore<ApplicationUser, int>>().FindByNameAsync(SellerUserName).Result.Id;
//        }

//        protected Offer GetOfferTakeAll()
//        {
//            return _offerTakeAll;
            
//        }

//        protected Offer GetOfferNoTakeAll()
//        {
//            return _offerNoTakeAll;
//        }

//        protected Tuple<OfferProductSize, OfferProduct, Offer> GetOfferNoTakeAllWithProductNotSetLineBuy()
//        {
//            var offer = GetOfferNoTakeAll();
//            var offerProductSize = offer
//                .OfferProducts
//                .Where(x => !x.LineBuy)  
//                .SelectMany(x => x.OfferProductSizes)
//                .SingleOrDefault(x => x.Size.Name == "38");  
//            var offerProduct = offer.OfferProducts.Single(x => x.OfferProductSizes
//                    .Any(y => y.Equals(offerProductSize)));
//            return
//                new Tuple<OfferProductSize, OfferProduct, Offer>(
//                    offerProductSize,
//                    offerProduct,
//                    offer);
//        }

//        protected Tuple<OfferProductSize, OfferProduct, Offer> GetOfferNoTakeAllWithProductSetLineBuy()
//        {
//            var offer = GetOfferNoTakeAll();
//            var offerProductSize = 
//                offer
//                    .OfferProducts  
//                    .Where(x => x.LineBuy)
//                    .SelectMany(x => x.OfferProductSizes)
//                    .SingleOrDefault(x => x.Size.Name == "38");
//            var offerProduct = offer.OfferProducts.Single(x => x.OfferProductSizes
//                    .Any(y => y.Equals(offerProductSize)));
//            return
//                new Tuple<OfferProductSize, OfferProduct, Offer>(
//                    offerProductSize,
//                    offerProduct,
//                    offer);
//        }

//        #endregion

//        protected class ExpectedCartTotals
//        {
//            public int ItemsCount { get; set; }
//            public int UnitsCount { get; set; }
//            public decimal TotalOfferCost { get; set; }
//            public Currency Currency { get; set; }
//        }
//    }

//    internal class FakeContextProvider : IContextProvider
//    {
//        private readonly ApplicationUser _applicationUser;

//        public FakeContextProvider(IUserStore<ApplicationUser, int> userStore, 
//            string username)
//        {
//            _applicationUser = userStore.FindByNameAsync(username).Result;
//        }

//        public int GetLoggedInUserId()
//        {
//            return _applicationUser.Id;
//        }
//    }
//}



