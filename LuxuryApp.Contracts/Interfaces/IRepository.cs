﻿using System.Data;
using System.Data.SqlClient;

namespace LuxuryApp.Contracts.Interfaces
{
    public interface IRepository
    {
        int ExecuteProcedure(string storedProcedureName);

        int ExecuteProcedure(string storedProcedureName, SqlParameter[] commandParameters);

        IDataReader LoadData(string storedProcedureName, SqlParameter commandParameter);

        IDataReader LoadData(string storedProcedureName, SqlParameter[] commandParameters);
    }
}
