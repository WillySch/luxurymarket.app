﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.SellerProductSync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Repository
{
    public interface ISellerAPISettingRepository
    {
        Task<APISettings> GetSellerApiSettings(SellerAPISettingType? sellerAPIType);
    }
}
