﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Orders;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Repository
{
    public interface IOrderRepository
    {
        Task<int> SubmitOrder(int cartId, int paymentMethodId, int userId, DateTimeOffset date);

        Task<OrderPoNumberForEmail> GetPoNumbersForEmails(int orderId);

        Task<Order> GetOrderDetailsForBuyer(int orderId);
        Task<Order> GetOrderDetailsForBuyerByOrderSellerId(int orderSellerId);

        Task<OrderSellerManageList> FilterSellerOrders(RequestFilterOrders model, int userId);
        Task<OrderBuyerManageList> FilterBuyerOrders(RequestFilterOrders model, int userId);

        Task<OrderSellerView> GetOrderItemForSellerById(int orderSellerId);

        Task<IEnumerable<PaymentMethod>> GetPaymentMethod();
        Task<IEnumerable<OrderStatusType>> GetSuborderStatusTypes();
        Task<IEnumerable<bool>> VerifyCartItemsBeforeSubmit();

        int UpdateStatus(OrderStatusUpdateView model, double orderConfirmationPercentage, int userId, DateTimeOffset date, out string errorMessage);

        Task<string> ConfirmUnits(OrderConfirmUnitView model, int userId, DateTimeOffset date);

        Task<OrderProductTotalForSeller> GetOrderProductTotal(int orderSellerProductId);

        string CanConfirmUnits(int orderProductId, int userId);

        BuyerEmailDataAfterSellerConfirmation GetBuyerDataAfterSellerConfirmation(int orderSellerId);

        Task<OrderFileList> GetFilesAsync(int? mainOrderId, int? orderId);

        Task<int> UpdateFileAsync(OrderFile model, int userId);

        int InsertFile(OrderFile model, int userId);

        List<SuborderDataForPDF> GetSuborderData(int orderSellerId);

        Task<OrderSellerPackageCollection> GetOrderSellerPackages(int ordersellerId);

        Task<int> InsertOrderSellerPackingInformation(OrderSellerPackingInformationInsert model, int userId);

        Task<bool> IsSubmittedPackageInformation(int ordersellerId);

        Task<OrderSellerPackingInformationInsert> GetOrderSellerPackingInformation(int orderSellerId);

        bool IsUserAssociatedToMainOrder(int orderId, int userId, int? companyId);

        bool IsUserAssociatedToOrderSeller(int orderSellerId, int userId, int? companyId);
    }
}
