﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.Ftp;

namespace LuxuryApp.Contracts.Repository
{
    public interface IFtpRepository
    {
        FtpConfig[] GetAllFtpConfigs(FtpConfigType? type = null);

        int EnqueueItem(FtpQueueItem item);

        FtpQueueItem[] GetItemsToProcess(FtpDirectionType directionTypeId);

        void SetItemUploadStatus(int ftpQueueItemId, FtpUploadStatus status, string error);

        string GetApplicationSettingsByName(string name);
    }
}