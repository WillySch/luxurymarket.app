﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Models.Cart;
using LuxuryApp.Contracts.Models.Offer;

namespace LuxuryApp.Contracts.Repository
{
    public interface IOfferRepository
    {
        Offer GetOfferById(int offerId);
        bool IsOfferTakeAll(int offerId);
        bool IsOfferTakeAllForOfferProductSizeId(int offerProductSizeId);
        bool IsProductLineBuyForOfferProductSizeId(int offerProductSizeId);
        int GetOfferProductIdForOfferProductSizeId(int offerProductSizeId);
        int GetOfferIdForOfferProductSizeId(int offerProductSizeId);

        Task<OfferProductSizeIdAncestors[]> TaskGetOfferProductSizeIdsAncestors(int[] offerProductSizeIds);
        Task<OfferProductSizeCartBusinessFlags[]> TaskGetOfferProductSizeCartBusinessFlags(int[] offerProductSizeIds);

        int SaveOffer(Offer offer, int createdBy);
        void DeleteOffer(int offerId);

        Task<bool> IsActiveOffer(int offerId);
    }
}
