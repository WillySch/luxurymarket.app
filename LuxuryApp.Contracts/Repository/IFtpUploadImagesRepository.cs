﻿using LuxuryApp.Contracts.Models.Ftp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Repository
{
    public interface IFtpUploadImagesRepository
    {
        Task<int> FtpUploadImagesAsync(List<FtpProductImageItem> items, int createdBy);

        Task<int> InsertScheduleAsync(FtpProductImagesSchedule item);

    }
}