﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Models;
using System.Data;

namespace LuxuryApp.Contracts.Repository
{
    public interface IOfferImportRepository
    {
        Task<int> InsertBatchItems(string fileName, DataTable products, DataTable productSizes, int companyId, int regionId, int userId);

        Task<int> UpdateOfferImportGeneralData(OfferImport item, int userId);

        Task<OfferImportBachItem> GetOfferProductFullData(int offerImportBatchItemId);

        Task UpdateOfferImportItem(OfferImportBachItemEditModel item, int userId, DateTimeOffset datetime, bool mapSizes);

        void BatchUpdateOfferImportItems(OfferItemsBatchUpdateModel model, int userId, DateTimeOffset datetime, out string errorMessage);

        Task<int> AddUpdateOfferImportItemSize(OfferImportBachItemSizeModel item, int userId);

        Task<IEnumerable<OfferImportBatchItemSize>> GetOfferImportItemSizes(int offerImportBatchItemId);

        Task<int> UpdateOfferItemsLineBuy(OfferImportBatchEditLineBuy item, int userId);

        Task<IEnumerable<OfferImportBatchItemError>> Validate(int? offerImportId, int? offerImportItemId,bool onlyActive);

        Task<IEnumerable<OfferImportFile>> GetFiles(int offerImportId);

        Task<int> UpdateFile(OfferImportFile file, int userId);

        int InsertFiles(OfferImportFile file, int userId);

        Task<IEnumerable<string>> GetModelNumberByBrand(ProductSearchMappingCriteria item);

        Task<int> InsertOfferImportError(OfferImportReportError model, int userId);

        Task<OfferImportSummary> GetOfferImportSummary(int offerImportBatchId);

        Task<IEnumerable<OfferImportBachItem>> GetOfferImportProductItems(int? offerImportBatchId, int? offerImportBatchItemId);
    }
}
