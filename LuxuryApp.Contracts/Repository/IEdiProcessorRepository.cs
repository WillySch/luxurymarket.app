﻿using LuxuryApp.Contracts.Models;
using System;
using System.Data;

namespace LuxuryApp.Contracts.Repository
{
    public interface IEdiProcessorRepository
    {
        int InsertEdi997Items(int ftpQueueItemId,
                              int ediFileTypeId,
                              EdiExtractedACK997 item,
                              int? createdBy);

        int InsertEdi856Items(DataTable products, int ftpQueueItemId, int ediFileTypeId
                                                                 , DateTime? InterchangeDateTime
                                                                 , DateTime? ActualDepartureDate
                                                                 , DateTime? EstimatedArrivalDate
                                                                 , DateTime? ActualArrivalDate
                                                                 , DateTime? CarrierPickupDate
                                                                 , DateTime? DeliveryToCustomer
                                                                 , int? createdBy);
    }
}
