﻿using LuxuryApp.Contracts.Models.SellerProductSync;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Agents
{
    public interface ISellerProductSyncAgent
    {
        Task<int> SyncProducts(APISettings settings);

        Task<int> SyncProductStock(APISettings sellerId);
    }
}
