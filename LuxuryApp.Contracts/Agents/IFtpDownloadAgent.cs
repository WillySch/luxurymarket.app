﻿namespace LuxuryApp.Contracts.Agents
{
    public interface IFtpDownloadAgent
    {
       void DownloadFtpItems(string localDestinationPath);
    }
}
