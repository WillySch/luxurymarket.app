﻿using System.IO;

namespace LuxuryApp.Contracts.Agents
{
    public interface IEdiProcessorAgent
    {
        void ProcessItemsFromQueue(string localDirectoryPath, int? createdBy);
    }
}
