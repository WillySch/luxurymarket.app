﻿using System;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Agents
{
    public interface IFtpUploadImageAgent
    {
        Task<double> UploadImagesAsync();
    }
}
