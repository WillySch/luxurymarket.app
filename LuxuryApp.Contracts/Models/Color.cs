﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models
{
    public class Color : Attribute
    {
        public int BrandId { get; set; }

        public int StandardColorId { get; set; }

        public StandardColorsModel StandardColor { get; set; }
    }
}
