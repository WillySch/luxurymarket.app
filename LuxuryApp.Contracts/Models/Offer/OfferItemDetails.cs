﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Offer
{
    public class OfferItemDetails
    {
        public int OfferNumber { get; set; }

        public DateTimeOffset StartDate { get; set; }

        public DateTimeOffset EndDate { get; set; }

        public bool TakeAll { get; set; }

        public int ProductID { get; set; }

        public string ProductName { get; set; }

        public string ModelNumber { get; set; }

        public string ColorCode { get; set; }

        public string MaterialCode { get; set; }

        public double RetailPrice { get; set; }

        public double OfferCost { get; set; }

        public double CustomerUnitPrice { get; set; }

        public int TotalUnits { get; set; }

        public double CustomerTotalPrice { get; set; }

        public double CustomerDiscount { get; set; }

        public string BrandName { get; set; }

        public string CategoryName { get; set; }

        public string ProductMainImageName { get; set; }

        public int OfferStatusID { get; set; }

        public string OfferStatusName { get; set; }
        public double OfferCostValueConverted { get; set; }
        public double OfferTotalPrice { get; set; }
        public int CurrencyID { get; set; }
    }
}
