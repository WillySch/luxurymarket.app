﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class OfferProductSizeCartBusinessFlags
    {
        public int OfferProductSizeId { get; set; }
        public bool TakeAll { get; set; }
        public bool LineBuy { get; set; }
    }
}
