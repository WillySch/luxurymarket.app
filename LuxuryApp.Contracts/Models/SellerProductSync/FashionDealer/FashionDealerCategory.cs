﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.SellerProductSync.FashionDealer
{
    public class FashionDealerCategoriesList
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "data")]
        public CategoriesData Data { get; set; }
    }

    public class CategoriesData
    {
        [JsonProperty(PropertyName = "categories")]
        public List<Category> Categories { get; set; }
    }

    public class Category
    {
        [JsonProperty(PropertyName = "_id")]
        [JsonConverter(typeof(CategoryIdDataConverter))]
        public CatId Id { get; set; }

        [JsonProperty(PropertyName = "parent")]
        [JsonConverter(typeof(ParentIdDataConverter))]
        public Parent Parent { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "name_en")]
        public string NameEn { get; set; }

        [JsonProperty(PropertyName = "master")]
        public bool Master { get; set; }

        [JsonProperty(PropertyName = "leaf")]
        public bool? Leaf { get; set; }
    }

    public class CatId
    {
        [JsonProperty(PropertyName = "id")]
        public string CategoryId { get; set; }
    }

    public class Parent
    {
        [JsonProperty(PropertyName = "id")]
        public string ParentId { get; set; }
    }

    #region Custom JsonConverter
    //create a custom json converter for category id
    class CategoryIdDataConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(CatId));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            if (token.Type == JTokenType.Object)
            {
                var json = token.ToString().Replace("$id", "id");
                var response = JsonConvert.DeserializeObject<CatId>(json);
                return response;
            }
            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }

    //create a custom json converter for parent id
    class ParentIdDataConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(Parent));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            if (token.Type == JTokenType.Object)
            {
                var json = token.ToString().Replace("$id", "id");
                var response = JsonConvert.DeserializeObject<Parent>(json);
                return response;
            }
            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }
    #endregion
}
