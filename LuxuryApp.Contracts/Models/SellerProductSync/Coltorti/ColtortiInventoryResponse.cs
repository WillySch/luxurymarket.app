﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.SellerProductSync.Coltorti
{
    public class ColtortiInventoryResponse
    {
        public int total { get; set; }

        public Dictionary<string, Dictionary<string, int>> scalars { get; set; }
    }
}
