﻿using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models.SellerProductSync
{
    public class APISettings
    {
        public string BaseAddress { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public int SellerID { get; set; }

        public bool NeedsPreAuthentication { get; set; }

        public int ProductDelayMinutes { get; set; }

        public int InventoryDelayMinutes { get; set; }

        public int Id { get; set; }

        public SellerAPISettingType SellerType { get; set; }
    }

}
