﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.SellerProductSync
{
    public class InventorySize
    {
        public string SellerSku { get; set; }

        public string SellerSize { get; set; }

        public int Quantity { get; set; }
    }
}
