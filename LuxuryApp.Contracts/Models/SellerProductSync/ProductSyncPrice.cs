﻿using LuxuryApp.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.SellerProductSync
{
    public class ProductSyncPrice
    {
        public string SellerSKU { get; set; }

        public CurrencyType Currency { get; set; }

        public double? Price { get; set; }
    }
}
