﻿namespace LuxuryApp.Contracts.Models
{
    public class Origin
    {
        public int Id { get; set; }

        public string Name { get; set; }

       public string Description { get; set; }

        public int DisplayOrder { get; set; }

        public StandardOriginModel StandardOrigin { get; set; }
}
}
