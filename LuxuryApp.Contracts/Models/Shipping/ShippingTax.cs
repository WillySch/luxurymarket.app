﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Shipping
{
    public class ShippingTax
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Days { get; set; }
        public decimal ApparelTax { get; set; }
        public decimal NonApparelTax { get; set; }
    }
}
