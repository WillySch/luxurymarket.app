﻿using System;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImportSummary
    {
        public int OfferImportBatchId { get; set; }

        public string BatchNumber { get; set; }

        public DateTimeOffset? StartDate { get; set; }

        public DateTimeOffset? EndDate { get; set; }

        public bool TakeAll { get; set; }

        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public double CompanyFee { get; set; }

        public int TotalProducts { get; set; }

        public int TotalNumberOfUnits { get; set; }

        public double TotalCostOffer { get; set; }

        public double SellerTotalPrice { get; set; }

        public int? StatusId { get; set; }

        public string StatusName { get; set; }

        public int CurrencyID { get; set; }
    }
}
