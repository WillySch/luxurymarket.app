﻿using LuxuryApp.Contracts.Enums;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBatchItemBase
    {
        public int OfferImportBatchItemId { get; set; }

        public int OfferImportBatchId { get; set; }

        public string Sku { get; set; }

        public string VendorSku { get; set; }

        public string BrandName { get; set; }

        public string BusinessName { get; set; }

        public string ModelNumber { get; set; }

        public string ColorCode { get; set; }

        public string MaterialCode { get; set; }

        public string SizeTypeName { get; set; }

        public string OfferCost { get; set; }

        public string RetailPrice { get; set; }

        public string Discount { get; set; }

        public string TotalQuantity { get; set; }

        public string TotalPrice { get; set; }

        public List<OfferImportBatchItemSize> Sizes { get; set; }

    }
}
