﻿using System;

namespace LuxuryApp.Contracts.Models
{
    /// <summary>
    /// Reminder : the order of the properties must be the same with the oder in the OfferProductBatch table
    /// </summary>
    [Serializable]
    public class OfferImportExcelProduct
    {
        public string Brand { get; set; }

        public string Gender { get; set; }

        public string ModelNumber { get; set; }

        public string ColorCode { get; set; }

        public string MaterialCode { get; set; }

        public string VendorSku { get; set; }

        public string CurrencyName { get; set; }

        public string RetailPrice { get; set; }

        public string OfferCost { get; set; }

        public string Discount { get; set; }

        public string TotalQuantity { get; set; }

        public string TotalPrice { get; set; }

        public int ProductIndex { get; set; }

       }

    public class ProductSizeImportModel
    {
        public int ProductIndex { get; set; }

        public string SizeName { get; set; }

        public string Quantity { get; set; }
    }
}