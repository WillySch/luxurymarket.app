﻿
namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBatchItemSizeModel
    {
        public int? SizeTypeSizeId { get; set; }

        public int? SellerSizeMappingId { get; set; }

        public string Size { get; set; }

        public int Quantity { get; set; }
    }
}
