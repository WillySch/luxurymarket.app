﻿using System.ComponentModel.DataAnnotations;
using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models
{
    public class RegistrationRequest
    {
        public int RegistrationRequestID { get; set; }

        [Required]
        public string ContactName { get; set; }

        [Required]
        public string CompanyName { get; set; }

        [Required]
        public CompanyType CompanyType { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string ZipCode { get; set; }

        [Required]
        public string StreetAddress { get; set; }

        public int? StateId { get; set; }

        public int CountryId { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public StatusType StatusType { get; set; }
    }
}
