﻿using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderSellerPackageInsert
    {
        public double BoxSizeLength { get; set; }

        public double BoxSizeWidth { get; set; }

        public double BoxSizeDepth { get; set; }

        public int Quantity { get; set; }

        public double Weight { get; set; }
    }

    public class OrderSellerPackage : OrderSellerPackageInsert
    {
    }

    public class OrderSellerPackageCollection
    {
        public OrderSellerPackageCollection()
        {
            Items = new List<OrderSellerPackage>();
        }
        public IEnumerable<OrderSellerPackage> Items { get; set; }
    }
}
