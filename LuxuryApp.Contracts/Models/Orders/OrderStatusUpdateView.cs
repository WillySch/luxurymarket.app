﻿using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderStatusUpdateView
    {
        public int OrderSellerId { get; set; }

        public int StatusTypeId { get; set; }

        public OrderCustomerType OrderCustomerType { get; set; }
    }
}
