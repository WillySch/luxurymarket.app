﻿namespace LuxuryApp.Contracts.Models.Orders
{
    public class QualityControlFailProductsEmailModel
    {
        public string PoNumber { get; set; }

        public string[] EmailAddresses { get; set; }

        public byte[] QualityControlExcelFileContent { get; set; }
    }
}
