﻿
namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderStatusType
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

}