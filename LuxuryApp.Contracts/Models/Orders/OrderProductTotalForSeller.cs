﻿namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderProductTotalForSeller
    {
        public int OrderSellerProductId { get; set; }

        public int TotalUnits { get; set; }

        public double TotalCost { get; set; }

        public int OrderSellerId { get; set; }

        public SuborderTotalForSeller SuborderSummary { get; set; }
    }

    public class SuborderTotalForSeller
    {
        public int OrderSellerId { get; set; }

        public int TotalUnits { get; set; }

        public int TotalCost { get; set; }

        public int TotalStyles { get; set; }
    }
}
