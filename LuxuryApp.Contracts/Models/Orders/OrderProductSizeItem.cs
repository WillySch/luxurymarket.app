﻿using System;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderProductSizeItem
    {
        public int OrderSellerItemId { get; set; }

        public int OrderSellerProductId { get; set; }

        public int Units { get; set; }

        public int? ConfirmedUnits { get; set; }

        public string SizeName { get; set; }

        public int DisplayUnits { get; set; }
    }
}
