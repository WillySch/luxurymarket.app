﻿namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderSubmitDataView
    {
        public int CartId { get; set; }

        public int PaymentMethodId { get; set; }
    }
}
