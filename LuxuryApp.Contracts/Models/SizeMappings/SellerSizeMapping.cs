﻿
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.SizeMappings
{
    public class SellerSizeMapping
    {
        public SellerSizeMapping()
        {
            Sizes = new List<SellerSizes>();
        }
        public int SellerId { get; set; }

        public SizeType SizeType { get; set; }

        public SizeRegion SizeRegion { get; set; }

        public IEnumerable<SellerSizes> Sizes { get; set; }
    }

    public class SellerSizes
    {
        public int Id { get; set; }

        public string Size { get; set; }

        public string VendorSize { get; set; }
    }
}
