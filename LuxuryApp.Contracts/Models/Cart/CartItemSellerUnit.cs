using System;
using Newtonsoft.Json;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class CartItemSellerUnit
    {
        public CartItemSellerUnitIdentifier CartItemSellerUnitIdentifier { get; set; }
        public string SellerAlias { get; set; }

        [JsonIgnore]
        public int ShippingTaxId { get; set; }

        public string ShipsFromRegion { get; set; }
        public int UnitsCount { get; set; }

        public decimal CustomerUnitPrice { get; set; }
        public decimal LMUnitPrice { get; set; }

        public decimal DiscountPercentage { get; set; }
        public decimal TotalCustomerCost { get; set; }
        public decimal RetailPrice { get; set; }

        public decimal UnitShippingTaxPercentage { get; set; }

        public decimal UnitShippingCost { get; set; }
       
        public decimal TotalShippingCost { get; set; }

        public decimal TotalLandedCost {
            get {
                return TotalCustomerCost + TotalShippingCost;
            }
        }

        public int OfferId { get; set; }

        public bool IsTakeOffer { get; set; }
        public bool IsLineBuy { get; set; }

        public bool Active { get; set; }

        public int ReservedTimeSeconds { get; set; }

        public bool IsReserved { get { return ReservedTimeSeconds > 0; } }

        public bool IsActiveOffer { get; set; }

        public CartItemSellerUnitSize[] SellerUnitSizes { get; set; }

        public CartItemSellerUnit()
        {
            SellerUnitSizes = new CartItemSellerUnitSize[0];
        }
    }

    public class CartItemSellerUnitIdentifier : IEquatable<CartItemSellerUnitIdentifier>
    {
        public int CartId { get; set; }
        public int OfferProductId { get; set; }

        #region equality

        public bool Equals(CartItemSellerUnitIdentifier other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return CartId == other.CartId && OfferProductId == other.OfferProductId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((CartItemSellerUnitIdentifier)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (CartId * 397) ^ OfferProductId;
            }
        }

        public static bool operator ==(CartItemSellerUnitIdentifier left, CartItemSellerUnitIdentifier right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(CartItemSellerUnitIdentifier left, CartItemSellerUnitIdentifier right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}