﻿namespace LuxuryApp.Contracts.Models.Cart
{
    public class CartTotalSummary
    {
        public int CartId { get; set; }

        public int ProductStyles { get; set; }

        public int UnitsCount { get; set; }

        public decimal ProductCosts { get; set; }

        public decimal ShippingCost { get; set; }

        public decimal TotalLandedCost { get; set; }

        public int TotalStyles { get; set; }
    }
}
