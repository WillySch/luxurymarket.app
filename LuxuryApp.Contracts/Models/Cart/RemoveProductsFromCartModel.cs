﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class RemoveProductsFromCartModel
    {
        public int BuyerCompanyId { get; set; }
        public int[] ProductIds { get; set; }

        public RemoveProductsFromCartModel()
        {
            ProductIds = new int[0];
        }
    }
}
