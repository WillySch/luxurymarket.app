﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class OfferHasReservedItem
    {
        public int OfferId { get; set; }

        public int OfferProductId { get; set; }

        public bool IsActiveOffer { get; set; }

        public bool HasReservedItems { get; set; }

    }

    public class OfferHasAvailableQuantity
    {
        public int OfferProductSizeID { get; set; }

        public bool HasAvailableQuantity { get; set; }

        public bool IsActiveOffer { get; set; }

        public int AvailableQuantity { get; set; }

        public int ReservedQuantity { get; set; }
    }
}
