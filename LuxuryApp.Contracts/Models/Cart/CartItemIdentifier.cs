﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class CartItemIdentifier
    {
        public int CartId { get; set; }
        public int BuyerCompanyId { get; set; }
        public int CartItemSellerUnitSizeId { get; set; }
        public int OfferProductSizeId { get; set; }
        public int OfferProductId { get; set; }
        public int OfferId { get; set; }
    }
}
