﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class RemoveCartItemSellerUnitResult
    {
        public CartSummary CartSummary { get; set; }

        public CartItem[] AffectedRemainingCartItems { get; set; }
        public int[] RemovedProductIds { get; set; }

        public RemoveCartItemSellerUnitResult()
        {
            AffectedRemainingCartItems = new CartItem[0];
            RemovedProductIds = new int[0];
        }
    }
}
