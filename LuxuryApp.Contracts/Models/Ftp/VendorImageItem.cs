﻿
namespace LuxuryApp.Contracts.Models.Ftp
{
    public class VendorImageItem
    {
        public int Id { get; set; }

        public string SellerSKU { get; set; }

        public int ProductId { get; set; }

        public string Url { get; set; }

        public string FileName { get; set; }

        public byte[] FileContentBytes { get; set; }

        public int FileSizeBytes { get; set; }

        public bool IsProcessed { get; set; }

        public string Error { get; set; }
    }
}