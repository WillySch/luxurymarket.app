﻿using LuxuryApp.Contracts.Enums;
using System;

namespace LuxuryApp.Contracts.Models.Ftp
{
    public class FtpQueueItem
    {
        public int Id { get; set; }
        public int FtpConfigId { get; set; }
        public int? OrderSellerId { get; set; }
        public string PONumber { get; set; }
        public string FileName { get; set; }
        public byte[] FileContentBytes { get; set; }
        public string LocalDirectory { get; set; }
        public string FileSizeBytes { get; set; }
        public DateTime? StatusDate { get; set; }
        public EdiFileType? EdiType
        {
            get
            {
                EdiFileType? ediType;
                switch (LocalDirectory)
                {
                    case "997ACK":
                        ediType = EdiFileType.Edi997ACK;
                        break;
                    case "856OH":
                        ediType = EdiFileType.Edi856OH;
                        break;
                    case "856ASN":
                        ediType = EdiFileType.Edi856ASN;
                        break;
                    default:
                        ediType = null;
                        break;
                }
                return ediType;


            }

        }
    }
}