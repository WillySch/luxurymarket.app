﻿
namespace LuxuryApp.Contracts.Models.Ftp
{
    public class FtpProductImageItem
    {
        public string Sku { get; set; }

        public string FileName { get; set; }

        public string OriginalFileName { get; set; }

        public int FileSizeBytes { get; set; }

        public bool IsUploaded { get; set; }

        public string Error { get; set; }

        public byte[] FileContentBytes { get; set; }

        public string FilePath { get; set; }
    }
}