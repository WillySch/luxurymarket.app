﻿namespace LuxuryApp.Contracts.Models.Home
{
    public class CustomerServiceRequestModel
    {
        public int StatusID { get; set; }

        public string ContactName { get; set; }

        public string CompanyName { get; set; }

        public string ContactEmail { get; set; }

        public string ContactPhoneNumber { get; set; }

        public int TopicID { get; set; }

        public string QuestionText { get; set; }
    }
}
