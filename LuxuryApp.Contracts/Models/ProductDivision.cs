﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models
{
    public class ProductDivision
    {
        public int ProductId { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
    }
}
