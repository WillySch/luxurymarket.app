﻿using System;

namespace LuxuryApp.Contracts.Models
{
    public abstract class EdiExtractedData
    {
        string _PONumber;
        public string PONumber
        {
            get
            {
                if (_PONumber == null) return null;
                return !(_PONumber.StartsWith("#")) ? $"#{_PONumber }" : _PONumber;
            }
            set { _PONumber = value; }
        }

        public DateTime? InterchangeDateTime { get; set; } //ISA09 & ISA10
    }
}
