﻿using System;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models
{
    public class EdiExtracted865Data : EdiExtractedData
    {
        public List<Edi865Product> Products { get; set; }

        public DateTime? ActualDepartureDate { get; set; }

        public DateTime? EstimatedArrivalDate { get; set; }

        public DateTime? ActualArrivalDate { get; set; }

        public DateTime? CarrierPickupDate { get; set; }

        public DateTime? DeliveryToCustomer { get; set; }
    }

    public class Edi865Product
    {
        public int ProductIndex { get; set; }

        public string PONumber { get; set; }

        public string SKU { get; set; }

        public string Size { get; set; }

        public int? Quantity { get; set; }

        public decimal? NumberCases { get; set; }

        public decimal? Volume { get; set; }
    }
}
