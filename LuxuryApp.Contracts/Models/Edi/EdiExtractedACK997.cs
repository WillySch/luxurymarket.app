﻿using System;

namespace LuxuryApp.Contracts.Models
{
    public class EdiExtractedACK997 : EdiExtractedData
    {
        public bool IsAccepted => FunctionalGroupAcknowledgeCode == "A";

        public DateTimeOffset? PickupDate { get; set; }

        public string FunctionalIdentifierCode { get; set; }   //AK101-   BK   - Booking Request (856)

        public string TransactionSetIdentifierCode { get; set; }   //AK201  -856 Booking Request

        public string TransactionSetControlNumber { get; set; }   //AK202

        public string TransactionSetAcknowledgmentCode { get; set; }   //AK501  - A ACCEPTED  OR E ERROR

        public string FunctionalGroupAcknowledgeCode { get; set; }   // //AK901  - A ACCEPTED  OR E ERROR
    }
}
