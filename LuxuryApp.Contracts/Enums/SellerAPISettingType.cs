﻿namespace LuxuryApp.Contracts.Enums
{
    public enum SellerAPISettingType
    {
        None = 0,
        Coltorti = 1,
        AlDuca = 2,
        FashionDealer = 3,
        Icon = 4,
    }
}
