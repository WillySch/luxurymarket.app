﻿namespace LuxuryApp.Contracts.Enums
{
    public enum OrderStatusTypes
    {
        None = 0,

        PendingConfirmation = 1,

        ConfirmPendingPayment = 2,

        Reject = 3,

        Cancel = 4,

        PartialConfirmation = 5,

        StartShipping = 6,

        BookingRequest = 7,

        BookingConfirmed = 8,

        InShipperWarehouse = 9,

        QualityControlStarted = 10,

        QualityControlRestarted = 11,

        Deleted = 12,

        ClearQualityControl = 13,

        QualityControlFail = 14,

        Paid = 15,

        PaymentPending = 16,

        PaymentFailed = 17,

        InTransit = 18,

        InShipperDestinationWarehouse = 19,

        Delivered = 20
    }
}
