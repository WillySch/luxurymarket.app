﻿namespace LuxuryApp.Contracts.Enums
{
    public enum StatusType
    {
        None,

        Pending,

        Aproved,

        Declined
    }
}
