﻿
namespace LuxuryApp.Contracts.Enums
{
    public enum FtpConfigType
    {
        OrderTracking=1,

        OrderBooking = 2,

        OutBound = 3
    }
}
