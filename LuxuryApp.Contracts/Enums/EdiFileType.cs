﻿namespace LuxuryApp.Contracts.Enums
{
    public enum EdiFileType
    {
        None = 0,

        Edi997ACK = 1,

        Edi856OH = 2,

        Edi856ASN = 3,

    }
}
