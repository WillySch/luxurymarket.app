﻿namespace LuxuryApp.Contracts.Enums
{
    public enum CustomerServiceRequestStatuses
    {
        New = 1,

        InProgress = 2,

        Resolved = 3
    }
}
