﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    public interface ICustomerService
    {
        Task<ApiResult<Customer>> GetCustomerByUserId(int userId);

        Task<ApiResult<bool>> UpdateAccountInfo(AccountInfo model, int userId, DateTimeOffset currentDate);

        Task<ApiResult<Customer>> GetCustomerWithCompaniesAsync(int userId);

        Task<Customer> GetCurrentCustomerCompanies();
    }
}
