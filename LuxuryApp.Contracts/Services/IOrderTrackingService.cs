﻿using System;
using LuxuryApp.Core.Infrastructure.Api.Models;

namespace LuxuryApp.Contracts.Services
{
    public interface IOrderTrackingService
    {
        ApiResult<Byte[]> GetSellerOrderX2Document(int orderSellerId,out string poNumber);
        ApiResult PushSellerOrderX2Document(int orderSellerId);
    }
}
