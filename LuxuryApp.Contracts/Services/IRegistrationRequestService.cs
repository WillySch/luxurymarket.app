﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    /// <summary>
    /// Registration Request Service
    /// </summary>
    public interface IRegistrationRequestService
    {
        /// <summary>
        /// Save Registration Request
        /// </summary>
        /// <param name="model"></param>
        /// <returns>the id of the registration request</returns>
        Task<ApiResult<int>> CreateRequestAsync(RegistrationRequest model);
    }
}
