﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models.Offer;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    /// <summary>
    /// OfferImportService interface
    /// </summary>
    public interface IOfferService
    {
        OfferSearchResponse Search(OfferSearch model);

        Task<IEnumerable<OfferStatusResponse>> GetStatuses();

        OfferLiveSummary GetLiveSummary(OfferLiveSummaryRequest model);

        Task<ApiResult<bool>> IsActiveOffer(int offerId);

        Task<OfferSellerProducts> GetSellerProducts(SellerProductsSearch search, bool overridePageSize = false, int newPageSize = 50);

        Task<OfferSellerSizes> GetSellerSizesForProduct(int sellerProductId);

        Task<ApiResult<OfferImport>> CreateOffer(OfferCreate model, int companyId);

        OfferImport GetProducts(int offerNumber);

        void UpdateOfferEndDate(Offer model);

        void DeleteOffer(int offerId);

        Task<List<ProductOfferSize>> GetOfferProductSizes(OfferProductSizesRequest model);
    }
}
