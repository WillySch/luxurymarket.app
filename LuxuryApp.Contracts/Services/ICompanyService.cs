﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    public interface ICompanyService
    {
        Task<ApiResult<IEnumerable<Company>>> GetCompaniesByCustomerId(int customerId, int companyTypeId = 0);

        Task<ApiResult<Company>> GetCompanyById(int companyId);

        Task<ApiResult<bool>> AcceptTermsAndConditions(int companyId, int userId, DateTimeOffset currentDate);
    }
}
