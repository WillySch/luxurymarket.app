﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models.Shipment;
using LuxuryApp.Contracts.Models.Shipping;

namespace LuxuryApp.Contracts.Services
{
    public interface IShippingService
    {
        ApiResult<Shipping> PreviewShippingForCart(int cartId);
        ApiResult<ShippingSummary> CancelShipment(CancelShipmentModel model);
    }
}
