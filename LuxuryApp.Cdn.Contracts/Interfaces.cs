﻿using LuxuryApp.Cdn.Contracts.Model;
using System.Collections;
using System.Collections.Generic;

namespace LuxuryApp.Cdn.Contracts
{
    public interface ICdnAuthor
    {
        int UserId { get; set; }
        string UserIdSource { get; set; }
    }

    public interface ICdnSettings
    {
        int ConfigurationId { get; set; }
    }

    public interface IFileCdnService<out ICdnAuthor, out ICdnSettings> 
    {
        ICdnAuthor Author { get; }
        ICdnSettings Settings { get; }

        CdnResult Upload(string key, byte[] content);
        CdnResult<string> GetContentUrl(string key);
        CdnResult Remove(string key);

        IEnumerable<CdnResult> Upload(IEnumerable<CdnUploadParameter> parameters);
        IEnumerable<CdnResult<string>> GetContentUrls(IEnumerable<string> keys);
        IEnumerable<CdnResult> Remove(IEnumerable<string> keys);
    }
}
