﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Cdn.Contracts.Model
{
    public class CdnAuthor : ICdnAuthor
    {
        public int UserId { get; set; }
        public string UserIdSource { get; set; }
    }
}
