﻿using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Runtime.Serialization;

namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    [Serializable]
    public class ApiException : Exception
    {
        public ApiResultCode Code { get; protected set; }
        public string MoreInfo { get; set; }
       
        protected ApiException() { }
        protected ApiException(Exception e) : base("", e) { }


        public ApiException(string message) : base(message)
        {
            Code = ApiResultCode.BadRequest;
        }

        public ApiException(string message, Exception innerException) : base(message, innerException)
        {
            Code = ApiResultCode.BadRequest;
        }

        protected ApiException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Code = ApiResultCode.BadRequest;
        }
    }
}
