﻿using System;
using LuxuryApp.Core.Infrastructure.Api.Exceptions;
using LuxuryApp.Core.Infrastructure.Api.Models;

namespace LuxuryApp.Core.Infrastructure.Api.Extensions
{
    public static class ExceptionsExtensions
    {
        public static ErrorResponse ToErrorResponse(this ApiException apiEx)
        {
            return new ErrorResponse
            {
                ErrorCode = apiEx.Code,
                Message = apiEx.Message,
                MoreInfo = apiEx.MoreInfo
            };
        }

        public static ErrorResponse ToErrorResponse(this Exception ex)
        {
            return new ErrorResponse
            {
                ErrorCode = ApiResultCode.InternalError,
                Message = ex.Message,
                MoreInfo = string.Empty
            };
        }
    }
}
