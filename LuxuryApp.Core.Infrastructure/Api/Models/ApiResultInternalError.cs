using LuxuryApp.Core.Infrastructure.Api.Exceptions;
using System;

namespace LuxuryApp.Core.Infrastructure.Api.Models
{
    public class ApiResultInternalError<T> : ApiResult<T>
    {
        public string ErrorId { get; protected set; }

        public ApiResultInternalError(T data, string errorId) : base(data)
        {
            Code = ApiResultCode.InternalError;
            ErrorId = errorId;
            var message = $"Internal error. Contact support an mentiond error id:{errorId}";
            BusinessRulesFailedMessages = new[] {message};
            Exception = new ApiExceptionInternalError(new Exception(message));
        }
    }
}