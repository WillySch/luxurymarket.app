﻿using System.Web;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Principal;

namespace LuxuryApp.Core.Infrastructure.Authorization
{
    public class WebContextProvider : IContextProvider
    {
        public int GetLoggedInUserId()
        {
            var owinContext = HttpContext.Current.GetOwinContext();
            var user = owinContext.Request?.User;
            var userId = user?
                .Identity?
                .GetUserId<int>() ?? 0;

            //var claimIdent = (ClaimsIdentity)user.Identity as ClaimsIdentity;
            //var value = claimIdent != null && claimIdent.HasClaim(c => c.Type == "ValueID") ? claimIdent.FindFirst("ValueID").Value: null;
            //var userId = 0;
            //int.TryParse(value, out userId);

            return userId;
        }
    }
}
