﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using LuxuryApp.Utils.Helpers;
using System.IO;

namespace LuxuryApp.Core.Infrastructure.Providers
{
    public class ImportProvider : MultipartFormDataStreamProvider
    {
        public ImportProvider(string uploadPath)
             : base(uploadPath)
        {

        }

        public override string GetLocalFileName(HttpContentHeaders headers)
        {
            string fileName = headers.ContentDisposition.FileName;
            if (string.IsNullOrWhiteSpace(fileName))
            {
                fileName = Guid.NewGuid().ToString();
            }
            //this is here because Chrome submits files in quotation marks which get treated as part of the filename and get escaped
            return fileName.Replace("\"", string.Empty).AppendTimeStamp();
        }
    }
}
