﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using LuxuryApp.Core.Infrastructure.Api.Models;

namespace LuxuryApp.Core.Infrastructure.Extensions
{
    public static class ApiResultExtesions
    {
        public static IHttpActionResult ToHttpActionResult(this ApiResult apiResult, HttpRequestMessage request, ApiController controller = null)
        {
            if (request == null)
            {
                request = GetFakeHttpRequestMessage();
            }

            if (controller != null && apiResult.IsSuccesfull)
            {
                return new OkNegotiatedContentResult<ApiResult>(apiResult, controller);
            }

            return new ResponseMessageResult(apiResult.ToHttpResponseMessage(request));
        }

        public static HttpResponseMessage ToHttpResponseMessage(this ApiResult apiResult, HttpRequestMessage request)
        {
            return request.CreateResponse((HttpStatusCode)(int)apiResult.Code,
                apiResult);
        }

        public static IHttpActionResult ToHttpActionResult<T>(this ApiResult<T> apiResult, HttpRequestMessage request)
        {
            if (request == null)
            {
                request = GetFakeHttpRequestMessage();
            }

            if (apiResult.IsSuccesfull && typeof(T) == typeof(byte[]))
            {
                return new ResponseMessageResult(
                    request.CreateResponse((HttpStatusCode) (int) apiResult.Code,
                        apiResult.Data));
            }
            return new ResponseMessageResult(
                request.CreateResponse((HttpStatusCode)(int)apiResult.Code,
                apiResult));
        }

        public static IHttpActionResult ToHttpActionResult(this ApiResult apiResult, ApiController controller)
        {
            return apiResult.ToHttpActionResult(controller.Request, controller);
        }

        public static IHttpActionResult ToHttpActionResult<T>(this ApiResult<T> apiResult, ApiController controller)
        {
            return apiResult.ToHttpActionResult(controller.Request);
        }

        public static HttpRequestMessage GetFakeHttpRequestMessage()
        {
            var request = new HttpRequestMessage();
            request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            return request;
        }
    }
}
