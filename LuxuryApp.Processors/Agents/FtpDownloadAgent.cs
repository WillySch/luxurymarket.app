﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.Infrastructure.Logging;
using System;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.NotificationService;
using LuxuryApp.Contracts.Enums;
using System.IO;
using System.Net;
using System.Collections.Generic;
using LuxuryApp.Contracts.Models.Ftp;

namespace LuxuryApp.Processors.Agents
{
    public class FtpDownloadAgent : IFtpDownloadAgent
    {
        private readonly IFtpRepository _ftpRepository;
        private readonly Func<INotificationHandler<EmailModel>> _emailServiceFactory;
        private ILogger _logger = new LuxuryLogger();

        public FtpDownloadAgent(IFtpRepository ftpRepository, Func<INotificationHandler<EmailModel>> emailServiceFactory, ILogger logger)
        {
            _ftpRepository = ftpRepository;
            _emailServiceFactory = emailServiceFactory;
            _logger = logger;
        }

        public void DownloadFtpItems(string localDestinationPath)
        {
            var ftpConfigs = _ftpRepository.GetAllFtpConfigs(FtpConfigType.OutBound);

            if (ftpConfigs.Length == 0)
            {
                _logger.Information("No configuration found for download files.");
                return;
            }
            foreach (var configItem in ftpConfigs)
            {
                configItem.Host = (configItem.Host.EndsWith("/") ? configItem.Host : configItem.Host + "/");
                configItem.UploadDirectory = (configItem.UploadDirectory.EndsWith("/") ? configItem.UploadDirectory : configItem.UploadDirectory + "/");
                var url = configItem.Host + configItem.UploadDirectory;
                var credentials = new NetworkCredential(configItem.Username, configItem.Password);

                DownloadFtpDirectory(url, credentials, localDestinationPath, configItem.Id, localDestinationPath);
            }
        }

        //option b: try  WinSCP .NET assembly  :   session.GetFiles("/directory/to/download/*", @"C:\target\directory\*").Check()
        void DownloadFtpDirectory(string url, NetworkCredential credentials, string localPath, int ftpConfigId, string localDestinationPath)
        {
            FtpWebRequest request = CreateFtpWebRequest(url, credentials);
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

            List<string> lines = new List<string>();

            using (FtpWebResponse listResponse = (FtpWebResponse)request.GetResponse())
            using (Stream listStream = listResponse.GetResponseStream())
            using (StreamReader listReader = new StreamReader(listStream))
            {
                while (!listReader.EndOfStream)
                {
                    lines.Add(listReader.ReadLine());
                }
            }

            foreach (string line in lines)
            {
                string[] tokens = line.Split(new[] { ' ' }, 9, StringSplitOptions.RemoveEmptyEntries);
                string name = tokens[3];
                string permissions = tokens[2];
                var date = Convert.ToDateTime(tokens[0] + " " + tokens[1]);
                string localFilePath = Path.Combine(localPath, name);
                string fileUrl = url + name;

                if (permissions == "<DIR>")
                {
                    if (!Directory.Exists(localFilePath))
                    {
                        Directory.CreateDirectory(localFilePath);
                    }

                    DownloadFtpDirectory(fileUrl + "/", credentials, localFilePath, ftpConfigId, localDestinationPath);
                }
                else
                {
                    FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                    downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                    downloadRequest.Credentials = credentials;
                    //var fileName = localFilePath.AppendTimeStamp();

                    try
                    {
                        long fileLength = 0;
                        using (FtpWebResponse downloadResponse = (FtpWebResponse)downloadRequest.GetResponse())
                        using (Stream sourceStream = downloadResponse.GetResponseStream())
                        {
                            using (Stream targetStream = File.Create(localFilePath))
                            {
                                byte[] buffer = new byte[10240];
                                int read;
                                while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    targetStream.Write(buffer, 0, read);
                                    fileLength = targetStream.Length;
                                }
                            }
                        }
                        var fileName = Path.GetFileName(localFilePath);
                        var remoteDirectory = localFilePath.Replace(localDestinationPath + "\\", "").Replace("\\" + fileName, "");
                        SaveFileInDb(fileName, ftpConfigId, fileLength, remoteDirectory, date);

                        DeleteFile(fileUrl, credentials);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error($"Ftp download for {fileUrl} failed. Ex: {ex.ToString()}");
                    }
                }
            }
        }

        private void DeleteFile(string fileUrl, NetworkCredential credentials)
        {
            FtpWebRequest reqFTP = (FtpWebRequest)WebRequest.Create(fileUrl);
            reqFTP.Method = WebRequestMethods.Ftp.DeleteFile;
            reqFTP.Credentials = credentials;
            reqFTP.GetResponse().Close();
        }
        private FtpWebRequest CreateFtpWebRequest(string url, NetworkCredential credentials)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
            request.KeepAlive = true;
            request.UsePassive = false;
            request.UseBinary = true;
            request.Credentials = credentials;

            return request;
        }

        private void SaveFileInDb(string fileName, int ftpConfigId, long fileLength, string localFilePath, DateTime? date)
        {
            var currentDate = date ?? DateTimeOffset.UtcNow;
            var ftpQueueItem = new FtpQueueItem
            {
                FtpConfigId = ftpConfigId,
                FileName = fileName,
                LocalDirectory = localFilePath
            };

            _ftpRepository.EnqueueItem(ftpQueueItem);
        }
    }
}
