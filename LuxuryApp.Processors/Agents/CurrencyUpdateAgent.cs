﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using Newtonsoft.Json;
using Nito.AsyncEx;


namespace LuxuryApp.Processors.Agents
{
    public class CurrencyUpdateAgent : ICurrencyUpdateAgent
    {
        private readonly ICurrencyRepository _currencyRepository;

        public CurrencyUpdateAgent(ICurrencyRepository currencyRepository)
        {
            _currencyRepository = currencyRepository;
        }

        public void UpdateCurrencies()
        {
            var currencies = _currencyRepository.GetAllCurrencies();
            var fixerCurrenxy = GetFixerCurrencyRates("EUR", currencies.Select(x => x.Name).ToArray());
            var newCurrencies = FixerCurrency2Currencies(fixerCurrenxy, currencies);
            _currencyRepository.UpdateCurrencyRates(newCurrencies);
        }

        private Currency[] FixerCurrency2Currencies(FixerCurrency fixerCurrency, Currency[] currentCurrencies)
        {

            //////////////////////////////////////////////////
            //// This is an override for a quick fix in order to setup the service to 
            //// update the EUR - Dollar exchange rate
            //////////////////////////////////////////////////
            var dollarToEurXRate = fixerCurrency.Rates.First(x => x.Key.Equals("USD")).Value;
            var currencyToUpdate = currentCurrencies.First(x => x.Name.Equals("EUR"));
            currencyToUpdate.Rate = dollarToEurXRate;

            return new Currency[] { currencyToUpdate };

            ////////////////////////////////////////////////////////////

            //var result =
            //(from currentCurrency in currentCurrencies
            // join fi in fixerCurrency.Rates on currentCurrency.Name equals fi.Key
            // select new Currency
            // {
            //     CurrencyId = currentCurrency.CurrencyId,
            //     Name = currentCurrency.Name,
            //     Rate = fi.Value,
            // }).ToArray();

            //return result;
        }

        private FixerCurrency GetFixerCurrencyRates(string baseCurrency, string[] currencies)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://api.fixer.io");
            var uriQuery = $"latest?base={baseCurrency}&symbols={string.Join(",", currencies)}";
            var json = AsyncContext.Run(() => client.GetStringAsync(uriQuery));
            var fixerCurrency = JsonConvert.DeserializeObject<FixerCurrency>(json);
            return fixerCurrency;
        }

        private class FixerCurrency
        {
            [JsonProperty("base")]
            public string Base { get; set; }

            [JsonProperty("rates")]
            public Dictionary<string, decimal> Rates { get; set; }
        };
    }
}
