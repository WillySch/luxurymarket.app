﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.Infrastructure.Logging;
using System;
using LuxuryApp.Contracts.Enums;
using System.IO;
using LuxuryApp.Contracts.Models.Ftp;
using LuxuryApp.Contracts.Models;
using System.Linq;
using System.Collections.Generic;
using LuxuryApp.Utils.Helpers;
using System.Data;
using Gs1.Vics.Contracts.Factories;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.NotificationService;

namespace LuxuryApp.Processors.Agents
{
    public class EdiProcessorAgent : IEdiProcessorAgent
    {
        private readonly IFtpRepository _ftpRepository;
        private readonly IEdiDeserializeServiceFactory _ediDeserializeServiceFactory;
        private readonly IEdiProcessorRepository _ediProcessorRepository;

        private readonly Func<INotificationHandler<EmailModel>> _emailServiceFactory;
        private ILogger _logger;

        public EdiProcessorAgent(IFtpRepository ftpRepository,
                                 Func<INotificationHandler<EmailModel>> emailServiceFactory,
                                 IEdiDeserializeServiceFactory ediDeserializeServiceFactory,
                                 IEdiProcessorRepository ediProcessorRepository,
                                 ILogger logger)
        {
            _ftpRepository = ftpRepository;
            _emailServiceFactory = emailServiceFactory;
            _logger = logger;
            _ediDeserializeServiceFactory = ediDeserializeServiceFactory;
            _ediProcessorRepository = ediProcessorRepository;
        }

        public void ProcessItemsFromQueue(string localDirectoryPath, int? createdBy)
        {
            var ftpQueueItems = _ftpRepository.GetItemsToProcess(FtpDirectionType.Outbound).OrderBy(x => x.StatusDate).OrderBy(x => (int)x.EdiType);
            _logger.Information($"ProcessItemsFromQueue  found {ftpQueueItems.Count()} items");

            foreach (var ci in ftpQueueItems)
            {
                ProcessItem(ci, localDirectoryPath, createdBy);
            }
        }

        private void ProcessItem(FtpQueueItem item, string localDirectoryPath, int? createdBy)
        {
            _logger.Information($"Start processing  FtpQueuItemID = {item.Id}");

            var itemPath = Path.Combine(localDirectoryPath, item.LocalDirectory, item.FileName);

            if (string.IsNullOrWhiteSpace(itemPath) || !File.Exists(itemPath))
            {
                _ftpRepository.SetItemUploadStatus(item.Id, FtpUploadStatus.Failed, "File not found.");
                return;
            }

            try
            {
                if (item.EdiType == EdiFileType.Edi997ACK)
                {
                    var items = _ediDeserializeServiceFactory.Deserialize<EdiExtractedACK997>(itemPath).ToList();
                    foreach (var item997 in items)
                    {
                        _ediProcessorRepository.InsertEdi997Items(item.Id, (int)item.EdiType, item997, createdBy);
                    }
                }
                if (item.EdiType == EdiFileType.Edi856OH || item.EdiType == EdiFileType.Edi856ASN)
                {
                    var items = _ediDeserializeServiceFactory.Deserialize<EdiExtracted865Data>(itemPath).ToList();
                    var products = GetProductsDataTable(items);
                    var first = items.FirstOrDefault();

                    _ediProcessorRepository.InsertEdi856Items(products, item.Id, (int)item.EdiType
                                                             , first.InterchangeDateTime
                                                             , first.ActualDepartureDate
                                                             , first.EstimatedArrivalDate
                                                             , first.ActualArrivalDate
                                                             , first.CarrierPickupDate
                                                             , first.DeliveryToCustomer
                                                             , createdBy);

                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Error at processing  FtpQueuItemID = {item.Id} .Error:{ex.ToString()}");
                _ftpRepository.SetItemUploadStatus(item.Id, FtpUploadStatus.Failed, $"{ex.ToString()}");
                return;
            }

            _logger.Information($"End processing  FtpQueuItemID = {item.Id}");

        }


        private DataTable GetProductsDataTable(List<EdiExtracted865Data> items)
        {
            var result = items.SelectMany(x => x.Products).ToList();

            return result.ConvertToDataTable();
        }
    }
}
