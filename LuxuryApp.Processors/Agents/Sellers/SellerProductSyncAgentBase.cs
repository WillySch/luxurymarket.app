﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.Infrastructure.Logging;
using System;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Agents.Sellers.Icon
{
    public abstract class SellerProductSyncAgentBase<TApiSettings> : ISellerProductSyncAgent<TApiSettings>
        where TApiSettings : APISettings
    {
        private Func<ILogger> logger;
        private Func<ISellerProductSyncRepository> sellerProductSyncRepository;
        private Func<ISellerAPISettingRepository> sellerAPISettingRepository;
        private int _pageSize = 500;
        private Func<ISellerApiSettingService> _sellerApiSettingService;

        #region Constructor         

        public int PageSize { get => _pageSize; set => _pageSize = value; }
        public Func<ILogger> Logger { get => logger; set => logger = value; }
        public Func<ISellerProductSyncRepository> SellerProductSyncRepository { get => sellerProductSyncRepository; set => sellerProductSyncRepository = value; }
        public Func<ISellerAPISettingRepository> SellerAPISettingRepository { get => sellerAPISettingRepository; set => sellerAPISettingRepository = value; }
        public Func<ISellerApiSettingService> SellerApiSettingService { get => _sellerApiSettingService; set => _sellerApiSettingService = value; }

        #endregion

        #region Public

        public abstract Task<int> SyncProducts(APISettings settings);

        public abstract Task<int> SyncProductStock(APISettings settings);

        public APISettings GetSettings(SellerAPISettingType type)
        {
            var settings = SellerApiSettingService().GetSellerApiSettings(type).Result;
            return settings;
        }
        #endregion
    }
}
