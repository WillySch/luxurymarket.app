﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Models.SellerProductSync.FashionDealer;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.Processors.Helpers;
using LuxuryApp.Utils.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Agents.Sellers
{
    public class FashionDealerProductSyncAgent : ISellerProductSyncAgent
    {
        #region Proprietes

        private readonly Func<ILogger> _logger;
        private readonly Func<ISellerProductSyncRepository> _sellerProductSyncRepository;
        private FashionDealerCategoriesAgent _sellerCategoriesAgent;
        private readonly int _commandTimeoutSeconds = 0;

        #endregion

        #region Constructor

        public FashionDealerProductSyncAgent(Func<ILogger> logger,
                                             Func<ISellerProductSyncRepository> sellerProductSyncRepository,
                                             FashionDealerCategoriesAgent sellerCategoriesAgent
                                             )
        {
            _logger = logger;
            _sellerProductSyncRepository = sellerProductSyncRepository;
            _sellerCategoriesAgent = sellerCategoriesAgent;
        }

        #endregion

        #region Public Methods

        public async Task<int> SyncProducts(APISettings settings)
        {
            var mainBatchGuid = Guid.NewGuid();

            _logger().Information($"Starting upload from queue batch:{mainBatchGuid}");

            var products = await GetProducts(settings, mainBatchGuid);

            return 1;//todo change to return the batchid or the number of items inserted!
        }

        public async Task<int> SyncProductStock(APISettings settings)
        {
            return 1;
        }

        #endregion

        #region Privates

        private async Task<List<ProductSyncItemBase>> GetProducts(APISettings settings, Guid mainBatchGuid)
        {
            //get categories
            var categoriesList = await GetCategories(settings);

            var listMapped = new List<ProductSyncItemBase>();
            var stop = false;
            var currentStep = 1;
            while (!stop)
            {
                _logger().Information("Authentication Phase for step {0}", currentStep);
                var httpRequest = GetWebRequest(settings, currentStep);
                var httpResponse = GetWebResponse(httpRequest);
                var fashionDealerProductModel = ProcessWebResponse(httpResponse, currentStep);

                if (!fashionDealerProductModel.Status.Equals("ok"))
                {
                    _logger().Error($"Unable to authenticate to vendor API at step {0}!!!. Process is stopped!!! status={fashionDealerProductModel.Status}", currentStep);
                    return null;
                }

                var listMappedPerStep = GetMappedProducts(categoriesList, fashionDealerProductModel.Data.Products);
                var listMappedImagePerStep = GetMappedImages(fashionDealerProductModel.Data.Products);

                _logger().Information("Mapped with success. Starting db insert for step {0}", currentStep);
                var resultProducts = await InsertProductsAsync(listMappedPerStep, listMappedImagePerStep, mainBatchGuid, settings.SellerID);

                if (resultProducts)
                {
                    _logger().Information("Products inserted into database for step {0}", currentStep);
                }
                else
                {
                    _logger().Information("Products failed into database for step {0}", currentStep);
                }

                listMapped.AddRange(listMappedPerStep);

                _logger().Information("Starting inserting stock for step {0}", currentStep);
                var resultInventory = await InsertStockAsync(fashionDealerProductModel.Data.Products, settings.SellerID);
                if (resultInventory)
                {
                    _logger().Information("Products stock inserted into database for step {0}", currentStep);
                }
                else
                {
                    _logger().Information("Products stock failed into database for step {0}", currentStep);
                }

                if (fashionDealerProductModel.NextStep == 0)
                {
                    stop = true;
                }
                else
                {
                    currentStep = fashionDealerProductModel.NextStep;
                }
            }

            return listMapped;
        }

        private async Task<List<Category>> GetCategories(APISettings settings)
        {
            var result = await _sellerCategoriesAgent.SyncCategories(settings);
            return result;
        }

        private HttpWebRequest GetWebRequest(APISettings settings, int currentStep)
        {
            var address = settings.BaseAddress + WebConfigs.FashionDealerGetProductsMethod;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(address);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //merchantId = username from settings
                //token = passsword from settings
                string json = "{\"merchantId\":\"" + settings.Username + "\"," +
                              "\"token\":\"" + settings.Password + "\"," +
                              "\"step\": " + currentStep + "}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            return httpWebRequest;
        }

        private HttpWebResponse GetWebResponse(HttpWebRequest httpWebRequest)
        {
            return (HttpWebResponse)httpWebRequest.GetResponse();
        }

        private FashionDealerProductsList ProcessWebResponse(HttpWebResponse httpResponse, int currentStep)
        {
            var fashionDealerProductModel = new FashionDealerProductsList();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                _logger().Information("Authentication Phase Complete. Starting product sync for step {0}", currentStep);
                try
                {
                    fashionDealerProductModel = JsonConvert.DeserializeObject<FashionDealerProductsList>(result);
                    _logger().Information("Done deserialiazation JSON. Start mapping products for step {0}", currentStep);
                }
                catch (Exception ex)
                {
                    _logger().Error(String.Format("Failed to process step {0}", currentStep), ex);
                }
            }
            return fashionDealerProductModel;
        }

        private List<ProductSyncItemBase> GetMappedProducts(List<Category> categoryList, List<FashionDealerProduct> fashionDealerProductsList)
        {
            var listMappedPerStep = new List<ProductSyncItemBase>();
            foreach (var fashionDealerProduct in fashionDealerProductsList)
            {
                var mappedProduct = FashionDealerMapping.MapProductsList(categoryList, fashionDealerProduct);
                listMappedPerStep.Add(mappedProduct);
            }
            return listMappedPerStep;
        }

        private List<ProductSyncImageItem> GetMappedImages(List<FashionDealerProduct> fashionDealerProductsList)
        {
            var listImagesMapped = fashionDealerProductsList.SelectMany(x => x.MapImage()).GroupBy(x => new { x.SellerSKU, x.ImageUrl }).Select(x => x.First()).ToList();
            return listImagesMapped;
        }

        private async Task<bool> InsertProductsAsync(List<ProductSyncItemBase> listMappedPerStep, List<ProductSyncImageItem> listMappedImagePerStep, Guid mainBatchGuid, int sellerId)
        {
            var listToInsert = listMappedPerStep.ConvertToDataTable();
            var imageListToInsert = listMappedImagePerStep.ConvertToDataTable();
            var result = await _sellerProductSyncRepository().InsertProductsAsync(listToInsert, imageListToInsert, mainBatchGuid, sellerId);
            return result > 0;
        }

        private async Task<bool> InsertStockAsync(List<FashionDealerProduct> productsList, int sellerId)
        {
            var stockItemDataTable = productsList.Select(x => new { SellerSKU = x.SkuParent, SellerSize = x.Size, Quantity = x.Qty }).Distinct().ToList().ConvertToDataTable();

            var result = await _sellerProductSyncRepository().SaveInventoryAsync(stockItemDataTable, sellerId, _commandTimeoutSeconds);

            return result > 0;
        }

        #endregion
    }
}
