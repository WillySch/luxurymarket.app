﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.Infrastructure.Api.Helpers;
using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.Utils.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Agents.Sellers.Icon
{
    //todo: template design pattern?
    public class IconProductSyncAgent : ISellerProductSyncAgent
    {
        private readonly Func<ILogger> _logger;
        private readonly Func<ISellerProductSyncRepository> _sellerProductSyncRepository;
        private readonly Func<ISellerAPISettingRepository> _sellerAPISettingRepository;
        private const int _pageSize = 500;
        private const string application_code = "38F276D1-A694-47FC-9F86-4AD5A3E72F44"; //todo - move this in appsettings db
        private const string _IconDefaultSizeRegion = "US";
        #region Constructor         

        public IconProductSyncAgent(Func<ISellerAPISettingRepository> sellerAPISettingRepository,
                                     Func<ISellerProductSyncRepository> sellerProductSyncRepository, Func<ILogger> logger)
        {
            _logger = logger;
            _sellerAPISettingRepository = sellerAPISettingRepository;
            _sellerProductSyncRepository = sellerProductSyncRepository;
        }

        #endregion

        #region Public

        public async Task<int> SyncProducts(APISettings settings)
        {
            var mainBatchID = Guid.NewGuid();

            _logger().Information($"SyncProducts: Starting upload from queue batch:{Guid.NewGuid()}");

            var accessToken = await Authenticate(settings);
            if (string.IsNullOrWhiteSpace(accessToken))
            {
                _logger().Error("Token was not provided! Sync aboard");
                return 0;
            }
            var lastSyncDate = await GetLastSyncDateAsync(settings.SellerID);

            await InsertSyncProducts(settings, lastSyncDate, mainBatchID, accessToken);

            _logger().Trace("Products inserted into database");

            return 1;//todo change to return the batchid or the number of items inserted!
        }

        public async Task<int> SyncProductStock(APISettings settings)
        {
            _logger().Information($"{settings.SellerType.ToString()} - SyncProductsStock: Starting upload stocks");

            var accessToken = await Authenticate(settings);
            if (string.IsNullOrWhiteSpace(accessToken))
            {
                _logger().Error($"{settings.SellerType.ToString()} Token was not provided! Sync aboard");
                return 0;
            }

            var currentPage = 0;
            var responseCount = _pageSize;
            var totalResponseCount = 0;
            while (responseCount > 0 && responseCount == _pageSize)
            {
                try
                {
                    currentPage++;
                    var sellerSkus = await GetSellerSkusAsync(settings.SellerID, currentPage);
                    responseCount = sellerSkus.Count;
                    totalResponseCount += responseCount;
                    if (responseCount > 0)
                        await InsertStock(settings, sellerSkus, accessToken);
                }
                catch (AggregateException ex)
                {
                    _logger().Error($"SyncProductStock AggregateException- at page {currentPage} - error: {ex.ToString()}");
                }
                catch (Exception ex)
                {
                    _logger().Error($"SyncProductStock - at page {currentPage} - error: {ex.ToString()}");

                }
            }
            _logger().Information("Total items inventory from seller: {0}.", totalResponseCount);
            return totalResponseCount;
        }

        #endregion

        #region Privates 

        private async Task<string> Authenticate(APISettings settings)
        {
            var dataRequest = new
            {
                application_code = application_code,
                data = new
                {
                    email = settings.Username,
                    password = settings.Password
                }
            };

            var authResponse = await GetApiResponse<IconAuthentication>(settings, "user/login", dataRequest);
            return authResponse?.SessionToken;
        }

        private async Task InsertSyncProducts(APISettings settings, DateTimeOffset? lastSyncDate, Guid mainBatchID, string token)
        {
            var currentPage = 0;
            var responseCount = _pageSize;
            var totalResponseCount =0;
            while (responseCount > 0 && responseCount == _pageSize)
            {
                try
                {
                    currentPage++;
                    var dataRequest = new
                    {
                        session_token = token,
                        last_request_date = lastSyncDate,
                        data = new
                        {
                            CurrentPage = _pageSize,
                            PageCount = currentPage
                        }
                    };

                    var syncProducts = await GetApiResponse<List<IconProductItem>>(settings, "APIProducts/getproducts", dataRequest);
                    responseCount = syncProducts?.Count() ?? 0;
                    totalResponseCount += responseCount;
                    if (syncProducts?.Any() == true)
                    {
                        var imageList = syncProducts.Where(x => !string.IsNullOrWhiteSpace(x.Images) && !string.IsNullOrWhiteSpace(x.SKU)).
                                                     Select(x => x.ToImage()).Distinct().ToList();
                        var imageDt = imageList.ConvertToDataTable();

                        var productsList = syncProducts.Where(x => !string.IsNullOrWhiteSpace(x.SKU)).Select(x => x.ToProduct()).Distinct().ToList();
                        productsList.ForEach(x => x.SizeTypeDescription = !string.IsNullOrWhiteSpace(x.SizeTypeDescription) ? x.SizeTypeDescription : _IconDefaultSizeRegion);
                        var productsDt = productsList.ConvertToDataTable();

                        productsDt.Columns.Remove("Images");
                        productsDt.Columns.Remove("SKU");

                        await _sellerProductSyncRepository().InsertProductsAsync(productsDt, imageDt, mainBatchID, settings.SellerID);

                        _logger().Information("Step {0} inserted with success.", responseCount);
                    }

                }
                catch (AggregateException ex)
                {
                    _logger().Error($"InsertSyncProducts AggregateException {ex.ToString()}");
                }
                catch (Exception ex)
                {
                    _logger().Error($"InsertSyncProducts  {ex.ToString()}");
                }
            }
            _logger().Information("Total items from seller: {0}.", totalResponseCount);
            }

        private async Task<int> InsertStock(APISettings settings, List<string> sellerSkus, string token)
        {
            var dataRequest = new
            {
                session_token = token,
                data = new
                {
                    SKUs = sellerSkus.ToArray()
                }
            };

            var syncInventory = await GetApiResponse<List<IconInventoryResponse>>(settings, "APIProducts/getinventory", dataRequest);
            if (syncInventory?.Any() == true)
            {
                var stockItemDataTable = syncInventory.Select(x=>x.ToInventorySize()).Distinct().ToList().ConvertToDataTable();
                var result = await _sellerProductSyncRepository().SaveInventoryAsync(stockItemDataTable, settings.SellerID, 0);
                return result;
            }
            return 0;
        }

        private async Task<List<string>> GetSellerSkusAsync(int sellerId, int currentPage)
        {
            var sellerSkusList = await _sellerProductSyncRepository().GetSellerSkusAsync(sellerId, _pageSize, currentPage);

            return sellerSkusList;
        }

        private async Task<T> GetApiResponse<T>(APISettings settings, string requestRoute, object parameterRequest) where T : class
        {
            var apiRoute = UriHelper.CombineUriToString(settings.BaseAddress, requestRoute);
            var response = await ClientHelper.PostContentAsync<IconApiResponse>(null, new Uri(settings.BaseAddress), requestRoute, parameterRequest);
            if (response == null || response.HasError)
            {
                _logger().Error($"Error received from Icon call to { UriHelper.CombineUriToString(settings.BaseAddress, requestRoute) }; request-data={parameterRequest.ToString()}; response={response?.Errors?.ToString()}");
                return null;
            }

            List<string> deserializeErrorList = new List<string>();
            var responseT = JsonConvert.DeserializeObject<T>(response.Response?.ToString(), new JsonSerializerSettings
            {
                Error = (sender, errorEventArgs) =>
                   {
                       deserializeErrorList.Add(errorEventArgs.ErrorContext.Error.Message);
                       errorEventArgs.ErrorContext.Handled = true;
                   }
            });

            if (deserializeErrorList.Any())
            {
                _logger().Error($"Error deserialize json received from  from Icon call to {apiRoute}; request-data={parameterRequest.ToString()}; response={response.Response?.ToString()}; errors={string.Join(";", deserializeErrorList)}");
                return null;
            }
            return responseT;
        }

        private async Task<DateTimeOffset?> GetLastSyncDateAsync(int sellerId)
        {
            var date = await _sellerProductSyncRepository().GetLastSyncDateAsync(sellerId);
            return date;
        }


        #endregion
    }
}
