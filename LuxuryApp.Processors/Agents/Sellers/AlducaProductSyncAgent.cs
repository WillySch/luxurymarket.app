﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.Processors.Helpers;
using LuxuryApp.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace LuxuryApp.Processors.Agents.Sellers
{
    public class AlducaProductSyncAgent : ISellerProductSyncAgent
    {
        private readonly Func<ILogger> _logger;
        private readonly Func<ISellerProductSyncRepository> _sellerProductSyncRepository;
        private readonly Func<ISellerAPISettingRepository> _sellerAPISettingRepository;
        private readonly int _commandTimeoutSeconds = 0;
        private const int _stepSize = 500;

        #region Constructor         

        public AlducaProductSyncAgent(Func<ISellerAPISettingRepository> sellerAPISettingRepository,
                                     Func<ISellerProductSyncRepository> sellerProductSyncRepository, Func<ILogger> logger)
        {
            _logger = logger;
            _sellerAPISettingRepository = sellerAPISettingRepository;
            _sellerProductSyncRepository = sellerProductSyncRepository;
        }

        #endregion

        #region Public

        public async Task<int> SyncProducts(APISettings settings)
        {
            var mainBatchID = Guid.NewGuid();

            _logger().Information($"Starting upload from queue batch:{Guid.NewGuid()}");

            var products = GetProducts(settings);

            _logger().Information("Mapped with success. Starting db insert");

            var resultProducts = await InsertProductsAsync(products, mainBatchID, settings.SellerID);

            _logger().Trace("Products inserted into database");

            var resultInventory = await InsertStock(products, settings.SellerID);

            return 1;//todo change to return the batchid or the number of items inserted!
        }

        public async Task<int> SyncProductStock(APISettings settings)
        {
            return await Task.FromResult<int>(0);
            //throw new NotImplementedException();
        }

        #endregion

        #region Privates 

        private List<AlducaProduct> GetProducts(APISettings settings)
        {
            var _url = settings.BaseAddress + "/Services.asmx";
            var _action = settings.BaseAddress + "/GetSku4Platform";

            XmlDocument soapEnvelopeXml = CreateSoapEnvelope(settings);
            HttpWebRequest webRequest = CreateWebRequest(_url, _action);
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            // begin async call to web request.
            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            // suspend this thread until call is complete. You might want to do something usefull here like update your UI.
            asyncResult.AsyncWaitHandle.WaitOne();

            using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
            {
                var productsList = ProcessWebResponse(webResponse);
                _logger().Information("Done deserialiazation XML. Start mapping products.");
                return productsList;
            }
        }

        public async Task<bool> InsertProductsAsync(List<AlducaProduct> productsList, Guid mainBatchID, int sellerId)
        {
            //  var listImagesMapped = new List<ProductSyncImageItem>();
            var listMapped = productsList.GroupBy(x => x.ProductID).Select(x => x.First()).Select(x => x.Map()).ToList();
            var listImagesMapped = productsList.SelectMany(x => x.ImageMap()).GroupBy(x => new { x.SellerSKU, x.ImageUrl }).Select(x => x.First()).ToList();

            var step = 0;
            var listToInsert = listMapped.ChunkBy(_stepSize);

            foreach (var list in listToInsert)
            {
                _logger().Information("Start insert step {0}", step);

                // process to break into user type data tables
                var productImages = (from product in list
                                     join image in listImagesMapped
                                     on product.SellerSKU equals image.SellerSKU
                                     select image).Distinct();
                var imageDt = productImages.ToList().ConvertToDataTable();

                var productsDt = list.ConvertToDataTable();
                productsDt.Columns.Remove("ImageUrls");
                await _sellerProductSyncRepository().InsertProductsAsync(productsDt, imageDt, mainBatchID, sellerId);

                _logger().Information("Step {0} insertted with success.", step++);
            }

            return true;
        }

        private Task<int> InsertStock(List<AlducaProduct> productsList, int sellerId)
        {
            //var stockItemDataTable = productsList.Select(x => new { SellerSKU = x.ProductID, SellerSize = x.Size, Quantity = x.Stock }).Distinct().ToList().ConvertToDataTable();

            //var result = await _sellerProductSyncRepository().SaveInventoryAsync(stockItemDataTable, sellerId, _commandTimeoutSeconds);

            //return result;
            return Task.FromResult(0);
        }

        private async Task<List<string>> GetSellerSkusAsync(int sellerId)
        {
            var sellerSkusList = await _sellerProductSyncRepository().GetSellerSkusAsync(sellerId, null, null);

            return sellerSkusList;
        }

        private static XmlDocument CreateSoapEnvelope(APISettings settings)
        {
            XmlDocument soapEnvelop = new XmlDocument();

            var xmlString = @"<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
                                                   xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" 
                                                   xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">
                                    <soap12:Body>
                                        <GetSku4Platform xmlns=""" + settings.BaseAddress + @""">
                                            <Username>" + settings.Username + @"</Username>
                                            <Password>" + settings.Password + @"</Password>
                                        </GetSku4Platform>
                                    </soap12:Body>
                                </soap12:Envelope>";

            soapEnvelop.LoadXml(xmlString);
            return soapEnvelop;
        }

        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }

        private List<AlducaProduct> ProcessWebResponse(WebResponse webResponse)
        {
            List<AlducaProduct> itemList = null;
            using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
            {
                _logger().Trace(String.Format("Sync done. Starting XML Deserialization "));

                string soapResult = rd.ReadToEnd();

                XDocument doc = XDocument.Parse(soapResult);
                IEnumerable<XElement> responses = doc.Descendants("SkuStok");
                XmlSerializer serializer = new XmlSerializer(typeof(AlducaProduct));
                itemList = new List<AlducaProduct>();
                foreach (XElement response in responses)
                {
                    StringReader reader = new StringReader(response.ToString());
                    itemList.Add((AlducaProduct)serializer.Deserialize(reader));
                }
            }
            return itemList;
        }

        #endregion
    }
}
