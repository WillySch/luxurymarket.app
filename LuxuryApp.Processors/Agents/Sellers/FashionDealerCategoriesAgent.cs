﻿using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Models.SellerProductSync.FashionDealer;
using LuxuryApp.Core.Infrastructure.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Agents.Sellers
{
    public class FashionDealerCategoriesAgent
    {
        #region Proprietes

        private readonly Func<ILogger> _logger;

        #endregion

        #region Constructor

        public FashionDealerCategoriesAgent(Func<ILogger> logger)
        {
            _logger = logger;
        }

        #endregion

        #region Public Methods

        public async Task<List<Category>> SyncCategories(APISettings settings)
        {
            _logger().Information("Starting sync categories");

            var categories = await GetCategories(settings);

            return categories;
        }

        #endregion

        #region Privates Methods

        private async Task<List<Category>> GetCategories(APISettings settings)
        {
            _logger().Information("Categories Authentication Phase");
            var httpRequest = GetWebRequest(settings);
            var httpResponse = GetWebResponse(httpRequest);
            var fashionDealerCategoriesModel = ProcessWebResponse(httpResponse);

            if (!fashionDealerCategoriesModel.Status.Equals("ok"))
            {
                _logger().Error("Unable to authenticate to vendor API for categories");
                return null;
            }

            return fashionDealerCategoriesModel.Data.Categories;
        }

        private HttpWebRequest GetWebRequest(APISettings settings)
        {
            var address = settings.BaseAddress + WebConfigs.FashionDealerGetCategoriesMethod;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(address);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //merchantId = username from settings
                //token = passsword from settings
                string json = "{\"merchantId\":\"" + settings.Username + "\"," +
                              "\"token\":\"" + settings.Password + "\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            return httpWebRequest;
        }

        private HttpWebResponse GetWebResponse(HttpWebRequest httpWebRequest)
        {
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                _logger().Error("Unable to authenticate to vendor API for categories. Error: {0}", ex);
            }
            return response;
        }

        private FashionDealerCategoriesList ProcessWebResponse(HttpWebResponse httpResponse)
        {
            var fashionDealerCategoryModel = new FashionDealerCategoriesList();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                _logger().Information("Categories Authentication Phase Complete. Starting category sync");
                try
                {
                    fashionDealerCategoryModel = JsonConvert.DeserializeObject<FashionDealerCategoriesList>(result);
                    _logger().Information("Done deserialiazation JSON for categories.");
                }
                catch (Exception ex)
                {
                    _logger().Error(String.Format("Failed to process categories. Error: "), ex);
                }
            }
            return fashionDealerCategoryModel;
        }

        #endregion
    }
}
