﻿
namespace LuxuryApp.Processors.Agents.Sellers.Icon
{
    public class IconApiError
    {
        public IconApiError() { }

        public int Code { get; set; }

        public string Title { get; set; }

         public string Message { get; set; }
    }
}
