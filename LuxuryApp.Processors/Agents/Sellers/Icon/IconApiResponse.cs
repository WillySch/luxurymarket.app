﻿using System.Collections.Generic;

namespace LuxuryApp.Processors.Agents.Sellers.Icon
{
    public class IconApiResponse
    {
        public List<IconApiError> Errors { get; set; }

        public bool HasError { get; set; }

        public object Response { get; set; }
    }
}
