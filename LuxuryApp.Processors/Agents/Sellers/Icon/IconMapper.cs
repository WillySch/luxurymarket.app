﻿using LuxuryApp.Contracts.Models.SellerProductSync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Agents.Sellers.Icon
{
    public static class IconMapper
    {
        public static InventorySize ToInventorySize(this IconInventoryResponse model)
        {
              return new InventorySize
            {
                Quantity =(int)Math.Floor(Convert.ToDouble(model.Quantity)),
                SellerSize = model.Size,
                SellerSku = model.SKU
            };
        }

        public static ProductSyncItemBase ToProduct(this IconProductItem model)
        {
            var item = (ProductSyncItemBase)model;
            item.SellerSKU = model.SKU;
            return item;
        }

        public static ProductSyncImageItem ToImage(this IconProductItem model)
        {
            return new ProductSyncImageItem { SellerSKU = model.SKU, ImageUrl = model.Images };
        }
    }
}
