﻿
using LuxuryApp.Contracts.Models.SellerProductSync;

namespace LuxuryApp.Processors.Agents.Sellers.Icon
{
    public class IconProductItem : ProductSyncItemBase
    {
        public ProductSyncItemBase ConvertToBase(IconProductItem item) { return item; }

        public string SKU { get; set; }

        public string Images { get; set; }
    }
}
