﻿
namespace LuxuryApp.Processors.Agents.Sellers.Icon
{
    public class IconInventoryResponse
    {
        public string SKU { get; set; }

        public string Quantity { get; set; }

        public string Size { get; set; }
    }
}
