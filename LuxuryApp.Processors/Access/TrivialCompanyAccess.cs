﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Access;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.Infrastructure.Authorization;

namespace LuxuryApp.Processors.Access
{
    public class TrivialCompanyAccess : ICompanyAccess
    {
        private readonly IContextProvider _contextProvider;
        private readonly ICompanyRepository _companyRepository;
        private readonly ICartRepository _cartRepository;

        public TrivialCompanyAccess(
            IContextProvider contextProvider,
            ICompanyRepository companyRepository,
            ICartRepository cartRepository)
        {
            _contextProvider = contextProvider;
            _companyRepository = companyRepository;
            _cartRepository = cartRepository;
        }

        public bool CanViewCartForCompany(int companyId)
        {
            return IsUserAssociatedToCompany(companyId);
        }

        public bool CanAddToCartForCompany(int companyId)
        {
            return IsUserAssociatedToCompany(companyId);
        }

        private bool IsUserAssociatedToCompany(int companyId)
        {
            var userId = _contextProvider.GetLoggedInUserId();
            bool result = _companyRepository.IsUserAssociatedToCompany(userId, companyId);
            return result;
        }

        public bool CanUserSubmitCart(int cartId)
        {
            var userId = _contextProvider.GetLoggedInUserId();
            bool result = _cartRepository.IsUserAssociatedToCart(cartId, userId);
            return result;
        }

        public bool CanAddOffers(int companyId)
        {
            return false;
        }

        public bool CanViewOrders(int companyId)
        {
            return false;
        }
    }
}
