﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess;
using System.Collections.Generic;

namespace LuxuryApp.Processors.Readers
{
    public static class MaterialReader
    {
        public static List<Material> ToMaterialList(this DataReader reader)
        {
            if (reader == null) return null;

            var items = new List<Material>();

            while (reader.Read())
            {
                var item = new Material();
                item.Id = reader.GetInt("MaterialID");
                item.StandardMaterialId = reader.GetInt("StandardMaterialID");
                item.Description = reader.GetString("Description");
                item.DisplayOrder = reader.GetIntNullable("DisplayOrder");
                item.Code = reader.GetString("MaterialCode");
                item.BrandId = reader.GetInt("BrandID");

                items.Add(item);
            }

            reader.Close();

            return items;
        }

    }
}
