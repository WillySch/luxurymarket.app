﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess;
using System.Collections.Generic;

namespace LuxuryApp.Processors.Readers
{
    public static class CurrencyReader
    {
        public static List<Currency> ToCurrencyList(this DataReader reader)
        {
            if (reader == null) return null;

            var listItems = new List<Currency>();

            while (reader.Read())
            {
                var model = new Currency();
                model.CurrencyId = reader.GetInt("CurrencyID");
                model.Name = reader.GetString("Name");
                model.Rate = reader.GetDecimal("Rate");
                listItems.Add(model);
            }

            reader.Close();

            return listItems;
        }

    }
}