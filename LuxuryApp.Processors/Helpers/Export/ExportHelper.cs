﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Contracts.Models.Cart;
using LuxuryApp.Contracts.Models.Orders;
using LuxuryApp.Processors.Resources.Export;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;

namespace LuxuryApp.Processors.Helpers.Export
{
    public class ExportHelper
    {
        #region Public Methods

        //DEPRECATED - OLD EXPORT CART - NOW IS SAME TEMPLATE AS BUYER ORDER
        public static byte[] Old_ExportCartExcel(Cart cart)
        {
            using (var output = new MemoryStream())
            {
                // Create the package and make sure you wrap it in a using statement
                using (var package = new ExcelPackage(output))
                {
                    // Add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(ExcelNames.CART_WORKSHEET_NAME);

                    // Add some formatting to the worksheet
                    worksheet.DefaultColWidth = 20;
                    worksheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    // First row
                    int rowNumber = 1;
                    worksheet.Cells["A1:A2"].Merge = true;
                    worksheet.Cells["B1:V1"].Merge = true;
                    worksheet.Row(rowNumber).Height = 80;
                    AddImage(worksheet, 0, rowNumber - 1, WebConfigs.ExportCartLogoUrl, ImageTypeExport.Logo);

                    // Second row
                    worksheet.Row(++rowNumber).Height = 20;
                    worksheet.Cells[rowNumber, 2].Value = DateTime.Now.ToString();
                    worksheet.Cells[rowNumber, 8].Value = CartExcelColumnNames.Total;
                    worksheet.Cells[rowNumber, 9].Value = CartExcelColumnNames.Total;
                    worksheet.Cells[rowNumber, 10].Value = CartExcelColumnNames.Total;

                    // Third row
                    worksheet.Row(++rowNumber).Height = 15;
                    worksheet.Cells[rowNumber, 1].Value = CartExcelColumnNames.Picture;
                    worksheet.Cells[rowNumber, 2].Value = CartExcelColumnNames.Brand;
                    worksheet.Cells[rowNumber, 3].Value = CartExcelColumnNames.SKU;
                    worksheet.Cells[rowNumber, 4].Value = CartExcelColumnNames.Color;
                    worksheet.Cells[rowNumber, 5].Value = CartExcelColumnNames.Quantity;
                    worksheet.Cells[rowNumber, 6].Value = CartExcelColumnNames.UnitCost;
                    worksheet.Cells[rowNumber, 7].Value = CartExcelColumnNames.UnitLandedCost;
                    worksheet.Cells[rowNumber, 8].Value = CartExcelColumnNames.TotalProductCost;
                    worksheet.Cells[rowNumber, 9].Value = CartExcelColumnNames.RetailValue;
                    worksheet.Cells[rowNumber, 10].Value = CartExcelColumnNames.TotalLandedCost;

                    worksheet.Row(rowNumber).Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Row(rowNumber).Style.Font.Color.SetColor(Color.White);
                    rowNumber++;

                    var currencySymbol = cart.Currency.Name.Equals(CurrencyType.EUR.ToString()) ? ExcelNames.CURRENCY_SYMBOL_EUR : ExcelNames.CURRENCY_SYMBOL_USD;
                    foreach (var cartItem in cart.Items)
                    {
                        foreach (var item in cartItem.SellerUnits)
                        {
                            AddImage(worksheet, 0, rowNumber - 1, cartItem.ProductMainImageLink, ImageTypeExport.ProductImages);
                            worksheet.Cells[rowNumber, 2].Value = cartItem.BrandName;
                            worksheet.Cells[rowNumber, 3].Value = cartItem.ProductSku;
                            worksheet.Cells[rowNumber, 4].Value = cartItem.StandardColorName;
                            worksheet.Cells[rowNumber, 5].Value = item.UnitsCount.ToString();
                            worksheet.Cells[rowNumber, 6].Value = item.CustomerUnitPrice;
                            worksheet.Cells[rowNumber, 7].Value = item.TotalShippingCost;
                            worksheet.Cells[rowNumber, 8].Value = item.TotalCustomerCost;
                            worksheet.Cells[rowNumber, 9].Value = item.RetailPrice;
                            worksheet.Cells[rowNumber, 10].Value = cartItem.TotalLandedCost;

                            worksheet.Cells[rowNumber, 6, rowNumber, 10].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                            worksheet.Row(rowNumber++).Height = 90;
                        }
                    }

                    worksheet.Column(1).Width = 36;
                    worksheet.Column(8).Width = 25;
                    worksheet.Column(9).Width = 25;
                    worksheet.Column(10).Width = 25;

                    worksheet.Column(2).AutoFit();
                    worksheet.Column(3).AutoFit();
                    worksheet.Column(4).AutoFit();
                    worksheet.Column(5).AutoFit();
                    worksheet.Column(6).AutoFit();
                    worksheet.Column(7).AutoFit();
                    worksheet.Column(8).AutoFit();
                    worksheet.Column(9).AutoFit();
                    worksheet.Column(10).AutoFit();

                    // save our new workbook
                    package.Save();

                    return output.ToArray();
                }
            }
        }

        public static byte[] ExportCartExcel(Cart cart)
        {
            var currencySymbol = cart.Currency.Name.Equals(CurrencyType.EUR.ToString()) ? ExcelNames.CURRENCY_SYMBOL_EUR : ExcelNames.CURRENCY_SYMBOL_USD;

            using (var output = new MemoryStream())
            {
                // Create the package and make sure you wrap it in a using statement
                using (var package = new ExcelPackage(output))
                {
                    #region Style Summary

                    // Add a new worksheet to the empty workbook
                    var worksheet = package.Workbook.Worksheets.Add(ExcelNames.CART_SUMMARY_WORKSHEET_NAME);

                    // Add some formatting to the worksheet
                    worksheet.DefaultColWidth = 20;
                    worksheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    // First row
                    var rowNumber = 1;
                    worksheet.Cells["A1:A4"].Merge = true;
                    AddImage(worksheet, 0, rowNumber - 1, WebConfigs.ExportCartLogoUrl, ImageTypeExport.Logo);
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.TOTAL_COST;
                    worksheet.Cells[rowNumber, 3].Value = cart.TotalCustomerCost;

                    worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowNumber, 3].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                    worksheet.Cells[rowNumber, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2].Style.Font.Color.SetColor(Color.White);

                    // Second row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.TOTAL_UNITS;
                    worksheet.Cells[rowNumber, 3].Value = cart.UnitsCount;

                    worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2].Style.Font.Color.SetColor(Color.White);

                    // Third row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.TOTAL_STYLES;
                    worksheet.Cells[rowNumber, 3].Value = cart.ItemsCount;

                    worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2].Style.Font.Color.SetColor(Color.White);

                    // Fourth row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.CREATED_DATE;
                    worksheet.Cells[rowNumber, 3].Value = DateTime.Now.ToString();

                    worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2].Style.Font.Color.SetColor(Color.White);

                    // Empty row
                    rowNumber++;
                    worksheet.Row(rowNumber).Height = 39;

                    // Header for table with items
                    rowNumber++;
                    worksheet.Cells[rowNumber, 1].Value = OrderExcelColumnNames.PICTURE;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.BRAND;
                    worksheet.Cells[rowNumber, 3].Value = OrderExcelColumnNames.CATEGORY;
                    worksheet.Cells[rowNumber, 4].Value = OrderExcelColumnNames.PRODUCT_NAME;
                    worksheet.Cells[rowNumber, 5].Value = OrderExcelColumnNames.SKU;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.COLOR_DESCRIPTION;
                    worksheet.Cells[rowNumber, 7].Value = OrderExcelColumnNames.MATERIAL_DESCRIPTION;
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.SIZE_TYPE;
                    worksheet.Cells[rowNumber, 9].Value = OrderExcelColumnNames.UNITS;
                    worksheet.Cells[rowNumber, 10].Value = OrderExcelColumnNames.CURRENCY;
                    worksheet.Cells[rowNumber, 11].Value = OrderExcelColumnNames.RETAIL_PRICE;
                    worksheet.Cells[rowNumber, 12].Value = OrderExcelColumnNames.UNIT_COST;
                    worksheet.Cells[rowNumber, 13].Value = OrderExcelColumnNames.UNIT_DISCOUNT;
                    worksheet.Cells[rowNumber, 14].Value = OrderExcelColumnNames.TOTAL_COST;

                    worksheet.Cells[rowNumber, 1, rowNumber, 14].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 1, rowNumber, 14].Style.Font.Color.SetColor(Color.White);

                    // Table with items
                    rowNumber++;
                    foreach (var cartItem in cart.Items)
                    {
                        foreach (var sellerUnit in cartItem.SellerUnits)
                        {
                            var sizeTypeName = sellerUnit.SellerUnitSizes.FirstOrDefault() != null ? sellerUnit.SellerUnitSizes.FirstOrDefault().SizeTypeName : "N/A";

                            AddImage(worksheet, 0, rowNumber - 1, cartItem.ProductMainImageLink, ImageTypeExport.ProductImages);
                            worksheet.Cells[rowNumber, 2].Value = cartItem.BrandName;
                            worksheet.Cells[rowNumber, 3].Value = cartItem.CategoryName;
                            worksheet.Cells[rowNumber, 4].Value = cartItem.ProductName;
                            worksheet.Cells[rowNumber, 5].Value = cartItem.ProductSku;
                            worksheet.Cells[rowNumber, 6].Value = cartItem.StandardColorName;
                            worksheet.Cells[rowNumber, 7].Value = cartItem.StandardMaterialName;
                            worksheet.Cells[rowNumber, 8].Value = sizeTypeName;
                            worksheet.Cells[rowNumber, 9].Value = sellerUnit.UnitsCount;
                            worksheet.Cells[rowNumber, 10].Value = cart.Currency.Name;
                            worksheet.Cells[rowNumber, 11].Value = sellerUnit.RetailPrice;
                            worksheet.Cells[rowNumber, 12].Value = sellerUnit.LMUnitPrice;
                            worksheet.Cells[rowNumber, 13].Value = sellerUnit.DiscountPercentage / 100;
                            worksheet.Cells[rowNumber, 14].Value = sellerUnit.TotalLandedCost;

                            worksheet.Cells[rowNumber, 11, rowNumber, 12].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                            worksheet.Cells[rowNumber, 14].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                            worksheet.Cells[rowNumber, 13].Style.Numberformat.Format = "#0.00%";
                            worksheet.Row(rowNumber++).Height = 100;
                        }
                    }

                    // Add width to first column for logo
                    worksheet.Column(1).Width = 36;

                    worksheet.Column(2).AutoFit();
                    worksheet.Column(3).AutoFit();
                    worksheet.Column(4).AutoFit();
                    worksheet.Column(5).AutoFit();
                    worksheet.Column(6).AutoFit();
                    worksheet.Column(7).AutoFit();
                    worksheet.Column(8).AutoFit();
                    worksheet.Column(9).AutoFit();
                    worksheet.Column(10).AutoFit();
                    worksheet.Column(11).AutoFit();
                    worksheet.Column(12).AutoFit();
                    worksheet.Column(13).AutoFit();
                    worksheet.Column(14).AutoFit();

                    #endregion

                    #region Size details

                    // Add a new worksheet to the empty workbook
                    worksheet = package.Workbook.Worksheets.Add(ExcelNames.CART_DETAILS_WORKSHEET_NAME);

                    // Add some formatting to the worksheet
                    worksheet.DefaultColWidth = 20;
                    worksheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    // First row
                    rowNumber = 1;
                    worksheet.Cells["A1:A4"].Merge = true;
                    AddImage(worksheet, 0, rowNumber - 1, WebConfigs.ExportCartLogoUrl, ImageTypeExport.Logo);
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.TOTAL_COST;
                    worksheet.Cells[rowNumber, 3].Value = cart.TotalCustomerCost;

                    worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowNumber, 3].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                    worksheet.Cells[rowNumber, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2].Style.Font.Color.SetColor(Color.White);

                    // Second row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.TOTAL_UNITS;
                    worksheet.Cells[rowNumber, 3].Value = cart.UnitsCount;

                    worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2].Style.Font.Color.SetColor(Color.White);

                    // Third row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.TOTAL_STYLES;
                    worksheet.Cells[rowNumber, 3].Value = cart.ItemsCount;

                    worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2].Style.Font.Color.SetColor(Color.White);

                    // Fourth row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.CREATED_DATE;
                    worksheet.Cells[rowNumber, 3].Value = DateTime.Now.ToString();

                    worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2].Style.Font.Color.SetColor(Color.White);

                    // Empty row
                    rowNumber++;
                    worksheet.Row(rowNumber).Height = 39;

                    // Header for table with items
                    rowNumber++;
                    worksheet.Cells[rowNumber, 1].Value = OrderExcelColumnNames.BRAND;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.SKU;
                    worksheet.Cells[rowNumber, 3].Value = OrderExcelColumnNames.CATEGORY;
                    worksheet.Cells[rowNumber, 4].Value = OrderExcelColumnNames.PRODUCT_NAME;
                    worksheet.Cells[rowNumber, 5].Value = OrderExcelColumnNames.COLOR_DESCRIPTION;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.MATERIAL_DESCRIPTION;
                    worksheet.Cells[rowNumber, 7].Value = OrderExcelColumnNames.SIZE_TYPE;
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.SIZE;
                    worksheet.Cells[rowNumber, 9].Value = OrderExcelColumnNames.UNITS;
                    worksheet.Cells[rowNumber, 10].Value = OrderExcelColumnNames.CURRENCY;
                    worksheet.Cells[rowNumber, 11].Value = OrderExcelColumnNames.RETAIL_PRICE;
                    worksheet.Cells[rowNumber, 12].Value = OrderExcelColumnNames.UNIT_COST;
                    worksheet.Cells[rowNumber, 13].Value = OrderExcelColumnNames.UNIT_DISCOUNT;
                    worksheet.Cells[rowNumber, 14].Value = OrderExcelColumnNames.TOTAL_COST;

                    worksheet.Cells[rowNumber, 1, rowNumber, 14].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 1, rowNumber, 14].Style.Font.Color.SetColor(Color.White);

                    // Table with items
                    rowNumber++;
                    foreach (var cartItem in cart.Items)
                    {
                        foreach (var sellerUnit in cartItem.SellerUnits)
                        {
                            foreach (var unitSizes in sellerUnit.SellerUnitSizes)
                            {
                                var totalCost = sellerUnit.LMUnitPrice * unitSizes.Quantity;

                                worksheet.Cells[rowNumber, 1].Value = cartItem.BrandName;
                                worksheet.Cells[rowNumber, 2].Value = cartItem.ProductSku;
                                worksheet.Cells[rowNumber, 3].Value = cartItem.CategoryName;
                                worksheet.Cells[rowNumber, 4].Value = cartItem.ProductName;
                                worksheet.Cells[rowNumber, 5].Value = cartItem.StandardColorName;
                                worksheet.Cells[rowNumber, 6].Value = cartItem.StandardMaterialName;
                                worksheet.Cells[rowNumber, 7].Value = unitSizes.SizeTypeName;
                                worksheet.Cells[rowNumber, 8].Value = unitSizes.SizeName;
                                worksheet.Cells[rowNumber, 9].Value = unitSizes.Quantity;
                                worksheet.Cells[rowNumber, 10].Value = cart.Currency.Name;
                                worksheet.Cells[rowNumber, 11].Value = sellerUnit.RetailPrice;
                                worksheet.Cells[rowNumber, 12].Value = sellerUnit.LMUnitPrice;
                                worksheet.Cells[rowNumber, 13].Value = sellerUnit.DiscountPercentage / 100;
                                worksheet.Cells[rowNumber, 14].Value = totalCost;

                                worksheet.Cells[rowNumber, 11, rowNumber, 12].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                                worksheet.Cells[rowNumber, 14].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                                worksheet.Cells[rowNumber, 13].Style.Numberformat.Format = "#0.00%";
                                rowNumber++;
                            }
                        }
                    }

                    // Add width to first column for logo
                    worksheet.Column(1).Width = 36;

                    worksheet.Column(2).AutoFit();
                    worksheet.Column(3).AutoFit();
                    worksheet.Column(4).AutoFit();
                    worksheet.Column(5).AutoFit();
                    worksheet.Column(6).AutoFit();
                    worksheet.Column(7).AutoFit();
                    worksheet.Column(8).AutoFit();
                    worksheet.Column(9).AutoFit();
                    worksheet.Column(10).AutoFit();
                    worksheet.Column(11).AutoFit();
                    worksheet.Column(12).AutoFit();
                    worksheet.Column(13).AutoFit();
                    worksheet.Column(14).AutoFit();

                    #endregion

                    // save our new workbook
                    package.Save();

                    return output.ToArray();
                }
            }
        }

        public static byte[] ExportBuyerOrderExcel(Order order, bool excludeShippingRelatedColumns = false)
        {
            var currencySymbol = order.CurrencyName.Equals(CurrencyType.EUR.ToString()) ? ExcelNames.CURRENCY_SYMBOL_EUR : ExcelNames.CURRENCY_SYMBOL_USD;

            using (var output = new MemoryStream())
            {
                // Create the package and make sure you wrap it in a using statement
                using (var package = new ExcelPackage(output))
                {
                    #region Style Summary

                    // Add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(ExcelNames.ORDER_SUMMARY_WORKSHEET_NAME);

                    // Add some formatting to the worksheet
                    worksheet.DefaultColWidth = 20;
                    worksheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    // First row
                    var rowNumber = 1;
                    worksheet.Cells["A1:A3"].Merge = true;
                    AddImage(worksheet, 0, rowNumber - 1, WebConfigs.ExportCartLogoUrl, ImageTypeExport.Logo);
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.ORDER_PO_NUMBER;
                    worksheet.Cells[rowNumber, 3].Value = OrderExcelColumnNames.BUYER;
                    worksheet.Cells[rowNumber, 4].Value = OrderExcelColumnNames.PAYMENT_METHOD_NAME;
                    worksheet.Cells[rowNumber, 5].Value = OrderExcelColumnNames.CREATED_DATE;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.PO_NUMBER;
                    worksheet.Cells[rowNumber, 7].Value = OrderExcelColumnNames.STATUS;
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.TOTAL_PO_LANDED;

                    worksheet.Cells[rowNumber, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowNumber, 9].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                    worksheet.Cells[rowNumber, 2, rowNumber, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2, rowNumber, 8].Style.Font.Color.SetColor(Color.White);

                    // Second row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 2].Value = order.PONumber;
                    worksheet.Cells[rowNumber, 3].Value = order.BuyerCompanyName;
                    worksheet.Cells[rowNumber, 4].Value = order.PaymentMethodName;
                    worksheet.Cells[rowNumber, 5].Value = order.CreatedDate.DateTime.ToString();
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.TOTAL_UNITS;
                    worksheet.Cells[rowNumber, 9].Value = order.TotalUnits;

                    worksheet.Cells[rowNumber, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 8].Style.Font.Color.SetColor(Color.White);

                    // Third row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.TOTAL_STYLES;
                    worksheet.Cells[rowNumber, 9].Value = order.TotalStyles;

                    worksheet.Cells[rowNumber, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 8].Style.Font.Color.SetColor(Color.White);

                    // Empty row
                    rowNumber = order.OrderSellerList.Count() > 2 ? order.OrderSellerList.Count() + 2 : 4;
                    // 99 is the full height of logo; 15 is the default height for excel column
                    var emptyRowHeight = 99 - ((rowNumber - 1) * 15);
                    if (emptyRowHeight > 15)
                    {
                        worksheet.Row(rowNumber).Height = emptyRowHeight;
                    }

                    // Header for table with items
                    rowNumber++;
                    worksheet.Cells[rowNumber, 1].Value = OrderExcelColumnNames.PICTURE;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.BRAND;
                    worksheet.Cells[rowNumber, 3].Value = OrderExcelColumnNames.CATEGORY;
                    worksheet.Cells[rowNumber, 4].Value = OrderExcelColumnNames.PRODUCT_NAME;
                    worksheet.Cells[rowNumber, 5].Value = OrderExcelColumnNames.SKU;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.COLOR_DESCRIPTION;
                    worksheet.Cells[rowNumber, 7].Value = OrderExcelColumnNames.MATERIAL_DESCRIPTION;
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.SIZE_TYPE;
                    worksheet.Cells[rowNumber, 9].Value = OrderExcelColumnNames.UNITS;
                    worksheet.Cells[rowNumber, 10].Value = OrderExcelColumnNames.CURRENCY;
                    worksheet.Cells[rowNumber, 11].Value = OrderExcelColumnNames.RETAIL_PRICE;
                    if (excludeShippingRelatedColumns)
                    {
                        worksheet.Cells[rowNumber, 12].Value = OrderExcelColumnNames.DISCOUNT;
                        worksheet.Cells[rowNumber, 13].Value = OrderExcelColumnNames.UNIT_LANDED_COST;
                        worksheet.Cells[rowNumber, 14].Value = OrderExcelColumnNames.TOTAL_LANDED_COST;

                        worksheet.Cells[rowNumber, 1, rowNumber, 14].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[rowNumber, 1, rowNumber, 14].Style.Font.Color.SetColor(Color.White);
                    }
                    else
                    {
                        worksheet.Cells[rowNumber, 12].Value = OrderExcelColumnNames.LM_UNIT_COST;
                        worksheet.Cells[rowNumber, 13].Value = OrderExcelColumnNames.DISCOUNT;
                        worksheet.Cells[rowNumber, 14].Value = OrderExcelColumnNames.UNIT_LANDED_COST;
                        worksheet.Cells[rowNumber, 15].Value = OrderExcelColumnNames.TOTAL_PRODUCT_COST;
                        worksheet.Cells[rowNumber, 16].Value = OrderExcelColumnNames.TOTAL_LANDED_COST;

                        worksheet.Cells[rowNumber, 1, rowNumber, 16].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[rowNumber, 1, rowNumber, 16].Style.Font.Color.SetColor(Color.White);
                    }

                    // Table with items
                    rowNumber++;
                    double totalLandedCost = 0;
                    var orderSellerList = new Dictionary<string, string>();
                    foreach (var ord in order.OrderSellerList)
                    {
                        totalLandedCost += ord.TotalLandedCost;
                        orderSellerList.Add(ord.PONumber, ord.StatusTypeName);

                        foreach (var product in ord.ProductItems)
                        {
                            var itemShippingCost = Math.Ceiling(product.CustomerUnitPrice * product.UnitShippingTaxPercentage / 100) + product.CustomerUnitPrice;

                            AddImage(worksheet, 0, rowNumber - 1, product.ProductDetails.MainImageLink, ImageTypeExport.ProductImages);
                            worksheet.Cells[rowNumber, 2].Value = product.ProductDetails.BrandName;
                            worksheet.Cells[rowNumber, 3].Value = product.ProductDetails.CategoryName;
                            worksheet.Cells[rowNumber, 4].Value = product.ProductDetails.ProductName;
                            worksheet.Cells[rowNumber, 5].Value = product.ProductDetails.SKU;
                            worksheet.Cells[rowNumber, 6].Value = product.ProductDetails.StandardColor;
                            worksheet.Cells[rowNumber, 7].Value = product.ProductDetails.StandardMaterial;
                            worksheet.Cells[rowNumber, 8].Value = product.ProductDetails.SizeTypeName;
                            worksheet.Cells[rowNumber, 9].Value = product.TotalUnits;
                            worksheet.Cells[rowNumber, 10].Value = order.CurrencyName;
                            worksheet.Cells[rowNumber, 11].Value = product.ProductDetails.RetailPrice;
                            if (excludeShippingRelatedColumns)
                            {
                                worksheet.Cells[rowNumber, 12].Value = product.UnitDiscountPercentage / 100;
                                worksheet.Cells[rowNumber, 13].Value = itemShippingCost;
                                worksheet.Cells[rowNumber, 14].Value = product.TotalLandedCost;

                                worksheet.Cells[rowNumber, 11].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                                worksheet.Cells[rowNumber, 13, rowNumber, 14].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                                worksheet.Cells[rowNumber, 12].Style.Numberformat.Format = "#0.00%";
                            }
                            else
                            {
                                worksheet.Cells[rowNumber, 12].Value = product.CustomerUnitPrice;
                                worksheet.Cells[rowNumber, 13].Value = product.UnitDiscountPercentage / 100;
                                worksheet.Cells[rowNumber, 14].Value = itemShippingCost;
                                worksheet.Cells[rowNumber, 15].Value = product.TotalProductCost;
                                worksheet.Cells[rowNumber, 16].Value = product.TotalLandedCost;

                                worksheet.Cells[rowNumber, 11, rowNumber, 12].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                                worksheet.Cells[rowNumber, 14, rowNumber, 16].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                                worksheet.Cells[rowNumber, 13].Style.Numberformat.Format = "#0.00%";
                            }

                            worksheet.Row(rowNumber++).Height = 100;
                        }
                    }

                    // First table information
                    rowNumber = 1;
                    worksheet.Cells[rowNumber, 9].Value = totalLandedCost;

                    // Populate Po Number and status of an order seller
                    foreach (var ordSel in orderSellerList)
                    {
                        worksheet.Cells[++rowNumber, 6].Value = ordSel.Key;
                        worksheet.Cells[rowNumber, 7].Value = ordSel.Value;
                    }

                    // Add width to first column for logo
                    worksheet.Column(1).Width = 36;

                    worksheet.Column(2).AutoFit();
                    worksheet.Column(3).AutoFit();
                    worksheet.Column(4).AutoFit();
                    worksheet.Column(5).AutoFit();
                    worksheet.Column(6).AutoFit();
                    worksheet.Column(7).AutoFit();
                    worksheet.Column(8).AutoFit();
                    worksheet.Column(9).AutoFit();
                    worksheet.Column(10).AutoFit();
                    worksheet.Column(11).AutoFit();
                    worksheet.Column(12).AutoFit();
                    worksheet.Column(13).AutoFit();
                    worksheet.Column(14).AutoFit();
                    if (!excludeShippingRelatedColumns)
                    {
                        worksheet.Column(15).AutoFit();
                        worksheet.Column(16).AutoFit();
                    }


                    #endregion

                    #region Style details

                    // Add a new worksheet to the empty workbook
                    worksheet = package.Workbook.Worksheets.Add(ExcelNames.ORDER_DETAILS_WORKSHEET_NAME);

                    // Add some formatting to the worksheet
                    worksheet.DefaultColWidth = 20;
                    worksheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    // First row
                    rowNumber = 1;
                    worksheet.Cells["A1:A3"].Merge = true;
                    AddImage(worksheet, 0, rowNumber - 1, WebConfigs.ExportCartLogoUrl, ImageTypeExport.Logo);
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.ORDER_PO_NUMBER;
                    worksheet.Cells[rowNumber, 3].Value = OrderExcelColumnNames.BUYER;
                    worksheet.Cells[rowNumber, 4].Value = OrderExcelColumnNames.PAYMENT_METHOD_NAME;
                    worksheet.Cells[rowNumber, 5].Value = OrderExcelColumnNames.CREATED_DATE;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.PO_NUMBER;
                    worksheet.Cells[rowNumber, 7].Value = OrderExcelColumnNames.STATUS;
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.TOTAL_PO_LANDED;

                    worksheet.Cells[rowNumber, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowNumber, 9].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                    worksheet.Cells[rowNumber, 2, rowNumber, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2, rowNumber, 8].Style.Font.Color.SetColor(Color.White);

                    // Second row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 2].Value = order.PONumber;
                    worksheet.Cells[rowNumber, 3].Value = order.BuyerCompanyName;
                    worksheet.Cells[rowNumber, 4].Value = order.PaymentMethodName;
                    worksheet.Cells[rowNumber, 5].Value = order.CreatedDate.DateTime.ToString();
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.TOTAL_UNITS;
                    worksheet.Cells[rowNumber, 9].Value = order.TotalUnits;

                    worksheet.Cells[rowNumber, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 8].Style.Font.Color.SetColor(Color.White);

                    // Third row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.TOTAL_STYLES;
                    worksheet.Cells[rowNumber, 9].Value = order.TotalStyles;

                    worksheet.Cells[rowNumber, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 8].Style.Font.Color.SetColor(Color.White);

                    // Empty row
                    rowNumber = order.OrderSellerList.Count() > 2 ? order.OrderSellerList.Count() + 2 : 4;
                    if (emptyRowHeight > 15)
                    {
                        worksheet.Row(rowNumber).Height = emptyRowHeight;
                    }

                    // Header for table with items
                    rowNumber++;
                    worksheet.Cells[rowNumber, 1].Value = OrderExcelColumnNames.BRAND;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.SKU;
                    worksheet.Cells[rowNumber, 3].Value = OrderExcelColumnNames.CATEGORY;
                    worksheet.Cells[rowNumber, 4].Value = OrderExcelColumnNames.PRODUCT_NAME;
                    worksheet.Cells[rowNumber, 5].Value = OrderExcelColumnNames.COLOR_DESCRIPTION;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.MATERIAL_DESCRIPTION;
                    worksheet.Cells[rowNumber, 7].Value = OrderExcelColumnNames.SIZE_TYPE;
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.SIZE;
                    worksheet.Cells[rowNumber, 9].Value = OrderExcelColumnNames.UNITS;
                    worksheet.Cells[rowNumber, 10].Value = OrderExcelColumnNames.CURRENCY;
                    worksheet.Cells[rowNumber, 11].Value = OrderExcelColumnNames.RETAIL_PRICE;
                    if (excludeShippingRelatedColumns)
                    {
                        worksheet.Cells[rowNumber, 12].Value = OrderExcelColumnNames.DISCOUNT;
                        worksheet.Cells[rowNumber, 13].Value = OrderExcelColumnNames.UNIT_LANDED_COST;
                        worksheet.Cells[rowNumber, 14].Value = OrderExcelColumnNames.TOTAL_LANDED_COST;

                        worksheet.Cells[rowNumber, 1, rowNumber, 14].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[rowNumber, 1, rowNumber, 14].Style.Font.Color.SetColor(Color.White);
                    }
                    else
                    {
                        worksheet.Cells[rowNumber, 12].Value = OrderExcelColumnNames.LM_UNIT_COST;
                        worksheet.Cells[rowNumber, 13].Value = OrderExcelColumnNames.DISCOUNT;
                        worksheet.Cells[rowNumber, 14].Value = OrderExcelColumnNames.UNIT_LANDED_COST;
                        worksheet.Cells[rowNumber, 15].Value = OrderExcelColumnNames.TOTAL_PRODUCT_COST;
                        worksheet.Cells[rowNumber, 16].Value = OrderExcelColumnNames.TOTAL_LANDED_COST;

                        worksheet.Cells[rowNumber, 1, rowNumber, 16].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[rowNumber, 1, rowNumber, 16].Style.Font.Color.SetColor(Color.White);
                    }


                    // Table with items
                    rowNumber++;
                    orderSellerList = new Dictionary<string, string>();
                    foreach (var ord in order.OrderSellerList)
                    {
                        orderSellerList.Add(ord.PONumber, ord.StatusTypeName);

                        foreach (var product in ord.ProductItems)
                        {
                            foreach (var size in product.SizeItems)
                            {
                                var units = size.ConfirmedUnits == null ? size.Units : (int)size.ConfirmedUnits;
                                var itemShippingCost = Math.Ceiling(product.CustomerUnitPrice * product.UnitShippingTaxPercentage / 100) + product.CustomerUnitPrice;
                                var itemTotalCost = units * product.CustomerUnitPrice;
                                var itemTotalLandedCost = units * itemShippingCost;

                                worksheet.Cells[rowNumber, 1].Value = product.ProductDetails.BrandName;
                                worksheet.Cells[rowNumber, 2].Value = product.ProductDetails.SKU;
                                worksheet.Cells[rowNumber, 3].Value = product.ProductDetails.CategoryName;
                                worksheet.Cells[rowNumber, 4].Value = product.ProductDetails.ProductName;
                                worksheet.Cells[rowNumber, 5].Value = product.ProductDetails.StandardColor;
                                worksheet.Cells[rowNumber, 6].Value = product.ProductDetails.StandardMaterial;
                                worksheet.Cells[rowNumber, 7].Value = product.ProductDetails.SizeTypeName;
                                worksheet.Cells[rowNumber, 8].Value = size.SizeName;
                                worksheet.Cells[rowNumber, 9].Value = units;
                                worksheet.Cells[rowNumber, 10].Value = order.CurrencyName;
                                worksheet.Cells[rowNumber, 11].Value = product.ProductDetails.RetailPrice;
                                if (excludeShippingRelatedColumns)
                                {
                                    worksheet.Cells[rowNumber, 12].Value = product.UnitDiscountPercentage / 100;
                                    worksheet.Cells[rowNumber, 13].Value = itemShippingCost;
                                    worksheet.Cells[rowNumber, 14].Value = itemTotalLandedCost;

                                    worksheet.Cells[rowNumber, 11].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                                    worksheet.Cells[rowNumber, 13, rowNumber, 14].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                                    worksheet.Cells[rowNumber, 12].Style.Numberformat.Format = "#0.00%";
                                }
                                else
                                {
                                    worksheet.Cells[rowNumber, 12].Value = product.CustomerUnitPrice;
                                    worksheet.Cells[rowNumber, 13].Value = product.UnitDiscountPercentage / 100;
                                    worksheet.Cells[rowNumber, 14].Value = itemShippingCost;
                                    worksheet.Cells[rowNumber, 15].Value = itemTotalCost;
                                    worksheet.Cells[rowNumber, 16].Value = itemTotalLandedCost;

                                    worksheet.Cells[rowNumber, 11, rowNumber, 12].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                                    worksheet.Cells[rowNumber, 14, rowNumber, 16].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                                    worksheet.Cells[rowNumber, 13].Style.Numberformat.Format = "#0.00%";
                                }

                                rowNumber++;
                            }
                        }
                    }

                    // First table information
                    rowNumber = 1;
                    worksheet.Cells[rowNumber, 9].Value = totalLandedCost;

                    // Populate Po Number and status of an order seller
                    foreach (var ordSel in orderSellerList)
                    {
                        worksheet.Cells[++rowNumber, 6].Value = ordSel.Key;
                        worksheet.Cells[rowNumber, 7].Value = ordSel.Value;
                    }

                    // Add width to first column for logo
                    worksheet.Column(1).Width = 36;

                    worksheet.Column(2).AutoFit();
                    worksheet.Column(3).AutoFit();
                    worksheet.Column(4).AutoFit();
                    worksheet.Column(5).AutoFit();
                    worksheet.Column(6).AutoFit();
                    worksheet.Column(7).AutoFit();
                    worksheet.Column(8).AutoFit();
                    worksheet.Column(9).AutoFit();
                    worksheet.Column(10).AutoFit();
                    worksheet.Column(11).AutoFit();
                    worksheet.Column(12).AutoFit();
                    worksheet.Column(13).AutoFit();
                    worksheet.Column(14).AutoFit();
                    if (!excludeShippingRelatedColumns)
                    {
                        worksheet.Column(15).AutoFit();
                        worksheet.Column(16).AutoFit();
                    }

                    #endregion

                    // save our new workbook
                    package.Save();

                    return output.ToArray();
                }
            }
        }

        public static byte[] ExportSellerOrderExcel(OrderSellerView order)
        {
            var currencySymbol = order.ProductItems.FirstOrDefault().CurrencyName.Equals(CurrencyType.EUR.ToString()) ? ExcelNames.CURRENCY_SYMBOL_EUR : ExcelNames.CURRENCY_SYMBOL_USD;

            using (var output = new MemoryStream())
            {
                // Create the package and make sure you wrap it in a using statement
                using (var package = new ExcelPackage(output))
                {
                    #region Style Summary

                    // Add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(ExcelNames.ORDER_SUMMARY_WORKSHEET_NAME);

                    // Add some formatting to the worksheet
                    worksheet.DefaultColWidth = 20;
                    worksheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    // First row
                    var rowNumber = 1;
                    worksheet.Cells["A1:A3"].Merge = true;
                    AddImage(worksheet, 0, rowNumber - 1, WebConfigs.ExportCartLogoUrl, ImageTypeExport.Logo);
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.ORDER_PO_NUMBER;
                    worksheet.Cells[rowNumber, 3].Value = OrderExcelColumnNames.SUPPLIER;
                    worksheet.Cells[rowNumber, 4].Value = OrderExcelColumnNames.PAYMENT_METHOD_NAME;
                    worksheet.Cells[rowNumber, 5].Value = OrderExcelColumnNames.CREATED_DATE;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.TOTAL_PO;

                    worksheet.Cells[rowNumber, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowNumber, 7].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                    worksheet.Cells[rowNumber, 2, rowNumber, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2, rowNumber, 6].Style.Font.Color.SetColor(Color.White);

                    // Second row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 2].Value = order.PONumber;
                    worksheet.Cells[rowNumber, 3].Value = order.SellerCompanyName;
                    worksheet.Cells[rowNumber, 4].Value = order.StatusTypeName;
                    worksheet.Cells[rowNumber, 5].Value = order.CreatedDate.DateTime.ToString();
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.TOTAL_UNITS;

                    worksheet.Cells[rowNumber, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 6].Style.Font.Color.SetColor(Color.White);

                    // Third row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.TOTAL_STYLES;
                    worksheet.Cells[rowNumber, 7].Value = order.ProductItems.Count();

                    worksheet.Cells[rowNumber, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 6].Style.Font.Color.SetColor(Color.White);

                    // Empty row
                    rowNumber++;
                    worksheet.Row(rowNumber).Height = 54;

                    // Header for table with items
                    rowNumber++;
                    worksheet.Cells[rowNumber, 1].Value = OrderExcelColumnNames.PICTURE;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.BRAND;
                    worksheet.Cells[rowNumber, 3].Value = OrderExcelColumnNames.CATEGORY;
                    worksheet.Cells[rowNumber, 4].Value = OrderExcelColumnNames.PRODUCT_NAME;
                    worksheet.Cells[rowNumber, 5].Value = OrderExcelColumnNames.LM_SKU;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.COLOR_CODE;
                    worksheet.Cells[rowNumber, 7].Value = OrderExcelColumnNames.MATERIAL_CODE;
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.SIZE_TYPE;
                    worksheet.Cells[rowNumber, 9].Value = OrderExcelColumnNames.UNITS;
                    worksheet.Cells[rowNumber, 10].Value = OrderExcelColumnNames.CURRENCY;
                    worksheet.Cells[rowNumber, 11].Value = OrderExcelColumnNames.OFFER_UNIT_COST;
                    worksheet.Cells[rowNumber, 12].Value = OrderExcelColumnNames.TOTAL_PRODUCT_COST;

                    worksheet.Cells[rowNumber, 1, rowNumber, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 1, rowNumber, 12].Style.Font.Color.SetColor(Color.White);

                    // Table with items
                    rowNumber++;
                    double totalOrderCost = 0;
                    var totalUnits = 0;
                    foreach (var product in order.ProductItems)
                    {
                        totalOrderCost += product.TotalProductCost;
                        totalUnits += product.TotalUnits;

                        AddImage(worksheet, 0, rowNumber - 1, product.ProductDetails.MainImageLink, ImageTypeExport.ProductImages);
                        worksheet.Cells[rowNumber, 2].Value = product.ProductDetails.BrandName;
                        worksheet.Cells[rowNumber, 3].Value = product.ProductDetails.CategoryName;
                        worksheet.Cells[rowNumber, 4].Value = product.ProductDetails.ProductName;
                        worksheet.Cells[rowNumber, 5].Value = product.ProductDetails.SKU;
                        worksheet.Cells[rowNumber, 6].Value = product.ProductDetails.ColorCode;
                        worksheet.Cells[rowNumber, 7].Value = product.ProductDetails.MaterialCode;
                        worksheet.Cells[rowNumber, 8].Value = product.ProductDetails.SizeTypeName;
                        worksheet.Cells[rowNumber, 9].Value = product.TotalUnits;
                        worksheet.Cells[rowNumber, 10].Value = product.CurrencyName;
                        worksheet.Cells[rowNumber, 11].Value = product.OfferCost;
                        worksheet.Cells[rowNumber, 12].Value = product.TotalProductCost;

                        worksheet.Cells[rowNumber, 11, rowNumber, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[rowNumber, 11, rowNumber, 12].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                        worksheet.Row(rowNumber++).Height = 100;
                    }

                    // First table information
                    rowNumber = 1;
                    worksheet.Cells[rowNumber, 7].Value = totalOrderCost;
                    worksheet.Cells[++rowNumber, 7].Value = totalUnits;

                    // Add width to first column for logo
                    worksheet.Column(1).Width = 36;

                    worksheet.Column(2).AutoFit();
                    worksheet.Column(3).AutoFit();
                    worksheet.Column(4).AutoFit();
                    worksheet.Column(5).AutoFit();
                    worksheet.Column(6).AutoFit();
                    worksheet.Column(7).AutoFit();
                    worksheet.Column(8).AutoFit();
                    worksheet.Column(9).AutoFit();
                    worksheet.Column(10).AutoFit();
                    worksheet.Column(11).AutoFit();
                    worksheet.Column(12).AutoFit();

                    #endregion

                    #region Style details

                    // Add a new worksheet to the empty workbook
                    worksheet = package.Workbook.Worksheets.Add(ExcelNames.ORDER_DETAILS_WORKSHEET_NAME);

                    // Add some formatting to the worksheet
                    worksheet.DefaultColWidth = 20;
                    worksheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    // First row
                    rowNumber = 1;
                    worksheet.Cells["A1:A3"].Merge = true;
                    AddImage(worksheet, 0, rowNumber - 1, WebConfigs.ExportCartLogoUrl, ImageTypeExport.Logo);
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.ORDER_PO_NUMBER;
                    worksheet.Cells[rowNumber, 3].Value = OrderExcelColumnNames.SUPPLIER;
                    worksheet.Cells[rowNumber, 4].Value = OrderExcelColumnNames.PAYMENT_METHOD_NAME;
                    worksheet.Cells[rowNumber, 5].Value = OrderExcelColumnNames.CREATED_DATE;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.TOTAL_PO;

                    worksheet.Cells[rowNumber, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowNumber, 7].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                    worksheet.Cells[rowNumber, 2, rowNumber, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 2, rowNumber, 6].Style.Font.Color.SetColor(Color.White);

                    // Second row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 2].Value = order.PONumber;
                    worksheet.Cells[rowNumber, 3].Value = order.SellerCompanyName;
                    worksheet.Cells[rowNumber, 4].Value = order.StatusTypeName;
                    worksheet.Cells[rowNumber, 5].Value = order.CreatedDate.DateTime.ToString();
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.TOTAL_UNITS;

                    worksheet.Cells[rowNumber, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 6].Style.Font.Color.SetColor(Color.White);

                    // Third row
                    rowNumber++;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.TOTAL_STYLES;
                    worksheet.Cells[rowNumber, 7].Value = order.ProductItems.Count();

                    worksheet.Cells[rowNumber, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[rowNumber, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 6].Style.Font.Color.SetColor(Color.White);

                    // Empty row
                    rowNumber++;
                    worksheet.Row(rowNumber).Height = 54;

                    // Header for table with items
                    rowNumber++;
                    worksheet.Cells[rowNumber, 1].Value = OrderExcelColumnNames.BRAND;
                    worksheet.Cells[rowNumber, 2].Value = OrderExcelColumnNames.LM_SKU;
                    worksheet.Cells[rowNumber, 3].Value = OrderExcelColumnNames.CATEGORY;
                    worksheet.Cells[rowNumber, 4].Value = OrderExcelColumnNames.PRODUCT_NAME;
                    worksheet.Cells[rowNumber, 5].Value = OrderExcelColumnNames.MODEL_NUMBER;
                    worksheet.Cells[rowNumber, 6].Value = OrderExcelColumnNames.COLOR_CODE;
                    worksheet.Cells[rowNumber, 7].Value = OrderExcelColumnNames.MATERIAL_CODE;
                    worksheet.Cells[rowNumber, 8].Value = OrderExcelColumnNames.SIZE_TYPE;
                    worksheet.Cells[rowNumber, 9].Value = OrderExcelColumnNames.SIZE;
                    worksheet.Cells[rowNumber, 10].Value = OrderExcelColumnNames.UNITS;
                    worksheet.Cells[rowNumber, 11].Value = OrderExcelColumnNames.CURRENCY;
                    worksheet.Cells[rowNumber, 12].Value = OrderExcelColumnNames.OFFER_UNIT_COST;
                    worksheet.Cells[rowNumber, 13].Value = OrderExcelColumnNames.TOTAL_COST;

                    worksheet.Cells[rowNumber, 1, rowNumber, 13].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[rowNumber, 1, rowNumber, 13].Style.Font.Color.SetColor(Color.White);

                    // Table with items
                    rowNumber++;
                    foreach (var product in order.ProductItems)
                    {
                        foreach (var size in product.SizeItems)
                        {
                            var units = size.ConfirmedUnits == null ? size.Units : (int)size.ConfirmedUnits;
                            double totalCost = units * product.OfferCost;

                            worksheet.Cells[rowNumber, 1].Value = product.ProductDetails.BrandName;
                            worksheet.Cells[rowNumber, 2].Value = product.ProductDetails.SKU;
                            worksheet.Cells[rowNumber, 3].Value = product.ProductDetails.CategoryName;
                            worksheet.Cells[rowNumber, 4].Value = product.ProductDetails.ProductName;
                            worksheet.Cells[rowNumber, 5].Value = product.ProductDetails.ModelNumber;
                            worksheet.Cells[rowNumber, 6].Value = product.ProductDetails.ColorCode;
                            worksheet.Cells[rowNumber, 7].Value = product.ProductDetails.MaterialCode;
                            worksheet.Cells[rowNumber, 8].Value = product.ProductDetails.SizeTypeName;
                            worksheet.Cells[rowNumber, 9].Value = size.SizeName;
                            worksheet.Cells[rowNumber, 10].Value = units;
                            worksheet.Cells[rowNumber, 11].Value = product.CurrencyName;
                            worksheet.Cells[rowNumber, 12].Value = product.OfferCost;
                            worksheet.Cells[rowNumber, 13].Value = totalCost;

                            worksheet.Cells[rowNumber, 12, rowNumber, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Cells[rowNumber, 12, rowNumber, 13].Style.Numberformat.Format = "\"" + currencySymbol + "\" #,##0.00";
                            rowNumber++;
                        }
                    }

                    // First table information
                    rowNumber = 1;
                    worksheet.Cells[rowNumber, 7].Value = totalOrderCost;
                    worksheet.Cells[++rowNumber, 7].Value = totalUnits;

                    // Add width to first column for logo
                    worksheet.Column(1).Width = 36;

                    worksheet.Column(2).AutoFit();
                    worksheet.Column(3).AutoFit();
                    worksheet.Column(4).AutoFit();
                    worksheet.Column(5).AutoFit();
                    worksheet.Column(6).AutoFit();
                    worksheet.Column(7).AutoFit();
                    worksheet.Column(8).AutoFit();
                    worksheet.Column(9).AutoFit();
                    worksheet.Column(10).AutoFit();
                    worksheet.Column(11).AutoFit();
                    worksheet.Column(12).AutoFit();
                    worksheet.Column(13).AutoFit();

                    #endregion

                    // save our new workbook
                    package.Save();

                    return output.ToArray();
                }
            }
        }

        #endregion

        #region Private Methods

        private static void AddImage(ExcelWorksheet ws, int columnIndex, int rowIndex, string imageURL, ImageTypeExport imageType)
        {
            using (var wc = new WebClient())
            {
                try
                {
                    using (var s = wc.OpenRead(imageURL))
                    {
                        using (var bmp = new Bitmap(s))
                        {
                            if (bmp != null)
                            {
                                var picture = ws.Drawings.AddPicture("product-image-" + rowIndex.ToString() + columnIndex.ToString(), bmp);
                                picture.From.Column = columnIndex;
                                picture.From.Row = rowIndex;
                                switch (imageType)
                                {
                                    case ImageTypeExport.Logo:
                                        break;
                                    case ImageTypeExport.ProductImages:
                                        picture.From.ColumnOff = Pixel2MTU(80); //80 pixel space for better alignment
                                        picture.From.RowOff = Pixel2MTU(40);    //10 pixel space for better alignment
                                        break;
                                }
                                picture.SetSize(100);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        private static int Pixel2MTU(int pixels)
        {
            var mtus = pixels * 9525;
            return mtus;
        }

        private enum ImageTypeExport
        {
            Logo = 1,

            ProductImages = 2
        }

        #endregion
    }
}
