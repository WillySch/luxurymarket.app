﻿using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Models.SellerProductSync.FashionDealer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Processors.Helpers
{
    public static class FashionDealerMapping
    {
        public static ProductSyncItemBase MapProductsList(List<Category> categoryList, FashionDealerProduct product)
        {
            var skuSplitted = product.SkuParent.Split('_').ToList();
            var modelNumber = skuSplitted.FirstOrDefault();
            var colorCode = skuSplitted.LastOrDefault();

            var description = !string.IsNullOrWhiteSpace(product.DescEn) ? product.DescEn.Trim() : string.Empty;
            description += !string.IsNullOrWhiteSpace(product.CareEn) ? " <br />" + product.CareEn.Trim() : string.Empty;
            description += !string.IsNullOrWhiteSpace(product.DimH) ? " <br /> DimH: " + product.DimH.Trim() : string.Empty;
            description += !string.IsNullOrWhiteSpace(product.DimW) ? " <br /> DimW: " + product.DimW.Trim() : string.Empty;
            description += !string.IsNullOrWhiteSpace(product.DimD) ? " <br /> DimD: " + product.DimD.Trim() : string.Empty;
            description += !string.IsNullOrWhiteSpace(product.Weight) ? " <br /> Weight: " + product.Weight.Trim() : string.Empty;

            //add size type as default from config
            var sizeType = WebConfigs.FashionDealerSizeTypeRegion;

            //map hierarchy
            var division = string.Empty;
            var department = string.Empty;
            var category = string.Empty;
            var hierarchyList = MapHierarchy(categoryList, product);
            if (hierarchyList != null)
            {
                division = hierarchyList.ElementAtOrDefault(0) != null ? hierarchyList.ElementAtOrDefault(0).Trim() : string.Empty;
                department = hierarchyList.ElementAtOrDefault(1) != null ? hierarchyList.ElementAtOrDefault(1).Trim() : string.Empty;
                //concat category if there are multiple names in hierarchies and skip over division and department
                category = String.Join(" ", hierarchyList.Skip(2)).Trim();
            }

            return new ProductSyncItemBase
            {
                SellerSKU = !string.IsNullOrWhiteSpace(product.SkuParent) ? product.SkuParent.Trim() : null,
                ModelNumber = !string.IsNullOrWhiteSpace(modelNumber) ? modelNumber.Trim() : null,
                MaterialCode = null,
                ColorCode = !string.IsNullOrWhiteSpace(colorCode) ? colorCode.Trim() : null,
                Color = !string.IsNullOrWhiteSpace(product.ColorEn) ? product.ColorEn.Trim() : null,
                Material = !string.IsNullOrWhiteSpace(product.MaterialEn) ? product.MaterialEn.Trim() : null,
                Brand = !string.IsNullOrWhiteSpace(product.Brand) ? product.Brand.Trim() : null,
                Season = !string.IsNullOrWhiteSpace(product.Season) ? product.Season.Trim() : null,
                SizeType = sizeType,
                SizeTypeDescription = sizeType,
                Name = !string.IsNullOrWhiteSpace(product.TitleEn) ? product.TitleEn.Trim() : null,
                Description = description,
                SellerRetailPrice = Convert.ToDouble(product.StockPrice),
                Business = !string.IsNullOrWhiteSpace(product.Sex) ? product.Sex.Trim() : null,
                Division = division,
                Department = department,
                Category = category,
                Origin = !string.IsNullOrWhiteSpace(product.MadeEn) ? product.MadeEn.Trim() : null
            };
        }

        public static List<ProductSyncImageItem> MapImage(this FashionDealerProduct product)
        {
            var itemImagesList = new List<ProductSyncImageItem>();
            foreach (var image in product.Images)
            {
                itemImagesList.Add(new ProductSyncImageItem
                {
                    SellerSKU = product.SkuParent.Trim(),
                    ImageUrl = image.Trim()
                });
            }
            return itemImagesList;
        }

        private static List<string> MapHierarchy(List<Category> categoryList, FashionDealerProduct product)
        {
            if (categoryList == null)
            {
                return null;
            }

            var productCatId = product.Categories != null ? product.Categories.FirstOrDefault().CategoryId : string.Empty;
            var hierarchyNamesList = new List<string>();

            //find leaf category by id
            var category = categoryList.FirstOrDefault(c => c.Id.CategoryId.Equals(productCatId));
            if (category == null)
            {
                return null;
            }
            else
            {
                hierarchyNamesList.Add(category.NameEn);

                //search up in category hierarchy by parent id
                while (category != null && category.Parent != null)
                {
                    category = categoryList.FirstOrDefault(c => c.Id.CategoryId.Equals(category.Parent.ParentId));
                    if (category != null)
                    {
                        hierarchyNamesList.Add(category.NameEn);
                    }
                }

                //reverse hierarchy list
                hierarchyNamesList.Reverse();

                return hierarchyNamesList;
            }

        }
    }
}
