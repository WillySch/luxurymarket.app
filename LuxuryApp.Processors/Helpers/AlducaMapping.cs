﻿using LuxuryApp.Contracts.Models.SellerProductSync;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Processors.Helpers
{
    public static class AlducaMapping
    {
        public static List<ProductSyncImageItem> ImageMap(this AlducaProduct product)
        {
            //process image url
            var itemImagesList = new List<ProductSyncImageItem>();
            var listOfImagesSplitted = product.ItemImages.Split(',').ToList();
            int indexOfImage = 1;
            foreach (var image in listOfImagesSplitted)
            {
                var imageReplaced = image.Replace("pic" + indexOfImage++ + ":", "");
                if (!String.IsNullOrWhiteSpace(imageReplaced))
                {
                    itemImagesList.Add(new ProductSyncImageItem { SellerSKU = product.ProductID?.Trim(), ImageUrl = imageReplaced.Trim() });
                }
            }
            return itemImagesList;
        }
        public static ProductSyncItem Map(this AlducaProduct product)
        {

            return new ProductSyncItem
            {
                SellerSKU = product.ProductID?.Trim(),
                ModelNumber = product.Model?.Trim(),
                //MaterialCode = null,
                // ColorCode = null,
                Color = product.Color?.Trim(),
                Material = product.Texture?.Trim(),
                Brand = product.Brand?.Trim(),
                Season = product.Season?.Trim(),
                SizeType = product.CountrySize,
                SizeTypeDescription = product.CountrySize,
                Name = product.Title,
                Description = product.ItemDescription?.Trim(),
                SellerRetailPrice = Convert.ToDouble(product.MarketPrice),
                Business = product.Suitable?.Trim(),
                // Division = "",
                //Department = "",
                Category = product.Category.Replace(product.Suitable?.Trim(), "")?.Trim(),
                Origin = product.Made?.Trim()
            };
        }
    }
}
