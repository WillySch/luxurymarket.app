﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Contracts.Access;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models.Cart;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Authorization;
using LuxuryApp.Processors.Helpers.Export;
using LuxuryApp.Resources.Errors;
using LuxuryApp.Processors.Resources;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Helpers;

namespace LuxuryApp.Processors.Services
{
    public class CartService : ICartService
    {
        private readonly IContextProvider _contextProvider;
        private readonly ICompanyAccess _access;
        private readonly IOfferRepository _offerRepository;
        private readonly ICartRepository _cartRepository;
        private readonly ICurrencyService _currencyService;
        private readonly IProductService _productService;

        public CartService(IContextProvider contextProvider,
            ICompanyAccess companyAccess,
            IOfferRepository offerRepository,
            ICartRepository cartRepository,
            ICurrencyService currencyService,
            IProductService productService)
        {
            _contextProvider = contextProvider;
            _access = companyAccess;
            _offerRepository = offerRepository;
            _cartRepository = cartRepository;
            _currencyService = currencyService;
            _productService = productService;
        }

        public ApiResult<CartSummary> GetCartSummaryForBuyer(int buyerCompanyId)
        {
            if (!_access.CanAddToCartForCompany(buyerCompanyId))
            {
                return new ApiResultForbidden<CartSummary>(null);
            }

            var cartId = _cartRepository.GetOrCreateCartId(
                _contextProvider.GetLoggedInUserId(),
                buyerCompanyId,
                GetDefaultCurrency().CurrencyId);

            _cartRepository.UpdateCartTotalsByCartId(cartId);
            var cartSummary = _cartRepository.GetCartSummary(cartId);
            return cartSummary.ToApiResult();
        }

        public ApiResult<CartOfferProductSummary> AddOfferProductSizeToCart(SaveOfferProductSizesToCartModel model)
        {

            if (!_access.CanAddToCartForCompany(model.BuyerCompanyId))
            {
                return new ApiResultForbidden<CartOfferProductSummary>(null);
            }

            var cartId = _cartRepository.GetOrCreateCartId(_contextProvider.GetLoggedInUserId(), model.BuyerCompanyId,
                GetDefaultCurrency().CurrencyId);

            var errors = AddItemsToCart(cartId, model.ItemQuantities);

            if (!string.IsNullOrWhiteSpace(errors))
                return new ApiResultForbidden<CartOfferProductSummary>(null, errors);

            var cartSummary = _cartRepository.GetCartSummary(cartId);
            var cartItems = _cartRepository.GetCartTimes(cartId);
            return new CartOfferProductSummary() { Items = cartItems, CartSummaryItem = cartSummary }.ToApiResult();
        }

        private string AddItemsToCart(int cartIt, SaveOfferProductSizesToCartModel.ItemQuantity[] itemQuantities)
        {
            var offerProductSizeIds = itemQuantities.Select(x => x.OfferProductSizeId).ToArray();
            var businessFlagsGetter = _offerRepository.TaskGetOfferProductSizeCartBusinessFlags(offerProductSizeIds);
            var ancestorsGetter = _offerRepository.TaskGetOfferProductSizeIdsAncestors(offerProductSizeIds);

            var bussinessFlags = businessFlagsGetter.Result;
            var offerProductSizeIdAncestors = ancestorsGetter.Result;


            int[] offerIdsTakeAll;
            int[] offerProductSizesIdsLineBuy;
            int[] onlyOfferProductSizesIds;

            SplitIdsByBusinessFlags(bussinessFlags, offerProductSizeIdAncestors,
                out offerIdsTakeAll, out offerProductSizesIdsLineBuy, out onlyOfferProductSizesIds);

            var onlyOfferProductSizes =
            (from opsi in onlyOfferProductSizesIds
             join iq in itemQuantities on opsi equals iq.OfferProductSizeId
             select iq).ToArray();

            if (offerIdsTakeAll.Any())
            {
                foreach (var offerid in offerIdsTakeAll)
                {
                    var errorHasReservedItems = CheckHasReservedItems(offerid, null, cartIt);
                    if (!string.IsNullOrWhiteSpace(errorHasReservedItems))
                        return errorHasReservedItems;
                }
                _cartRepository.AddEntireOffersToCart(cartIt, offerIdsTakeAll);
            }
            if (offerProductSizesIdsLineBuy.Any())
            {
                foreach (var offerProductId in offerProductSizesIdsLineBuy)
                {
                    var errorHasReservedItems = CheckHasReservedItems(null, offerProductId, cartIt);
                    if (!string.IsNullOrWhiteSpace(errorHasReservedItems))
                        return errorHasReservedItems;
                }
                _cartRepository.AddEntireOfferProductsToCart(cartIt, offerProductSizesIdsLineBuy);
            }
            if (onlyOfferProductSizes.Any())
            {
                foreach (var offerProductSize in onlyOfferProductSizes)
                {
                    var errorMessage = OfferProductSizeHasAvailableQuantity(offerProductSize.OfferProductSizeId, offerProductSize.Quantity, cartIt);
                    if (!string.IsNullOrWhiteSpace(errorMessage))
                        return errorMessage;
                }
                _cartRepository.AddOfferProductSizesToCart(cartIt, onlyOfferProductSizes);
            }

            return string.Empty;
        }

        private void SplitIdsByBusinessFlags(OfferProductSizeCartBusinessFlags[] bussinessFlags,
            OfferProductSizeIdAncestors[] offerProductSizeIdAncestors,
            out int[] offerIdsTakeAll,
            out int[] offerProductSizesIdsLineBuy,
            out int[] onlyOfferProductSizesIds)
        {
            offerIdsTakeAll =
            (from b in bussinessFlags
             where b.TakeAll
             join a in offerProductSizeIdAncestors on b.OfferProductSizeId equals a.OfferProductSizeId
             select a.OfferId).Distinct().ToArray();
            offerProductSizesIdsLineBuy =
            (from b in bussinessFlags
             where b.LineBuy && !b.TakeAll
             join a in offerProductSizeIdAncestors on b.OfferProductSizeId equals a.OfferProductSizeId
             select a.OfferProductId).Distinct().ToArray();
            onlyOfferProductSizesIds =
            (from b in bussinessFlags
             where !b.LineBuy && !b.TakeAll
             select b.OfferProductSizeId).ToArray();
        }


        //private void AddItemsToCart(int cartIt, AddOfferProductSizesToCartModel.ItemQuantity[] itemQuantities)
        //{
        //    foreach (var itemQuantity in itemQuantities)
        //    {
        //        AddItemToCart(cartIt, itemQuantity);
        //    }
        //}

        //private void AddItemToCart(int cartId, AddOfferProductSizesToCartModel.ItemQuantity itemQuantity)
        //{
        //    var offerProductSizeId = itemQuantity.OfferProductSizeId;
        //    if (_offerRepository.IsOfferTakeAllForOfferProductSizeId(offerProductSizeId))
        //    {
        //        var offerId = _offerRepository.GetOfferIdForOfferProductSizeId(offerProductSizeId);
        //        _cartRepository.AddEntireOfferToCart(cartId, offerId);
        //    }else if (_offerRepository.IsProductLineBuyForOfferProductSizeId(offerProductSizeId))
        //    {
        //        var offerProductId = _offerRepository.GetOfferProductIdForOfferProductSizeId(offerProductSizeId);
        //        _cartRepository.AddEntireOfferProductToCart(cartId, offerProductId);
        //    }
        //    else
        //    {
        //        var quantity = itemQuantity.Quantity;

        //        //todo: quantity validation
        //        _cartRepository.AddOfferProductSizeToCart(cartId, offerProductSizeId, quantity);
        //    }
        //}

        public ApiResult<CartOfferProductSummary> AddEntireOfferToCart(AddEntireOfferToCartModel model)
        {
            return AddEntireOfferToCart(model.BuyerCompanyId, model.OfferId);
        }

        public ApiResult<CartOfferProductSummary> AddProductsToCart(AddProductsToCartModel model)
        {
            return AddProductsToCart(model.BuyerCompanyId, model.ProductId);
        }

        public ApiResult<CartOfferProductSummary> AddAllProductToCart(AddAllProductToCartModel model)
        {
            return AddAllProductToCart(model.BuyerCompanyId, model.Search);
        }

        public ApiResult<SetCartItemActiveFlagResult> SetCartItemSellerUnitSizeActiveFlag(SetCartItemActiveModel model)
        {
            var cartItemIds = _cartRepository.GetCartItemIdentifiersByCartItemSellerUnitIdentifier(model.CartItemSellerUnitIdentifier);
            if (!cartItemIds.Select(x => x.BuyerCompanyId).Distinct().All(bc => _access.CanAddToCartForCompany(bc)))
            {
                return new ApiResultForbidden<SetCartItemActiveFlagResult>(null);
            }

            var cartId = cartItemIds.Select(x => x.CartId).Distinct().Single();

            var offerProductSizeIds = cartItemIds.Select(x => x.OfferProductSizeId).ToArray();
            var businessFlagsGetter = _offerRepository.TaskGetOfferProductSizeCartBusinessFlags(offerProductSizeIds);

            var bussinessFlags = businessFlagsGetter.Result;
            var offerProductSizeIdAncestors = cartItemIds.Select(x =>
                new OfferProductSizeIdAncestors
                {
                    OfferId = x.OfferId,
                    OfferProductId = x.OfferProductId,
                    OfferProductSizeId = x.OfferProductSizeId,
                }).ToArray();

            int[] offerIdsTakeAll;
            int[] offerProductSizesIdsLineBuy;
            int[] onlyOfferProductSizesIds;

            SplitIdsByBusinessFlags(bussinessFlags, offerProductSizeIdAncestors,
                out offerIdsTakeAll, out offerProductSizesIdsLineBuy, out onlyOfferProductSizesIds);

            var activeFlag = model.ActiveFlag;

            if (offerIdsTakeAll.Any())
            {
                _cartRepository.SetActiveFlagForEntireOffersUnderCart(cartId, offerIdsTakeAll, activeFlag);
            }
            if (offerProductSizesIdsLineBuy.Any())
            {
                _cartRepository.SetActiveFlagForEntireOfferProductsUnderCart(cartId, offerProductSizesIdsLineBuy, activeFlag);
            }
            if (onlyOfferProductSizesIds.Any())
            {
                _cartRepository.SetActiveFlagForOfferProductSizesUnderCart(cartId, offerProductSizeIds, activeFlag);
            }

            #region todo: optimize by giving only what changed

            var cartSummary = _cartRepository.GetCartSummary(cartId);
            var cartItems = _cartRepository.GetCartItems(cartId);

            var r = new SetCartItemActiveFlagResult
            {
                CartSummary = cartSummary,
                CartItems = cartItems,
            };

            return r.ToApiResult();

            #endregion

            //if (cartItemIds == null)
            //{
            //    return new ApiResultForbidden<SetCartItemActiveFlagResult>(null); // todo: unprocessable entity
            //}
            //var activeFlag = model.ActiveFlag;
            //var cartId = cartItemIds.CartId;
            //var offerProductSizeId = cartItemIds.OfferProductSizeId;
            //if (_offerRepository.IsOfferTakeAllForOfferProductSizeId(offerProductSizeId))
            //{
            //    var offerId = cartItemIds.OfferId;
            //    var result = _cartRepository.SetActiveFlagForEntireOfferUnderCart(cartId, offerId, activeFlag);
            //    return result.ToApiResult();
            //}
            //else if (_offerRepository.IsProductLineBuyForOfferProductSizeId(offerProductSizeId))
            //{
            //    var offerProductId = cartItemIds.OfferProductId;
            //    var result = _cartRepository.SetActiveFlagForEntireOfferProductUnderCart(cartId, offerProductId, activeFlag);
            //    return result.ToApiResult();
            //}
            //els
            //{e
            //    var result = _cartRepository.SetActiveFlagForOfferProductSizeUnderCart(cartId, offerProductSizeId, activeFlag);
            //    return result.ToApiResult();
            //}

        }

        public ApiResult<UpdateCartItemsQuantitiesResult> UpdateCartItemsQuantities(SaveOfferProductSizesToCartModel model)
        {

            if (!_access.CanAddToCartForCompany(model.BuyerCompanyId))
            {
                return new ApiResultForbidden<UpdateCartItemsQuantitiesResult>(null);
            }

            if (model.ItemQuantities.Any(x => x.Quantity < 0))
            {
                return new ApiResultUnprocessableEntity<UpdateCartItemsQuantitiesResult>(null, ErrorMessages.MessageQuantityShouldBeNonNegative);
            }

            var cartId = _cartRepository.GetOrCreateCartId(_contextProvider.GetLoggedInUserId(), model.BuyerCompanyId,
                GetDefaultCurrency().CurrencyId);
            var cart = _cartRepository.GetCart(cartId);

            var nonAvailableQuantities =
            (from iq in model.ItemQuantities
             join cisi in
             cart.Items.SelectMany(
                 x => x.SellerUnits.SelectMany(y => x.SellerUnits.SelectMany(z => z.SellerUnitSizes)))
             on iq.OfferProductSizeId equals cisi.OfferProductSizeId
             where iq.Quantity > cisi.AvailableQuantity
             select new { iq, cisi }).ToArray();
            if (nonAvailableQuantities.Any())
            {
                var messages =
                    nonAvailableQuantities.Select(x =>
                        string.Format(ErrorMessages.FormatQuantityOutOfStock,
                            x.iq.Quantity,
                            x.cisi.AvailableQuantity)).ToArray();
                return new ApiResultUnprocessableEntity<UpdateCartItemsQuantitiesResult>(null, messages);
            }

            int[] offerIdsTakeAll;
            int[] offerProductSizesIdsLineBuy;
            int[] onlyOfferProductSizesIds;


            var offerProductSizeIds = model.ItemQuantities.Select(x => x.OfferProductSizeId).ToArray();
            var businessFlagsGetter = _offerRepository.TaskGetOfferProductSizeCartBusinessFlags(offerProductSizeIds);
            var ancestorsGetter = _offerRepository.TaskGetOfferProductSizeIdsAncestors(offerProductSizeIds);

            var bussinessFlags = businessFlagsGetter.Result;
            var offerProductSizeIdAncestors = ancestorsGetter.Result;

            SplitIdsByBusinessFlags(bussinessFlags, offerProductSizeIdAncestors,
                out offerIdsTakeAll, out offerProductSizesIdsLineBuy, out onlyOfferProductSizesIds);

            if (offerIdsTakeAll.Any())
            {
                return new ApiResultUnprocessableEntity<UpdateCartItemsQuantitiesResult>(null, ErrorMessages.MessageCartItemsHasOfferTakeAll);
            }
            if (offerProductSizesIdsLineBuy.Any())
            {
                return new ApiResultUnprocessableEntity<UpdateCartItemsQuantitiesResult>(null, ErrorMessages.MessageCartItemsHasProductLineBuy);
            }
            if (!onlyOfferProductSizesIds.Any())
            {
                return new ApiResultUnprocessableEntity<UpdateCartItemsQuantitiesResult>(null, ErrorMessages.MessageCartItemNotFound);
            }

            foreach (var item in model.ItemQuantities)
            {
                var errorMessage = OfferProductSizeHasAvailableQuantity(item.OfferProductSizeId, item.Quantity, cartId);
                if (!string.IsNullOrWhiteSpace(errorMessage))
                    return new ApiResultForbidden<UpdateCartItemsQuantitiesResult>(null, errorMessage);
            }

            _cartRepository.AddOfferProductSizesToCart(cartId, model.ItemQuantities);

            // todo: optimize
            var cartSummary = _cartRepository.GetCartSummary(cartId);
            cart = _cartRepository.GetCart(cartId);
            var cartItemSummaries =
            (from
                ci in cart.Items
             group ci by ci.ProductId
                into pgci
             select new CartItemSummary
             {
                 ProductId = pgci.Key,
                 ItemTotalOfferCost = pgci.First().TotalLandedCost,
                 CartItemSummaries = pgci.SelectMany(x => x.SellerUnits).Select(cisu => new CartItemSellerSummary
                 {

                     CartItemSellerUnitIdentifier = cisu.CartItemSellerUnitIdentifier,
                     UnitsCount = cisu.UnitsCount,
                     TotalOfferCost = cisu.TotalLandedCost,
                     OfferId = cisu.OfferId
                 }).ToArray(),
             }).ToArray();

            var r = new UpdateCartItemsQuantitiesResult
            {
                CartSummary = cartSummary,
                CartItemSummaries = cartItemSummaries,
            };
            return r.ToApiResult();
        }

        public ApiResult<CartSummary> EmptyCartForBuyer(int buyerCompanyId)
        {
            var apiResultCartSummary = GetCartSummaryForBuyer(buyerCompanyId);
            if (!apiResultCartSummary.IsSuccesfull)
            {
                return apiResultCartSummary;
            }
            var cartId = apiResultCartSummary.Data.CartId;
            _cartRepository.EmptyCart(cartId);
            return _cartRepository.GetCartSummary(cartId).ToApiResult();
        }

        public ApiResult<RemoveProductFromCartResult> RemoveProductsFromCart(RemoveProductsFromCartModel model)
        {
            if (!_access.CanAddToCartForCompany(model.BuyerCompanyId))
            {
                return new ApiResultForbidden<RemoveProductFromCartResult>(null);
            }
            var cart = GetCartForBuyer(model.BuyerCompanyId).Data;

            var itemIdsForProductsIncludedInTakeAllOffersToRemove = GetItemsIdsForProductsIncludedInTakeAllOffers(cart, model.ProductIds);
            var itemIdsForProductsNotIncludedInTakeAllOffers = GetItemsIdsForProductsNotIncludedInTakeAllOffers(cart, model.ProductIds);

            var itemIdsToRemove =
                itemIdsForProductsNotIncludedInTakeAllOffers
                .Union(itemIdsForProductsIncludedInTakeAllOffersToRemove)
                    .ToArray();

            var beforeRemoveProductIds = cart.Items.Select(x => x.ProductId).ToArray();

            _cartRepository.RemoveCartItems(cart.Id, itemIdsToRemove);
            var cartSummary = _cartRepository.GetCartSummary(cart.Id);

            var afterRemoveProductIds = _cartRepository.GetCartItems(cart.Id).Select(x => x.ProductId);

            var removedProductIds = beforeRemoveProductIds.Except(afterRemoveProductIds).ToArray();
            var result = new RemoveProductFromCartResult
            {
                CartSummary = cartSummary,
                RemovedProductIds = removedProductIds,
            };
            return result.ToApiResult();
        }

        public ApiResult<RemoveCartItemSellerUnitResult> RemoveItemSellerUnitFromCart(RemoveItemSellerUnitFromCartModel model)
        {
            if (!_access.CanAddToCartForCompany(model.BuyerCompanyId))
            {
                return new ApiResultForbidden<RemoveCartItemSellerUnitResult>(null);
            }

            var cart = GetCartForBuyer(model.BuyerCompanyId).Data;

            var itemIdsForProductsIncludedInTakeAllOffersToRemove = GetItemsIdsForItemSellerUnitsIncludedInTakeAllOffers
            (
                cart,
                new[] { model.CartItemSellerUnitIdentifier }
            );
            var itemIdsForProductsNotIncludedInTakeAllOffers = GetItemsIdsForForItemSellerUnitsNotIncludedInTakeAllOffers
            (
                cart,
                new[] { model.CartItemSellerUnitIdentifier }
            );

            var itemIdsToRemove =
                itemIdsForProductsNotIncludedInTakeAllOffers
                .Union(itemIdsForProductsIncludedInTakeAllOffersToRemove)
                    .ToArray();
            var beforeRemoveCartItems = cart.Items;
            _cartRepository.RemoveCartItems(cart.Id, itemIdsToRemove);
            var cartSummary = _cartRepository.GetCartSummary(cart.Id);
            var afterRemoveCartItems = _cartRepository.GetCartItems(cart.Id);

            var result = ComputeResult(cartSummary, beforeRemoveCartItems, afterRemoveCartItems);
            return result.ToApiResult();
        }

        public ApiResult<CartTotalSummary> GetTotalSummary(int cartId, bool onlyValidItems)
        {
            if (!_access.CanUserSubmitCart(cartId))
            {
                return new ApiResultForbidden<CartTotalSummary>(null);
            }

            return _cartRepository.GetTotalSummary(cartId, onlyValidItems, _contextProvider.GetLoggedInUserId()).ToApiResult();
        }

        private RemoveCartItemSellerUnitResult ComputeResult(CartSummary cartSummary, CartItem[] beforeRemoveCartItems, CartItem[] afterRemoveCartItems)
        {
            var removedProducIds =
                beforeRemoveCartItems.Select(x => x.ProductId).Except(afterRemoveCartItems.Select(x => x.ProductId))
                    .ToArray();
            var remainingItems =
                beforeRemoveCartItems.Where(x => removedProducIds.All(p => p != x.ProductId));
            // Quick fix: Just return the new cart items - no new logic necesarry for now
            //var affectedItems =
            //    remainingItems.Except(afterRemoveCartItems,
            //        new Utils.Helpers.EqualityComparer<CartItem>(
            //            (x, y) =>
            //            {
            //                var eq = x.CartId == y.CartId &&
            //                         x.ProductId == y.ProductId &&
            //                         x.AverageUnitCost == y.AverageUnitCost;
            //                //Math.Abs(x.AverageUnitOfferCost - y.AverageUnitOfferCost) < (decimal)0.0001;
            //                return eq;
            //            })).ToArray();

            //foreach (var affectedItem in affectedItems)
            //{
            //    var product = afterRemoveCartItems.Where(x => x.ProductId == affectedItem.ProductId).FirstOrDefault();
            //    if (product != null)
            //    {
            //        affectedItem.SellerUnits = product.SellerUnits;
            //    }
            //}

            var result = new RemoveCartItemSellerUnitResult
            {
                CartSummary = cartSummary,
                RemovedProductIds = removedProducIds,
                AffectedRemainingCartItems = afterRemoveCartItems,
            };
            return result;
        }

        private int[] GetItemsIdsForForItemSellerUnitsNotIncludedInTakeAllOffers(Cart cart, CartItemSellerUnitIdentifier[] cartItemSellerUnitIdentifiers)
        {
            var result =
                (from cisi in cartItemSellerUnitIdentifiers
                 join csi in cart.Items.SelectMany(x => x.SellerUnits)
                 on cisi equals csi.CartItemSellerUnitIdentifier
                 select csi)
                    .SelectMany(t => t.SellerUnitSizes)
                    .Select(s => s.Id)
                    .ToArray();
            return result;
        }

        private int[] GetItemsIdsForItemSellerUnitsIncludedInTakeAllOffers(Cart cart, CartItemSellerUnitIdentifier[] cartItemSellerUnitIdentifiers)
        {
            var sellerUnits = cart.Items.SelectMany(x => x.SellerUnits).ToArray();
            var offerIds = (from cisi in cartItemSellerUnitIdentifiers
                            join csi in sellerUnits
                                .Where(y => y.IsTakeOffer)
                            on cisi equals csi.CartItemSellerUnitIdentifier
                            select csi.OfferId);
            var result =
                (from oid in offerIds
                 join su in sellerUnits on oid equals su.OfferId
                 select su)
                    .SelectMany(x => x.SellerUnitSizes)
                    .Select(y => y.Id)
                    .ToArray();
            return result;
        }

        private int[] GetItemsIdsForProductsNotIncludedInTakeAllOffers(Cart cart, int[] productIds)
        {
            var result =
                (from pi in productIds
                 join ci in cart.Items on pi equals ci.ProductId
                 from csi in ci.SellerUnits
                    .Where(y => !y.IsTakeOffer)
                 select csi)
                    .SelectMany(tz => tz.SellerUnitSizes)
                    .Select(sh => sh.Id)
                    .ToArray();
            return result;
        }

        private int[] GetItemsIdsForProductsIncludedInTakeAllOffers(Cart cart, int[] productIds)
        {
            var sellerUnits = cart.Items.SelectMany(x => x.SellerUnits).ToArray();
            var offerIds = (from pi in productIds
                            join ci in cart.Items on pi equals ci.ProductId
                            from csi in ci.SellerUnits
                                .Where(y => y.IsTakeOffer)
                            select csi.OfferId).ToArray();
            var result =
                (from oid in offerIds
                 join su in sellerUnits on oid equals su.OfferId
                 select su)
                    .SelectMany(x => x.SellerUnitSizes)
                    .Select(y => y.Id)
                    .ToArray();
            return result;
        }

        private ApiResult<CartOfferProductSummary> AddEntireOfferToCart(int buyerCompanyId, int offerId)
        {
            if (!_access.CanAddToCartForCompany(buyerCompanyId))
            {
                return new ApiResultForbidden<CartOfferProductSummary>(null);
            }

            var cartId = _cartRepository.GetOrCreateCartId(_contextProvider.GetLoggedInUserId(), buyerCompanyId,
                    GetDefaultCurrency().CurrencyId);

            var errorHasReservedItems = CheckHasReservedItems(offerId, null, cartId);
            if (!string.IsNullOrWhiteSpace(errorHasReservedItems))
                return new ApiResultForbidden<CartOfferProductSummary>(null, errorHasReservedItems);

            _cartRepository.AddEntireOfferToCart(cartId, offerId);

            var cartSummary = _cartRepository.GetCartSummary(cartId);
            var cartItems = _cartRepository.GetCartTimes(cartId);

            return new CartOfferProductSummary() { Items = cartItems, CartSummaryItem = cartSummary }.ToApiResult();
        }

        private ApiResult<CartOfferProductSummary> AddProductsToCart(int buyerCompanyId, List<int> productId)
        {
            if (!_access.CanAddToCartForCompany(buyerCompanyId))
            {
                return new ApiResultForbidden<CartOfferProductSummary>(null);
            }

            var cartId = _cartRepository.GetOrCreateCartId(_contextProvider.GetLoggedInUserId(), buyerCompanyId,
                    GetDefaultCurrency().CurrencyId);

            _cartRepository.AddProductsToCart(cartId, productId.ToArray());

            var cartSummary = _cartRepository.GetCartSummary(cartId);
            var cartItems = _cartRepository.GetCartTimes(cartId);

            return new CartOfferProductSummary() { Items = cartItems, CartSummaryItem = cartSummary }.ToApiResult();
        }

        private ApiResult<CartOfferProductSummary> AddAllProductToCart(int buyerCompanyId, ProductSearchItems search)
        {
            if (!_access.CanAddToCartForCompany(buyerCompanyId))
            {
                return new ApiResultForbidden<CartOfferProductSummary>(null);
            }

            var cartId = _cartRepository.GetOrCreateCartId(_contextProvider.GetLoggedInUserId(), buyerCompanyId,
                    GetDefaultCurrency().CurrencyId);

            search.PageSize = WebConfigs.ProductsPageSize;
            var products = _productService.SearchProduct(search).Products;
            var productIds = products.Select(x => x.Id);

            _cartRepository.AddProductsToCart(cartId, productIds.ToArray());

            var cartSummary = _cartRepository.GetCartSummary(cartId);
            var cartItems = _cartRepository.GetCartTimes(cartId);

            return new CartOfferProductSummary() { Items = cartItems, CartSummaryItem = cartSummary }.ToApiResult();
        }

        public ApiResult<Cart> GetCartForBuyer(int buyerCompanyId)
        {
            if (!_access.CanViewCartForCompany(buyerCompanyId))
            {
                return new ApiResultForbidden<Cart>(null);
            }

            var cartId = _cartRepository.GetOrCreateCartId(
                _contextProvider.GetLoggedInUserId(),
                buyerCompanyId,
                GetDefaultCurrency().CurrencyId);
            return _cartRepository.GetCart(cartId).ToApiResult();
        }

        public ApiResult<CartSplitPreview> GetCartSplitPreview(int buyerCompanyId)
        {
            if (!_access.CanViewCartForCompany(buyerCompanyId))
            {
                return new ApiResultForbidden<CartSplitPreview>(null);
            }

            var cartId = _cartRepository.GetOrCreateCartId(
                _contextProvider.GetLoggedInUserId(),
                buyerCompanyId,
                GetDefaultCurrency().CurrencyId);
            return _cartRepository.GetCartSplitPreview(cartId).ToApiResult();
        }

        private Currency GetDefaultCurrency()
        {
            return _currencyService.GetByName("USD").SingleOrDefault();
        }

        public ApiResult<byte[]> ExportCartExcel(int buyerCompanyId)
        {
            var cartInformation = GetCartForBuyer(buyerCompanyId).Data;
            cartInformation.Items = cartInformation.Items.OrderBy(x => x.BrandName).ThenBy(x => x.CategoryName).ToArray();
            var cartExcelFileContent = ExportHelper.ExportCartExcel(cartInformation);
            return cartExcelFileContent.ToApiResult(cartExcelFileContent.Length);
        }

        public ApiResult<CartItemValidatelCollection> GetInvalidItems(int cartId)
        {
            if (!_access.CanUserSubmitCart(cartId))
            {
                return new ApiResultForbidden<CartItemValidatelCollection>(null);
            }

            return _cartRepository.GetInvalidItems(cartId).ToApiResult();
        }

        public ApiResult<CartItemReservedCollection> RelockProduct(int cartId, int offerProductId)
        {
            if (!_access.CanUserSubmitCart(cartId))
            {
                return new ApiResultForbidden<CartItemReservedCollection>(null);
            }
            var updatedItems = _cartRepository.RelockCartProduct(cartId, offerProductId);

            var cartItems = _cartRepository.GetCartTimes(cartId);

            foreach (var item in cartItems)
            {
                var updatedCartItem = updatedItems.Where(x => x.CartItemId == item.CartItemId).FirstOrDefault();
                if (updatedCartItem != null && updatedCartItem.CartItemId > 0)
                {
                    item.ReservedStatusType = updatedCartItem.ReservedStatusType;
                    item.ReservedMessage = item.ReservedStatusType == CartReservedStatusType.Failed_OfferIsNotActive ? ErrorMessages.OfferIsNotActive :
                                           item.ReservedStatusType == CartReservedStatusType.Success ? ErrorMessages.CartItemReserveWithSuccess :
                                           item.ReservedStatusType == CartReservedStatusType.PartialReserved ? ErrorMessages.CartItemReservePartial :
                                           item.ReservedStatusType == CartReservedStatusType.ReservedByOthers ? ErrorMessages.CartItemReserveFailed_ReservedByOthers :
                                           item.ReservedStatusType == CartReservedStatusType.Failed_InvalidQuantity ? ErrorMessages.CartItemReserveFailed_InvalidQuantity
                                            : string.Empty;
                }
            }

            var collection = new CartItemReservedCollection { Items = cartItems };

            var statuses = updatedItems.Where(x => x.ReservedStatusType != CartReservedStatusType.None).Select(x => x.ReservedStatusType).Distinct().ToList();
            if (statuses.Count() == 1)
                collection.ReservedStatusType = statuses.FirstOrDefault();
            else if (statuses.Where(x => x == CartReservedStatusType.Failed_OfferIsNotActive).Any())
                collection.ReservedStatusType = CartReservedStatusType.Failed_OfferIsNotActive;
            else if (statuses.Where(x => x == CartReservedStatusType.Success || x == CartReservedStatusType.PartialReserved).Any())
                collection.ReservedStatusType = CartReservedStatusType.PartialReserved;

            collection.ReservedMessage = collection.ReservedStatusType == CartReservedStatusType.Failed_OfferIsNotActive ? ErrorMessages.OfferIsNotActive :
                                        collection.ReservedStatusType == CartReservedStatusType.Success ? ErrorMessages.CartItemReserveWithSuccess :
                                        collection.ReservedStatusType == CartReservedStatusType.PartialReserved ? ErrorMessages.CartItemReservePartial :
                                        collection.ReservedStatusType == CartReservedStatusType.ReservedByOthers ? ErrorMessages.CartItemReserveFailed_ReservedByOthers :
                                        collection.ReservedStatusType == CartReservedStatusType.Failed_InvalidQuantity ? ErrorMessages.CartItemReserveFailed_InvalidQuantity
                                         : string.Empty;

            return collection.ToApiResult();
        }

        public ApiResult<CartItemReservedCollection> GetCartTimes(int cartId)
        {
            if (!_access.CanUserSubmitCart(cartId))
            {
                return new ApiResultForbidden<CartItemReservedCollection>(null);
            }
            var cartItems = _cartRepository.GetCartTimes(cartId);
            var collection = new CartItemReservedCollection { Items = cartItems };
            return collection.ToApiResult();
        }

        private string CheckHasReservedItems(int? offerId, int? offerProductId, int cartId)
        {
            //check for line buy or take all 
            var checkReservation = _cartRepository.HasReservedItemsByOtherUsers(offerId, offerProductId, cartId);
            if (!checkReservation.IsActiveOffer)
            {
                return ErrorMessages.OfferIsNotActive;
            }
            if (checkReservation.HasReservedItems)
            {
                return ErrorMessages.MessageCartHasReservedItems;
            }
            return string.Empty;
        }

        public string OfferProductSizeHasAvailableQuantity(int offerProductSizeId, int quantity, int? cartId)
        {
            var result = _cartRepository.OfferProductSizeHasAvailableQuantity(offerProductSizeId, quantity, cartId);
            if (!result.IsActiveOffer)
            {
                return ErrorMessages.OfferIsNotActive;
            }
            if (!result.HasAvailableQuantity)
            {
                return ErrorMessages.MessageCartQuantityNotAvailable;
            }
            return string.Empty;
        }

    }
}
