﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Contracts.Access;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models.Cart;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Authorization;

namespace LuxuryApp.Processors.Services
{
    public class UserService : IUserService
    {
        private readonly IContextProvider _contextProvider;
        private readonly ICustomerRepository _customerRepository;

        public UserService(IContextProvider contextProvider,
           ICustomerRepository customerRepository)
        {
            _contextProvider = contextProvider;
            _customerRepository = customerRepository;
        }

        public Task<ApiResult<Customer>> GetCustomerWithCompaniesAsync(int orderId)
        {
            throw new NotImplementedException();
        }
    }
}
