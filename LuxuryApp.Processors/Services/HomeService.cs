﻿using LuxuryApp.Contracts.Models.Home;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Collections.Generic;

namespace LuxuryApp.Processors.Services
{
    public class HomeService : IHomeService
    {
        public Func<IHomeRepository> _homeRepositoryFactory;

        public HomeService(Func<IHomeRepository> homeRepositoryFactory)
        {
            _homeRepositoryFactory = homeRepositoryFactory;
        }

        public List<CustomerServiceTopicsModel> GetServiceTopics(int topicId)
        {
            var result = _homeRepositoryFactory().GetServiceTopics(topicId);
            return result;
        }

        public ApiResult<int> InsertCustomerServiceRequest(CustomerServiceRequestModel model, int userId, DateTimeOffset date)
        {
            var result = _homeRepositoryFactory().InsertCustomerServiceRequest(model, userId, date);
            return result.ToApiResult();
        }
    }
}
