﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Processors.Readers;

namespace LuxuryApp.Processors.Services
{
    public class CountryService
    {
        private readonly Repository _connection;

        public CountryService()
        {
            _connection = new Repository();
        }

        public List<Country> Search(int? id)
        {
            var parameters = new List<SqlParameter>
            {
                 new SqlParameter("@ID", id),
             };
            var reader = _connection.LoadData(StoredProcedureNames.Countries_Search, parameters.ToArray());
            return reader.ToCountryList();
        }
    }
}
