﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Processors.Readers;
using LuxuryApp.Core.DataAccess.Repository;
using System.Data;
using System.Linq;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.DataAccess;
using System.Text;
using LuxuryApp.Core.Infrastructure.Authorization;

namespace LuxuryApp.Processors.Services
{
    public class ProductService : IProductService
    {
        private readonly Repository _connection;
        private readonly IContextProvider _contextProvider;
        public ProductService(IContextProvider contextProvider)
        {
            _connection = new Repository();
            _contextProvider = contextProvider;
        }

        public Product GetProductById(int productId, int companyID)
        {
            if (productId <= 0)
                throw new Exception("Invalid  Id");
            var parameters = new List<SqlParameter>() {
                new SqlParameter("@ProductID", productId),
                new SqlParameter("@CompanyID", companyID),
                new SqlParameter("@UserID",_contextProvider.GetLoggedInUserId())
            };          

            var reader = _connection.LoadData(StoredProcedureNames.Products_GetByID, parameters.ToArray());
            return reader.ToProductDetails();

        }

        public Product GetProductBySku(string sku)
        {
            if (string.IsNullOrWhiteSpace(sku))
                throw new Exception("SKU required");
            var parameter = new SqlParameter("@SKU", sku);

            var reader = _connection.LoadData(StoredProcedureNames.Products_GetBySKU, parameter);
            return reader.ToProductDetails();

        }

        public ProductSearchResponse SearchProduct(ProductSearchItems model)
        {
            var brandIDs = new SqlParameter("@BrandIDs", SqlDbType.Structured);
            brandIDs.Value = ListToDataTable(model.BrandIDs);

            var colorIDs = new SqlParameter("@ColorIDs", SqlDbType.Structured);
            colorIDs.Value = ListToDataTable(model.ColorIDs);

            var categoryIDs = new SqlParameter("@CategoryIDs", SqlDbType.Structured);
            categoryIDs.Value = ListToDataTable(model.CategoryIDs);

            // keyword search
            StringBuilder query = new StringBuilder();
            if (model.Keyword != null)
            {
                var ks = GetNormalizedSearchKeywords(model.Keyword);
                query.Append("(");
                for (int i = 0; i < ks.Length; i++)
                {
                    query.AppendFormat("(p.Name like '%{0}%' or p.SKU like '%{0}%' or p.ModelNumber like '%{0}%' or b.Name like '%{0}%' or cat.Name like '%{0}%' or col.ColorCode like '%{0}%' or scol.Name like '%{0}%')", ks[i]);
                    if (i + 1 < ks.Length)
                    {
                        query.Append(" and ");
                    }
                }
                query.Append(")");
            }

            var parameters = new SqlParameter[]
            {
              new SqlParameter("@PageSize", model.PageSize),
              new SqlParameter("@PageNumber", model.Page),
              new SqlParameter("@sort", (int)model.Sort),
              new SqlParameter("@Keyword", query.ToString()),
              new SqlParameter("@BusinessID", model.BusinessID),
              new SqlParameter("@DivisionID", model.DivisionID),
              new SqlParameter("@DepartmentID", model.DepartmentID),

              categoryIDs,
              brandIDs,
              colorIDs,

              new SqlParameter("@FeaturedEventID", model.FeaturedEventID),
              new SqlParameter("@SellerCompanyID", model.SellerCompanyID),
              new SqlParameter("@CompanyID", model.CompanyID),
              new SqlParameter("@OfferID", model.OfferID)
            };

            var reader = _connection.LoadData(StoredProcedureNames.Products_Search, parameters);
            return reader.ToProductSearchResult();
        }

        public IEnumerable<ProductSearchResult> GetBestSellers(int numberOfItems, int companyID)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@NumberOfItems", numberOfItems),
              new SqlParameter("@CompanyID", companyID)
            };

            var reader = _connection.LoadData(StoredProcedureNames.Products_GetBestSellers, parameters);
            return reader.ToProductListResult();
        }

        public IEnumerable<ProductSearchResult> SearchProduct(int featuredEventID)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@FeaturedEventID", featuredEventID)
            };

            var reader = _connection.LoadData(StoredProcedureNames.Products_Search_FeaturedEvent, parameters);
            return null;// reader.ToProductSearchResult();
        }

        public ApiResult<ProductSearchResult[]> SearchProduct(ProductFilterModel model)
        {
            const string sql =
                @"SELECT
	                ProductId as ID
		                    ,[Name] as ProductName
		                    ,[Sku]
		                    ,[MainImageID]
		                    ,[MainImageName]
		                    ,[BrandID]
		                    ,[BrandName]
		                    ,LMPrice
		                    ,Newest
		                    ,BusinessID
		                    ,DivisionID
		                    ,DepartmentID
		                    ,CategoryID
		                    ,Rn
		                    ,TotalRows
		                    from DBO.fn_ProductSearch (@businessIds, @divisionsIds, @departmentIds, @categoriesIds, 
			                @sort, @companyId,
			                @rowNumberLowerBound, @rowNumberUpperBound)";
            var parameters = new[]
            {
                new SqlParameter("@businessIds", SqlDbType.Structured) {Value = model.BusinessIds.ToIdList(), TypeName = "dbo.IdList"},
                new SqlParameter("@divisionsIds", SqlDbType.Structured) {Value = model.DivisionIds.ToIdList(), TypeName = "dbo.IdList"},
                new SqlParameter("@departmentIds", SqlDbType.Structured) {Value = model.DepartmentIds.ToIdList(), TypeName = "dbo.IdList"},
                new SqlParameter("@categoriesIds", SqlDbType.Structured) {Value = model.CategoriesIds.ToIdList(), TypeName = "dbo.IdList"},
                new SqlParameter("@sort", SqlDbType.Int) {Value = (int)model.Sort},
                new SqlParameter("@companyId", SqlDbType.Int) {Value = model.CompanyId},
                new SqlParameter("@rowNumberLowerBound", SqlDbType.Int) {Value = (model.PageNumber-1)*model.PageSize+1},
                new SqlParameter("@rowNumberUpperBound", SqlDbType.Int) {Value = (model.PageNumber)*model.PageSize},
            };
            var reader = _connection.ExcuteSqlReader(sql, parameters);
            int totalRows;
            var result = reader.ToProductSearchResultPaginated(out totalRows).ToArray();
            return result.ToApiResult(totalRows);
        }

        private DataTable ListToDataTable(List<int> ids)
        {
            DataTable idsDataTable = null;
            if (ids != null)
            {
                idsDataTable = new DataTable();
                idsDataTable.Columns.Add("ID", typeof(int));
                foreach (var prodId in ids)
                {
                    idsDataTable.Rows.Add(prodId);
                }
            }

            return idsDataTable;
        }

        private string[] GetNormalizedSearchKeywords(string keyword)
        {
            if (keyword == null) keyword = string.Empty;

            keyword = keyword.Trim().ToLower().Replace("'", String.Empty).Replace("\"", String.Empty).Replace(">", String.Empty).Replace("<", String.Empty).Replace("&", String.Empty).Replace("select", String.Empty).Replace("insert", String.Empty).Replace("drop", String.Empty).Replace("update", String.Empty).Replace("exec", String.Empty);

            string[] kwds = keyword.Split(new char[] { ' ', ',', '\t', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            return kwds;
        }
    }
}
