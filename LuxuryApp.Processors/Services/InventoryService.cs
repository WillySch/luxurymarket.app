﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Processors.Readers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Services
{
    public class InventoryService
    {
        private readonly Repository _connection;

        public InventoryService()
        {
            _connection = new Repository();
        }

        public APISettings GetSellerApiSettings(SellerAPISettingType sellerAPIType)
        {
            var parameters = new List<SqlParameter>
            {
                 new SqlParameter("@SellerAPIID", (int)sellerAPIType)
             };
            var reader = _connection.LoadData(StoredProcedureNames.Inventory_GetSellerAPISettings, parameters.ToArray());
            return reader.ToAPISettings();
        }

        public List<string> GetSellerSKUsForInventoryUpdate(int sellerID)
        {
            var parameters = new List<SqlParameter>
            {
                 new SqlParameter("@SellerID", sellerID)
             };
            var reader = _connection.LoadData(StoredProcedureNames.Inventory_GetSellerSKUsForInventoryUpdate, parameters.ToArray());
            return reader.ToListOfSKUs();
        }

        public void SaveInventory(List<InventoryResponse> inventory, int sellerID)
        {
            var inventorySqlModelList = new DataTable();
            inventorySqlModelList.Columns.Add("SellerSKU");
            inventorySqlModelList.Columns.Add("SellerSize");
            inventorySqlModelList.Columns.Add("Quantity");

            foreach (var item in inventory)
            {
                foreach (var size in item.Sizes)
                {
                    inventorySqlModelList.Rows.Add(item.VendorSKU.Trim(), size.SellerSize.Trim(), size.Quantity);
                }
            }
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["InventoryUpdateConnectionString"].ToString());
                SqlCommand cmd = new SqlCommand("InventoryUpdate_SaveInventory", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SellerID", SqlDbType.Int)).Value = sellerID;
                cmd.Parameters.Add(new SqlParameter("@InventoryList", SqlDbType.Structured)).Value = inventorySqlModelList;

                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
