﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gs1.Vics.Contracts.Factories;
using Gs1.Vics.Contracts.Models.Common;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Addresses;
using LuxuryApp.Contracts.Models.Ftp;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models.Orders;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using Nito.AsyncEx;
using Gs1.Vics.Contracts.Models.Booking;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.NotificationService;
using System.Configuration;
using LuxuryApp.Contracts.Helpers;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Processors.Services
{
    public class OrderBookingService : IOrderBookingService
    {
        private readonly IAddressService _addressService;
        private readonly IOrderRepository _orderRepository;
        private readonly IEdiSerializeServiceFactory _ediSerializeServiceFactory;
        private readonly ICompanyRepository _companyRepository;
        private readonly IFtpRepository _ftpRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly INotificationHandler<EmailModel> _emailServiceFactory;

        public OrderBookingService(
            IEdiSerializeServiceFactory ediSerializeServiceFactory,
            IOrderRepository orderRepository,
            IAddressService addressService,
            ICompanyRepository companyRepository,
            IFtpRepository ftpRepository,
            ICustomerRepository customerRepository,
            INotificationHandler<EmailModel> emailServiceFactory
            )
        {
            _ediSerializeServiceFactory = ediSerializeServiceFactory;
            _orderRepository = orderRepository;
            _addressService = addressService;
            _companyRepository = companyRepository;
            _ftpRepository = ftpRepository;
            _customerRepository = customerRepository;
            _emailServiceFactory = emailServiceFactory;
        }

        public ApiResult<byte[]> GetSellerOrderBookingDocument(int orderSellerId, out string poNumber, Bookin865Data data = null)
        {
            if (data == null)
                data = GetSellerOrderBookingData(orderSellerId);
            var x2Booking = MapOrderToBooking(data);
            poNumber = data.Order.PONumber;

            var result = _ediSerializeServiceFactory.Serialize(x2Booking);

            return result;
        }

        private Bookin865Data GetSellerOrderBookingData(int orderSellerId)
        {
            var data = new Bookin865Data();

            data.Order = AsyncContext.Run(() => _orderRepository.GetOrderItemForSellerById(orderSellerId));

            data.SellerAddress =
                AsyncContext.Run(() => _addressService.GetAllByCompanyId(data.Order.SellerCompanyId))
                    .FirstOrDefault(x => x.IsMain) ?? new AddressInfo(); ;
            data.SellerCompany = AsyncContext.Run(() => _companyRepository.GetCompanyById(data.Order.SellerCompanyId));

            data.BuyerAddress =
                AsyncContext
                    .Run(() => _addressService.GetAllByCompanyId(data.Order.BuyerCompanyId))
                    .FirstOrDefault(x => x.IsMain) ?? new AddressInfo(); ;
            data.BuyerCompany = AsyncContext.Run(() => _companyRepository.GetCompanyById(data.Order.BuyerCompanyId));
            data.BuyerContactData = AsyncContext.Run(() => _customerRepository.GetCustomerByUserId(data.Order.CreatedBy));
            data.PackingInformation = AsyncContext.Run(() => _orderRepository.GetOrderSellerPackingInformation(orderSellerId)) ?? new OrderSellerPackingInformationInsert();

            return data;
        }

        public async Task<ApiResult> SendEmailAsync(int orderSellerId)
        {
            var data = GetSellerOrderBookingData(orderSellerId);
            if (data == null)
            {
                return new ApiResult(null, null);
            }
            await _emailServiceFactory.SendAsync(new EmailModel
            {
                HasOwnMasterTemplate = false,
                MasterTemplateName = "MasterGrayBackground",
                DataBag = new
                {
                    LogoUrl = WebConfigs.LogoUrl,
                    PONumber = data.Order.PONumber,
                    CreatedDate = data.Order.CreatedDate.Date.ToShortDateString(),

                    BuyerCompanyName = data.BuyerCompany.CompanyName,
                    BuyerContactName = data.BuyerContactData.FullName,
                    BuyerStreet = data.BuyerAddress?.Street1 ?? string.Empty,
                    BuyerCity = data.BuyerAddress?.City ?? string.Empty,
                    BuyerCountryName = data.BuyerAddress?.CountryName ?? string.Empty,
                    BuyerCountryCode = data.BuyerAddress?.CountryIso2Code ?? string.Empty,
                    BuyerState = data.BuyerAddress?.StateName ?? string.Empty,

                    SellerCompanyName = data.SellerCompany.CompanyName,
                    SellerContactName = data.PackingInformation.ContactName,
                    SellerPhone = data.PackingInformation.ContactPhone,
                    SellerContactEmail = data.PackingInformation.ContactEmail,
                    SellerStreet = data.SellerAddress?.Street1 ?? string.Empty,
                    SellerCity = data.SellerAddress?.City ?? string.Empty,
                    SellerCountryName = data.SellerAddress?.CountryName ?? string.Empty,
                    SellerCountryCode = data.SellerAddress?.CountryIso2Code ?? string.Empty,
                    SellerState = data.BuyerAddress?.StateName ?? string.Empty,

                    StartPickupDate = data.PackingInformation.StartPickupDate.Date.ToShortDateString(),
                    FromPickupHour = data.PackingInformation.FromPickupHour.ToString(@"hh\:mm"),
                    ToPickupHour = data.PackingInformation.ToPickupHour.ToString(@"hh\:mm"),
                    EndPickupDate = data.PackingInformation.EndPickupDate.Date.ToShortDateString(),
                    Notes = data.PackingInformation.Notes,

                    TotalQuantity = data.PackingInformation.PackageDimensionsList.Sum(x => x.Quantity).ToString(),
                    TotalLength = data.PackingInformation.PackageDimensionsList.Sum(x => x.BoxSizeLength).ToString(),
                    TotalDepth = data.PackingInformation.PackageDimensionsList.Sum(x => x.BoxSizeDepth).ToString(),
                    TotalWidth = data.PackingInformation.PackageDimensionsList.Sum(x => x.BoxSizeWidth).ToString(),
                    TotalWeight = data.PackingInformation.PackageDimensionsList.Sum(x => x.Weight).ToString(),
                    TotalBoxes = data.PackingInformation.PackageDimensionsList.Count(),
                    AverageWeight = (data.PackingInformation.PackageDimensionsList.Sum(x => x.Weight) / data.PackingInformation.PackageDimensionsList.Count()).ToString("#.##"),

                    PackingInformation = data.PackingInformation.PackageDimensionsList
                },
                Subject = $"Booking856 LUXURY001 {data.Order.PONumber}",
                To = ConfigurationManager.AppSettings["AlpiNotificationEmail"],
                EmailTemplateName = "Booking856",
                UseRazor = true
            });


            return new ApiResult();
        }

        private OrderBooking MapOrderToBooking(Bookin865Data data)
        {
            var date = DateTimeOffset.UtcNow;
            var totalUnits = data.Order.ProductItems?.Sum(x => x.TotalUnits) ?? 0;
            var totalPackages = data.PackingInformation.PackageDimensionsList?.Sum(x => x.Quantity) ?? 0;

            var hierarchyLevel = 0;
            var result = new OrderBooking
            {
                OrderHeader = new OrderBookingHeader()
                {
                    InterchangeControlVersionNumber = "00401",//from document
                    InterchangeControlNumber = data.Order.OrderSellerId,
                    InterchangeSenderId = "LUXURY001",
                    InterchangeReceiverId = "IT01027240488",
                    ApplicationSenderCode = "LUXURY001",
                    ApplicationReceiverCode = "IT01027240488",

                    PoNumber = data.Order.PONumber,
                    TestIndicator = TestIndicator.Test,
                    InterchangeDateTime = date,
                    GroupDateTime = date,
                    TransactionSetControlNumber = data.Order.OrderSellerId,
                    ProcessDate = data.Order.CreatedDate,
                    HierarchicalStructureCode = 1
                },
                ShipmentDetails = new BookingShipmentLevel[] {
                       MapShipmentLevel(data.SellerCompany,
                                        data.BuyerCompany,
                                        data.SellerAddress,
                                        data.BuyerAddress,
                                        data.BuyerContactData,
                                        totalPackages,
                                        totalUnits,
                                        data.Order.PONumber, data.PackingInformation,date,ref hierarchyLevel,data.Order.ProductItems)
                       }
            };

            return result;
        }

        private BookingShipmentLevel MapShipmentLevel(Company sellerCompany,
                                                      Company buyerCompany,
                                                      AddressInfo sellerAddressInfo,
                                                      AddressInfo buyerAddressInfo,
                                                      Customer buyerContact,
                                                      int totalPackages,
                                                      int totalUnits,
                                                      string PONumber,
                                                     OrderSellerPackingInformationInsert packingData,
                                                      DateTimeOffset date, ref int hierarchyLevel,
                                                      IEnumerable<OrderProductItemSellerView> productItems)
        {
            var shipmentLevel = new BookingShipmentLevel
            {
                HierarchicalIdNumber = hierarchyLevel++,
                TotalCartons = totalPackages,
                TotalUnits = totalUnits,
                LocationIdentifier = sellerAddressInfo?.CountryIso2Code,
                CountryCode = sellerAddressInfo?.CountryIso2Code,
                PONumber = PONumber,
                SellerName = packingData.ContactName,
                SellerCompanyName = sellerCompany.CompanyName,
                SellerAddress1 = sellerAddressInfo?.Street1 ?? string.Empty,
                SellerAddress2 = sellerAddressInfo?.Street2 ?? string.Empty,
                SellerCity = sellerAddressInfo?.City,
                AddressTypeName = sellerAddressInfo?.AddressTypeName ?? string.Empty,
                SellerIdentificationCode = sellerCompany.CompanyId.ToString(),
                SellerAddressInformation = "SellerAddressInformation?",//TODO
                SellerCityName = sellerAddressInfo?.City ?? string.Empty,
                SellerStateOrProvinceCode = sellerAddressInfo?.StateName ?? string.Empty,
                SellerPostalCode = sellerAddressInfo?.ZipCode,
                SellerCountryCode = sellerAddressInfo?.CountryIso2Code,
                SellerRegion = sellerAddressInfo?.CountryIso2Code,
                BookedSupplierDate = date,
                DateSellerReady = packingData.StartPickupDate,
                CompanyId = buyerCompany.CompanyId,
                BuyerCompanyName = buyerCompany.CompanyName,
                BuyerName = buyerContact?.FullName,
                CountryIso2Code = buyerAddressInfo?.CountryIso2Code ?? string.Empty

            };

            shipmentLevel.OrderDetails = new BookingOrderLevel[] { GetOrderLevel(ref hierarchyLevel, PONumber, shipmentLevel.HierarchicalIdNumber, productItems) };
            return shipmentLevel;
        }



        private BookingOrderLevel GetOrderLevel(ref int hierarchyLevel, string poNumber, int hierarchicalParentIdNumber, IEnumerable<OrderProductItemSellerView> productItems)
        {
            return new BookingOrderLevel
            {
                HierarchicalIdNumber = hierarchyLevel++,
                PONumber = poNumber,
                VendorBookingNumber = poNumber,
                HierarchicalParentIdNumber = hierarchicalParentIdNumber,
                ItemDetails = MapItems(productItems)
            };
        }

        private BookingItemLevel[] MapItems(IEnumerable<OrderProductItemSellerView> productItems)
        {
            var ods =
            (from p in productItems
             from s in p.SizeItems
             select new BookingItemLevel
             {
                 Quantity = s.ConfirmedUnits ?? s.Units,
                 VendorStyleNumber = p.ProductDetails.SKU,
                 ColorDescription = p.ProductDetails.ColorCode,
                 SizeDescription = p.ProductDetails.SizeTypeName,
                 UnitOfMeasure = "EA", // todo: softcode?

             }).ToArray();
            for (var i = 0; i < ods.Length; i++)
            {
                ods[i].LineItemNumber = i + 1;
            }
            return ods;
        }

        public ApiResult PushSellerOrderBookingDocument(int orderSellerId)
        {
            var poNumber = string.Empty;
            var orderResult = GetSellerOrderBookingDocument(orderSellerId,out poNumber);
            if (!orderResult.IsSuccesfull)
            {
                return new ApiResult(orderResult.BusinessRulesFailedMessages, orderResult.Exception);
            }

            var orderData = orderResult.Data;

            var ftpConfig = _ftpRepository.GetAllFtpConfigs(FtpConfigType.OrderBooking).FirstOrDefault();
            var currentDate = DateTimeOffset.UtcNow;
            var ftpQueueItem = new FtpQueueItem
            {
                FtpConfigId = ftpConfig.Id,
                FileName = $"Booking856_{poNumber}_{currentDate.ToString("yyyyMMddHHmmss")}",
                FileContentBytes = orderData,
                PONumber= poNumber,
                OrderSellerId= orderSellerId
            };

            _ftpRepository.EnqueueItem(ftpQueueItem);
            return new ApiResult();
        }

    }
}
