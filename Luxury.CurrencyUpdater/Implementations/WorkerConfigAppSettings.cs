﻿using System;
using System.Configuration;
using LuxuryApp.Contracts.BackgroudServices;

namespace Luxury.CurrencyUpdater.Implementations
{
    public class WorkerConfigAppSettings : IWorkerConfig
    {
        public int DelaySeconds
        {
            get
            {
                int value = 0;
                if (!int.TryParse(ConfigurationManager.AppSettings["WorkerPeriodSeconds"] ?? "", out value))
                {
                    value = 10;
                };
                return value;
            }
        }

    }
}
