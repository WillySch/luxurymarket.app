﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LuxuryApp.Contracts.BackgroudServices;

namespace Luxury.CurrencyUpdater
{
    public partial class ServiceCurrency : ServiceBase
    {
        private readonly IWorker _worker;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        public ServiceCurrency(IWorker worker)
        {
            _worker = worker;
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _worker.Start(_cancellationTokenSource.Token);
        }

        protected override void OnStop()
        {
            _cancellationTokenSource.Cancel();
        }
    }
}
