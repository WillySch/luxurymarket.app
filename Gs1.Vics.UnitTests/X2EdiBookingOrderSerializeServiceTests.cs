﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Gs1.Vics.Contracts.Factories;
using Gs1.Vics.Contracts.Models.Order;
using Gs1.Vics.Contracts.Services;
using Gs1.Vics.Implementations.Factories;
using NUnit.Framework;
using Gs1.Vics.Contracts.Models.Common;

namespace Gs1.Vics.UnitTests
{
    [TestFixture]
    public class X2EdiBookingOrderSerializeServiceTests
    {
        protected IEdiSerializeServiceFactory EdiSerializeServiceFactory =
            new X2EdiSerializeServiceFactory();

        [Test]
        public void SerializeOrder_ShouldReturnExpected()
        {
            // Arrange
            var expected = Resources.Edi856Booking;

            // Act
            var actual =
                Encoding.ASCII.GetString(EdiSerializeServiceFactory.Serialize(GetValidOrder()).Data);

            // Assert
            actual.ShouldBeEquivalentTo(expected);
        }

        private Order GetValidOrder()
        {
            return new Order
            {
                OrderHeader = new OrderHeader
                {
                    //InterchangeIdQualifier1 = "01",
                    InterchangeSenderId = "006987321 ", //todo: fillspace?
                    //InterchangeIdQualifier2 = "12",
                    InterchangeReceiverId = "0574112233 ", //todo: fillspace?
                    InterchangeDateTime = new DateTimeOffset(new DateTime(2016, 6, 8, 2, 30, 9)),
                    InterchangeControlVersionNumber = "00405",
                    InterchangeControlNumber = 9,
                    TestIndicator = TestIndicator.Production,
                    ApplicationSenderCode = "006987321",
                    ApplicationReceiverCode = "0574112233",
                    GroupDateTime = new DateTimeOffset(new DateTime(2016, 5, 8, 2, 30, 0)),
                    TransactionSetControlNumber = "0012",
                    PoNumber = "0006222782",
                    PurchaseOrderDate = new DateTimeOffset(new DateTime(2016, 4, 11)),
                    VendorOrSupplierSiteNumber = "7011367",
                    DepartmentNumber = "0419",
                    ManufacturerNumber = "55",
                    TermsOfSale = TermsOfSale.BasicTerm,
                    TermsNetDays = 30,
                    RequestedShipDate = new DateTimeOffset(new DateTime(2016,5,18)),
                    CancelAfterDate = new DateTimeOffset(new DateTime(2016, 6, 15)),

                    ShipmentMethod = 'M',

                    SupplierName = "Supplier Name",
                    SupplierIdentificationCode = "S_000",
                    SupplierAddressInformation = "Via Alamanni 23",
                    SupplierCityName = "FLORENCE",
                    SupplierStateOrProvinceCode = "FI",
                    SupplierPostalCode = "50100",
                    SupplierCountry = "IT^",

                    ShipToDestinationName = "Destination Name",
                    ShipToIdentificationCode = "C_0004",
                    ShipToAddressInformation = "Via Valentini 14",
                    ShipToCityName = "PRATO",
                    ShipToStateOrProvinceCode = "PO",
                    ShipToPostalCode = "59100",
                    ShipToCountryCode = "IT^",
                },
                OrderDetails = new OrderDetail[]
                {
                    new OrderDetail
                    {
                        LineItemNumber = 1,
                        QuantityOrdered = 2,
                        UnitOfMeasure = "EA",
                        UnitCost = 273,
                        EanNumber = "5057042010811",
                        VendorStyleNumber = "702B17ZAS4N45",
                        SkuNumber = "SKUNUMBER",
                        UnitPrice = 650,
                        Quantity = 1,
                        ProductDescription = "Name Product1",
                        ColorDescription = "BLACK",
                        SizeDescription = "35",
                        HangOrFlat = "FLAT",
                        ManufacturerName = "Manufacturer Name",
                        ManufacturerIdentificationCode = "M_010",
                        ManufacturerAddressInformation = "Via Villamagna 69",
                        ManufacturerCityName = "FLORENCE",
                        ManufacturerStateOrProvinceCode = "FI",
                        ManufacturerPostalCode = "50100",
                        ManufacturerCountryCode = "IT^",
                    },
                    new OrderDetail
                    {
                        LineItemNumber = 2,
                        QuantityOrdered = 2,
                        UnitOfMeasure = "EA",
                        UnitCost = 273,
                        EanNumber = "5057042010850",
                        VendorStyleNumber = "702B17ZAS4N45",
                        SkuNumber = "SKUNUMBER",
                        UnitPrice = 650,
                        Quantity = 1,
                        ProductDescription = "Name Product1",
                        ColorDescription = "BLACK",
                        SizeDescription = "35.5",
                        HangOrFlat = "FLAT",
                        ManufacturerName = "Manufacturer Name",
                        ManufacturerIdentificationCode = "M_010",
                        ManufacturerAddressInformation = "Via Villamagna 69",
                        ManufacturerCityName = "FLORENCE",
                        ManufacturerStateOrProvinceCode = "FI",
                        ManufacturerPostalCode = "50100",
                        ManufacturerCountryCode = "IT^",
                    },
                    new OrderDetail
                    {
                        LineItemNumber = 3,
                        QuantityOrdered = 4,
                        UnitOfMeasure = "EA",
                        UnitCost = 273,
                        EanNumber = "5057041910868",
                        VendorStyleNumber = "702B17ZAS4N45",
                        SkuNumber = "SKUNUMBER",
                        UnitPrice = 650,
                        Quantity = 1,
                        ProductDescription = "Name Product1",
                        ColorDescription = "BLACK",
                        SizeDescription = "36",
                        HangOrFlat = "FLAT",
                        ManufacturerName = "Manufacturer Name",
                        ManufacturerIdentificationCode = "M_010",
                        ManufacturerAddressInformation = "Via Villamagna 69",
                        ManufacturerCityName = "FLORENCE",
                        ManufacturerStateOrProvinceCode = "FI",
                        ManufacturerPostalCode = "50100",
                        ManufacturerCountryCode = "IT^",
                    }

                }
            };
        }
    }
}
