﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Services;

namespace Test_ExportOrderX2
{
    public partial class Booking856 : Form
    {
        private readonly IOrderBookingService _orderBookingService;
        private readonly IFtpUploadAgent _ftpUploadAgent;

        public Booking856(IOrderBookingService orderBookingService, IFtpUploadAgent ftpUploadAgent)
        {
            _orderBookingService = orderBookingService;
            _ftpUploadAgent = ftpUploadAgent;

            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            var orderId = GetOrderId();
            if (orderId <= 0)
            {
                return;
            }
            txtGeneratedX2.Text = GetX2Order(orderId);
        }

        private int GetOrderId()
        {
            var s = txtOrderId.Text;
            int orderId;
            if (!int.TryParse(s, out orderId))
            {
                MessageBox.Show("Invalid order id. must be int");
                return -1;
            }
            return orderId;
        }

        private string GetX2Order(int orderId)
        {
            var poNumber = string.Empty;
            var r = _orderBookingService.GetSellerOrderBookingDocument(orderId,out  poNumber);
            if (r.IsSuccesfull)
            {
                return Encoding.ASCII.GetString(r.Data);
            }
            MessageBox.Show(r.BusinessRulesFailedMessages.FirstOrDefault());
            return "";
        }

        private void btnFtpEnqueue_Click(object sender, EventArgs e)
        {
            var orderId = GetOrderId();
            if (orderId <= 0)
            {
                return;
            }

            var r = _orderBookingService.PushSellerOrderBookingDocument(orderId);
            if (r.IsSuccesfull)
            {
                MessageBox.Show("Successfull", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(r.BusinessRulesFailedMessages.FirstOrDefault(), "Failed", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            _ftpUploadAgent.UploadFromQueue();
            MessageBox.Show("Successfull", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Booking856_Load(object sender, EventArgs e)
        {

        }
    }
}
