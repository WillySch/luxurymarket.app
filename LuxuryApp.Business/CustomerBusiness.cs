﻿using System.Data;
using System.Data.SqlClient;
using LuxuryApp.Api.Models.Customer;
using LuxuryApp.Core.DataAccess;

namespace LuxuryApp.Business
{
    public class CustomerBusiness
    {
        public void SaveCustomerRequest(CusomerRequest model)
        {
            var parameters = new SqlParameter[11];
            parameters[0] = new SqlParameter("@ContactName", model.ContactName);
            parameters[1] = new SqlParameter("@Email", model.Email);
            parameters[2] = new SqlParameter("@PhoneNumber", model.PhoneNumber);

            parameters[3] = new SqlParameter("@CompanyName", model.CompanyName);
            parameters[4] = new SqlParameter("@City", model.Address.City);
            parameters[5] = new SqlParameter("@State", model.Address.State);
            parameters[6] = new SqlParameter("@ZipCode", model.Address.ZipCode);
            parameters[7] = new SqlParameter("@Country", model.Address.Country);
            parameters[8] = new SqlParameter("@RetailIdNumber", model.RetailIdNumber);
            parameters[9] = new SqlParameter("@CompanyWebsite", model.CompanyWebsite);
            parameters[10] = new SqlParameter("@Description", model.Description);

            var returnValue = SqlHelper.ExecuteNonQuery(WebConfiguration.ConnectionString, CommandType.StoredProcedure, "usp_Customer_CreateRequest", parameters);

        }
    }
}