﻿using LuxuryApp.Cdn.CdnServers.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LuxuryApp.Cdn.WebApi.Helpers
{
    public class CDNSettings
    {
        static int MaxFileSizeBytes = Convert.ToInt32(ConfigurationManager.AppSettings["MaxFileSizeBytes"]);
        public string[] ImageFormats = { "image/jpg", "image/jpeg", "image/pjpeg", "image/gif", "image/x-png", "image/png" };

        public string _Bucket = ConfigurationManager.AppSettings["Bucket"];
        public string _KeyId = ConfigurationManager.AppSettings["KeyId"];
        public string _KeySecret = ConfigurationManager.AppSettings["KeySecret"];
        public string _Region = ConfigurationManager.AppSettings["Region"];

        public string _LocalRootPath = ConfigurationManager.AppSettings["LocalRootPath"];
        public string _WebRootPath = ConfigurationManager.AppSettings["WebRootPath"];

        public string _UserIdSource = ConfigurationManager.AppSettings["UserIdSource"];

        public AmazonS3CdnSettings AmazonS3CdnSettings
        {
            get
            {
                return new AmazonS3CdnSettings
                {
                    ConfigurationId = 1,
                    Bucket = _Bucket,
                    KeyId = _KeyId,
                    KeySecret = _KeySecret,
                    Region = _Region

                };
            }
        }

        public LocalCdnSettings LocalCdnSettings
        {
            get
            {
                return new LocalCdnSettings
                {
                    ConfigurationId = 2,
                    LocalRootPath = _LocalRootPath,
                    WebRootPath = _WebRootPath
                };
            }
        }

    }
}