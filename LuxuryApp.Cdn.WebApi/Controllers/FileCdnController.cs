﻿using LuxuryApp.Cdn.Contracts;
using LuxuryApp.Cdn.Contracts.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace LuxuryApp.Cdn.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/filecdn")]
    public class FileCdnController : ApiController
    {
        private IFileCdnService<ICdnAuthor, ICdnSettings> _service;

        public FileCdnController(IFileCdnService<ICdnAuthor, ICdnSettings> service)
        {
            this._service = service;
        }

        [HttpPost]
        [Route("uploadsingle")]
        public IHttpActionResult UploadSingle(CdnUploadParameter uploadParam)
        {
            return Ok(_service.Upload(uploadParam.Key, uploadParam.Content));
        }

        [HttpPost]
        [Route("getcontenturl")]
        public IHttpActionResult GetContentUrl([FromBody]string key)
        {
            return Ok(_service.GetContentUrl(key));
        }

        [HttpPost]
        [Route("removesingle")]
        public IHttpActionResult RemoveSingle([FromBody]string key)
        {
            return Ok(_service.Remove(key));
        }

        [HttpPost]
        [Route("uploadbatch")]
        public IHttpActionResult Upload(IEnumerable<CdnUploadParameter> parameters)
        {
            return Ok(_service.Upload(parameters));
        }

        [HttpPost]
        [Route("getcontenturls")]
        public IHttpActionResult GetContentUrls([FromBody]IEnumerable<string> keys)
        {
            return Ok(_service.GetContentUrls(keys));
        }

        [HttpPost]
        [Route("removebatch")]
        public IHttpActionResult Remove([FromBody]IEnumerable<string> keys)
        {
            return Ok(_service.Remove(keys));
        }
    }
}