(function () {
  'use strict';

  angular.module('luxurymarket.directives')
    .directive('passwordCheckPreconditions', [
    PasswordCheckPreconditions
  ]);


  function PasswordCheckPreconditions() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '='
      },
      templateUrl: 'auth/password-check-precondition.html',
      link: function (scope, elem, attrs) {

        scope.pwdUpper =  false;
        scope.pwdNo = false;
        scope.pwdLength = false;
        scope.$watch('ngModel', function (value) {

          if (value && value.length >= 6) {
            scope.pwdLength = true;
          } else {
            scope.pwdLength = false;
          }

          if (value && /[A-Z]/.test(value)) {
            scope.pwdUpper = true;
          } else {
            scope.pwdUpper = false;
          }

          if (value && /\d/.test(value)) {
            scope.pwdNo = true;
          } else {
            scope.pwdNo = false;
          }
        });
        scope.valid = function (pwdType) {
          if (pwdType === true) {
            return true;
          }
          return false;
        };
      }
    };
  }
})(angular);
