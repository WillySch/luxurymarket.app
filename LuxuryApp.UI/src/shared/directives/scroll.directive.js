(function () {
  'use strict';

  angular.module('luxurymarket.directives')
    .directive('scroll', [
      '$window',
      '$state',
      Scroll
    ]);

    function Scroll($window, $state) {
      return function(scope, element, attrs) {

        scope.lastScrollTop = 0;
        angular.element($window).bind("scroll", function() {

          if ($state.current.name === 'site.products-wrapper.list') {

            if ($window.pageYOffset >= 100) {
                scope.boolChangeClass = true;
                element[0].classList.add('sticky-element');
                document.getElementsByClassName('header-site')[0].classList.add('sticky-element');
                document.getElementsByClassName('footer-site')[0].classList.add('sticky-element');
                scope.$emit('sticky');
            } else {
                scope.boolChangeClass = false;
                element[0].classList.remove('sticky-element');
                document.getElementsByClassName('header-site')[0].classList.remove('sticky-element');
                document.getElementsByClassName('footer-site')[0].classList.remove('sticky-element');
                scope.$emit('not-sticky');
            }

            // scope.st = element.pageYOffset;
            // if (scope.st > scope.lastScrollTop) {
            //   element[0].classList.add('sticky-element');
            //   scope.boolChangeClass = true;
            //   console.log('scroll top');
            // } else {
            //   console.log('scroll down');
            //   scope.boolChangeClass = false
            //   element[0].classList.add('sticky-element');
            // }
            //
            // if (this.pageYOffset >= 30) {
            //   scope.boolChangeClass = true;
            //   //  console.log('Scrolled below header.');
            //   // if (element[0].classList.contains('sticky-element')) {
            //       element[0].classList.add('sticky-element');
            //   // }
            // } else {
            //   scope.boolChangeClass = false;
            //   //  console.log('Header is in view.');
            //   element[0].classList.remove('sticky-element');
            // }
            scope.lastScrollTop = scope.st;
            scope.$apply();

          } else {
            document.getElementsByClassName('header-site')[0].classList.remove('sticky-element');
            document.getElementsByClassName('footer-site')[0].classList.remove('sticky-element');
          }
        });
    };
    }

})(angular);
