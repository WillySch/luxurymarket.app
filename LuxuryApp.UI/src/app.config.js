﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket')
        .run(['AuthenticationService', '$rootScope', '$state', '$translate', '$translatePartialLoader', 'COMPANY_TYPES', '$templateCache', '$window', '$cookies',
            function (AuthenticationService, $rootScope, $state, $translate, $translatePartialLoader, COMPANY_TYPES, $templateCache, $window, $cookies) {
                AuthenticationService.fillAuthData();

                // clear cache on templates
                //$templateCache.removeAll();

                //// Override String Metods
                /////////////////////////////////////////////////////
                if (!String.prototype.startsWith) {
                    String.prototype.startsWith = function (searchString, position) {
                        position = position || 0;
                        return this.indexOf(searchString, position) === position;
                    };
                }

                if (!String.prototype.endsWith) {
                    String.prototype.endsWith = function (searchString) {
                        return this.indexOf(searchString, this.length - searchString.length) !== -1;
                    };
                }
                ////////////////////////////////////////////////////

                $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                    function _loadTranslation(translationPartId) {
                        $translatePartialLoader.addPart('site-menu');

                        if (typeof translationPartId === 'undefined') {
                            return;
                        }

                        $translatePartialLoader.addPart(translationPartId);
                    }

                    if (!AuthenticationService.isLoggedIn()) {

                      $cookies.putObject('previousState', {
                        stateName: toState.name ,
                        stateParam: toParams
                      });

                        if (!toState.name.startsWith("auth.")) {
                            event.preventDefault();
                            $state.go('auth.login');
                        }
                        else {
                            // load translation
                            _loadTranslation(toState.translationPartId);
                        }
                    }
                    else {
                        if (toState.name.startsWith("auth.") && !toState.name.startsWith("auth.static.")) {
                            $state.go('site.home');
                        }
                        else {
                            //if (toState.name.startsWith("site.cart") && AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Seller) {
                            //    $state.go('site.home');
                            //    return false;
                            //}

                            // load translation
                            _loadTranslation(toState.translationPartId);
                        }
                    }
                });

                $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                    $window.scroll(0, 0);
                    if (toState.name.startsWith('site.')) {
                        $rootScope.$emit('updateMenu', toState, toParams);
                    }
                    if ($state.current.name !== 'auth.static.terms-and-conditions' && AuthenticationService.isLoggedIn()) {
                        var userData = AuthenticationService.getCurrentUserInfo();
                        if (!userData.userInfo.Companies[0].HasAcceptedTermsAndConditions) {
                            $rootScope.$broadcast('showTermsAndConditionsPopup');
                        }
                    }
                    $window._mfq.push(["newPageView", $state.href(toState.name, toParams)]);
                });

                $rootScope.$on('$translatePartialLoaderStructureChanged', function () {
                    $translate.refresh();
                });
            }])
        .config(['localStorageServiceProvider', '$httpProvider', '$locationProvider', '$translateProvider', 'NotificationProvider', 'APP_VERSION', 'uiSelectConfig',
                function (localStorageServiceProvider, $httpProvider, $locationProvider, $translateProvider, NotificationProvider, APP_VERSION, uiSelectConfig) {
                    // setup prefix of local storage
                    localStorageServiceProvider.setPrefix('ls.luxurymarket');

                    // inject interceptors
                    $httpProvider.interceptors.push('authInterceptorService');

                    $httpProvider.interceptors.push(['$templateCache', function ($templateCache) {
                        return {
                            'request': function (request) {
                                if (request.url.endsWith(".json")) {
                                    // cache miss
                                    // Item is not in $templateCache so add our query string
                                    request.url = request.url + '?v=' + APP_VERSION;
                                }
                                return request;
                            }
                        };
                    }]);

                    // remove # from urls- TODO
                    //$locationProvider.html5Mode({
                    //    enabled: true,
                    //    requireBase: false
                    //});

                    // translation config
                    $translateProvider
                        .useLoader('$translatePartialLoader', {
                            urlTemplate: '/i18n/{part}/{lang}.json'
                        })
                        .preferredLanguage('en')
                        .useLoaderCache(true)
                        .useSanitizeValueStrategy('sanitizeParameters');

                    NotificationProvider.setOptions({
                        startTop: 50,
                        startRight: 0,
                        positionX: 'center'
                    });

                }]);
})(window.angular);
