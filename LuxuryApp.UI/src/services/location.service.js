﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('LocationService', LocationService);

    LocationService.$inject = ['$http', 'localStorageService', 'SERVICES_CONFIG'];

    function LocationService($http, localStorageService, SERVICES_CONFIG) {

        var svc = {
            getCountries: getCountries,
            getStates: getStates
        };

        return svc;

        function getCountries() {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/countries')
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'Unable to load countries' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'Unable to load countries' };
                });
        }

        function getStates(countryId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/states/country/' + countryId)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'Unable to load states' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'Unable to load states' };
                });
        }

    }

})(window.angular);