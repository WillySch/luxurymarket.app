﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.message')
        .controller('MessageController', MessageController);

    MessageController.$inject = ['$state', 'MESSAGE_TYPES'];

    function MessageController($state, MESSAGE_TYPES) {
        var vm = this;

        // variables
        vm.message = _getMessage($state.params.type);

        function _getMessage(type) {
            switch (type) {
                case MESSAGE_TYPES.RequestSent:
                    return 'Your request was sent for processing. We will review your request and respond within 24 hours. Thank you!';
                case MESSAGE_TYPES.ForgotPassword:
                    return 'Instructions to reset your password have been sent to the provided email. Please check your email for these next steps.';
                default:
                    return null;
            }
        }
    }
})(window.angular);