﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.forgotpassword')
        .controller('ForgotPasswordController', ForgotPasswordController);

    ForgotPasswordController.$inject = ['AuthenticationService', '$state', 'MESSAGE_TYPES', 'localStorageService'];

    function ForgotPasswordController(AuthenticationService, $state, MESSAGE_TYPES, localStorageService) {
        var vm = this;

        // view model
        vm.forgotPasswordModel = {
            email: null,
            sendEmail: true
        };

        // view variables
        vm.isLoading = false;
        vm.hasError = false;
        vm.error_message = null;

        // functions
        vm.changePassword = changePassword;
        vm.validateField = validateField;

        function validateField(fieldName) {
          if (vm.forgotForm[fieldName].$invalid && vm.forgotForm[fieldName].$dirty) {
            return true;
          }

          if (vm.forgotForm.$submitted && vm.forgotForm[fieldName].$invalid) {
            return true;
          }

          return false;
        }

        function changePassword() {
            if (vm.forgotForm.$invalid) {
                return;
            }

            vm.isLoading = true;
            localStorageService.clearAll();
            AuthenticationService.requestPasswordChange(vm.forgotPasswordModel).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    $state.go('auth.message', {
                        type: MESSAGE_TYPES.ForgotPassword
                    });
                }
                else {
                    vm.hasError = true;
                    vm.error_message = response.error_message || 'Unable to find email address. Please try again';
                }
            });
        }

    }
})(window.angular);
