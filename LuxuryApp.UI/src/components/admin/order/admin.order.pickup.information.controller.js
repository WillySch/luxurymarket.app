﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.order')
        .controller('OrderPickupInformationController', OrderPickupInformationController);

    OrderPickupInformationController.$inject = ['OrderService', '$stateParams', '$state', '_', 'Notification', '$scope', '$uibModal', 'AuthenticationService', 'COMPANY_TYPES'];

    function OrderPickupInformationController(OrderService, $stateParams, $state, _, Notification, $scope, $uibModal, AuthenticationService, COMPANY_TYPES) {
        var vm = this;

        // variables
        vm.isEdit = false;
        vm.isLoading = false;
        vm.details = {};
        vm.details.orderPickupStartDate = null;
        vm.details.orderPickupEndDate = null;
        vm.pickupInformationModel = {};
        vm.pickupInformationModel.pickupStartHour = null;
        vm.orderPickupStartHourDisplay = null;
        vm.orderPickupEndHourDisplay = null;
        vm.boxSizeTypeList = [];
        vm.totalQty = 0;
        vm.totalWeight = 0;
        vm.averageWeight = 0;
        vm.boxSizeModel = {};

        // functions
        vm.saveBoxSizeType = saveBoxSizeType;
        vm.populateForEditBoxSizeType = populateForEditBoxSizeType;
        vm.deleteBoxSizeType = deleteBoxSizeType;
        vm.savePickupInformation = savePickupInformation;
        vm.startDateOnSetTime = startDateOnSetTime;
        vm.endDateOnSetTime = endDateOnSetTime;
        vm.startDateBeforeRender = startDateBeforeRender;
        vm.endDateBeforeRender = endDateBeforeRender;
        vm.startHourOnSetTime = startHourOnSetTime;
        vm.endHourOnSetTime = endHourOnSetTime;
        vm.endHourBeforeRender = endHourBeforeRender;

        // events
        $scope.$on('deleteBoxSizeType', _deleteBoxSizeType);
        $scope.$on('updateBoxSizeType', _updateBoxSizeType);

        // init
        _init();

        // functions implementation
        function saveBoxSizeType($event) {

            var boxSizeToUpdate;
            var form = vm.pickupInformationForm;

            $event.preventDefault();

            if (form.dimensionsSubform.$invalid) {
                form.dimensionsSubform.$setDirty();
                return false;
            }
            if (form.boxQty.$invalid) {
                form.boxQty.$setDirty();
                return false;
            }

            if (angular.isUndefined(vm.boxSizeModel.weight)) {
                vm.boxSizeModel.weight = 0;
            }

            if (!vm.isEdit) {
                boxSizeToUpdate = _.find(vm.boxSizeTypeList, { length: vm.boxSizeModel.length, width: vm.boxSizeModel.width, depth: vm.boxSizeModel.depth });
                if (typeof boxSizeToUpdate !== 'undefined') {
                    //ask to add quantity and weight to existing
                    $uibModal.open({
                        templateUrl: 'admin/order/pickup-information-box-size-confirm-update.html',
                        controller: 'UpdatePickupInformationBoxSizeController',
                        controllerAs: 'upibs',
                        resolve: {
                            boxSizeType: [function () {
                                return vm.boxSizeModel;
                            }]
                        }
                    });
                }
                else {
                    //add new box size type
                    vm.boxSizeModel.boxSizeId = new Date().getTime();
                    vm.boxSizeTypeList.push(vm.boxSizeModel);
                    vm.boxSizeModel = {};
                    vm.pickupInformationForm.$setPristine();
                    calculateSummary();
                    Notification('Box size type added with success.');
                }
            }
            else {
                //edit box size type
                boxSizeToUpdate = _.find(vm.boxSizeTypeList, { boxSizeId: vm.boxSizeModel.boxSizeId });
                angular.merge(boxSizeToUpdate, vm.boxSizeModel);
                vm.isEdit = false;
                vm.boxSizeModel = {};
                vm.pickupInformationForm.$setPristine();
                calculateSummary();
                Notification('Box size type edited with success.');
            }
        }

        function populateForEditBoxSizeType($event, boxSizeRowId) {
            $event.preventDefault();
            vm.boxSizeModel = _.findWhere(vm.boxSizeTypeList, { boxSizeId: boxSizeRowId });
            vm.isEdit = true;
        }

        function deleteBoxSizeType($event, boxSizeType) {
            $event.preventDefault();
            $event.stopPropagation();

            $uibModal.open({
                templateUrl: 'admin/order/pickup-information-box-size-confirm-delete.html',
                controller: 'DeletePickupInformationBoxSizeController',
                controllerAs: 'dpibs',
                resolve: {
                    boxSizeType: [function () {
                        return boxSizeType;
                    }]
                }
            });
        }

        function savePickupInformation() {
            var form = vm.pickupInformationForm;
            if (form.contactName.$invalid || form.contactPhone.$invalid || form.contactEmail.$invalid) {
                Notification('Please add contact information.');
                return false;
            }

            if (_.isUndefined(vm.pickupInformationModel.pickupStartHour) || _.isUndefined(vm.pickupInformationModel.pickupEndHour)) {
                Notification('You need to select order pickup hours.');
                return false;
            }

            if (vm.details.orderPickupStartDate === null || vm.details.orderPickupEndDate === null) {
                Notification('You need to select order pickup dates.');
                return false;
            }

            if (vm.boxSizeTypeList.length === 0) {
                Notification('You need to add at least one box.');
                return false;
            }

            var pickupStartHour = new Date(vm.pickupInformationModel.pickupStartHour);
            pickupStartHour = pickupStartHour.toLocaleTimeString('en-GB');

            var pickupEndHour = new Date(vm.pickupInformationModel.pickupEndHour);
            pickupEndHour = pickupEndHour.toLocaleTimeString('en-GB');

            var PackageDimensionsList = [];
            _.forEach(vm.boxSizeTypeList, function (boxSize) {
                var packageDimension = {};
                packageDimension.BoxSizeLength = boxSize.length;
                packageDimension.BoxSizeWidth = boxSize.width;
                packageDimension.BoxSizeDepth = boxSize.depth;
                packageDimension.Quantity = boxSize.quantity;
                packageDimension.Weight = boxSize.weight;
                PackageDimensionsList.push(packageDimension);
            });

            vm.isLoading = true;
            OrderService.insertPackageInformation({
                "OrderSellerId": vm.details.orderSellerId,
                "ContactName": vm.pickupInformationModel.contactName,
                "ContactPhone": vm.pickupInformationModel.contactPhone,
                "ContactEmail": vm.pickupInformationModel.contactEmail,
                "FromPickupHour": pickupStartHour,
                "ToPickupHour": pickupEndHour,
                "StartPickupDate": vm.details.orderPickupStartDate,
                "EndPickupDate": vm.details.orderPickupEndDate,
                "Notes": vm.pickupInformationModel.notes,
                "PackageDimensionsList": PackageDimensionsList
            }).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    Notification('Package information saved with success.');
                    $state.go('admin.orders');
                }
                else {
                    Notification('There was an error. Please try again');
                }
            });
        }

        // Date time picker functions
        ///////////////////////////////////
        function startDateOnSetTime() {
            $scope.$broadcast('order-pickup-start-date-changed');
        }

        function endDateOnSetTime() {
            $scope.$broadcast('order-pickup-end-date-changed');
        }

        function startDateBeforeRender($event, $dates) {
            var activeDate = moment().subtract(1, 'days');

            $dates.filter(function (date) {
                return date.localDateValue() < activeDate.valueOf();
            }).forEach(function (date) {
                date.selectable = false;
            });
        }

        function endDateBeforeRender($view, $dates) {
            var activeDate = null;
            if (vm.details.orderPickupStartDate) {
                activeDate = moment(vm.details.orderPickupStartDate).subtract(1, $view).add(1, 'minute');
            }
            else {
                activeDate = moment().subtract(1, 'days');
            }
            $dates.filter(function (date) {
                return date.localDateValue() <= activeDate.valueOf();
            }).forEach(function (date) {
                date.selectable = false;
            });
        }

        function startHourOnSetTime() {
            $scope.$broadcast('order-pickup-start-hour-changed');
            vm.orderPickupStartHourDisplay = moment(vm.pickupInformationModel.pickupStartHour).format('H:mm');
        }

        function endHourOnSetTime() {
            $scope.$broadcast('order-pickup-end-hour-changed');
            vm.orderPickupEndHourDisplay = moment(vm.pickupInformationModel.pickupEndHour).format('H:mm');
        }

        function endHourBeforeRender($view, $dates) {
            var activeDate = null;
            if (vm.pickupInformationModel.pickupStartHour) {
                activeDate = moment(vm.pickupInformationModel.pickupStartHour).subtract(1, $view).add(1, 'minute');
            }
            else {
                activeDate = moment().subtract(1, 'days');
            }
            $dates.filter(function (date) {
                return date.localDateValue() <= activeDate.valueOf();
            }).forEach(function (date) {
                date.selectable = false;
            });
        }
        ///////////////////////////////////

        // private functions
        //////////////////////////////////////
        function _init() {
            vm.isChecking = true;
            if (AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Buyer) {
                $state.go('admin.orders');
                return;
            }

            OrderService.isSubmittedPackageInformation($stateParams.orderId)
                  .then(function (response) {
                      vm.isChecking = false;
                      if (response.success) {
                          if (response.data) {
                              Notification('Already submitted pickup information.');
                              $state.go('admin.order-details', {
                                  orderId: $stateParams.orderId
                              });
                          }
                          else {
                              OrderService.getDetails($stateParams.orderId)
                                    .then(function (response) {
                                        if (response.success) {
                                            vm.details = response.data;
                                            vm.details.orderCDate = new Date(response.data.createdDate);
                                            vm.details.orderPickupStartDate = null;
                                            vm.details.orderPickupEndDate = null;
                                        }
                                        else {
                                            // show error
                                        }
                                    });
                          }
                      }
                      else {
                          // show error
                      }
                  });

        }

        function _deleteBoxSizeType($event, boxSizeRowId) {
            vm.boxSizeTypeList = _.without(vm.boxSizeTypeList, _.findWhere(vm.boxSizeTypeList, { boxSizeId: boxSizeRowId }));
            vm.boxSizeModel = {};
            vm.isEdit = false;
            calculateSummary();
            Notification('Box size type deleted with success.');
        }

        function _updateBoxSizeType($event, boxSizeType) {
            var boxSizeToUpdate = _.find(vm.boxSizeTypeList, { length: boxSizeType.length, width: boxSizeType.width, depth: boxSizeType.depth });
            boxSizeType.quantity = parseInt(boxSizeType.quantity, 10) + parseInt(boxSizeToUpdate.quantity, 10);
            boxSizeType.weight = parseInt(boxSizeType.weight, 10) + parseInt(boxSizeToUpdate.weight, 10);
            angular.merge(boxSizeToUpdate, boxSizeType);
            vm.boxSizeModel = {};
            vm.pickupInformationForm.$setPristine();
            calculateSummary();
            Notification('Box size type updated with success.');
        }

        function calculateSummary() {
            vm.totalQty = 0;
            vm.totalWeight = 0;
            vm.averageWeight = 0;
            for (var i = 0; i < vm.boxSizeTypeList.length; i++) {
                vm.totalQty += parseInt(vm.boxSizeTypeList[i].quantity);
                vm.totalWeight += parseFloat(vm.boxSizeTypeList[i].weight);
            }
            if (vm.boxSizeTypeList.length > 0) {
                vm.averageWeight = vm.totalWeight / vm.boxSizeTypeList.length;
                vm.averageWeight = parseFloat(vm.averageWeight).toFixed(2);
            }
            vm.totalWeight = parseFloat(vm.totalWeight).toFixed(2);
        }
    }
})(window.angular);
