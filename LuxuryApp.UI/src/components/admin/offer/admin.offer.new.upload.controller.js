﻿(function (angular) {

    'use strict';


    angular.module('luxurymarket.components.admin.offer')
        .controller('UploadNewOfferController', UploadNewOfferController);

    UploadNewOfferController.$inject = ['$scope', 'OfferService', '$state'];

    function UploadNewOfferController($scope, OfferService, $state) {

        // update current steps
        $scope.$emit('updateSteps', { number: 1, completed: false, active: true });

        //  view models
        $scope.currentFileName = null;
        $scope.error = null;
        $scope.progress = 0;
        $scope.isLoading = false;

        // private
        var _upload = null;

        // functions
        $scope.upload = upload;
        $scope.cancelUpload = cancelUpload;

        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        });

        function cancelUpload() {
            _reset(true);

            // to do abort
        }

        function upload(files) {
            if (files && files.length) {
                var file = files[0];
                if (!file.$error) {
                    $scope.currentFileName = file.name;
                    $scope.progress = 5;
                    _reset();
                    $scope.isLoading = true;
                    OfferService.uploadNewOffer(file).then(function (response) {
                        $scope.isLoading = false;
                        if (response.success) {
                            // redirect to step 2
                            $scope.$emit('updateSteps', { number: 1, complete: true, active: true });
                            $state.go('admin.new-offer.details', {
                                details: response.data,
                                offerId: response.data.id,
                                uniqueId: response.data.id + (new Date().toString())
                            });
                        }
                        else {
                            $scope.error = response.error_message;
                        }
                    }, null, function (evt) {
                        $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                    });
                }
            }
        }

        function _reset(all) {
            if (typeof all === 'undefined') {
                all = false;
            }

            if (all) {
                $scope.currentFileName = null;
            }

            $scope.progress = 0;
            $scope.error = null;
        }
    }

})(window.angular);