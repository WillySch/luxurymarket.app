(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.menu')
      .controller('AdminController', [
        '$state',
        AdminController
      ]);


    function AdminController($state) {
      var vm = this;

      //view vars
      vm.isCreateOffer = isCreateOffer;

      function isCreateOffer() {
        // console.log($state.is('admin.create-offer'));
        return $state.is('admin.create-offer');
      }


    }

})(window.angular);
