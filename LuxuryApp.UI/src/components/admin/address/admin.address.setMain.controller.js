﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.address')
        .controller('AddressSetMainController', AddressSetMainController);

    AddressSetMainController.$inject = ['$stateParams', '$rootScope', '$uibModalInstance', 'AddressService', 'localStorageService', 'Notification', 'addressId', '$translatePartialLoader'];

    function AddressSetMainController($stateParams, $rootScope, $uibModalInstance, AddressService, localStorageService, Notification, addressId, $translatePartialLoader) {
        var vm = this;

        // view models
        vm.addressId = addressId;

        // functions
        vm.close = close;
        vm.setMain = setMain;

        // view variables
        vm.isLoading = false;

        // init
        $translatePartialLoader.addPart('address-set-main');

        // functions      
        function close() {
            $uibModalInstance.dismiss('cancel');
        }

        function setMain() {
            vm.isLoading = true;
            AddressService.setMain(vm.addressId).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    close();
                    Notification('Address marked as main');
                    $rootScope.$broadcast('onUpdateMainAddress', vm.addressId);
                }
                else {
                    Notification('There was an error. Please try again.');
                }
            });
        }

        // private functions
    }
})(window.angular);