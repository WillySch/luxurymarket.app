﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.address')
        .controller('AddressDetailsController', AddressDetailsController);

    AddressDetailsController.$inject = ['$stateParams', '$rootScope', '$uibModalInstance', 'AddressService', 'LocationService', 'localStorageService', 'Notification', 'address', 'addressType', 'isEdit', '$translatePartialLoader'];

    function AddressDetailsController($stateParams, $rootScope, $uibModalInstance, AddressService, LocationService, localStorageService, Notification, address, addressType, isEdit, $translatePartialLoader) {
        var vm = this;

        // view models
        vm.addressModel = {};
        angular.merge(vm.addressModel, address);
        vm.addressModel.addressTypeId = addressType;
        vm.addressModel.isEdit = isEdit;

        // functions
        
        vm.close = close;
        vm.saveInfo = saveInfo;
        vm.updateStates = updateStates;

        // view variables
        vm.isLoading = false;
        vm.countries = null;
        vm.states = [{
            stateId: null,
            stateName: 'STATE or OTHER'
        }];

        // init
        _init();
        if (isEdit) {
            updateStates(true);
        }
        else {
            vm.addressModel.countryId = null;
            vm.addressModel.stateId = null;
        }
        $translatePartialLoader.addPart('address-details');

        // functions      
        function close() {
            $uibModalInstance.dismiss('cancel');
        }

        function saveInfo() {
            if (vm.addressForm.$invalid) {
                return;
            }
            vm.isLoading = true;
            AddressService.saveInfo(vm.addressModel).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    close();
                    vm.addressModel.addressId = response.data;
                    $rootScope.$broadcast('onUpdateAddress', vm.addressModel);
                    Notification('Address saved with success');
                }
                else {
                    Notification('There was an error. Please try again.');
                }
            });
        }

        function updateStates(fromInit) {
            if (!fromInit) {
                vm.addressModel.stateId = null;
            }
            LocationService.getStates(vm.addressModel.countryId).then(function (response) {
                if (response.success) {
                    response.data.splice(0, 0, {
                        stateId: null,
                        stateName: 'STATE or OTHER'
                    });
                    vm.states = response.data;
                }
                else {
                    vm.states = [{
                        stateId: null,
                        stateName: 'STATE or OTHER'
                    }];
                }
            });
        }

        // private functions
        function _init() {
            LocationService.getCountries().then(function (response) {
                if (response.success) {
                    response.data.splice(0, 0, {
                        id: null,
                        countryName: 'COUNTRY'
                    });
                    vm.countries = response.data;
                }
                else {
                    vm.countries = [{
                        id: null,
                        countryName: 'COUNTRY'
                    }];
                }
            });
        }
    }
})(window.angular);