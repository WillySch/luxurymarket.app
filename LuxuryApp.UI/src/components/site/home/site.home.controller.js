﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.home')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['HomeService', 'FEATURED_EVENT_IMAGE_TYPES', '_', 'CdnService', '$state', 'FEATURED_EVENT_THEME_TYPES', 'PRODUCT_LIST_TYPE', 'ProductService'];

    function HomeController(HomeService, FEATURED_EVENT_IMAGE_TYPES, _, CdnService, $state, FEATURED_EVENT_THEME_TYPES, PRODUCT_LIST_TYPE, ProductService) {
        var vm = this;

        // view models
        vm.featuredEvents = {
            hero: null,
            bigTile: null,
            secondaryTiles: [],
            additionalTiles: []
        };
        vm.featuredEventsThemeTypes = FEATURED_EVENT_THEME_TYPES;
        //vm.bestSellers = [];

        // functions
        vm.goToFEProducts = goToFEProducts;
        vm.imageLoaded = imageLoaded;
        vm.goToProductDetails = goToProductDetails;
        vm.goToBrandProducts = goToBrandProducts;

        // init
        _loadFeaturedEvents();
        //_loadBestSellers();

        function goToFEProducts($event, featuredEvent) {
            $event.preventDefault();

            if (!featuredEvent.hasProductsAssigned) {
                return false;
            }

            $state.go('site.products-wrapper.list', {
                featuredEventId: featuredEvent.featuredEventID,
                header: featuredEvent.title.replace(/\*/g, ''),
                subtitle: featuredEvent.subtitle.replace(/\*/g, ''),
                listType: PRODUCT_LIST_TYPE.OTHER
            });
        }

        // functions implementation
        function imageLoaded(entity) {
            entity.showImage = true;
        }

        function goToProductDetails($event, bestSeller) {
            $event.preventDefault();
            $event.stopPropagation();

            $state.go('site.products-wrapper.details', {
                id: bestSeller.id,
                listParams: {
                    businessId: bestSeller.businessID,
                    divisionId: bestSeller.divisionID,
                    departmentId: bestSeller.departmentID,
                    listType: PRODUCT_LIST_TYPE.OTHER
                }
            });
        }

        function goToBrandProducts($event, brandId, brandName) {
            $event.preventDefault();
            $event.stopPropagation();

            $state.go('site.products-wrapper.list', {
                brandId: brandId,
                header: brandName,
                listType: PRODUCT_LIST_TYPE.OTHER
            });
        }

        // private functions
        ////////////////////////////////////////////////
        function _loadFeaturedEvents() {
            HomeService.getFeaturedEvents().then(function (response) {
                if (response.success) {
                    var events = _.sortBy(response.data, 'displayOrder');

                    // hero image
                    vm.featuredEvents.hero = _.findWhere(events, { isHero: true });
                    vm.featuredEvents.hero.imageToDisplay = _.findWhere(vm.featuredEvents.hero.images, { type: FEATURED_EVENT_IMAGE_TYPES.Hero_Image });
                    vm.featuredEvents.hero.showImage = true;

                    vm.featuredEvents.hero.htmltitle = vm.featuredEvents.hero.title.replace(/\*/g, '<br/>');
                    vm.featuredEvents.hero.htmlsubtitle = vm.featuredEvents.hero.subtitle.replace(/\*/g, '<br/>');


                    var otherEvents = _.where(events, { isHero: false });
                    if (otherEvents) {
                        _.forEach(otherEvents, function (value, index) {
                            if (index === 0) {
                                // big title
                                value.imageToDisplay = _.findWhere(value.images, { type: FEATURED_EVENT_IMAGE_TYPES.Big_Tiles });
                                vm.featuredEvents.bigTile = otherEvents[0];
                                vm.featuredEvents.bigTile.htmltitle = vm.featuredEvents.bigTile.title.replace(/\*/g, '<br/>');
                                vm.featuredEvents.bigTile.htmlsubtitle = vm.featuredEvents.bigTile.subtitle.replace(/\*/g, '<br/>');
                            }
                            else {
                                value.imageToDisplay = _.findWhere(value.images, { type: FEATURED_EVENT_IMAGE_TYPES.Small_Tiles });
                                value.htmltitle = value.title.replace(/\*/g, '<br/>');
                                value.htmlsubtitle = value.subtitle.replace(/\*/g, '<br/>');
                                if (index > 0 && index < 3) {
                                    // secondary tiles
                                    vm.featuredEvents.secondaryTiles.push(value);
                                }
                                else {
                                    // additional tiles
                                    vm.featuredEvents.additionalTiles.push(value);
                                }
                            }
                        });
                    }
                }
                else {
                    // show error
                }
            });
        }

        function _loadBestSellers() {
            vm.isLoadingBestSellers = true;
            ProductService.getBestSellers()
                .then(function (response) {
                    vm.isLoadingBestSellers = false;
                    if (response.success) {
                        vm.bestSellers = response.data;
                    }
                    else {
                        // to do
                    }
                });
        }
    }
})(window.angular);