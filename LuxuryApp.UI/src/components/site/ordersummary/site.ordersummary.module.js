﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.ordersummary', []);

})(window.angular);