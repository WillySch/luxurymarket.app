﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.shipment')
        .controller('DeleteShipmentController', DeleteShipmentController);

    DeleteShipmentController.$inject = ['$rootScope', '$uibModalInstance', 'Notification', 'cartId', 'shipment', 'CartService', 'AuthenticationService', '$translatePartialLoader'];

    function DeleteShipmentController($rootScope, $uibModalInstance, Notification, cartId, shipment, CartService, AuthenticationService, $translatePartialLoader) {
        var vm = this;

        // view models
        vm.isLoading = false;
        vm.title = 'SHIPMENT_CONFIRM_CANCEL_TITLE';
        vm.question = 'SHIPMENT_CONFIRM_CANCEL_QUESTION';

        // functions
        vm.close = close;
        vm.submit = submit;

        //init
        $translatePartialLoader.addPart('manage-shippings-confirm-cancel');

        // functions      
        function close() {
            $uibModalInstance.dismiss('cancel');
        }

        function submit($event) {
            vm.isLoading = true;
            CartService.cancelShipment({
                cartId: cartId,
                shippingRegionId: shipment.shippingRegionId
            }).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    // sync ui
                    $rootScope.$broadcast('updateShipmentListAfterCancel', response.data, shipment);
                    // update cart summary
                    $rootScope.$broadcast('updateCart', true);
                    // close popup
                    close();
                }
                else {

                }
            });
        }
    }
})(window.angular);