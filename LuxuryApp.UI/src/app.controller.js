﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket')
        .controller('MainController', MainController);

    MainController.$inject = ['AuthenticationService', '$state', '$translate', '$cookies', '$scope', 'CartService', 'Notification', 'COMPANY_TYPES', '$uibModal', '$http'];

    function MainController(AuthenticationService, $state, $translate, $cookies, $scope, CartService, Notification, COMPANY_TYPES, $uibModal, $http) {
        var vm = this;
        var userLanguage = $cookies.get('selectedLang');

        // view models
        vm.language = userLanguage || 'Eng';
        vm.cart = {};
        vm.userInfo = null;
        //vm.isSeller = AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Seller;

        // functions
        vm.logout = logout;
        vm.translate = translate;
        vm.includes = includes;
        vm.navigateFromLogo = navigateFromLogo;

        // init
        translate(undefined, $cookies.get('selectedLang'));
        var userData = AuthenticationService.getCurrentUserInfo();
        if (userData && userData !== null) {
            _updateUserData(null, userData.userInfo);
        }

        // events
        $scope.$on('updateCart', _updateCart);
        $scope.$on('updateUserData', _updateUserData);
        $scope.$on('changeDisplayName', _changeDisplayName);
        $scope.$on('showTermsAndConditionsPopup', _showTermsAndConditionsPopup);

        function logout($event) {
            $event.preventDefault();
            AuthenticationService.logout();
            $state.go('auth.login');
        }

        function translate($event, language) {
            if (typeof $event !== 'undefined') {
                $event.preventDefault();
            }

            // do translation
            $translate.use(language);

            // update cookie
            $cookies.put('selectedLang', language);

            // update ui
            switch (language) {
                case 'en':
                    vm.language = 'Eng';
                    break;

                case 'it':
                    vm.language = 'It';
                    break;
            }
        }

        function includes(stateName) {
            return $state.includes(stateName);
        }

        function navigateFromLogo($event) {
            if (AuthenticationService.isLoggedIn()) {
                $state.go('site.home');
            }
            else {
                $state.go('auth.login');
            }
        }

        // private functions
        //////////////////////////////////////////////

        function _updateCart(e, firstTime, data) {
            if (typeof firstTime === 'undefined') {
                firstTime = false;
            }

            if (typeof data !== 'undefined') {
                vm.cart.itemsCount = data.itemsCount;
                vm.cart.unitsCount = data.unitsCount;
                vm.cart.totalCustomerCost = data.totalCustomerCost;
                vm.cart.totalLandedCost = data.totalLandedCost;
            }
            else {
                CartService.getCartSummary(AuthenticationService.getCurrentUserID()).then(function (response) {
                    if (response.success) {
                        vm.cart = response.data;
                    }
                });
            }

            if (!firstTime) {
                Notification('Cart Updated');
            }
        }

        function _updateUserData(e, data) {
            vm.userInfo = data;
            vm.userInfo.displayName = vm.userInfo.FirstName.split(' ')[0];

            if (AuthenticationService.isLoggedIn() && AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Buyer) {
                _updateCart(null, true);
            }
            else {
                vm.cart = vm.cart || {};
                vm.cart.itemsCount = 0;
                vm.cart.unitsCount = 0;
                vm.cart.totalCustomerCost = 0;
            }
        }

        function _changeDisplayName(e, contactName) {
            vm.userInfo.displayName = contactName.split(' ')[0];
            vm.userInfo.FirstName = vm.userInfo.displayName;
        }

        function _showTermsAndConditionsPopup($event) {
            var modalInstance = $uibModal.open({
                templateUrl: 'shared/terms-and-conditions-popup.html',
                controller: "TermsAndConditionsPopupController",
                controllerAs: "tacp",
                backdrop: 'static',
                keyboard: false
            });
        }
    }

})(window.angular);
