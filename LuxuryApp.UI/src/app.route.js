﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routeConfigs($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/site/home");

        $stateProvider
            .state('auth', {
              abstract: true,
              url: "/auth",
              controller: 'AdminController',
              controllerAs: 'ad',
              views: {
                'header': {
                  templateUrl: 'auth/header.html'
                },
                'content': {
                  templateUrl: "auth/index.html"
                },
                'footer': {
                  templateUrl: 'auth/footer.html'
                }
              }
            })
            .state('site', {
                url: "/site",
                abstract: true,
                views: {
                  'header': {
                    templateUrl: 'site/header.html'
                  },
                  'content': {
                    templateUrl: "site/index.html"
                  },
                  'footer': {
                    templateUrl: 'site/footer.html'
                  }
                }
            })
            .state('admin', {
                url: "/admin",
                abstract: true,
                views: {
                  'header': {
                    templateUrl: 'admin/header.html'
                  },
                  'content': {
                    templateUrl: "admin/index.html"
                  },
                  'footer': {
                    templateUrl: 'admin/footer.html'
                  }
                }
            });
    }

})(window.angular);
