﻿using LuxuryApp.Cdn.Image.Contracts.Model;

namespace LuxuryApp.Cdn.Image.Contracts
{
    public class CdnImageSizeList
    {
        public static CdnImageSize[] ImageSizes = new[]
{
                 new CdnImageSize
                 {
                     SubFolder = "50",
                     Size = 80, Dimension = CdnImageDimension.Width
                 },
                 new CdnImageSize
                 {
                     SubFolder = "100",
                     Size = 150, Dimension = CdnImageDimension.Width
                  },
                 new CdnImageSize
                 {
                     SubFolder = "250",
                     Size = 260,
                     Dimension = CdnImageDimension.Width
                 },
                 new CdnImageSize
                 {
                     SubFolder = "500",
                     Size = 400, Dimension = CdnImageDimension.Width
                 },
         };
    }
}
