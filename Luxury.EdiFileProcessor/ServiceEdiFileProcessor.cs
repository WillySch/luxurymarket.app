﻿using LuxuryApp.Contracts.BackgroudServices;
using System.ServiceProcess;
using System.Threading;

namespace Luxury.EdiFileProcessor
{
    public partial class ServiceEdiFileProcessor : ServiceBase
    {
        private readonly IWorker _worker;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        public ServiceEdiFileProcessor(IWorker worker)
        {
            InitializeComponent();

            // settings
            this.ServiceName = "Luxury.EdiFileProcessor";
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;

            _worker = worker;

        }

        protected override void OnStart(string[] args)
        {
            _worker.Start(_cancellationTokenSource.Token);
        }

        protected override void OnStop()
        {
            _cancellationTokenSource.Cancel();
        }

    }
}
