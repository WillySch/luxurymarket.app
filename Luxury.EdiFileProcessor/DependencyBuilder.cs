﻿using Autofac;
using Gs1.Vics.Contracts.Factories;
using Gs1.Vics.Contracts.Services;
using Gs1.Vics.Implementations.Factories;
using Gs1.Vics.Implementations.Services;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.DataAccess;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Core.Infrastructure.Authorization;
using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.NotificationService;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.Processors.Agents;
using LuxuryApp.Processors.Services;

namespace Luxury.EdiFileProcessor
{
    public class DependencyBuilder
    {
        public IContainer GetDependencyContainer()
        {
            var builder = new ContainerBuilder();

            //builder.RegisterInstance(configuration).AsSelf(); // needed for help area configuration


            builder.RegisterType<LuxuryLogger>().As<ILogger>();
            builder.RegisterType<LuxuryMarketConnectionStringProvider>()
                .As<IConnectionStringProvider>();

            builder.RegisterType<NullContextProvider>()
                .As<IContextProvider>().InstancePerDependency();
            builder.RegisterType<OfferRepository>()
                .As<IOfferRepository>();
          
            builder.RegisterType<X12EdiParseService>().As<IEdiParseService>();

            builder.RegisterType<X2EdiDeserializeServiceFactory>().As<IEdiDeserializeServiceFactory>();

            builder.RegisterType<X2Edi856DeserializeService>().As<IEdiDeserializeService<EdiExtracted865Data>>();
            builder.RegisterType<X2Edi997DeserializeService>().As<IEdiDeserializeService<EdiExtractedACK997>>();
        
            builder.RegisterType<EdiProcessorRepository>().As<IEdiProcessorRepository>();

            builder.RegisterType<FtpRepository>().As<IFtpRepository>();

            builder.RegisterType<EdiProcessorAgent>().As<IEdiProcessorAgent>();

            builder.RegisterType<EmailService<EmailModel>>().As<INotificationHandler<EmailModel>>();

            builder.RegisterType<ServiceWorker>().As<IWorker>();

            builder.RegisterType<ServiceEdiFileProcessor>().AsSelf();
            var container = builder.Build();
            return container;
        }
    }
}
