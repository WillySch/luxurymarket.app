﻿using System.Configuration;

namespace Luxury.EdiFileProcessor
{
    public static class ConfigAppSettings
    {
        public static int DefaultDelayMinutes
        {
            get
            {
                var value = 10;
                int.TryParse(ConfigurationManager.AppSettings["DefaultDelayMinutes"] ?? "", out value);
                return value;
            }
        }

        public static int? EdiUserId
        {
            get
            {
                var value = 0;
                int.TryParse(ConfigurationManager.AppSettings["EdiUserId"] ?? "", out value);
                return value > 0 ? (int?)value : null;
            }
        }

        public static string LocalPath
        {
            get
            {
                return ConfigurationManager.AppSettings["FTPOutboundLocalPath"] ?? "";
            }
        }

    }
}
