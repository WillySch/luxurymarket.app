﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Utils.Helpers;
using NLog;
using System;
using System.Diagnostics;
using System.Threading;

namespace Luxury.EdiFileProcessor
{
    public class ServiceWorker : IWorker
    {
        private readonly Func<IEdiProcessorAgent> _ediProcessorAgent;
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly string _ediLocalFilePath;
        private readonly int? _createdByUserId;

        public ServiceWorker(Func<IEdiProcessorAgent> ediProcessorAgent)
        {
            _ediProcessorAgent = ediProcessorAgent;
            _ediLocalFilePath = ConfigAppSettings.LocalPath;
            _createdByUserId = ConfigAppSettings.EdiUserId;
        }

        public void Start(CancellationToken cancellationToken)
        {
            try
            {
                _logger.Info($"Thread started at {DateTime.UtcNow}");
                new Thread(() => DoProcessingWork(_ediLocalFilePath, _createdByUserId)).Start();

                _logger.Info($"Thread ended at {DateTime.UtcNow}");
            }
            catch (Exception ex)
            {
                _logger.Error($"Thread error: {ex.ToString()}");
            }
        }

        #region Private Methods

        private void DoProcessingWork(string localDirectoryPath, int? createdBy)
        {
            Stopwatch watch = new Stopwatch();

            while (true)
            {
                _logger.Info($"Process started at {DateTime.UtcNow}");

                watch.Reset();
                watch.Start();
                _ediProcessorAgent().ProcessItemsFromQueue(localDirectoryPath, createdBy);
                watch.Stop();
                _logger.Info($"Process ended at {DateTime.UtcNow} and lasted {watch.Elapsed}. Next sleep perioad is {ConfigAppSettings.DefaultDelayMinutes} minutes");

                DelayExcecution(ConfigAppSettings.DefaultDelayMinutes);
            }
        }


        private void DelayExcecution(int numberMinutes)
        {
            Thread.Sleep(numberMinutes.ConvertToMilliseconds());
        }

        #endregion
    }
}
