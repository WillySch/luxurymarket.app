﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Luxury.EdiFileProcessor
{
    [RunInstaller(true)]
    public class ServiceEdiFileProcessorInstaller : Installer
    {
        public ServiceEdiFileProcessorInstaller()
        {
            var processInstaller = new ServiceProcessInstaller();
            var serviceInstaller = new ServiceInstaller();

            //set the privileges
            processInstaller.Account = ServiceAccount.LocalSystem;

            serviceInstaller.ServiceName = "Luxury.EdiFileProcessorService";
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            //must be the same as what was set in Program's constructor
            serviceInstaller.DisplayName = "Luxury.EdiFileProcessorService";
            this.Installers.Add(processInstaller);
            this.Installers.Add(serviceInstaller);

        }
    }
}
