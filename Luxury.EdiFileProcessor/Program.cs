﻿using Autofac;
using System;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;

namespace Luxury.EdiFileProcessor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                string parameter = string.Concat(args);
                switch (parameter)
                {
                    case "--install":
                        ManagedInstallerClass.InstallHelper(new string[]
                            {Assembly.GetExecutingAssembly().Location});
                        break;
                    case "--uninstall":
                        ManagedInstallerClass.InstallHelper(new string[]
                            {"/u", Assembly.GetExecutingAssembly().Location});
                        break;
                }
            }
            else
            {
                var container = new DependencyBuilder().GetDependencyContainer();
                ServiceBase[] servicesToRun = new[] { container.Resolve<ServiceEdiFileProcessor>() };
                ServiceBase.Run(servicesToRun);
            }

        }
    }

}
