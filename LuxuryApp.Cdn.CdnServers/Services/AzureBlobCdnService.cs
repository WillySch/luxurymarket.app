﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LuxuryApp.Cdn.CdnServers.Model;
using LuxuryApp.Cdn.Contracts;
using LuxuryApp.Cdn.Contracts.Model;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

// Namespace for CloudConfigurationManager
// Namespace for CloudStorageAccount

// Namespace for Blob storage types


namespace LuxuryApp.Cdn.CdnServers.Services
{
    public class AzureBlobCdnService: IFileCdnService<ICdnAuthor, AzureBlobCdnSettings>
    {
        public ICdnAuthor Author { get; }
        public AzureBlobCdnSettings Settings { get; }

        public AzureBlobCdnService(ICdnAuthor author, AzureBlobCdnSettings settings)
        {
            Author = author;
            Settings = settings;
        }

        public CdnResult Upload(string key, byte[] content)
        {
            if (key == null || content == null)
            {
                return new CdnResult(key, new CdnExceptionBadRequest());
            }
            var container = GetBlobContainer();
            var blockBlob = container.GetBlockBlobReference(key);
            var writeOptions = new BlobRequestOptions()
            {
                SingleBlobUploadThresholdInBytes = 64 * 1024 * 1024, //32MB is by default. 64MB
            };
            blockBlob.UploadFromByteArray(content, 0, content.Length, options: writeOptions);
            return new CdnResult(key);
        }

        public CdnResult<string> GetContentUrl(string key)
        {
            var container = GetBlobContainer();
            var blob = container.GetBlobReference(key);
            return new CdnResult<string>(key, blob.Uri.AbsoluteUri);
        }

        public CdnResult Remove(string key)
        {
            var container = GetBlobContainer();
            var blockBlob = container.GetBlockBlobReference(key);
            blockBlob.DeleteIfExists();
            return new CdnResult(key);
        }

        public IEnumerable<CdnResult> Upload(IEnumerable<CdnUploadParameter> parameters)
        {
            Task<CdnResult>[] tasks = parameters.Where(p => p.Key != null)
                .Select(x => Task.Run(() => Upload(x.Key, x.Content))).ToArray();
            Task.WaitAll(tasks);
            var result = tasks.Select(x => x.Result);
            return result;
        }

        public IEnumerable<CdnResult<string>> GetContentUrls(IEnumerable<string> keys)
        {
            Task<CdnResult<string>>[] tasks = keys
                .Where(k => k != null)
                .Select(key => Task.Run(() => GetContentUrl(key)))
                .ToArray();
            Task.WaitAll(tasks);
            var result = tasks.Select(x => x.Result);
            return result;
        }

        public IEnumerable<CdnResult> Remove(IEnumerable<string> keys)
        {
            Task<CdnResult>[] tasks = keys
                .Where(k => k != null)
                .Select(key => Task.Run(() => Remove(key)))
                .ToArray();
            Task.WaitAll(tasks);
            var result = tasks.Select(x => x.Result);
            return result;
        }

        private CloudBlobContainer GetBlobContainer()
        {
            var client = GetClient();
            var container = client.GetContainerReference(Settings.Container);
            container.CreateIfNotExists();
            return container;
        }

        private CloudBlobClient GetClient()
        {
            // Parse the connection string and return a reference to the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                Settings.StorageConnectionString);

            return storageAccount.CreateCloudBlobClient();
        }
    }
}
