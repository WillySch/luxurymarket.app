﻿using System.Collections.Generic;
using LuxuryApp.Cdn.CdnServers.Model;

namespace LuxuryApp.Cdn.CdnServers.Repository
{

    public interface IMultiCdnRepository 
    {
        IEnumerable<FileData> SaveFileData(IEnumerable<FileData> fileDatas);

        IEnumerable<FileData> GetFileData(IEnumerable<string> keys);

        void SetCdnFileStatuses(IEnumerable<FileConfigStatus> cdnFileStatuses);

        IEnumerable<FileConfigStatus> GetFileCdnStatuses(IEnumerable<FileConfigStatusRequest> fileConfigs);
    }
}
